<?php
/**
 * @package     DOCman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

class PlgSystemJoomlatoolsupdater extends JPlugin
{
    protected static $_extensions = array(
        'docman', 'fileman', 'logman', 'textman'
    );

    const BASE_URL = 'https://api.joomlatools.com/';

    public function onInstallerBeforePackageDownload(&$url, &$headers)
    {
        if (strpos($url, static::BASE_URL) === false) {
            return;
        }

        preg_match('#extension/([a-z_]+)\.zip#i', $url, $matches);

        if (empty($matches)) {
            return;
        }

        $component = $matches[1];
        $api_key   = $this->_getApiKey($component);

        if ($api_key) {
            $headers['Authorization'] = 'Bearer '.$api_key;
        } else {
            JLog::add(sprintf('API key for %s not found', $component), JLog::ERROR, 'jerror');
        }
    }

    public function onBeforeCompileHead()
    {
        $app = JFactory::getApplication();

        if ($app->isAdmin() && JFactory::getDocument()->getType() === 'html') {
            $option = str_replace('com_', '', $app->input->get('option'));

            if (in_array($option, static::$_extensions)) {
                $this->_addUpdateNotifier();
            }
        }
    }

    public function onGetIcons($context)
    {
        $this->_addUpdateNotifier();
    }

    protected function _addUpdateNotifier()
    {
        if (!JFactory::getUser()->authorise('core.manage', 'com_installer')) {
            return;
        }

        JHtml::_('jquery.framework');

        $option = str_replace('com_', '', JFactory::getApplication()->input->get('option'));
        $extension_list = static::$_extensions;

        if (in_array($option, $extension_list)) {
            $extension_list = array($option);
        }

        $extensions = array();
        $api_key    = '';

        foreach ($extension_list as $extension) {
            $manifest = sprintf(JPATH_ROOT.'/administrator/components/com_%1$s/%1$s.xml', $extension);

            if (file_exists($manifest)) {
                $version = 'unknown';

                try {
                    $query    = sprintf(/** @lang text */
                        "SELECT manifest_cache FROM #__extensions WHERE type = 'component' AND element = 'com_%s'",
                        $extension
                    );

                    if ($result = JFactory::getDbo()->setQuery($query)->loadResult()) {
                        $manifest = @json_decode($result);

                        if (is_object($manifest) && !empty($manifest->version)) {
                            $version = $manifest->version;
                        }
                    }
                }
                catch (Exception $e) {}

                if ($new_key = $this->_getApiKey($extension)) {
                    $api_key = $new_key;
                }

                $extensions[] = $extension.'@'.$version;
            }
        }

        if (!count($extensions)) {
            return;
        }

        $query = array(
            'joomla' => JVERSION,
            'php'    => PHP_VERSION,
            'extensions' => $extensions
        );

        $url = static::BASE_URL.'extensions.json?'.http_build_query($query);

        $token    = JSession::getFormToken() . '=' . 1;
        $updates_url      = JUri::base() . 'index.php?option=com_installer&view=update&task=update.find&' . $token;
        $updates_ajax_url = JUri::base() . 'index.php?option=com_installer&view=update&task=update.ajax&eid=0&skip=700&' . $token;

        $script = /** @lang javascript */
            <<<JS
                
jQuery(function($) {
    if (typeof sessionStorage === 'undefined' || typeof sessionStorage.joomlatools_updater_notified === 'undefined') {
        /**
         * Show notifications if there are available updates 
         */
        var showMessages = function(messages) {
            var error_container = $('#system-message-container');
        
            $.each(messages, function(i, message) {
                if (typeof message !== 'object' || typeof message.type === 'undefined') {
                    return;
                }   
    
                var type = message.type,
                    msg  = message.message.replace(/{upgrade_url}/g, '$updates_url'), 
                    c = error_container.find('.alert-joomlatoolsupdate.alert-'+type);
    
                if (c.length == 0) {
                    c = $('<div style="text-align: center" class="alert alert-joomlatoolsupdate alert-'+type+'"></div>');
                    error_container.append(c);
                }
    
                c.append($('<p>'+msg+'</p>'));
            });
        };
        
        $.ajax({
            url: '$url',
            dataType: 'json',
            cache: false,
            headers: {
                'Authorization': 'Bearer $api_key'
            }	
        }).done(function(response) {
            if (typeof response.data !== 'undefined') {
            
                if (response.data.length) {
                    showMessages(response.data);
            
                    $.ajax({url: '$updates_ajax_url'});
                }
            
                if (typeof sessionStorage !== 'undefined') {
                    sessionStorage.joomlatools_updater_notified = true;
                }
            }
        });
    }
});

JS;

        JFactory::getDocument()->addScriptDeclaration($script);
    }

    protected function _getApiKey($extension)
    {
        $key = null;
        $file = sprintf(JPATH_ROOT.'/administrator/components/com_%1$s/resources/install/.api.key', $extension);

        if (file_exists($file)) {
            $file = trim(@file_get_contents($file));
            $file = str_replace(array("\n", "\r"), '', $file);

            if (strlen($file) > 0 && strlen($file) < 2048) {
                $key = $file;
            }
        }

        return $key;
    }

}
