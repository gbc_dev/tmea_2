<?php
/**
* @package		Komento
* @copyright	Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Komento is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

jimport('joomla.plugin.plugin');
jimport('joomla.filesystem.file');

class plgSystemKomento extends JPlugin
{
	private $extension = null;

	/**
	 * Trigger for Sobipro
	 *
	 * @since	2.0.9
	 * @access	public
	 * @param	string
	 * @return	
	 */
	public function ContentDisplayEntryView(&$text)
	{
		// Skip komento trying to display comments in adding new listing
		if (JRequest::getVar( 'task' ) == 'entry.add') {
			return;
		}

		$article = new stdClass;
		$article->id = JRequest::getInt( 'sid' );
		$article->text = $text;
		
		$params = new stdClass();

		$this->execute(__FUNCTION__, null, $article, $params, null);
	}

	public function AfterDisplayEntryView()
	{
		// Skip komento trying to display comments in adding new listing
		if( JRequest::getVar( 'task' ) == 'entry.add' )
		{
			return;
		}
		
		$article		= new stdClass;
		$article->id	= JRequest::getInt( 'sid' );
		$article->text	= '';
		$params			= new stdClass;

		$this->execute( __FUNCTION__, null, $article, $params, null );
	}

	private function execute($eventTrigger, $context = '', &$article, &$params, $page = 0)
	{
		static $bootstrap = null;

		// If bootstrap isn't loaded yet, try to load the bootstrap
		if (is_null($bootstrap)) {

			$constants = JPATH_ROOT . '/components/com_komento/constants.php';

			if (!JFile::exists($constants)) {
				$bootstrap = false;

				return false;
			}

			// Include necessary files
			require_once($constants);
			require_once(KOMENTO_BOOTSTRAP);

			$bootstrap = true;
		}

		if ($bootstrap === false) {
			return false;
		}

		if (!$this->extension) {
			$this->extension = JRequest::getCmd('option');
		}

		// @task: trigger onAfterEventTriggered
		$result = Komento::onAfterEventTriggered(__CLASS__, $eventTrigger, $this->extension, $context, $article, $params);

		if (!$result) {
			return false;
		}

		// Passing in the data
		$options = array();
		$options['trigger'] = $eventTrigger;
		$options['context'] = $context;
		$options['params'] = $params;
		$options['page'] = $page;

		// Ready to Commentify!
		return Komento::commentify($this->extension, $article, $options);
	}
}
