<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
?>

<h2>
	<?php echo JText::_('COM_TMEA_COUNTRIES_COUNTRY_CONTACTS_VIEW_CONTACT_TITLE'); ?>: <i><?php echo $this->item->contact_title; ?></i>
	<!-- <span class="pull-right" style="font-weight:300; font-size:15px;">[<a href="<?php //echo JRoute::_('index.php?option=com_tmea_countries&task=contact.edit&id=' . (int) $this->item->id); ?>"><?php //echo JText::_('JACTION_EDIT') ?></a>]</span> -->
</h2>

<div class="row">
<div class="col-md-5">
<div class="ibox float-e-margins">
		<div class="ibox float-e-margins">
      <div class="ibox-title">
          <h5><?php echo $this->escape($this->item->contact_title); ?> Contacts</h5>
      </div>
      <div class="ibox-content">
          <table class="table table-hover no-margins">
              <thead>
              <tbody>
			<tr>
				<td>Country</td>
				<td><?php echo $this->escape($this->item->countryname); ?></td>
			</tr>
			<tr>
				<td>Contact_location</td>
				<td><?php echo $this->escape($this->item->contact_location); ?></td>
			</tr>
			<tr>
				<td>Email Address</td>
				<td><a href="mailto:<?php echo $this->escape($this->item->main_email_address); ?>"><?php echo $this->escape($this->item->main_email_address); ?></td>
			</tr>
	</tbody>
          </table>
      </div>
    </div>
	</div>
<table class="table table-striped hidden">
	<tbody>
			<tr>
				<td>Contact_title</td>
				<td><?php echo $this->escape($this->item->contact_title); ?></td>
			</tr>
			<tr>
				<td>Country</td>
				<td><?php echo $this->escape($this->item->country); ?></td>
			</tr>
			<tr>
				<td>Contact_location</td>
				<td><?php echo $this->escape($this->item->contact_location); ?></td>
			</tr>
			<tr>
				<td>Main_email_address</td>
				<td><a href="mailto:<?php echo $this->escape($this->item->main_email_address); ?>"><?php echo $this->escape($this->item->main_email_address); ?></td>
			</tr>
	</tbody>
</table>
</div>
<div class="col-md-7">
	<div class="contact_map">map</div>
</div>
</div>

<!-- <p><a href="index.php?option=com_tmea_countries&view=contacts"><?php //echo JText::_('JPREVIOUS'); ?></a></p> -->