<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");
?>
<style>
#sp-component{
	background-color: #fff;
	min-height: -webkit-fill-available;
	min-height: -moz-available;
	padding-bottom: 60px;
}
</style>
<h2>
	<?php e//cho JText::_('COM_TMEA_COUNTRIES_COUNTRY_CONTACTS_VIEW_CONTACT_TITLE'); ?><!-- : --> <i><?php //echo $this->item->contact_title; ?></i>
	<span class="pull-right" style="font-weight:300; font-size:15px;">[<a href="<?php echo JRoute::_('index.php?option=com_tmea_countries&task=contact.edit&id=' . (int) $this->item->id); ?>"><?php echo JText::_('JACTION_EDIT') ?></a>]</span> 
</h2>

<div class="row">
<div class="col-md-5">
<div class="ibox float-e-margins">
		<div class="">
      <div class="ibox-title">
          <h5><?php echo $this->escape($this->item->contact_title); ?> Contacts</h5>
      </div>
      <div class="ibox-content">
          <table class="table table-hover no-margins">
              <thead>
              <tbody>
			<tr>
				<td>Location</td>
				<td><?php echo $this->escape($this->item->contact_location); ?></td>
			</tr>
			<tr>
				<td>Email Address</td>
				<td><a href="mailto:<?php echo $this->escape($this->item->main_email_address); ?>"><?php echo $this->escape($this->item->main_email_address); ?></td>
			</tr>
	</tbody>
          </table>
      </div>
    </div>
	</div>
</div>
<div class="col-md-7">
	<div class="contact_map">
		<iframe width="600" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAwKsqQaVG8YHvHgwczFOeR_ia2U_o60nY&q=<?php echo $this->escape($this->item->contact_title); ?>,<?php echo $this->escape($this->item->contact_location); ?>" allowfullscreen>
</iframe>
	</div>
</div>
</div>