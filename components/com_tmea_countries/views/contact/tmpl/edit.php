<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");
?>
<style>
#sp-component{
	background-color: #fff;
	min-height: -webkit-fill-available;
	min-height: -moz-available;
	padding-bottom: 60px;
}
</style>
<h2>
	<?php echo JText::_('COM_TMEA_COUNTRIES_COUNTRY_CONTACTS_VIEW_CONTACT_TITLE'); ?>: <i><?php echo $this->item->contact_title; ?></i>
	<span class="pull-right" style="font-weight:300; font-size:15px;">[<a href="<?php echo JRoute::_('index.php?option=com_tmea_countries&task=contact.edit&id=' . (int) $this->item->id); ?>"><?php echo JText::_('JACTION_EDIT') ?></a>]</span> 
</h2>

<div class="row">
<div class="col-md-12">
<div class="ibox float-e-margins">
		<div class="">
      <div class="ibox-title">
          <h5><?php echo $this->escape($this->item->contact_title); ?> Contacts - e</h5>
      </div>
      <form action="<?php echo JRoute::_('index.php?option=com_tmea_countries&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="contact-form" class="form-validate">
	
	<div class="form-inline form-inline-header">
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('contact_title'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('contact_title'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('alias'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('alias'); ?></div>
		</div>
	</div>

	<div class="form-horizontal">
	<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', 'Contact', $this->item->id, true); ?>
		<div class="row-fluid">
			<div class="span9">
				<div class="row-fluid form-horizontal-desktop">			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('country'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('country'); ?></div>
			</div>			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_location'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_location'); ?></div>
			</div>			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('main_email_address'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('main_email_address'); ?></div>
			</div>
				</div>
			</div>
			<div class="span3">
				<?php echo JLayoutHelper::render('joomla.edit.global', $this); ?>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		
		<?php /*echo JHtml::_('bootstrap.addTab', 'myTab', 'images', JText::_('JGLOBAL_FIELDSET_IMAGE_OPTIONS', true)); ?>
		<div class="row-fluid">
			<div class="span6">
					<?php echo $this->form->getControlGroup('images'); ?>
					<?php foreach ($this->form->getGroup('images') as $field) : ?>
						<?php echo $field->getControlGroup(); ?>
					<?php endforeach; ?>
				</div>
			</div>
		<?php echo JHtml::_('bootstrap.endTab'); */?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('JGLOBAL_FIELDSET_PUBLISHING', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('joomla.edit.publishingdata', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('joomla.edit.metadata', $this); ?>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		
		<?php echo JLayoutHelper::render('joomla.edit.params', $this); ?>
				
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'acl', 'ACL Configuration', true); ?>		
		<div class="row-fluid">
			<div class="span12">
				<fieldset class="panelform">
					<legend><?php echo $this->item->contact_title ?></legend>
					<?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
					<?php echo JHtml::_('sliders.panel', JText::_('ACL Configuration'), 'access-rules'); ?>
					<?php echo $this->form->getInput('rules'); ?>
					<?php echo JHtml::_('sliders.end'); ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		
	<?php echo JHtml::_('bootstrap.endTabSet'); ?>
	</div>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
    </div>
	</div>
</div>
<div class="col-md-7 hidden">
	<div class="contact_map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d510564.32482804684!2d36.7073204!3d-1.3047958!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f1763e69b60db%3A0x5957dce16365dda1!2sTrade+Mark+East+Africa!5e0!3m2!1sen!2s!4v1504578897607" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>
</div>

<!-- <p><a href="index.php?option=com_tmea_countries&view=contacts"><?php //echo JText::_('JPREVIOUS'); ?></a></p> -->
