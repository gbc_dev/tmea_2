<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");
?>

<h2>
	<?php echo JText::_('COM_TMEA_COUNTRIES_COUNTRIES_VIEW_COUNTRY_TITLE'); ?>: <i><?php echo $this->item->country_name; ?></i>
	<!-- <span class="pull-right" style="font-weight:300; font-size:15px;">[<a href="<?php //echo JRoute::_('index.php?option=com_tmea_countries&task=country.edit&id=' . (int) $this->item->id); ?>"><?php //echo JText::_('JACTION_EDIT') ?></a>]</span> -->
</h2>
<?php //var_dump($this->item);?>
<div class="country_brief">
	<img class="img img-responsive center-block" src="<?php echo $this->item->banner_image;?>">
	<br />
	<?php echo $this->item->tol2j_countriy_brief;?>
</div>
<br />
<div class="row">
	<div class="col-md-7">
		<?php if (empty($this->programs)) {?>
			<div class="ibox float-e-margins">
	      <div class="ibox-title">
	          <h5><?php echo $this->item->country_name; ?> program list</h5>
	      </div>
	      <div class="ibox-content">
	    <p>No Content</p>
	      </div>
	    </div>
		<?php } else { ?>
		<div class="ibox float-e-margins">
      <div class="ibox-title">
          <h5><?php echo $this->item->country_name; ?> program list</h5>
      </div>
      <div class="ibox-content">
          <table class="table table-hover no-margins">
              <thead>
              <tr>
                  <th>Status</th>
                  <th>Completion Date</th>
                  <th>Manager</th>
                  <th>Location</th>
              </tr>
              </thead>
              <tbody>
								<?php foreach ($this->programs as $program) {?>
              <tr>
                  <td><small><a href="index.php?option=com_tmea_programs&view=program&id=<?php echo $program->id;?>"><?php echo $program->program_name;?></a></small></td>
                  <td><i class="fa fa-clock-o"></i> <?php echo JHtml::_('date', $program->created, 'd-m-Y'); ?></td>
                  <td><?php echo $program->manager;?></td>
                  <td class="text-navy"> <?php echo $this->item->country_name; ?></td>
              </tr>
							<?php
		                } ?>
              </tbody>
          </table>
      </div>
    </div>
		<?php } ?>
	</div>

	<div class="col-md-5">
		<?php if (empty($this->contacts)) { ?>
			<div class="ibox float-e-margins">
	      <div class="ibox-title">
	          <h5><?php echo $this->item->country_name; ?> contacts</h5>
	      </div>
	      <div class="ibox-content">
	    <p>No Content</p>
	      </div>
	    </div>
      <?php } else { ?>
				<div class="ibox float-e-margins">
		      <div class="ibox-title">
		          <h5><?php echo $this->item->country_name; ?> contacts</h5>
		      </div>
		      <div class="ibox-content">
		          <table class="table table-hover no-margins">
		              <thead>
		              <tr>
		                  <th>Contact</th>
		                  <th>Location</th>
		                  <th>Email</th>
		              </tr>
		              </thead>
		              <tbody>
										<?php foreach ($this->contacts as $contact) {?>
		              <tr>
		                  <td><small><a href="index.php?option=com_tmea_countries&view=contact&id=<?php echo $contact->id;?>"><?php echo $contact->contact_title;?></a></small></td>
		                  <td><i class="fa fa-location-arrow"></i> <?php echo $contact->contact_location;?></td>
		                  <td><?php echo $contact->main_email_address;?></td>
		              </tr>
									<?php
				                } ?>
		              </tbody>
		          </table>
		      </div>
		    </div>
		<table class="table table-hover hidden">
			<tbody>
				<thead>
					<tr>
							<th>Location</th>
					</tr>
			</thead>

						<?php
                        // display the results
                        // echo '<pre>';
                        // // var_dump($this->contacts);
                    // echo '</pre>';
                foreach ($this->contacts as $contact) {
                    ?>
					<tr>
						<td>
					<a href="index.php?option=com_tmea_countries&view=contact&id=<?php echo $contact->id; ?>"><?php echo $contact->contact_title; ?> - <?php echo $contact->main_email_address; ?></a>
						</td>
				</tr>
					<?php
                } ?>
				</tbody>
				</table>
				<?php
                            }?>
	</div>
</div>

<table class="table table-striped hidden">
	<tbody>
			<tr>
				<td>Country_name</td>
				<td><?php echo $this->escape($this->item->country_name); ?></td>
			</tr>
			<tr>
				<td>Country_data_manager</td>
				<td><?php echo $this->escape($this->item->country_data_manager); ?></td>
			</tr>
			<tr>
				<td>Tol2j_countriy_brief</td>
				<td><?php echo $this->escape($this->item->tol2j_countriy_brief); ?></td>
			</tr>
			<tr>
				<td>Banner_image</td>
				<td><?php echo $this->escape($this->item->banner_image); ?></td>
			</tr>
		<tr>
			<td>ID</td>
			<td><?php echo $this->escape($this->item->id); ?></td>
		</tr>
	</tbody>
</table>
<!-- <p><a href="index.php?option=com_tmea_countries&view=countries"><?php //echo JText::_('JPREVIOUS'); ?></a></p> -->
