<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Countries list view class.
 *
 * @package     Tmea_countries
 * @subpackage  Views
 */
class Tmea_countriesViewCountries extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	protected $toolbar;

	public function display($tpl = null)
	{
		$app = JFactory::getApplication();
		
		$this->items 		 = $this->get('Items');
		$this->state 		 = $this->get('State');
		$this->pagination 	 = $this->get('Pagination');
		$this->user		 	 = JFactory::getUser();
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$this->programs = $this->get('AllPrograms');
		$this->projects = $this->get('AllProjects');
		$this->LoggedinGroup = $this->get('LoggedinGrouped');
		// 
		// var_dump($this->projects );
		
		$active = $app->getMenu()->getActive();
		if ($active)
		{
			$this->params = $active->params;
		}
		else
		{
			$this->params = new JRegistry();
		}
		
		// Prepare the data.
		foreach ($this->items as $item)
		{
			$item->slug	= $item->alias ? ($item->id.':'.$item->alias) : $item->id;

			$temp = new JRegistry;
			$temp->loadString($item->params);
				
			$active = $app->getMenu()->getActive();
			$item->params = clone($this->params);
			$item->params->merge($temp);
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
			return false;
		}
		
		$user = JFactory::getUser();        // Get the user object
		$app  = JFactory::getApplication(); // Get the application

		if ($user->id != 0)
		{
		    // you are logged in
		}
		else 
		{
		    // Redirect the user
		    // $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
		    $msg = 'You must be logged in to view this content';
			$app->redirect(JRoute::_('index.php?option=com_users&view=login'), $msg);
		}

		parent::display($tpl);
	}
}
?>