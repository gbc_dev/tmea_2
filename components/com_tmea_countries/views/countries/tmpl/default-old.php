<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

// necessary libraries
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived	= $this->state->get('filter.published') == 2 ? true : false;
$trashed	= $this->state->get('filter.published') == -2 ? true : false;
$user = JFactory::getUser();
?>
<style>
.row2 {
	background-color: #e4e4e4;
}
h3 {
    color: #003c70;
}
#sp-component{
	background-color: #fff;
	min-height: -webkit-fill-available;
	padding-bottom: 120px;
}
</style>

<!-- <h2 align="hidden"><?php
// echo JText::_('COM_TMEA_COUNTRIES_COUNTRIES_VIEW_COUNTRIES_TITLE'); ?>
</h2> -->

<div class="containerx">
	<div class="col-md-12" id="AboutTMEAInCountriesx">
		<h3>About TMEA</h3>
		<p> Trade and Markets East Africa (TradeMark East Africa – TMEA) is an East African not-for profit Company Limited by Guarantee established in 2010 to support the growth of trade - both regional and international - in East Africa. TradeMark East Africa (TMEA) is focused on ensuring gains from trade result in tangible gains for East Africans.</p>
		<h4>TMEA Objectives</h4>
		<div class="col-md-6">
			<div class="jumbotron text-center">
		        <h1>Increased Access to Markets</h1>
		    </div>
		</div>
		<div class="col-md-6">
			<div class="jumbotron text-center">
		        <h1>Enhanced Trade Environment</h1>
		     </div>
		</div>
		<br />
	</div>

	<div class="col-md-5" id="SelectCountriesBox">
		<?php foreach ($this->items as $i => $item) :
		$canEdit	= $this->user->authorise('core.edit',       'com_tmea_countries'.'.country.'.$item->id);
		$canEditOwn	= $this->user->authorise('core.edit.own',   'com_tmea_countries'.'.country.'.$item->id) && $item->created_by == $this->user->id;
		$canDelete	= $this->user->authorise('core.delete',       'com_tmea_countries'.'.country.'.$item->id);
		$canCheckin	= $this->user->authorise('core.manage',     'com_checkin') || $item->checked_out == $this->user->id || $item->checked_out == 0;
		$canChange	= $this->user->authorise('core.edit.state', 'com_tmea_countries'.'.country.'.$item->id) && $canCheckin;
		?>

		<a href="<?php echo JRoute::_("index.php?option=com_tmea_countries&view=country&id=" . $item->id); ?>">
		<div class="col-xs-12 col-md-2 countries">
			<h2 style='color: #fff; margin-top: 15px;font-size: 20px; font-weight: 500; text-align: center;'>
				<?php echo $this->escape($item->country_name); ?>
			</h2>
			<?php //echo $this->escape($item->tol2j_countriy_brief); ?>
			<br />
			<?php
			//var_dump($item); ?>
		</div>
		</a>
		<?php endforeach ?>
	</div>

	<!-- Board Members View -->

	<?php
	$user = JFactory::getUser();
	$groups = $user->get('groups');
	$isBoard = false;
	if (in_array(10,$groups)){
		$isBoard = true;
		// echo $groups;
		}?>
	<?php if ($isBoard) :?>
		<div class="col-md-6" id="ProjectsOverviewCountriesxx">
		<h3>TMEA Programs Overview</h3>
		<?php

		// var_dump($this->programs);
			if (empty($this->programs)) { ?>
				<div class="ibox float-e-margins">
			      <div class="ibox-content">
			    	<p>No Content</p>
			      </div>
			    </div>
			<?php } else { ?>
				<div class="ibox float-e-margins">
			      <div class="ibox-title">
			      </div>
			      
			      <div class="ibox-content">
			          <table class="table table-hover no-margins table-condensed">
			              <thead>
			              <tr>
			                  <th>Status</th>
			                  <th>Completion Date</th>
			                  <th>Manager</th>
			                  <th>Location</th>
			              </tr>
			              </thead>
			              <tbody>
							<?php foreach ($this->projects as $project) { //var_dump($program);?>
			              <tr>
			                  <td>
			                  <small><a href="index.php?option=com_tmea_programs&view=project&id=<?php echo $project->id;?>"><?php echo $project->project_name;?></a></small>
			                  </td>
			                  <td><i class="fa fa-clock-o"></i> <small><?php echo JHtml::_('date', $project->end_date, 'd-m-Y'); ?></small></td>
			                  <td><small><?php echo $project->manager;?></small></td>
			                  <td class="text-navy"> <small><?php echo $project->location; ?></small></td>
			              </tr>
										<?php
					                } ?>
			              </tbody>
			          </table>
			      </div>
			    </div>
					<?php } ?>
				</div>

	<?php endif; ?>
	<!-- Board and council Members View -->

	<!-- NOC Members -->
	
	<!-- NOC Members -->
</div>


