<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

// necessary libraries
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived	= $this->state->get('filter.published') == 2 ? true : false;
$trashed	= $this->state->get('filter.published') == -2 ? true : false;
$user = JFactory::getUser();
?>
<style>
.row2 {
	background-color: #e4e4e4;
}
h3 {
    color: #003c70;
}
#sp-component{
	background-color: #fff;
	min-height: -webkit-fill-available;
	padding-bottom: 120px;
}
</style>

<!-- <h2 align="hidden"><?php
// echo JText::_('COM_TMEA_COUNTRIES_COUNTRIES_VIEW_COUNTRIES_TITLE'); ?>
</h2> -->
<div class="containerx">
	<img src="images/home-page.jpg" />
	<div class="hometext">
		<h3>About TMEA</h3>
		<hr />
		<p>TradeMark East Africa (TMEA) is an aid-for-trade organisation that was established with the aim of growing prosperity in East Africa through increased trade. TMEA operates on a not-for-profit basis and is funded by the development agencies of the following countries: Belgium, Canada, Denmark, Finland, the Netherlands, UK, and USA. TMEA works closely with East African Community (EAC) institutions, national governments, the private sector and civil society organisations.</p>

		<!-- Trigger the modal with a button -->
		  <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">Read More</button>

		  <!-- Modal -->
		  <div class="modal fade" id="myModal" role="dialog">
		        <div class="modal-dialog modal-lg">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title center-block">About TMEA</h4>
		        </div>
		        <div class="modal-body">
				<p>TradeMark East Africa (TMEA) is an aid-for-trade organisation that was established with the aim of growing prosperity in East Africa through increased trade. TMEA operates on a not-for-profit basis and is funded by the development agencies of the following countries: Belgium, Canada, Denmark, Finland, the Netherlands, UK, and USA. TMEA works closely with East African Community (EAC) institutions, national governments, the private sector and civil society organisations.</p>
				<p>TMEA seeks to increase trade by unlocking economic potential through three strategic objectives:</p>
				<ul>
				<li>Increasing  physical access to markets</li>
				<li>Enhancing trade environment</li>
				<li>Improving business competitiveness.</li>
				</ul>

				<p>Increased trade contributes to stronger economic growth, a reduction in poverty and subsequently greater prosperity. TMEA has its headquarters in Nairobi with offices in Arusha, Bujumbura, Dar es Salaam, Juba, Kampala and Kigali.</p>

				<p>To find out more, please visit the TMEA website at <a href="https://www.trademarkea.com" target="_blank">www.trademarkea.com</a>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
		  <!-- Modal -->
	</div>
</div>
<div class="containerx">
	<div class="col-md-12 hidden" id="AboutTMEAInCountriesx">
		<br />
		<img src="images/home-page.jpg" />
		<h3>About TMEA</h3>
		<p>TradeMark East Africa (TMEA) is an aid-for-trade organisation that was established with the aim of growing prosperity in East Africa through increased trade. TMEA operates on a not-for-profit basis and is funded by the development agencies of the following countries: Belgium, Canada, Denmark, Finland, the Netherlands, UK, and USA. TMEA works closely with East African Community (EAC) institutions, national governments, the private sector and civil society organisations.</p>
		<p>TMEA seeks to increase trade by unlocking economic potential through three strategic objectives:</p>
		<ul>
		<li>Increasing  physical access to markets</li>
		<li>Enhancing trade environment</li>
		<li>Improving business competitiveness.</li>
		</ul>

		<p>Increased trade contributes to stronger economic growth, a reduction in poverty and subsequently greater prosperity. TMEA has its headquarters in Nairobi with offices in Arusha, Bujumbura, Dar es Salaam, Juba, Kampala and Kigali.</p>

		<p>To find out more, please visit the TMEA website at <a href="https://www.trademarkea.com" target="_blank">www.trademarkea.com</a>

	</div>

		<!-- Countries View -->
		<?php
			$user = JFactory::getUser();
			$groups = $user->get('groups');
			$isCountry = false;
			if (in_array(10,$groups) || in_array(11,$groups) || in_array(35,$groups) || in_array(36,$groups) || in_array(37,$groups) ){
				$isCountry = true;
				}?>
			<?php if ($isCountry) {?>
				<div class="" id="SelectCountriesBox">
					<?php foreach ($this->items as $i => $item) :
					$canEdit	= $this->user->authorise('core.edit',       'com_tmea_countries'.'.country.'.$item->id);
					$canEditOwn	= $this->user->authorise('core.edit.own',   'com_tmea_countries'.'.country.'.$item->id) && $item->created_by == $this->user->id;
					$canDelete	= $this->user->authorise('core.delete',       'com_tmea_countries'.'.country.'.$item->id);
					$canCheckin	= $this->user->authorise('core.manage',     'com_checkin') || $item->checked_out == $this->user->id || $item->checked_out == 0;
					$canChange	= $this->user->authorise('core.edit.state', 'com_tmea_countries'.'.country.'.$item->id) && $canCheckin;
					?>
					<a href="<?php echo JRoute::_("index.php?option=com_tmea_countries&view=country&id=" . $item->id); ?>">
					<div class="col-xs-12 col-md-2 countries">
					<div class="">
					<img src="<?php echo $this->escape($item->flag); ?>" /> 
						<h2 style='color: #fff; margin-top: 15px;font-size: 20px; font-weight: 500; text-align: center;'>
							<?php echo $this->escape($item->country_name); ?>
						</h2>
						</div>
						<!-- <br /> -->
					</div>
					</a>
		<?php endforeach ?>
		<!-- Countries View -->
		<?php } ?>
	</div>

		<div class="containerx clearfix">
		<div class="col-md-6" id="ProjectsOverviewCountriesxx">
		
			<?php
			$user = JFactory::getUser();
			$groups = $user->get('groups');
			$isProgram = false;
			if (in_array(10,$groups) || in_array(11,$groups) ){
				$isProgram = true;
				}?>
			<?php if ($isProgram) {?>
				<div class="programs_list">
				<!-- Programs View -->
			<?php
			 if (empty($this->programs)) {?>
				<h3>TMEA Programs Overview</h3>
					<div class="ibox float-e-margins">
				      <div class="ibox-title">
				          <h5>Projects List</h5>
				      </div>
			      <div class="ibox-content">
			    	<p>No Content</p>
			      </div>
			    </div>
				<?php } else { ?>
				<h3>TMEA Programs Overview</h3>
					<div class="ibox float-e-margins">
		      			<div class="ibox-title">
		      		</div>
		      
		      <div class="ibox-content">
		          <table class="table table-hover no-margins table-condensed">
		              <thead>
		              <tr>
		                  <th>Name</th>
		                  <th>End Date</th>
		                  <th>Type</th>
		                  <th>Manager</th>
		              </tr>
		              </thead>
		              <tbody>
						<?php foreach ($this->programs as $program) { //var_dump($program);?>
		              <tr>
		                  <td>
		                  <small><a href="index.php?option=com_tmea_programs&view=program&id=<?php echo $program->id;?>"><?php echo $program->program_name;?></a></small>
		                  </td>
		                  <td><i class="fa fa-clock-o"></i> <small><?php echo JHtml::_('date', $program->enddate, 'd-m-Y'); ?></small></td>
		                  <td><small><?php echo $program->project_type;?></small></td>
		                  <td class="text-navy"> <small><?php echo $program->locationx; ?></small></td>
		              </tr>
									<?php
				                } ?>
		              </tbody>
		          </table>
		      </div>
		    </div>
				<?php } ?>
			<!-- Programs View -->
			</div>
			<?php } else {
				/*echo "<p>Your name is {$user->name}, your email is {$user->email}, and your username is {$user->username}</p>";*/
				?>
				<div class="projects_list">
			<!-- Projects View -->
			<?php
			 if (empty($this->projects)) {?>
				<h3>TMEA Projects Overview</h3>
					<div class="ibox float-e-margins">
				      <div class="ibox-title">
				          <h5>Projects List</h5>
				      </div>
			      <div class="ibox-content">
			    	<p>No Content</p>
			      </div>
			    </div>
				<?php } else { ?>
				<h3>TMEA Projects Overview</h3>
					<div class="ibox float-e-margins">
		      			<div class="ibox-title">
		      		</div>
		      
		      <div class="ibox-content">
		          <table class="table table-hover no-margins table-condensed">
		              <thead>
		              <tr>
		                  <th class="col-md-5">Name</th>
		                  <th class="col-md-2">End Date</th>
		                  <th class="col-md-2">Type</th>
		                  <th class="col-md-2">Manager</th>
		              </tr>
		              </thead>
		              <tbody>
						<?php foreach ($this->projects as $project) { /*var_dump($project);*/?>
		              <tr>
		                  <td>
		                  <small><a href="index.php?option=com_tmea_programs&view=project&id=<?php echo $project->id;?>"><?php echo $project->project_name;?></a></small>
		                  </td>
		                  <td> <small><?php echo JHtml::_('date', $project->end_date, 'd-m-Y'); ?></small></td>
		                  <td><small><?php echo $project->project_budget;?></small></td>
		                  <td class="text-navy"> <small><?php echo $project->manager; ?></small></td>
		              </tr>
									<?php
				                } ?>
		              </tbody>
		          </table>
		      </div>
		    </div>
				<?php } ?>
			<!-- Projects View -->
				</div>
			<?php } ?>
			</div>

			

			<div class="col-md-6" id="RecentDocumentsBoxx">
				<h3>Recent Documents</h3>
				<?php
				  $position = 'recent_documents'; /*position of module in the backend*/
				  $modules =& JModuleHelper::getModules($position);
				  if (!empty($modules)) { ?>
				    <section class="rowx recent_documents">
				      <?php
				      foreach ($modules as $module) {
				        echo JModuleHelper::renderModule($module);
				      }
				       ?>
				       <div class="clearfix"></div>
				       <br/>
				       </br/>
				   </section>
				  <?php  }?>
		  	</div>

		</div>

<!-- Events and Documents -->
<div class="brief_intro">


  <div class="col-md-12">
	<h3>Events Calender</h3>
	<?php
	  $position = 'upcoming_events'; //position of module in the backend
	  $modules =& JModuleHelper::getModules($position);
	  if (!empty($modules)) { ?>
	    <div class="ibox float-e-margins">
	<br />
	      <?php
	      foreach ($modules as $module) {
	        echo JModuleHelper::renderModule($module);
	      }
	       ?>
	       <div class="clearfix"></div>
	       <br/>
	       </br/>
	   </div>
	  <?php  }?>
  </div>
  </div>
  </div>
<!-- Events and Documents -->
		</div>
		<div class="clearfix"></div>

		<div class="col-md-12 donors">
			<!-- <h3>Our Proud Donors</h3> -->
			<br />
			<?php /*
	  $position = 'donors'; //position of module in the backend
	  $modules =& JModuleHelper::getModules($position);
	  if (!empty($modules)) { ?>
	      <?php
	      foreach ($modules as $module) {
	        echo JModuleHelper::renderModule($module);
	      }
	       ?>
	   </div>
	  <?php  }*/?>
	  <img class="img img-responsive" src="images/donor-strip.png" />
		</div>