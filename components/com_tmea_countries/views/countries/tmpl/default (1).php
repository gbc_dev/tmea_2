<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

// necessary libraries
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived	= $this->state->get('filter.published') == 2 ? true : false;
$trashed	= $this->state->get('filter.published') == -2 ? true : false;
$user = JFactory::getUser();
?>
<style>
.row2 {
	background-color: #e4e4e4;
}
</style>

<!-- <h2><?php //echo JText::_('COM_TMEA_COUNTRIES_COUNTRIES_VIEW_COUNTRIES_TITLE'); ?></h2> -->
<div class="containerx">
	<?php foreach ($this->items as $i => $item) :
	$canEdit	= $this->user->authorise('core.edit',       'com_tmea_countries'.'.country.'.$item->id);
	$canEditOwn	= $this->user->authorise('core.edit.own',   'com_tmea_countries'.'.country.'.$item->id) && $item->created_by == $this->user->id;
	$canDelete	= $this->user->authorise('core.delete',       'com_tmea_countries'.'.country.'.$item->id);
	$canCheckin	= $this->user->authorise('core.manage',     'com_checkin') || $item->checked_out == $this->user->id || $item->checked_out == 0;
	$canChange	= $this->user->authorise('core.edit.state', 'com_tmea_countries'.'.country.'.$item->id) && $canCheckin;
	?>
	<a href="<?php echo JRoute::_("index.php?option=com_tmea_countries&view=country&id=" . $item->id); ?>">
	<div class="col-xs-12 col-md-2 countries">
		<h2>
			<?php echo $this->escape($item->country_name); ?>
		</h2>
		<img class="img-responsive center-block" src="<?php echo $this->escape($item->banner_image); ?>" />
		<?php //echo $this->escape($item->tol2j_countriy_brief); ?>
		<br />
		<?php
		//var_dump($item); ?>
	</div>
	</a>
	<?php endforeach ?>

</div>

<div class="brief_intro">
	<div class="col-md-6">
	<h3>About TMEA</h3>
	<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
	<div class="col-md-6">
	<h3>TMEA Projects Overview</h3>
	<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
	<?php if (empty($this->programs)) {?>
			<div class="ibox float-e-margins">
	      <div class="ibox-title">
	          <h5><?php echo $this->item->country_name; ?> program list</h5>
	      </div>
	      <div class="ibox-content">
	    <p>No Content</p>
	      </div>
	    </div>
		<?php } else { ?>
		<div class="ibox float-e-margins">
      <div class="ibox-title">
          <h5> program list</h5>
      </div>
      <div class="ibox-content">
          <table class="table table-hover no-margins table-condensed">
              <thead>
              <tr>
                  <th>Status</th>
                  <th>Completion Date</th>
                  <th>Manager</th>
                  <th>Location</th>
              </tr>
              </thead>
              <tbody>
				<?php foreach ($this->programs as $program) {?>
              <tr>
                  <td>
                  <small><a href="index.php?option=com_tmea_programs&view=program&id=<?php echo $program->id;?>"><?php echo $program->program_name;?></a></small>
                  </td>
                  <td><i class="fa fa-clock-o"></i> <small><?php echo JHtml::_('date', $program->created, 'd-m-Y'); ?></small></td>
                  <td><small><?php echo $program->manager;?></small></td>
                  <td class="text-navy"> <small><?php echo $this->item->country_name; ?></small></td>
              </tr>
							<?php
		                } ?>
              </tbody>
          </table>
      </div>
    </div>
		<?php } ?>
	</div>
</div>
<form action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm" class="hidden">
	<?php
		// Search tools bar
		//echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
	?>
	<div>
		<p>
			<?php if ($user->authorise("core.create", "com_tmea_countries")) : ?>
				<button type="button" class="btn btn-success" onclick="Joomla.submitform('country.add')"><?php echo JText::_('JNEW') ?></button>
			<?php endif; ?>
			<?php if (($user->authorise("core.edit", "com_tmea_countries") || $user->authorise("core.edit.own", "com_tmea_countries")) && isset($this->items[0])) : ?>
				<button type="button" class="btn" onclick="Joomla.submitform('country.edit')"><?php echo JText::_('JEDIT') ?></button>
			<?php endif; ?>
			<?php if ($user->authorise("core.edit.state", "com_tmea_countries")) : ?>
				<?php if (isset($this->items[0]->published)) : ?>
					<button type="button" class="btn" onclick="Joomla.submitform('countries.publish')"><?php echo JText::_('JPUBLISH') ?></button>
					<button type="button" class="btn" onclick="Joomla.submitform('countries.unpublish')"><?php echo JText::_('JUNPUBLISH') ?></button>
					<button type="button" class="btn" onclick="Joomla.submitform('countries.archive')"><?php echo JText::_('JARCHIVE') ?></button>
				<?php elseif (isset($this->items[0])) : ?>
					<button type="button" class="btn btn-error" onclick="Joomla.submitform('countries.delete')"><?php echo JText::_('JDELETE') ?></button>
				<?php endif; ?>
				<?php if (isset($this->items[0]->published)) : ?>
					<?php if ($this->state->get('filter.published') == -2 && $user->authorise("core.delete", "com_tmea_countries")) : ?>
						<button type="button" class="btn btn-error" onclick="Joomla.submitform('countries.delete')"><?php echo JText::_('JDELETE') ?></button>
					<?php elseif ($this->state->get('filter.published') != -2 && $user->authorise("core.edit.state", "com_tmea_countries")) : ?>
						<button type="button" class="btn btn-warning" onclick="Joomla.submitform('countries.trash')"><?php echo JText::_('JTRASH') ?></button>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
		</p>
	</div>
	<table class="category table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="1%" class="hidden-phone">
					<?php echo JHtml::_('grid.checkall'); ?>
				</th>
				<th id="itemlist_header_title">
					<?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'a.country_name', $listDirn, $listOrder); ?>
				</th>
				<th class="nowrap left">
					<?php echo JHtml::_('grid.sort', JText::_('COM_TMEA_COUNTRIES_COUNTRIES_FIELD_COUNTRY_DATA_MANAGER_LABEL'), 'a.country_data_manager', $listDirn, $listOrder) ?>
				</th>
				<th class="nowrap left">
					<?php echo JHtml::_('grid.sort', JText::_('COM_TMEA_COUNTRIES_COUNTRIES_FIELD_TOL2J_COUNTRIY_BRIEF_LABEL'), 'a.tol2j_countriy_brief', $listDirn, $listOrder) ?>
				</th>
				<th class="nowrap left">
					<?php echo JHtml::_('grid.sort', JText::_('COM_TMEA_COUNTRIES_COUNTRIES_FIELD_BANNER_IMAGE_LABEL'), 'a.banner_image', $listDirn, $listOrder) ?>
				</th>
				<?php if ($this->params->get('list_show_author', 1)) : ?>
				<th id="itemlist_header_author">
					<?php echo JHtml::_('grid.sort', 'JAUTHOR', 'author', $listDirn, $listOrder); ?>
				</th>
				<?php endif; ?>
				<?php if ($this->user->authorise('core.edit') || $this->user->authorise('core.edit.own')) : ?>
				<th id="itemlist_header_edit"><?php echo JText::_('COM_TMEA_COUNTRIES_EDIT_ITEM'); ?></th>
				<?php endif; ?>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($this->items as $i => $item) :
		$canEdit	= $this->user->authorise('core.edit',       'com_tmea_countries'.'.country.'.$item->id);
		$canEditOwn	= $this->user->authorise('core.edit.own',   'com_tmea_countries'.'.country.'.$item->id) && $item->created_by == $this->user->id;
		$canDelete	= $this->user->authorise('core.delete',       'com_tmea_countries'.'.country.'.$item->id);
		$canCheckin	= $this->user->authorise('core.manage',     'com_checkin') || $item->checked_out == $this->user->id || $item->checked_out == 0;
		$canChange	= $this->user->authorise('core.edit.state', 'com_tmea_countries'.'.country.'.$item->id) && $canCheckin;
		?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
				<td headers="itemlist_header_title" class="list-title">
					<?php if (isset($item->access) && in_array($item->access, $this->user->getAuthorisedViewLevels())) : ?>
						<a href="<?php echo JRoute::_("index.php?option=com_tmea_countries&view=country&id=" . $item->id); ?>">
							<?php echo $this->escape($item->country_name); ?>
						</a>
					<?php else: ?>
						<?php echo $this->escape($item->country_name); ?>
					<?php endif; ?>
					<?php if ($item->published == 0) : ?>
						<span class="list-published label label-warning">
							<?php echo JText::_('JUNPUBLISHED'); ?>
						</span>
					<?php endif; ?>
					<?php if ($item->published == 2) : ?>
						<span class="list-published label label-info">
							<?php echo JText::_('JARCHIVED'); ?>
						</span>
					<?php endif; ?>
					<?php if ($item->published == -2) : ?>
						<span class="list-published label">
							<?php echo JText::_('JTRASHED'); ?>
						</span>
					<?php endif; ?>
					<?php if (strtotime($item->publish_up) > strtotime(JFactory::getDate())) : ?>
						<span class="list-published label label-warning">
							<?php echo JText::_('JNOTPUBLISHEDYET'); ?>
						</span>
					<?php endif; ?>
					<?php if ((strtotime($item->publish_down) < strtotime(JFactory::getDate())) && $item->publish_down != '0000-00-00 00:00:00') : ?>
						<span class="list-published label label-warning">
							<?php echo JText::_('JEXPIRED'); ?>
						</span>
					<?php endif; ?>
				</td>
				<td style="width:50%"><?php echo $this->escape($item->country_data_manager); ?></td>
				<td style="width:50%"><?php echo $this->escape($item->tol2j_countriy_brief); ?></td>
				<td style="width:50%"><?php echo $this->escape($item->banner_image); ?></td>
				<?php if ($this->params->get('list_show_author', 1)) : ?>
				<td class="small hidden-phone">
					<?php echo $this->escape($item->author_name); ?>
				</td>
				<?php endif; ?>
				<?php if ($this->user->authorise("core.edit") || $this->user->authorise("core.edit.own")) : ?>
				<td headers="itemlist_header_edit" class="list-edit">
					<?php if ($canEdit || $canEditOwn) : ?>
						<a href="<?php echo JRoute::_("index.php?option=com_tmea_countries&task=country.edit&id=" . $item->id); ?>"><i class="icon-edit"></i> <?php echo JText::_("JGLOBAL_EDIT"); ?></a>
					<?php endif; ?>
				</td>
				<?php endif; ?>
			</tr>
		<?php endforeach ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="7"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
	</table>
	<div>
		<input type="hidden" name="task" value=" " />
		<input type="hidden" name="boxchecked" value="0" />
		<!-- Sortierkriterien -->
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
