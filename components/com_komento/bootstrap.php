<?php
defined('_JEXEC') or die('Unauthorized Access');

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder' );
jimport('joomla.filesystem.path');
jimport('joomla.access.access');

require_once(__DIR__ . '/constants.php');
require_once(KOMENTO_HELPERS . '/version.php' );
require_once(KOMENTO_HELPERS . '/document.php' );
require_once(KOMENTO_HELPERS . '/helper.php' );
require_once(KOMENTO_HELPERS . '/router.php' );
require_once(KOMENTO_CLASSES . '/comment.php' );

// Load language here
// initially language is loaded in content plugin
// for custom integration that doesn't go through plugin, language is not loaded
// hence, language should be loaded in bootstrap

$lang = JFactory::getLanguage();
$path = JPATH_ROOT;

if (JFactory::getApplication()->isAdmin()) {
	$path .= '/administrator';
}

// Load English first as fallback
$konfig = Komento::getKonfig();

if ($konfig->get('enable_language_fallback')) {
	$lang->load('com_komento', $path, 'en-GB', true);
}

$lang->load('com_komento', $path, $lang->getDefault(), true);
$lang->load('com_komento', $path, null, true);
