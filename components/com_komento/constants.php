<?php
/**
* @package      Komento
* @copyright    Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* Komento is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

// @since Foundry 3.0
// Foundry
require_once(JPATH_ROOT . '/media/foundry/3.1/joomla/framework.php');
FD31_FoundryFramework::defineComponentConstants("Komento");

// Environment paths
define('KOMENTO_ROOT', JPATH_ROOT . '/components/com_komento');
define('KOMENTO_ADMIN_ROOT', JPATH_ADMINISTRATOR . '/components/com_komento');
define('KOMENTO_ASSETS', KOMENTO_ROOT . '/assets');
define('KOMENTO_HELPERS', KOMENTO_ROOT . '/helpers');
define('KOMENTO_HELPER', KOMENTO_HELPERS . '/helper.php');
define('KOMENTO_CONTROLLERS', KOMENTO_ROOT . '/controllers');
define('KOMENTO_MODELS', KOMENTO_ROOT . '/models');
define('KOMENTO_CLASSES', KOMENTO_ROOT . '/classes');
define('KOMENTO_TABLES', KOMENTO_ADMIN_ROOT . '/tables');
define('KOMENTO_THEMES', KOMENTO_ROOT . '/themes');
define('KOMENTO_MEDIA_ROOT', JPATH_ROOT . '/media');


define('KOMENTO_JS_ROOT', KOMENTO_MEDIA . '/js');
define('KOMENTO_CSS_ROOT', KOMENTO_MEDIA . '/css');
define('KOMENTO_SCRIPTS_ROOT', KOMENTO_MEDIA . '/scripts');
define('KOMENTO_SCRIPTS__ROOT', KOMENTO_MEDIA . '/scripts_');
define('KOMENTO_SPINNER', rtrim(JURI::root(), '/') . '/components/com_komento/assets/images/loader.gif');
define('KOMENTO_UPLOADS_ROOT', KOMENTO_MEDIA . '/uploads');
define('KOMENTO_PLUGINS' , KOMENTO_ROOT . '/komento_plugins');
define('KOMENTO_EMOTICONS_DIR', rtrim(JURI::root(), '/') . '/components/com_komento/themes/kuro/images/markitup/');
define('KOMENTO_BOOTSTRAP', KOMENTO_ROOT . '/bootstrap.php');

// Comment Statuses
define('KOMENTO_COMMENT_UNPUBLISHED', 0);
define('KOMENTO_COMMENT_PUBLISHED', 1);
define('KOMENTO_COMMENT_MODERATE', 2);

// Comment flags
define('KOMENTO_COMMENT_NOFLAG', 0);
define('KOMENTO_COMMENT_SPAM', 1);
define('KOMENTO_COMMENT_OFFENSIVE', 2);
define('KOMENTO_COMMENT_OFFTOPIC', 3);

// Updates server
define('KOMENTO_UPDATES_SERVER', 'stackideas.com');

// Themes
define('KOMENTO_THEME_BASE', 'kuro');

// Foundry
define('KOMENTO_FOUNDRY_ROOT', KOMENTO_MEDIA_ROOT . '/foundry/3.1');
define('KOMENTO_FOUNDRY_BOOTSTRAP', KOMENTO_FOUNDRY_ROOT .'/joomla/configuration.php');