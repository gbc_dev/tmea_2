<?php
/**
 * @package		Komento
 * @copyright	Copyright (C) 2012 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * Komento is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined( '_JEXEC' ) or die( 'Restricted access' ); 
?>

<script type="text/javascript">
Komento.module('komento.bbcode', function($) {
	var module = this;

	$.getBBcodeSettings = function() {
		var settings = {
			previewParserVar: 'data',
			markupSet: []
		};

		if(Komento.options.config.bbcode_bold == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_BOLD', true);?>",
				key:'B',
				openWith:'[b]',
				closeWith:'[/b]',
				className:'kmt-markitup-bold'
			});
		}

		if(Komento.options.config.bbcode_italic == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_ITALIC', true); ?>",
				key:'I',
				openWith:'[i]',
				closeWith:'[/i]',
				className:'kmt-markitup-italic'
			});
		}

		if(Komento.options.config.bbcode_underline == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_UNDERLINE', true); ?>",
				key:'U',
				openWith:'[u]',
				closeWith:'[/u]',
				className:'kmt-markitup-underline'
			});
		}

		if(Komento.options.config.bbcode_bold == 1 || Komento.options.config.bbcode_italic == 1 || Komento.options.config.bbcode_underline == 1) {
			settings.markupSet.push({separator:'---------------' });
		}

		if(Komento.options.config.bbcode_link == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_LINK', true); ?>",
				key:'L',
				openWith:'[url="[![Link:!:http://]!]"(!( title="[![Title]!]")!)]', closeWith:'[/url]',
				placeHolder: "<?php echo JText::_('COM_KOMENTO_BBCODE_LINK_TEXT', true); ?>",
				className:'kmt-markitup-link'
			});
		}

		if(Komento.options.config.bbcode_picture == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_PICTURE', true); ?>",
				key:'P',
				replaceWith:'[img][![Url]!][/img]',
				className:'kmt-markitup-picture'
			});
		}

		if(Komento.options.config.bbcode_video == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_VIDEO', true); ?>",
				// replaceWith: '[video][![' + "<?php echo JText::_('COM_KOMENTO_SUPPORTED_VIDEOS', true); ?>" + ']!][/video]',
				replaceWith: function(h) {
					Komento.ajax('site.views.komento.showVideoDialog', {
						caretPosition: h.caretPosition,
						element: $(h.textarea).attr('id')
					}, {
						success: function(title, html) {
							Komento.require().library('dialog').done(function() {
								$.dialog({
									content: html,
									title: title,
									width: 400,
									afterShow: function() {
										$('.foundryDialog').find('.videoUrl').focus();
									}
								});
							});
						}
					});
				},
				className: 'kmt-markitup-video'
			});
		}

		if(Komento.options.config.bbcode_link == 1 || Komento.options.config.bbcode_picture == 1) {
			settings.markupSet.push({separator:'---------------' });
		}

		if(Komento.options.config.bbcode_bulletlist == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_BULLETLIST', true); ?>",
				openWith:'[list]\n',
				closeWith:'\n[/list]',
				className:'kmt-markitup-bullet'
			});
		}

		if(Komento.options.config.bbcode_numericlist == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_NUMERICLIST', true); ?>",
				openWith:'[list=[![Starting number]!]]\n',
				closeWith:'\n[/list]',
				className:'kmt-markitup-numeric'
			});
		}

		if(Komento.options.config.bbcode_bullet == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_BULLET', true); ?>",
				openWith:'[*]',
				closeWith:'[/*]',
				className:'kmt-markitup-list'
			});
		}

		if(Komento.options.config.bbcode_bulletlist == 1 || Komento.options.config.bbcode_numericlist == 1 || Komento.options.config.bbcode_bullet == 1) {
			settings.markupSet.push({separator:'---------------' });
		}

		if(Komento.options.config.bbcode_quote == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_QUOTE', true); ?>",
				openWith:'[quote]',
				closeWith:'[/quote]',
				className:'kmt-markitup-quote'
			});
		}

		if(Komento.options.config.bbcode_code == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_CODE', true); ?>",
				openWith:'[code type="xml"]',
				closeWith:'[/code]',
				className:'kmt-markitup-code'
			});
		}

		if(Komento.options.config.bbcode_clean == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_CLEAN', true); ?>",
				className:"clean", replaceWith:function(markitup) { return markitup.selection.replace(/\[(.*?)\]/g, "") },
				className:'kmt-markitup-clean'
			});
		}

		if(Komento.options.config.bbcode_quote == 1 || Komento.options.config.bbcode_code || Komento.options.config.bbcode_clean == 1) {
			settings.markupSet.push({separator:'---------------' });
		}

		if(Komento.options.config.bbcode_smile == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_SMILE', true); ?>",
				openWith:':)',
				className:'kmt-markitup-smile'
			});
		}

		if(Komento.options.config.bbcode_happy == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_HAPPY', true); ?>",
				openWith:':D',
				className:'kmt-markitup-happy'
			});
		}

		if(Komento.options.config.bbcode_surprised == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_SURPRISED', true); ?>",
				openWith:':o',
				className:'kmt-markitup-surprised'
			});
		}

		if(Komento.options.config.bbcode_tongue == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_TONGUE', true); ?>",
				openWith:':p',
				className:'kmt-markitup-tongue'
			});
		}

		if(Komento.options.config.bbcode_unhappy == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_UNHAPPY', true); ?>",
				openWith:':(',
				className:'kmt-markitup-unhappy'
			});
		}

		if(Komento.options.config.bbcode_wink == 1) {
			settings.markupSet.push({
				name: "<?php echo JText::_('COM_KOMENTO_BBCODE_WINK', true); ?>",
				openWith:';)',
				className:'kmt-markitup-wink'
			});
		}

		if($.isArray(Komento.options.config.smileycode)) {
			$.each(Komento.options.config.smileycode, function(index, code) {
				settings.markupSet.push({
					name: code,
					openWith: code,
					className: 'kmt-markitup-custom-' + index
				});
			});
		}

		return settings;
	};

	module.resolve();
});

</script>
<div class="kmt-form-editor">
	<textarea id="commentInput" class="commentInput input textarea" cols="50" rows="10" tabindex="44" placeholder="<?php echo JText::_('COM_KOMENTO_FORM_WRITE_YOUR_COMMENTS'); ?>"></textarea>
	<?php // echo $editor->display('commentInput', '', '100%', '150', '10', '10', false); ?>
</div>
