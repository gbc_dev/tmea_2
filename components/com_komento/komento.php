<?php
/**
* @package      Komento
* @copyright    Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* Komento is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

require_once(JPATH_ROOT . '/components/com_komento/bootstrap.php');

// process ajax calls here
Komento::getHelper('Ajax')->process();

jimport('joomla.application.component.controller');

require_once(KOMENTO_ROOT . '/controller.php');

$task = JRequest::getCmd('task', 'display', 'GET');

if ($task == 'confirmSubscription') {

	$token = JRequest::getVar('token', '');

	if (empty($token)) {
		echo JText::_('COM_KOMENTO_INVALID_TOKEN');
		exit;
	}

	$model = Komento::getModel('subscription');
	$state = $model->confirmSubscription($token);
}

$controllerName	= JRequest::getCmd('controller', '');

$controller = KomentoController::getInstance($controllerName);
$controller->execute($task);
$controller->redirect();
