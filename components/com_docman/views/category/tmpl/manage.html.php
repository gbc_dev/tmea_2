<?
/**
 * @package     DOCman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('KOOWA') or die;

$show_delete  = isset($show_delete) ? $show_delete : $category->canPerform('delete');
$show_edit    = isset($show_edit) ? $show_edit : $category->canPerform('edit');
$button_size  = 'btn-'.(isset($button_size) ? $button_size : 'small');
?>

<? // Edit and delete buttons ?>
<? if (!($category->isLockable() && $category->isLocked()) && ($show_edit || $show_delete)): ?>
<div class="btn-toolbar koowa_toolbar">
  <div class="btn-group">
    <? // Edit ?>
    <? if ($show_edit): ?>
      <a class="btn btn-default <?= $button_size ?>"
         href="<?= helper('route.category', array(
           'entity' => $category,
           'view' => 'category',
           'layout' => 'form',
           'tmpl' => 'koowa'
         )); ?>"
      ><?= translate('Edit'); ?></a>
    <? endif ?>

    <? // Delete ?>
    <? if ($show_delete):
      $data = array(
        'method' => 'post',
        'url' => (string) helper('route.category',array('entity' => $category), true, false),
        'params' => array(
          'csrf_token' => object('user')->getSession()->getToken(),
          '_action'    => 'delete',
          '_referrer'  => base64_encode((string) object('request')->getUrl())
        )
      );
    ?>
    <?= helper('behavior.deletable'); ?>
    <a class="btn <?= $button_size ?> btn-danger docman-deletable" href="#" rel="<?= escape(json_encode($data)) ?>">
      <?= translate('Delete') ?>
    </a>
    <? endif ?>
  </div>
</div>
<? endif ?>
