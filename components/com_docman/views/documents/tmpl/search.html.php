<?
/**
 * @package     DOCman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('KOOWA') or die; ?>

<div class="well well-small k-filters k-filters--toggleable">
    <input class="k-checkbox-dropdown-toggle" id="k-checkbox-dropdown-toggle" type="checkbox"
        <?= !empty($filter_toggled) ? 'checked' : '' ?>
    >
    <label class="k-checkbox-dropdown-label" for="k-checkbox-dropdown-toggle"><?= translate('Search for documents'); ?></label>
    <div class="k-checkbox-dropdown-content">
        <div class="form-group">
            <label for="search">
                <?= translate('Find by title or description…') ?>
            </label>
            <input
                class="input-block-level"
                type="search"
                name="<?= !empty($filter_group) ? $filter_group.'[search]' : 'search' ?>"
                value="<?= $filter->search ?>" />
        </div>

        <? if ($params->get('show_category_filter', 1)): ?>
        <div class="form-group">
            <label class="control-label"><?= translate('Category') ?></label>
            <?= helper('listbox.categories', array(
                'deselect' => true,
                'check_access' => false,
                'name' => !empty($filter_group) ? $filter_group.'[category]' : 'category',
                'filter'  => $category_filter,
                'attribs' => array('id' => 'category', 'multiple' => true),
                'selected' => $filter->category
            )) ?>
        </div>
        <? endif; ?>

        <? if ($params->get('show_tag_filter', 1)): ?>
        <div class="form-group">
            <label class="control-label"><?= translate('Tags') ?></label>
            <?= helper('listbox.tags', array(
                'autocreate' => false,
                'name' => !empty($filter_group) ? $filter_group.'[tag][]' : 'tag[]',
                'value' => 'slug',
                'selected' => $filter->tag,
                'filter' => $tag_filter,
                'attribs'  => array(
                    'data-placeholder' => translate('All Tags'))
            )); ?>
        </div>
        <? endif ?>

        <? if ($params->get('show_owner_filter', 1)): ?>
        <div class="form-group">
            <label class="control-label"><?= translate('Owner') ?></label>
            <?= helper('listbox.users', array(
                'name' => !empty($filter_group) ? $filter_group.'[created_by]' : 'created_by',
                'selected' => $filter->created_by,
                'attribs'  => array('multiple' => true),
                'deselect' => true
            )) ?>
        </div>
        <? endif ?>

        <button class="btn btn-lg" type="submit"><?= translate('Search') ?></button>
    </div>
</div>

<? if ($filter->search && isset($documents) && !count($documents)): ?>

<? // No documents found message ?>
<div class="alert alert-warning"><?= import('com://site/docman.documents.no_results.html') ?></div>

<? endif ?>
