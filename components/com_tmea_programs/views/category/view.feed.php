<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Feed View class for the WebLinks component
 *
 * @package     Tmea_programs
 * @subpackage  Views
 */
class Tmea_programsViewCategory extends JViewCategoryfeed
{
	/**
	 * @var    string  The name of the view to link individual items to
	 * @since  3.2
	 */
	protected $viewName = 'program';
}
