<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");
?>

<style>
    #sp-component {
        background-color: #fff;
        min-height: -webkit-fill-available;
        min-height: -moz-available;
    }
    h2{
        color: #003c70;
    }
</style>
<?php if (!empty($this->item->banner)) {?>
<header class="entry-header">
        <div class="featuredImg">
        <h1 class="entry-title"><?php echo $this->item->project_name; ?></h1>
        <img width="1366" height="600" src="<?php echo $this->item->banner; ?>" class="featured wp-post-image" alt="<?php echo $this->item->program_name; ?>">      
      </div>
      <span class="pull-right" style="font-weight:300; font-size:15px;">[<a href="<?php echo JRoute::_('index.php?option=com_tmea_programs&task=project.edit&id=' . (int) $this->item->id); ?>"><?php echo JText::_('JACTION_EDIT') ?></a>]</span>
  </header>

<div class="col-md-12 hidden">
    <img src="<?php echo $this->escape($this->item->banner); ?>" />
<h2 class="hidden">
	<i><?php echo $this->item->project_name; ?></i>
	<span class="pull-right" style="font-weight:300; font-size:15px;">[<a href="<?php echo JRoute::_('index.php?option=com_tmea_programs&task=project.edit&id=' . (int) $this->item->id); ?>"><?php echo JText::_('JACTION_EDIT') ?></a>]</span>
</h2>
<br />
</div>
<?php } ?>

<div class="col-md-12">
    <div class="col-sm-7">
        <h1><i>Desired Results</i></h1>
        <hr>
        <?php echo $this->item->project_description; ?>
        
        <?php if (!empty($this->item->why)) {?>
        <h1><i>Why</i></h1>
        <?php echo $this->item->why; ?>
        <?php } ?>
        
        <?php if (!empty($this->item->why)) {?>
        <h1><i>What</i></h1>
        <?php echo $this->item->what; ?>
        <?php } ?>

        <?php if (!empty($this->item->why)) {?>
        <h1><i>How</i></h1>
        <?php echo $this->item->how; ?>
        <?php } ?>

        <?php if (!empty($this->item->achievements)) {?>
        <h1><i>Achievements</i></h1>
        <?php echo $this->item->achievements; ?>
        <?php } ?>
    </div>
    
    <div class="col-sm-5" id="SummaryBox">
        <h3>Summary Information</h3>
        <table class="table table-striped hiddenx" style="webkit-box-shadow: 0 1px 10px #333;
    -moz-box-shadow: 0 1px 10px #333;
    -ms-box-shadow: 0 1px 10px #333;
    box-shadow: 0 1px 10px #333;
    border-radius: 4px;">
	<tbody>
			<tr>
				<td style="font-weight: 500;">PROJECT NAME: </td>
				<td><?php echo $this->escape($this->item->project_name); ?></td>
			</tr>
			<tr>
				<td style="font-weight: 500;">COUNTRY: </td>
				<td><?php echo $this->escape($this->item->project_country); ?></td>
			</tr>
			<tr>
				<td style="font-weight: 500;">MANAGER: </td>
				<td><?php echo $this->escape($this->item->proj_manager); ?></td>
			</tr>
			<tr>
				<td style="font-weight: 500;">PROJECT TYPE:</td>
				<td><?php echo $this->escape($this->item->project_type); ?></td>
			</tr>
    		<tr>
    			<td style="font-weight: 500;">GRANT: </td>
    			<td><?php echo $this->escape($this->item->project_grant); ?></td>
    		</tr>
    		<tr>
    		    <td style="font-weight: 500;">IMPLEMENTOR: </td>
    			<td><?php echo $this->escape($this->item->project_implementors); ?></td>
    		</tr>
    		<tr>
    		    <td style="font-weight: 500;">BUDGET: </td>
    			<td><?php echo $this->escape($this->item->project_budget); ?></td>
    		</tr>
            <tr>
                <td style="font-weight: 500;">OUTCOME: </td>
                <td><?php echo $this->escape($this->item->outcome); ?></td>
            </tr>
            <tr>
                <td style="font-weight: 500;">TARGET GROUP: </td>
                <td><?php echo $this->escape($this->item->target); ?></td>
            </tr>
	</tbody>
</table>
    </div>
    <div class="col-sm-5 pull-right" id="RecentDocumentsBoxx">
        <h3>Recent Documents</h3>
        <?php
          $position = 'recent_documents'; /*position of module in the backend*/
          $modules =& JModuleHelper::getModules($position);
          if (!empty($modules)) { ?>
            <section class="rowx recent_documents">
              <?php
              foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
              }
               ?>
               <div class="clearfix"></div>
               <br/>
               </br/>
           </section>
          <?php  }?>
        </div>
</div>
<!-- <p><a href="index.php?option=com_tmea_programs&view=programs"><?php //echo JText::_('JPREVIOUS'); ?></a></p> -->	
