<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");
?>

<h2>
	<?php echo JText::_('COM_TMEA_PROGRAMS_PROGRAMS_VIEW_PROGRAM_TITLE'); ?>: <i><?php echo $this->item->program_name; ?></i>
	<span class="pull-right" style="font-weight:300; font-size:15px;">[<a href="<?php echo JRoute::_('index.php?option=com_tmea_programs&task=program.edit&id=' . (int) $this->item->id); ?>"><?php echo JText::_('JACTION_EDIT') ?></a>]</span>
</h2>

<table class="table table-striped">
	<tbody>
			<tr>
				<td>Program_name</td>
				<td><?php echo $this->escape($this->item->program_name); ?></td>
			</tr>
			<tr>
				<td>Country</td>
				<td><?php echo $this->escape($this->item->country); ?></td>
			</tr>
			<tr>
				<td>Program_manager</td>
				<td><?php echo $this->escape($this->item->program_manager); ?></td>
			</tr>
			<tr>
				<td>Program_documents</td>
				<td><?php echo $this->escape($this->item->program_documents); ?></td>
			</tr>
		<tr>
			<td>ID</td>
			<td><?php echo $this->escape($this->item->id); ?></td>
		</tr>
	</tbody>
</table>
<p><a href="index.php?option=com_tmea_programs&view=programs"><?php echo JText::_('JPREVIOUS'); ?></a></p>