<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");
// var_dump($this->item);
?>

<style>
    #sp-component {
        background-color: #fff;
        min-height: -webkit-fill-available;
        min-height: -moz-available;
    }
    h2, h3 {
    color: #003c70;
    }
</style>
<?php
if(!empty($this->item->banner)){
?>
<header class="entry-header">
        <div class="featuredImg">
        <h1 class="entry-title"><?php echo $this->item->program_name; ?></h1>
        <img width="1366" height="600" src="<?php echo $this->item->banner; ?>" class="featured wp-post-image" alt="<?php echo $this->item->program_name; ?>">      
      </div>
  </header>
  <?php 
} else{ ?>
<header class="entry-header">
        <h1 class="entry-title"><?php echo $this->item->program_name; ?></h1>  
  </header>
<?php } ?>
<h2 style="margin: 15px;">
	<?php //echo JText::_('Program'); ?><!-- : --> <i><?php //echo $this->item->program_name; ?></i>
	<span class="pull-right" style="font-weight:300; font-size:15px;">[<a href="<?php echo JRoute::_('index.php?option=com_tmea_programs&task=program.edit&id=' . (int) $this->item->id); ?>"><?php echo JText::_('JACTION_EDIT') ?></a>]</span>
</h2>

<div class="">
<div class="col-md-6">
  <?php 

  // var_dump($this->item);
  echo $this->item->project_description; ?>
</div>
<div class="col-md-6">
    <h3 style="margin-top:0px; margin-left: 10px;">Summary Information</h3>
<table class="table table-striped hiddenx" style="webkit-box-shadow: 0 1px 10px #333;
    -moz-box-shadow: 0 1px 10px #333;
    -ms-box-shadow: 0 1px 10px #333;
    box-shadow: 0 1px 10px #333;
    border-radius: 4px;
    width: -webkit-fill-available;
    width: -moz-available;
    margin: 15px;">
  <tbody>
      <tr>
        <td>NAME</td>
        <td><?php echo $this->escape($this->item->program_name); ?></td>
      </tr>
      <tr>
        <td>COUNTRY</td>
        <td><?php echo $this->escape($this->item->prog_country); ?></td>
      </tr>
      <tr>
        <td>PROGRAM MANAGER</td>
        <td><?php echo $this->escape($this->item->prog_manager); ?></td>
      </tr>
  </tbody>
</table>
</div>
</div>

<div class="brief_intro">
	<div class="col-md-6" id="ProjectsOverviewCountriesx">
	<h3>Projects in <?php echo $this->item->program_name; ?></h3>
	<?php if (empty($this->projects)) {?>
			<div class="ibox float-e-margins">
	      <div class="ibox-title">
	          <h5>No Projects found</h5>
	      </div>
	      <div class="ibox-content">
	    <!-- <p>No Content</p> -->
	      </div>
	    </div>
		<?php } else { ?>
		<div class="ibox float-e-margins">
      <div class="ibox-content">
          <table class="table table-hover no-margins table-condensed">
              <thead>
              <tr>
                  <th>Name</th>
                  <th>End Date</th>
                  <th>Manager</th>
                  <th>Location</th>
              </tr>
              </thead>
              <tbody>
				<?php foreach ($this->projects as $project) { //var_dump($program);?>
              <tr>
                  <td>
                  <small><a href="index.php?option=com_tmea_programs&view=project&id=<?php echo $project->id;?>"><?php echo $project->project_name;?></a></small>
                  </td>
                  <td><i class="fa fa-clock-o"></i> <small><?php echo JHtml::_('date', $project->end_date, 'd-m-Y'); ?></small></td>
                  <td><small><?php echo $project->manager;?></small></td>
                  <td class="text-navy"> <small><?php echo $project->location; ?></small></td>
              </tr>
							<?php
		                } ?>
              </tbody>
          </table>
      </div>
    </div>
		<?php } ?>
		
	</div>
	
	<div class="col-md-12" id="RecentDocumentsBoxx">
        <h3>Recent Documents</h3>
        <?php
          $position = 'recent_documents'; /*position of module in the backend*/
          $modules =& JModuleHelper::getModules($position);
          if (!empty($modules)) { ?>
            <section class="rowx recent_documents">
              <?php
              foreach ($modules as $module) {
                echo JModuleHelper::renderModule($module);
              }
               ?>
               <div class="clearfix"></div>
               <br/>
               </br/>
           </section>
          <?php  }?>
        </div>

<!-- <p><a href="index.php?option=com_tmea_programs&view=programs"><?php //echo JText::_('JPREVIOUS'); ?></a></p> -->
<!--<div id="featuredbox" class="featuredbox">-->
<!--	<div class="pagetitle animate-me fadeIn"><h1>Website Restyling</h1></div>-->
<!--	<div class="featured-background" style="background-image: url(https://demos.alka-web.com/woffice/allinone/wp-content/themes/woffice/images/1.jpg)" ;=""><div class="featured-layer"></div></div>-->
<!--</div>-->
