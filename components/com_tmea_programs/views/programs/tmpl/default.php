<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

// necessary libraries
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

// sort ordering and direction
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$archived	= $this->state->get('filter.published') == 2 ? true : false;
$trashed	= $this->state->get('filter.published') == -2 ? true : false;
$user = JFactory::getUser();

// var_dump($this->items);
?>
<style>
.row2 {
	background-color: #e4e4e4;
}
#sp-component{
	background-color: #fff;
	min-height: -webkit-fill-available;
	min-height: -moz-available;
	padding-bottom: 60px;
}
</style>

<!-- <h2><?php //echo JText::_('COM_TMEA_PROGRAMS_PROGRAMS_VIEW_PROGRAMS_TITLE'); ?></h2> -->
<div class="col-lg-12x">
	<!--<header id="featuredbox" class="centered has-search is-404"><div class="pagetitle animate-me fadeIn"><h1>Projects</h1><form role="search" method="get" action="https://demos.alka-web.com/woffice/allinone/">-->
	<!--				<input type="text" value="" name="s" id="s" placeholder="Search...">-->
	<!--				<input type="hidden" name="searchsubmit" id="searchsubmit" value="true"><input type="hidden" name="post_type" value="projects">-->
	<!--				<button type="submit" name="searchsubmit"><i class="fa fa-search"></i></button>-->
	<!--		</form></div><!-- .pagetitle -->
	<!--<div class="featured-background" style="background-image: url(https://demos.alka-web.com/woffice/allinone/wp-content/themes/woffice/images/1.jpg)" ;=""><div class="featured-layer"></div></div></header>-->

			<!-- START THE CONTENT CONTAINER -->
			<div id="content-container">

				<!-- START CONTENT -->
				<div id="content">
					<div id="post-216" class="post-216 page type-page status-publish hentry" style="">
							<div id="projects-page-content" class="box content">
								<!--<div class="intern-padding">-->
								<!--	<p style="text-align: center;">Welcome to the projects page, here you can see 2 examples of projects, you can add as many project as you like and there is many settings for each project (<strong>project date, links, tasks, files, comments…</strong>). If you have idea of new features for the this, please let us know.</p>-->
								<!--</div>-->
							</div>
                        <br />
                        <br />
						<!-- LOOP ALL THE PROJECTS-->
						<?php
						foreach ($this->items as $i => $item) :
						$canEdit	= $this->user->authorise('core.edit',       'com_tmea_programs'.'.program.'.$item->id);
						$canEditOwn	= $this->user->authorise('core.edit.own',   'com_tmea_programs'.'.program.'.$item->id) && $item->created_by == $this->user->id;
						$canDelete	= $this->user->authorise('core.delete',       'com_tmea_programs'.'.program.'.$item->id);
						$canCheckin	= $this->user->authorise('core.manage',     'com_checkin') || $item->checked_out == $this->user->id || $item->checked_out == 0;
						$canChange	= $this->user->authorise('core.edit.state', 'com_tmea_programs'.'.program.'.$item->id) && $canCheckin;

						?>
							<div class="col-md-12 project" style="border-radius: 4px; background-color: #f5f5f5; border: 1px solid; border-color: #e5e5e5; margin: 10px; -webkit-box-shadow: 0 1px 10px #333; -moz-box-shadow: 0 1px 10px #333; -ms-box-shadow: 0 1px 10px #333; box-shadow: 0 1px 10px #333;
">
								<?php //var_dump($item);?>
								<div class="intern-padding">
									<a href="<?php echo JRoute::_("index.php?option=com_tmea_programs&view=program&id=" . $item->id); ?>" rel="bookmark" class="project-head">

										<h2 class="project-title" style="font-weight: 400; color: #003c70;"><i class="fa fa-cubes"></i><?php echo $item->program_name; ?></h2>

										<span class="project-category"><i class="fa fa-tag"></i>Implementor: <?php echo $item->implementor; ?></span>
							            <!-- <span class="project-category"><i class="fa fa-money"></i> Budjet: <?php echo $this->escape($item->budget); ?></span> -->
									</a>
									<p class="project-excerpt"></p><p><?php echo $this->escape($item->project_description); ?></p>

									<div class="text-right">
										<a href="<?php echo JRoute::_("index.php?option=com_tmea_programs&view=program&id=" . $item->id); ?>" class="btn btn-default">See Programme <i class="fa fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
							<?php endforeach ?>



					</div>
				</div>

			</div>

        <div class="ibox float-e-margins hidden">
        <div class="ibox-title hidden">
						<div class="col-sm-3 pull-right">
								<div class="input-group">
									<input type="text" placeholder="Search" class="input-sm form-control">
									<span class="input-group-btn">
										<button type="button" class="btn btn-sm btn-primary"> Go!</button>
									</span>
								</div>
						</div>
        </div>
        <div class="ibox-content hidden">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Project </th>
                        <th>Manager </th>
                        <th>Project Type </th>
                        <th>Country </th>
                        <th>Implementor</th>
												<th>Grant</th>
												<th>Budget</th>
                        <th>End Date</th>
                    </tr>
                    </thead>
                    <tbody>
											<?php
											foreach ($this->items as $i => $item) :
											$canEdit	= $this->user->authorise('core.edit',       'com_tmea_programs'.'.program.'.$item->id);
											$canEditOwn	= $this->user->authorise('core.edit.own',   'com_tmea_programs'.'.program.'.$item->id) && $item->created_by == $this->user->id;
											$canDelete	= $this->user->authorise('core.delete',       'com_tmea_programs'.'.program.'.$item->id);
											$canCheckin	= $this->user->authorise('core.manage',     'com_checkin') || $item->checked_out == $this->user->id || $item->checked_out == 0;
											$canChange	= $this->user->authorise('core.edit.state', 'com_tmea_programs'.'.program.'.$item->id) && $canCheckin;

											?>
                    <tr class="row<?php echo $i % 2; ?>">
                        <td><?php echo $item->id; ?></td>
												<td headers="itemlist_header_title" class="list-title">
													<?php if (isset($item->access) && in_array($item->access, $this->user->getAuthorisedViewLevels())) : ?>
														<a href="<?php echo JRoute::_("index.php?option=com_tmea_programs&view=program&id=" . $item->id); ?>">
															<?php echo $this->escape($item->program_name); ?>
														</a>
													<?php else: ?>
														<?php echo $this->escape($item->program_name); ?>
													<?php endif; ?>
													<?php if ($item->published == 0) : ?>
														<span class="list-published label label-warning">
															<?php echo JText::_('JUNPUBLISHED'); ?>
														</span>
													<?php endif; ?>
													<?php if ($item->published == 2) : ?>
														<span class="list-published label label-info">
															<?php echo JText::_('JARCHIVED'); ?>
														</span>
													<?php endif; ?>
													<?php if ($item->published == -2) : ?>
														<span class="list-published label">
															<?php echo JText::_('JTRASHED'); ?>
														</span>
													<?php endif; ?>
													<?php if (strtotime($item->publish_up) > strtotime(JFactory::getDate())) : ?>
														<span class="list-published label label-warning">
															<?php echo JText::_('JNOTPUBLISHEDYET'); ?>
														</span>
													<?php endif; ?>
													<?php if ((strtotime($item->publish_down) < strtotime(JFactory::getDate())) && $item->publish_down != '0000-00-00 00:00:00') : ?>
														<span class="list-published label label-warning">
															<?php echo JText::_('JEXPIRED'); ?>
														</span>
													<?php endif; ?>
												</td>
                        <td><?php echo $this->escape($item->prog_manager); ?></td>
                        <td><?php echo $this->escape($item->project_type); ?></td>
                        <td><?php echo $this->escape($item->prog_country); ?></td>
                        <td><?php echo $this->escape($item->implementor); ?></td>
                        <td><?php echo $this->escape($item->project_grant); ?></td>
												<td><?php echo $this->escape($item->budget); ?></td>
												<td><?php //echo $this->escape($item->enddate); ?> <?php echo JHtml::_('date', $item->enddate, 'd-m-Y'); ?></td>
                    </tr>
										<?php endforeach ?>
                    </tbody>
                </table>
            </div>

        </div>

        </div>
        </div>

<form class="hidden" action="<?php JRoute::_('index.php?option=com_mythings&view=mythings'); ?>" method="post" name="adminForm" id="adminForm">
	<?php
		// Search tools bar
		echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
	?>
	<div>
		<p>
			<?php if ($user->authorise("core.create", "com_tmea_programs") || (count($user->getAuthorisedCategories('com_tmea_programs', 'core.create'))) > 0 ) : ?>
				<button type="button" class="btn btn-success" onclick="Joomla.submitform('program.add')"><?php echo JText::_('JNEW') ?></button>
			<?php endif; ?>
			<?php if (($user->authorise("core.edit", "com_tmea_programs") || $user->authorise("core.edit.own", "com_tmea_programs")) && isset($this->items[0])) : ?>
				<button type="button" class="btn" onclick="Joomla.submitform('program.edit')"><?php echo JText::_('JEDIT') ?></button>
			<?php endif; ?>
			<?php if ($user->authorise("core.edit.state", "com_tmea_programs")) : ?>
				<?php if (isset($this->items[0]->published)) : ?>
					<button type="button" class="btn" onclick="Joomla.submitform('programs.publish')"><?php echo JText::_('JPUBLISH') ?></button>
					<button type="button" class="btn" onclick="Joomla.submitform('programs.unpublish')"><?php echo JText::_('JUNPUBLISH') ?></button>
					<button type="button" class="btn" onclick="Joomla.submitform('programs.archive')"><?php echo JText::_('JARCHIVE') ?></button>
				<?php elseif (isset($this->items[0])) : ?>
					<button type="button" class="btn btn-error" onclick="Joomla.submitform('programs.delete')"><?php echo JText::_('JDELETE') ?></button>
				<?php endif; ?>
				<?php if (isset($this->items[0]->published)) : ?>
					<?php if ($this->state->get('filter.published') == -2 && $user->authorise("core.delete", "com_tmea_programs")) : ?>
						<button type="button" class="btn btn-error" onclick="Joomla.submitform('programs.delete')"><?php echo JText::_('JDELETE') ?></button>
					<?php elseif ($this->state->get('filter.published') != -2 && $user->authorise("core.edit.state", "com_tmea_programs")) : ?>
						<button type="button" class="btn btn-warning" onclick="Joomla.submitform('programs.trash')"><?php echo JText::_('JTRASH') ?></button>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
		</p>
	</div>
	<table class="category table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="1%" class="hidden-phone">
					<?php echo JHtml::_('grid.checkall'); ?>
				</th>
				<th id="itemlist_header_title">
					<?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'a.program_name', $listDirn, $listOrder); ?>
				</th>
				<th class="nowrap left">
					<?php echo JHtml::_('grid.sort', JText::_('COM_TMEA_PROGRAMS_PROGRAMS_FIELD_COUNTRY_LABEL'), 'a.country', $listDirn, $listOrder) ?>
				</th>
				<th class="nowrap left">
					<?php echo JHtml::_('grid.sort', JText::_('COM_TMEA_PROGRAMS_PROGRAMS_FIELD_PROGRAM_MANAGER_LABEL'), 'a.program_manager', $listDirn, $listOrder) ?>
				</th>
				<th class="nowrap left">
					<?php echo JHtml::_('grid.sort', JText::_('COM_TMEA_PROGRAMS_PROGRAMS_FIELD_PROGRAM_DOCUMENTS_LABEL'), 'a.program_documents', $listDirn, $listOrder) ?>
				</th>
				<?php if ($this->params->get('list_show_author', 1)) : ?>
				<th id="itemlist_header_author">
					<?php echo JHtml::_('grid.sort', 'JAUTHOR', 'author', $listDirn, $listOrder); ?>
				</th>
				<?php endif; ?>
				<?php if ($this->user->authorise('core.edit') || $this->user->authorise('core.edit.own')) : ?>
				<th id="itemlist_header_edit"><?php echo JText::_('COM_TMEA_PROGRAMS_EDIT_ITEM'); ?></th>
				<?php endif; ?>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($this->items as $i => $item) :
		$canEdit	= $this->user->authorise('core.edit',       'com_tmea_programs'.'.program.'.$item->id);
		$canEditOwn	= $this->user->authorise('core.edit.own',   'com_tmea_programs'.'.program.'.$item->id) && $item->created_by == $this->user->id;
		$canDelete	= $this->user->authorise('core.delete',       'com_tmea_programs'.'.program.'.$item->id);
		$canCheckin	= $this->user->authorise('core.manage',     'com_checkin') || $item->checked_out == $this->user->id || $item->checked_out == 0;
		$canChange	= $this->user->authorise('core.edit.state', 'com_tmea_programs'.'.program.'.$item->id) && $canCheckin;
		?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
				<td headers="itemlist_header_title" class="list-title">
					<?php if (isset($item->access) && in_array($item->access, $this->user->getAuthorisedViewLevels())) : ?>
						<a href="<?php echo JRoute::_("index.php?option=com_tmea_programs&view=program&id=" . $item->id); ?>">
							<?php echo $this->escape($item->program_name); ?>
						</a>
					<?php else: ?>
						<?php echo $this->escape($item->program_name); ?>
					<?php endif; ?>
					<?php if ($item->published == 0) : ?>
						<span class="list-published label label-warning">
							<?php echo JText::_('JUNPUBLISHED'); ?>
						</span>
					<?php endif; ?>
					<?php if ($item->published == 2) : ?>
						<span class="list-published label label-info">
							<?php echo JText::_('JARCHIVED'); ?>
						</span>
					<?php endif; ?>
					<?php if ($item->published == -2) : ?>
						<span class="list-published label">
							<?php echo JText::_('JTRASHED'); ?>
						</span>
					<?php endif; ?>
					<?php if (strtotime($item->publish_up) > strtotime(JFactory::getDate())) : ?>
						<span class="list-published label label-warning">
							<?php echo JText::_('JNOTPUBLISHEDYET'); ?>
						</span>
					<?php endif; ?>
					<?php if ((strtotime($item->publish_down) < strtotime(JFactory::getDate())) && $item->publish_down != '0000-00-00 00:00:00') : ?>
						<span class="list-published label label-warning">
							<?php echo JText::_('JEXPIRED'); ?>
						</span>
					<?php endif; ?>
				</td>
				<td style="width:50%"><?php echo $this->escape($item->country); ?></td>
				<td style="width:50%"><?php echo $this->escape($item->program_manager); ?></td>
				<td style="width:50%"><?php echo $this->escape($item->program_documents); ?></td>
				<?php if ($this->params->get('list_show_author', 1)) : ?>
				<td class="small hidden-phone">
					<?php echo $this->escape($item->author_name); ?>
				</td>
				<?php endif; ?>
				<?php if ($this->user->authorise("core.edit") || $this->user->authorise("core.edit.own")) : ?>
				<td headers="itemlist_header_edit" class="list-edit">
					<?php if ($canEdit || $canEditOwn) : ?>
						<a href="<?php echo JRoute::_("index.php?option=com_tmea_programs&task=program.edit&id=" . $item->id); ?>"><i class="icon-edit"></i> <?php echo JText::_("JGLOBAL_EDIT"); ?></a>
					<?php endif; ?>
				</td>
				<?php endif; ?>
			</tr>
		<?php endforeach ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="7"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
	</table>
	<div>
		<input type="hidden" name="task" value=" " />
		<input type="hidden" name="boxchecked" value="0" />
		<!-- Sortierkriterien -->
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
