<?php
/**
 * @author
 * @copyright
 * @license
 */

defined("_JEXEC") or die("Restricted access");

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';
$item = ModUser_sectionHelper::getItem();
$user = JFactory::getUser();


require JModuleHelper::getLayoutPath('mod_user_section', $params->get('layout', 'default'));
