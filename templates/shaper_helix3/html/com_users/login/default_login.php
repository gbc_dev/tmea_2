<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

// print_r(get_class_methods($this->form));

//die;
// var_dump($this->params);
?>
<div class="row">
	<div class="col-sm-6 col-sm-offset-3 text-center" style="background:#f5f5f5; vertical-align:middle; border: 1px solid #74bf45;">
		<div class="login<?php echo $this->pageclass_sfx?>">
			<?php //if ($this->params->get('show_page_heading')) : ?>
          <table border="0" align="center" width="80%">
            <tr><td><img style="float:left;" src="images/logo-200x90.png" />
              </td>
              <td>
                <h2 style="padding-left:30px;color:#003c70;">
                  EXTRANET LOGIN
				</h2>
              </td>
            </tr>
          </table>

			<?php //endif; ?>

			<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
			<div class="login-description">
			<?php endif; ?>

				<?php if ($this->params->get('logindescription_show') == 1) : ?>
					<?php echo $this->params->get('login_description'); ?>
				<?php endif; ?>

				<?php if (($this->params->get('login_image') != '')) :?>
					<img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JTEXT::_('COM_USERS_LOGIN_IMAGE_ALT')?>"/>
				<?php endif; ?>

			<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
			</div>
			<?php endif; ?>

          <table style="background:#ee2e24; height:3px;" align="center" height="3px" width="98%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td></td>
            </tr>
          </table>
          <br />
          <table border="0" width="80%" align="center">

			<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="form-validate">

				<?php /* Set placeholder for username, password and secretekey */
					$this->form->setFieldAttribute( 'username', 'hint', JText::_('COM_USERS_LOGIN_USERNAME_LABEL') );
					$this->form->setFieldAttribute( 'password', 'hint', JText::_('JGLOBAL_PASSWORD') );
					$this->form->setFieldAttribute( 'secretkey', 'hint', JText::_('JGLOBAL_SECRETKEY') );
				?>

				<?php foreach ($this->form->getFieldset('credentials') as $field) : ?>
              <tr>
                   <td>
					<?php if (!$field->hidden) : ?>
						<div class="form-group">
							<div class="group-control">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php endif; ?>
                   </td>
              </tr>
				<?php endforeach; ?>

				<?php if ($this->tfa): ?>

              <tr>
              <td>
					<div class="form-group">
						<div class="group-control">
							<?php echo $this->form->getField('secretkey')->input; ?>
						</div>
					</div>
              </td>
              </tr>
				<?php endif; ?>

				<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
              <tr>
              <td>
					<div class="checkbox">
						<label>
							<input id="remember" type="checkbox" name="remember" class="inputbox" value="yes">
							<?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME') ?>
						</label>
					</div>
              </td>
              </tr>
				<?php endif; ?>

              <tr>
                <td>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block">
						<?php echo JText::_('JLOGIN'); ?>
					</button>
				</div>
                </td>
              </tr>

				<input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />
				<?php echo JHtml::_('form.token'); ?>

			</form>
          </table>
		</div>



	</div>

</div>
<style type="text/css">
	#sp-footer,#sp-header-sticky-wrapper {
		display: none
	}
	#sp-main-body {
		padding: 7% 0 !important;
	}

</style>
