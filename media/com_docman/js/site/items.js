"use strict";

kQuery(function($) {

    //delete items button
    var delete_items_btn = '#toolbar-delete';

    //use the toolbar delete button data for all delete buttons
    var request_params = $(delete_items_btn).data('params');
    var request_prompt = $(delete_items_btn).data('prompt');

    //delete item button
    var delete_item_btn = 'a[data-action="delete-item"]';

    //checkboxes
    var item_checkbox = 'input[name="item-select"]';

    var deletable, deletable_container;

    //gallery view
    if($('.koowa_media--gallery').length) {
        deletable = '.koowa_media__item__content';
        deletable_container = '.koowa_media__item';
    //table view
    } else if ($('.docman_table_layout').length) {
        deletable = 'tr.docman_item';
        deletable_container = null;
    } else {
        deletable = delete_item_btn;
        deletable_container = null;
    }

    $(delete_items_btn).addClass('k-is-disabled').data('prompt', false);

    var deleteItem = function(element) {

        var elem   = $(element),
            path   = elem.data('url')    || elem.find(item_checkbox).data('url'),
            data   = elem.data('params') || request_params;

        if (path) {
            if (elem.data('ajax') === false) {
                new Koowa.Form({
                    'method': 'post',
                    'url'   : path,
                    'params': data
                }).submit();
            } else {
                $.ajax({
                    method : 'post',
                    url : path,
                    data : data,
                    beforeSend : function () {
                        elem.addClass('k-is-disabled');
                    },
                    success : function () {

                        var container = elem;

                        if(deletable_container){
                            container = elem.closest(deletable_container);
                        }

                        container.fadeOut(300, function() {
                            elem.remove();
                        });
                    }
                });
            }
        }
  };

  //checkbox event handler
  $('body').on('click', item_checkbox, function( event ){

      $(this).closest(deletable).toggleClass('selected');

      if($(item_checkbox + ':checked').length) {
         $(delete_items_btn).removeClass('k-is-disabled');
      } else {
         $(delete_items_btn).addClass('k-is-disabled');
      }
  }).on('click', delete_item_btn, function( event ){
      //delete item event handler

      event.preventDefault();

      var $this = $(this),
          elem = $this.closest(deletable),
          prompt = request_prompt || $this.data('prompt');

      if ($this.hasClass('k-is-disabled')) {
          return;
      }

      if (confirm(prompt)) {
          deleteItem(elem);
      }
  }).on('click', delete_items_btn, function ( event ) {

        event.preventDefault();

        if(confirm(request_prompt)) {
            $.each($(item_checkbox + ':checked'), function(index, checkbox){
                var elem = $(checkbox).closest(deletable);
                deleteItem(elem);
            });
        }

        $(delete_items_btn).addClass('k-is-disabled');
    });
});
