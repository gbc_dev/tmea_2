"use strict";

kQuery(function($) {

    $.fn.downloadLabel = function ( options ) {

        var settings = $.extend({
            'container' : '.docman_download_label',
            'label_play' : Koowa.translate('Play'),
            'label_view' : Koowa.translate('View'),
            'label_open' : Koowa.translate('Open'),
            'supported_image_extensions' : new Array (
                'jpg',
                'jpeg',
                'gif',
                'png',
                'tiff',
                'tif',
                'xbm',
                'bmp'
              )
        }, options);

        this.each(function(index, el){

            var label = $(el).find(settings.container);
            var mimetype = $(el).data('mimetype');
            var extension = $(el).data('extension');
            var gdocs_preview = Boolean($(el).data('gdocspreview'));

            var tmp = mimetype.split("/");
            var content_type = tmp[0];

            if (content_type == 'image' && settings.supported_image_extensions.indexOf(extension) !== -1) {
                label.text(settings.label_view);

            } else if (content_type == 'video' || content_type == 'audio') {

                var media = document.createElement(content_type);
                var can_play = media.canPlayType( mimetype );

                if( can_play == 'maybe' || can_play == 'probably' ) {
                    label.text(settings.label_play);
                }

            } else if (content_type == 'application') {

                $.each(navigator.mimeTypes, function(index, mt){

                    if ( mt.type == mimetype ) {

                        label.text( (gdocs_preview) ? settings.label_view : settings.label_open );

                        return false;
                    }
                });
            }
        });
    };

    var download_btn = $('a.docman_download__button');

    if ( navigator.mimeTypes != null && navigator.mimeTypes.length > 0 && download_btn.length > 0 ) {
        download_btn.downloadLabel();
    }
});
