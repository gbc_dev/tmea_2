<?php
/**
* @package		Komento
* @copyright	Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Komento is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div id="si" class="komento master row-table cell-top">
	<div class="container-nav" style="visibility: hidden;" data-komento-mobile-nav>
		<a class="nav-sidebar-toggle" data-toggle="collapse" data-target=".sidebar">
			<i class="fa fa-bars"></i>
		</a>
		<a class="nav-subhead-toggle" data-toggle="collapse" data-target=".subhead-collapse">
			<i class="fa fa-cog"></i>
		</a>
	</div>
	
	<?php include(__DIR__ . '/sidebar.php'); ?>

	<div class="content col-cell">
		<?php echo $output; ?>
	</div>
</div>

<script type="text/javascript">
Komento.ready(function($){

	var header = $('.header');
	var mobileNavigation = $('[data-komento-mobile-nav]');

	// When the page is loaded, ensure that the mobile navigation works
	mobileNavigation.appendTo(header);
	mobileNavigation.css('visibility', 'visible');

	$('[data-sidebar-parent]').click(function() {
	    var parent = $(this).parent();

	    parent.toggleClass('open active');
	});

	$('.nav-sidebar-toggle').click(function(){
    	$('body').toggleClass('show-sidebar');
    });
});
</script>
