<?php
/**
* @package		Komento
* @copyright	Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Komento is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

require_once(JPATH_ROOT . '/components/com_komento/constants.php');
require_once(KOMENTO_BOOTSTRAP);
require_once(JPATH_ADMINISTRATOR . '/components/com_komento/controllers/controller.php');

jimport('joomla.application.component.controller');

// Check access
$app = JFactory::getApplication();
$my = JFactory::getUser();

if (!$my->authorise('core.manage', 'com_komento')) {
	$app->redirect('index.php', JText::_('JERROR_ALERTNOAUTHOR'), 'error');
	return $app->close();
}

// Compile scripts
$compile = JRequest::getBool('compile');

if ($compile) {
	$minify = JRequest::getBool('minify', false);

	$compiler = Komento::getClass('compiler', 'KomentoCompiler');
	$results = $compiler->compile($minify);

	header('Content-type: text/x-json; UTF-8');
	echo json_encode($results);

	exit;
}

// Process AJAX calls
Komento::getHelper('Ajax')->process();

// Get the task
$task = JRequest::getCmd('task', 'display');

// We treat the view as the controller. Load other controller if there is any.
$controller	= JRequest::getCmd('c' , JRequest::getCmd('controller', ''));

// If task is getLanguage, then set c = lang
if ($task == 'getLanguage') {
	$controller = 'lang';
}

$controller	= KomentoController::getInstance($controller);
$controller->execute($task);
$controller->redirect();
