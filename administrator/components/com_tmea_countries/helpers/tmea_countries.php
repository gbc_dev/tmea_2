<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Tmea_countries helper class.
 *
 * @package     Tmea_countries
 * @subpackage  Helpers
 */
class Tmea_countriesHelper
{
	public static function addSubmenu($vName)
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_TMEA_COUNTRIES_SUBMENU_COUNTRIES'), 
			'index.php?option=com_tmea_countries&view=countries', 
			$vName == 'countries'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_TMEA_COUNTRIES_SUBMENU_COUNTRY_CONTACTS'), 
			'index.php?option=com_tmea_countries&view=contacts', 
			$vName == 'contacts'
		);

	}
	
	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_tmea_countries';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
	

}