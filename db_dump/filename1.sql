-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 31, 2017 at 06:52 PM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gbc_tmea_wip3`
--

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_assets`
--

DROP TABLE IF EXISTS `tol2j_assets`;
CREATE TABLE `tol2j_assets` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) UNSIGNED NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_assets`
--

INSERT INTO `tol2j_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 1, 466, 0, 'root.1', 'Root Asset', '{\"core.login.site\":{\"6\":1,\"2\":1},\"core.login.admin\":{\"6\":1,\"44\":1,\"34\":1,\"38\":1,\"39\":1,\"40\":1,\"2\":1},\"core.login.offline\":[],\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1,\"34\":1,\"38\":1,\"39\":1,\"40\":1,\"2\":1},\"core.create\":{\"6\":1,\"3\":1,\"34\":1,\"38\":1,\"39\":1,\"40\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1,\"34\":1,\"38\":1,\"39\":1,\"40\":1},\"core.edit.state\":{\"6\":1,\"5\":1,\"38\":1,\"39\":1,\"40\":1},\"core.edit.own\":{\"6\":1,\"3\":1,\"34\":1,\"38\":1,\"39\":1,\"40\":1}}'),
(2, 1, 2, 3, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 4, 11, 1, 'com_banners', 'com_banners', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(4, 1, 12, 13, 1, 'com_cache', 'com_cache', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(5, 1, 14, 15, 1, 'com_checkin', 'com_checkin', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(6, 1, 16, 17, 1, 'com_config', 'com_config', '{}'),
(7, 1, 18, 87, 1, 'com_contact', 'com_contact', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(8, 1, 88, 177, 1, 'com_content', 'com_content', '{\"core.admin\":{\"7\":1},\"core.options\":[],\"core.manage\":{\"6\":1,\"2\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(9, 1, 178, 179, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 180, 181, 1, 'com_installer', 'com_installer', '{\"core.admin\":[],\"core.manage\":{\"7\":0},\"core.delete\":{\"7\":0},\"core.edit.state\":{\"7\":0}}'),
(11, 1, 182, 183, 1, 'com_languages', 'com_languages', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(12, 1, 184, 185, 1, 'com_login', 'com_login', '{}'),
(13, 1, 186, 187, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 188, 189, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 190, 191, 1, 'com_media', 'com_media', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":{\"5\":1},\"core.edit\":[],\"core.edit.state\":[]}'),
(16, 1, 192, 195, 1, 'com_menus', 'com_menus', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"2\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(17, 1, 37, 38, 1, 'com_messages', 'com_messages', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(18, 1, 198, 269, 1, 'com_modules', 'com_modules', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(19, 1, 270, 277, 1, 'com_newsfeeds', 'com_newsfeeds', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(20, 1, 278, 279, 1, 'com_plugins', 'com_plugins', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(21, 1, 280, 281, 1, 'com_redirect', 'com_redirect', '{\"core.admin\":{\"7\":1},\"core.manage\":[]}'),
(22, 1, 282, 283, 1, 'com_search', 'com_search', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1,\"2\":1}}'),
(23, 1, 284, 285, 1, 'com_templates', 'com_templates', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(24, 1, 286, 289, 1, 'com_users', 'com_users', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"40\":1,\"2\":1,\"28\":1},\"core.create\":{\"40\":1,\"2\":1,\"28\":1},\"core.delete\":[],\"core.edit\":{\"40\":1,\"2\":1,\"28\":1},\"core.edit.state\":{\"40\":1,\"28\":1}}'),
(26, 1, 290, 291, 1, 'com_wrapper', 'com_wrapper', '{}'),
(33, 1, 352, 353, 1, 'com_finder', 'com_finder', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(34, 8, 105, 108, 2, 'com_content.category.9', 'Uncategorised', '{\"core.create\":{\"10\":0,\"12\":0},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(35, 3, 7, 8, 2, 'com_banners.category.10', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(36, 7, 23, 24, 2, 'com_contact.category.11', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(37, 19, 273, 274, 2, 'com_newsfeeds.category.12', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(40, 3, 9, 10, 2, 'com_banners.category.15', 'Sample Data-Banners', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(41, 7, 25, 86, 2, 'com_contact.category.16', 'Sample Data-Contact', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(42, 19, 275, 276, 2, 'com_newsfeeds.category.17', 'Sample Data-Newsfeeds', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(44, 8, 111, 152, 2, 'com_content.category.19', 'Joomla!', '{\"core.create\":{\"10\":0,\"12\":0},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(46, 44, 138, 151, 3, 'com_content.category.21', 'Components', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(59, 41, 26, 27, 3, 'com_contact.category.34', 'Park Site', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(60, 41, 28, 85, 3, 'com_contact.category.35', 'Shop Site', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(61, 60, 29, 30, 4, 'com_contact.category.36', 'Staff', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(62, 60, 31, 84, 4, 'com_contact.category.37', 'Fruit Encyclopedia', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(63, 62, 32, 33, 5, 'com_contact.category.38', 'A', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(64, 62, 34, 35, 5, 'com_contact.category.39', 'B', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(65, 62, 36, 37, 5, 'com_contact.category.40', 'C', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(66, 62, 38, 39, 5, 'com_contact.category.41', 'D', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(67, 62, 40, 41, 5, 'com_contact.category.42', 'E', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(68, 62, 42, 43, 5, 'com_contact.category.43', 'F', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(69, 62, 44, 45, 5, 'com_contact.category.44', 'G', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(70, 62, 46, 47, 5, 'com_contact.category.45', 'H', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(71, 62, 48, 49, 5, 'com_contact.category.46', 'I', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(72, 62, 50, 51, 5, 'com_contact.category.47', 'J', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(73, 62, 52, 53, 5, 'com_contact.category.48', 'K', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(74, 62, 54, 55, 5, 'com_contact.category.49', 'L', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(75, 62, 56, 57, 5, 'com_contact.category.50', 'M', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(76, 62, 58, 59, 5, 'com_contact.category.51', 'N', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(77, 62, 60, 61, 5, 'com_contact.category.52', 'O', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(78, 62, 62, 63, 5, 'com_contact.category.53', 'P', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(79, 62, 64, 65, 5, 'com_contact.category.54', 'Q', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(80, 62, 66, 67, 5, 'com_contact.category.55', 'R', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(81, 62, 68, 69, 5, 'com_contact.category.56', 'S', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(82, 62, 70, 71, 5, 'com_contact.category.57', 'T', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(83, 62, 72, 73, 5, 'com_contact.category.58', 'U', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(84, 62, 74, 75, 5, 'com_contact.category.59', 'V', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(85, 62, 76, 77, 5, 'com_contact.category.60', 'W', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(86, 62, 78, 79, 5, 'com_contact.category.61', 'X', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(87, 62, 80, 81, 5, 'com_contact.category.62', 'Y', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(88, 62, 82, 83, 5, 'com_contact.category.63', 'Z', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(89, 46, 139, 140, 4, 'com_content.article.1', 'Administrator Components', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(100, 44, 114, 115, 3, 'com_content.category.71', 'Milky Way', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(104, 44, 116, 117, 3, 'com_content.article.8', 'Beginners', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(105, 46, 141, 142, 4, 'com_content.article.9', 'Contact', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(106, 46, 143, 144, 4, 'com_content.article.10', 'Content', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(119, 44, 118, 119, 3, 'com_content.article.21', 'Getting Help', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(120, 44, 120, 121, 3, 'com_content.article.22', 'Getting Started', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(122, 44, 122, 123, 3, 'com_content.article.24', 'Joomla!', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(130, 44, 124, 125, 3, 'com_content.article.32', 'Parameters', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(133, 44, 126, 127, 3, 'com_content.article.35', 'Professionals', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(136, 44, 128, 129, 3, 'com_content.article.38', 'Sample Sites', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(137, 46, 145, 146, 4, 'com_content.article.39', 'Search', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(145, 44, 130, 131, 3, 'com_content.article.47', 'The Joomla! Community', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(146, 44, 132, 133, 3, 'com_content.article.48', 'The Joomla! Project', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(148, 44, 134, 135, 3, 'com_content.article.50', 'Upgraders', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(150, 46, 147, 148, 4, 'com_content.article.52', 'Users', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(151, 44, 136, 137, 3, 'com_content.article.53', 'Using Joomla!', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(158, 46, 149, 150, 4, 'com_content.article.60', 'News Feeds', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(168, 34, 106, 107, 3, 'com_content.article.67', 'What\'s New in 1.5?', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(169, 24, 287, 288, 2, 'com_users.category.77', 'Uncategorised', ''),
(173, 1, 354, 355, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{\"core.admin\":[],\"core.manage\":[],\"core.delete\":[],\"core.edit.state\":[]}'),
(175, 1, 356, 357, 1, 'com_tags', 'com_tags', '{\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1},\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(176, 1, 358, 359, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(177, 1, 360, 361, 1, 'com_ajax', 'com_ajax', '{}'),
(178, 1, 362, 363, 1, 'com_postinstall', 'com_postinstall', '{}'),
(179, 8, 153, 176, 2, 'com_content.category.79', 'Blog', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(180, 1, 364, 365, 1, 'com_sppagebuilder', 'sppagebuilder', '{\"core.manage\":{\"2\":1}}'),
(181, 18, 199, 200, 2, 'com_modules.module.91', 'SP Page Builder', ''),
(182, 18, 201, 202, 2, 'com_modules.module.92', 'SP Page Builder Admin Menu', ''),
(183, 1, 366, 367, 1, 'com_spsimpleportfolio', 'spsimpleportfolio', '{}'),
(185, 18, 203, 204, 2, 'com_modules.module.17', 'Breadcrumbs', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(186, 18, 205, 206, 2, 'com_modules.module.35', 'Search', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(187, 18, 207, 208, 2, 'com_modules.module.94', 'About Helix', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(188, 18, 209, 210, 2, 'com_modules.module.95', 'Latest News', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(189, 18, 211, 212, 2, 'com_modules.module.96', 'Our Partners', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(190, 18, 213, 214, 2, 'com_modules.module.97', 'Quick Link', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(191, 179, 154, 155, 3, 'com_content.category.80', 'News', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(192, 179, 156, 157, 3, 'com_content.category.81', 'Tutorial', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(193, 179, 158, 159, 3, 'com_content.category.82', 'Review', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(194, 179, 160, 161, 3, 'com_content.category.83', 'Updates', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(195, 179, 162, 163, 3, 'com_content.article.71', 'Doner spare ribs pastrami shank', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(196, 179, 164, 165, 3, 'com_content.article.72', 'Jerky shank chicken boudin', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(197, 179, 166, 167, 3, 'com_content.article.73', 'Pellentesque Habitant Morbi Tristique', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(198, 179, 168, 169, 3, 'com_content.article.74', 'Meatball kevin beef ribs shoulder', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(199, 179, 170, 171, 3, 'com_content.article.75', '5 Effective Email Unsubscribe Pages', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(200, 179, 172, 173, 3, 'com_content.article.76', 'Who Actually Clicks on Banner Ads?', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(201, 179, 174, 175, 3, 'com_content.article.77', 'See the new Miss Universe get her crown', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(202, 18, 215, 216, 2, 'com_modules.module.98', 'Portfolio Module', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(203, 18, 217, 218, 2, 'com_modules.module.99', 'Latest News', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(204, 18, 219, 220, 2, 'com_modules.module.100', 'Search', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(205, 18, 221, 222, 2, 'com_modules.module.101', 'Information', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(206, 18, 223, 224, 2, 'com_modules.module.102', 'Search', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(207, 18, 225, 226, 2, 'com_modules.module.103', 'Latest News', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(208, 18, 227, 228, 2, 'com_modules.module.104', 'Information', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(209, 18, 229, 230, 2, 'com_modules.module.105', 'Off Canvas Menu', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(211, 18, 231, 232, 2, 'com_modules.module.107', 'Portfolio Module -  Portfolio Home', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(212, 18, 233, 234, 2, 'com_modules.module.108', 'Recent Documents', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(215, 1, 368, 395, 1, 'com_docman', 'com_docman', '{\"core.admin\":[],\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"com_docman.download\":{\"1\":1}}'),
(216, 18, 235, 236, 2, 'com_modules.module.109', 'Folders', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(220, 18, 237, 238, 2, 'com_modules.module.110', 'Komento Activities', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(221, 18, 239, 240, 2, 'com_modules.module.111', 'Komento Comments', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(222, 1, 396, 397, 1, 'com_komento', 'com_komento', '{}'),
(261, 18, 241, 242, 2, 'com_modules.module.112', 'Menu', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(262, 18, 243, 244, 2, 'com_modules.module.113', ' Help', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(263, 18, 245, 246, 2, 'com_modules.module.114', 'Footer 1', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(264, 18, 247, 248, 2, 'com_modules.module.115', 'Footer 2', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(265, 18, 249, 250, 2, 'com_modules.module.116', 'mod_user_section', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(266, 18, 251, 252, 2, 'com_modules.module.117', 'Side Logo', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(267, 1, 398, 425, 1, 'com_tmea_countries', 'com_tmea_countries', '{\"core.manage\":{\"2\":1}}'),
(268, 267, 399, 400, 2, 'com_tmea_countries.country.1', 'Kenya', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(269, 267, 401, 402, 2, 'com_tmea_countries.contact.1', 'Burundi', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(270, 16, 193, 194, 2, 'com_menus.menu.10', 'Left Nav', '{}'),
(271, 1, 426, 457, 1, 'com_tmea_programs', 'com_tmea_programs', '{\"core.edit.state\":{\"1\":0,\"28\":1},\"core.edit.own\":{\"1\":0,\"28\":1},\"core.edit\":{\"1\":0,\"28\":1},\"core.delete\":{\"1\":0},\"core.create\":{\"1\":0,\"28\":1},\"core.manage\":{\"1\":0,\"28\":1,\"40\":1,\"2\":1,\"39\":1,\"31\":1,\"32\":1},\"core.admin\":{\"1\":0}}'),
(272, 271, 427, 456, 2, 'com_tmea_programs.category.84', 'General', '{}'),
(273, 272, 428, 429, 3, 'com_tmea_programs.program.1', 'Kenya', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(274, 267, 403, 404, 2, 'com_tmea_countries.country.2', 'Uganda', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(275, 267, 405, 406, 2, 'com_tmea_countries.country.3', 'Tanzania', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(276, 267, 407, 408, 2, 'com_tmea_countries.country.4', 'Burundi', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(277, 267, 409, 410, 2, 'com_tmea_countries.contact.2', 'Naivasha', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(278, 267, 411, 412, 2, 'com_tmea_countries.contact.3', 'Kampala', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(279, 272, 430, 431, 3, 'com_tmea_programs.program.2', 'Uganda', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(280, 272, 432, 433, 3, 'com_tmea_programs.program.3', 'Burundi', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(281, 272, 434, 435, 3, 'com_tmea_programs.program.4', 'Tanzania', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(282, 272, 436, 437, 3, 'com_tmea_programs.program.5', 'Rwanda-01', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(283, 267, 413, 414, 2, 'com_tmea_countries.country.5', 'Rwanda', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(284, 267, 415, 416, 2, 'com_tmea_countries.country.6', 'South Sudan', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(285, 272, 438, 439, 3, 'com_tmea_programs.program.6', 'Rwanda', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(286, 272, 440, 441, 3, 'com_tmea_programs.program.7', 'South Sudan', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(287, 272, 442, 443, 3, 'com_tmea_programs.program.8', 'Arusha', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(288, 272, 444, 445, 3, 'com_tmea_programs.program.9', 'Biz Competitiveness', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(289, 272, 446, 447, 3, 'com_tmea_programs.program.10', 'TECC', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(290, 272, 448, 449, 3, 'com_tmea_programs.program.11', 'Finance', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(291, 272, 450, 451, 3, 'com_tmea_programs.program.12', 'START (Research & Knowledge)', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(292, 267, 417, 418, 2, 'com_tmea_countries.country.7', 'General', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(294, 18, 253, 254, 2, 'com_modules.module.0', 'JEvents Calendar', '{}'),
(295, 1, 458, 463, 1, 'com_jevents', 'JEvents', '{\"core.manage\":{\"8\":1},\"core.create\":{\"8\":1},\"core.edit.own\":{\"8\":1},\"core.edit\":{\"8\":1},\"core.edit.state\":{\"8\":1},\"core.deleteall\":{\"8\":1},\"core.admin\":{\"8\":1}}'),
(296, 18, 255, 256, 2, 'com_modules.module.119', 'JEvents Calendar', '{}'),
(297, 18, 257, 258, 2, 'com_modules.module.120', 'JEvents Legend', '{}'),
(298, 18, 259, 260, 2, 'com_modules.module.121', 'JEvents Latest Events', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(299, 18, 261, 262, 2, 'com_modules.module.122', 'JEvents Filter', '{}'),
(300, 18, 263, 264, 2, 'com_modules.module.123', 'JEvents CustomModule', '{}'),
(301, 18, 265, 266, 2, 'com_modules.module.124', 'JEvents View Switcher', '{}'),
(302, 295, 459, 462, 2, 'com_jevents.category.0', 'Meetings', '{}'),
(303, 302, 460, 461, 3, 'com_jevents.category.86', 'Meetings', '{}'),
(304, 267, 419, 420, 2, 'com_tmea_countries.contact.0', 'Rwanda', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(315, 215, 369, 372, 2, 'com_docman.category.13', 'Board', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"com_docman.download\":[]}'),
(316, 215, 373, 376, 2, 'com_docman.category.14', 'Council', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"com_docman.download\":[]}'),
(317, 215, 377, 378, 2, 'com_docman.category.15', 'Financial Reports', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"com_docman.download\":[]}'),
(322, 215, 379, 380, 2, 'com_docman.category.17', 'Uganda', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"com_docman.download\":[]}'),
(323, 215, 381, 382, 2, 'com_docman.category.18', 'Tanzania', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"com_docman.download\":[]}'),
(324, 215, 383, 386, 2, 'com_docman.category.19', 'Burundi', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"com_docman.download\":[]}'),
(325, 215, 387, 388, 2, 'com_docman.category.20', 'Rwanda', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"com_docman.download\":[]}'),
(326, 215, 389, 390, 2, 'com_docman.category.21', 'South Sudan', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"com_docman.download\":[]}'),
(327, 315, 370, 371, 3, 'com_docman.document.43', 'TRADEMARK EASTAFRICA TEST DOCUMENT (Board)', '{}'),
(328, 324, 384, 385, 3, 'com_docman.document.44', 'Burundi', '{}'),
(331, 316, 374, 375, 3, 'com_docman.document.47', 'TRADEMARK EASTAFRICA TEST DOCUMENT (Council)', '{}'),
(332, 267, 421, 422, 2, 'com_tmea_countries.contact.4', 'Rwanda', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(333, 215, 391, 394, 2, 'com_docman.category.22', 'Kenya', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"com_docman.download\":[]}'),
(334, 18, 267, 268, 2, 'com_modules.module.86', 'Joomla Version', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"module.edit.frontend\":[]}'),
(335, 272, 452, 453, 3, 'com_tmea_programs.program.13', 'TMEA EAC Partnership Programme', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(336, 1, 464, 465, 1, 'not', 'false', '{\"core.manage\":{\"8\":1},\"core.create\":{\"8\":1},\"core.edit.own\":{\"8\":1},\"core.edit\":{\"8\":1},\"core.edit.state\":{\"8\":1},\"core.deleteall\":{\"8\":1}}'),
(337, 333, 392, 393, 3, 'com_docman.document.48', 'Projectplan sample2', '{}'),
(338, 267, 423, 424, 2, 'com_tmea_countries.contact.5', 'arusha_tepp', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(339, 272, 454, 455, 3, 'com_tmea_programs.program.14', 'New program', '{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_associations`
--

DROP TABLE IF EXISTS `tol2j_associations`;
CREATE TABLE `tol2j_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_banners`
--

DROP TABLE IF EXISTS `tol2j_banners`;
CREATE TABLE `tol2j_banners` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `custombannercode` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sticky` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_banners`
--

INSERT INTO `tol2j_banners` (`id`, `cid`, `type`, `name`, `alias`, `imptotal`, `impmade`, `clicks`, `clickurl`, `state`, `catid`, `description`, `custombannercode`, `sticky`, `ordering`, `metakey`, `params`, `own_prefix`, `metakey_prefix`, `purchase_type`, `track_clicks`, `track_impressions`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `reset`, `created`, `language`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `version`) VALUES
(2, 3, 0, 'Shop 1', 'shop-1', 0, 0, 0, 'http://shop.joomla.org/amazoncom-bookstores.html', 1, 15, 'Get books about Joomla! at the Joomla! Book Shop.', '', 0, 1, '', '{\"imageurl\":\"images\\/banners\\/white.png\",\"width\":\"\",\"height\":\"\",\"alt\":\"Joomla! Books\"}', 0, '', -1, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2011-01-01 00:00:01', 'en-GB', 42, 'Joomla', '0000-00-00 00:00:00', 0, 1),
(3, 2, 0, 'Shop 2', 'shop-2', 0, 0, 0, 'http://shop.joomla.org', 1, 15, 'T Shirts, caps and more from the Joomla! Shop.', '', 0, 2, '', '{\"imageurl\":\"images\\/banners\\/white.png\",\"width\":\"\",\"height\":\"\",\"alt\":\"Joomla! Shop\"}', 0, '', -1, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2011-01-01 00:00:01', 'en-GB', 42, 'Joomla', '0000-00-00 00:00:00', 0, 1),
(4, 1, 0, 'Support Joomla!', 'support-joomla', 0, 0, 0, 'http://contribute.joomla.org', 1, 15, 'Your contributions of time, talent and money make Joomla possible.', '', 0, 3, '', '{\"imageurl\":\"images\\/banners\\/white.png\",\"width\":\"\",\"height\":\"\",\"alt\":\"\"}', 0, '', -1, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2011-01-01 00:00:01', 'en-GB', 42, 'Joomla', '0000-00-00 00:00:00', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_banner_clients`
--

DROP TABLE IF EXISTS `tol2j_banner_clients`;
CREATE TABLE `tol2j_banner_clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extrainfo` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_banner_clients`
--

INSERT INTO `tol2j_banner_clients` (`id`, `name`, `contact`, `email`, `extrainfo`, `state`, `checked_out`, `checked_out_time`, `metakey`, `own_prefix`, `metakey_prefix`, `purchase_type`, `track_clicks`, `track_impressions`) VALUES
(1, 'Joomla!', 'Administrator', 'email@email.com', '', 1, 0, '0000-00-00 00:00:00', '', 0, '', -1, -1, -1),
(2, 'Shop', 'Example', 'example@example.com', '', 1, 0, '0000-00-00 00:00:00', '', 0, '', -1, 0, 0),
(3, 'Bookstore', 'Bookstore Example', 'example@example.com', '', 1, 0, '0000-00-00 00:00:00', '', 0, '', -1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_banner_tracks`
--

DROP TABLE IF EXISTS `tol2j_banner_tracks`;
CREATE TABLE `tol2j_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) UNSIGNED NOT NULL,
  `banner_id` int(10) UNSIGNED NOT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_categories`
--

DROP TABLE IF EXISTS `tol2j_categories`;
CREATE TABLE `tol2j_categories` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_categories`
--

INSERT INTO `tol2j_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 107, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(9, 34, 1, 111, 112, 3, 'meetings/root/uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(10, 35, 1, 183, 184, 3, 'meetings/root/uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\",\"foobar\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(11, 36, 1, 179, 180, 3, 'meetings/root/uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(12, 37, 1, 115, 116, 3, 'meetings/root/uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(15, 40, 1, 181, 182, 3, 'meetings/root/sample-data-banners', 'com_banners', 'Sample Data-Banners', 'sample-data-banners', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\",\"foobar\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(16, 41, 1, 117, 178, 3, 'meetings/root/sample-data-contact', 'com_contact', 'Sample Data-Contact', 'sample-data-contact', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(17, 42, 1, 113, 114, 3, 'meetings/root/sample-data-newsfeeds', 'com_newsfeeds', 'Sample Data-Newsfeeds', 'sample-data-newsfeeds', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(19, 44, 1, 107, 110, 3, 'meetings/root/joomla', 'com_content', 'Joomla!', 'joomla', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 864, '2015-02-02 06:05:15', 0, '*', 1),
(21, 46, 19, 108, 109, 4, 'meetings/root/joomla/components', 'com_content', 'Components', 'components', '', '<p>Components are larger extensions that produce the major content for your site. Each component has one or more \"views\" that control how content is displayed. In the Joomla administrator there are additional extensions such as Menus, Redirection, and the extension managers.</p>', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 864, '2015-02-02 06:05:23', 0, '*', 1),
(34, 59, 16, 118, 119, 4, 'meetings/root/sample-data-contact/park-site', 'com_contact', 'Park Site', 'park-site', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, 'en-GB', 1),
(35, 60, 16, 120, 177, 4, 'meetings/root/sample-data-contact/shop-site', 'com_contact', 'Shop Site', 'shop-site', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(36, 61, 35, 121, 122, 5, 'meetings/root/sample-data-contact/shop-site/staff', 'com_contact', 'Staff', 'staff', '', '<p>Please feel free to contact our staff at any time should you need assistance.</p>', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(37, 62, 35, 123, 176, 5, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia', 'com_contact', 'Fruit Encyclopedia', 'fruit-encyclopedia', '', '<p> </p><p>Our directory of information about different kinds of fruit.</p><p>We love fruit and want the world to know more about all of its many varieties.</p><p>Although it is small now, we work on it whenever we have a chance.</p><p>All of the images can be found in <a href=\"http://commons.wikimedia.org/wiki/Main_Page\">Wikimedia Commons</a>.</p><p><img src=\"images/sampledata/fruitshop/apple.jpg\" border=\"0\" alt=\"Apples\" title=\"Apples\" /></p><p><em>This encyclopedia is implemented using the contact component, each fruit a separate contact and a category for each letter. A CSS style is used to create the horizontal layout of the alphabet headings. </em></p><p><em>If you wanted to, you could allow some users (such as your growers) to have access to just this category in the contact component and let them help you to create new content for the encyclopedia.</em></p><p> </p>', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(38, 63, 37, 124, 125, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/a', 'com_contact', 'A', 'a', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(39, 64, 37, 126, 127, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/b', 'com_contact', 'B', 'b', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(40, 65, 37, 128, 129, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/c', 'com_contact', 'C', 'c', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(41, 66, 37, 130, 131, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/d', 'com_contact', 'D', 'd', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(42, 67, 37, 132, 133, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/e', 'com_contact', 'E', 'e', '', '', 0, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(43, 68, 37, 134, 135, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/f', 'com_contact', 'F', 'f', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(44, 69, 37, 136, 137, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/g', 'com_contact', 'G', 'g', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(45, 70, 37, 138, 139, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/h', 'com_contact', 'H', 'h', '', '', 0, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(46, 71, 37, 140, 141, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/i', 'com_contact', 'I', 'i', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(47, 72, 37, 142, 143, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/j', 'com_contact', 'J', 'j', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(48, 73, 37, 144, 145, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/k', 'com_contact', 'K', 'k', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(49, 74, 37, 146, 147, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/l', 'com_contact', 'L', 'l', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(50, 75, 37, 148, 149, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/m', 'com_contact', 'M', 'm', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(51, 76, 37, 150, 151, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/n', 'com_contact', 'N', 'n', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(52, 77, 37, 152, 153, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/o', 'com_contact', 'O', 'o', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(53, 78, 37, 154, 155, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/p', 'com_contact', 'P', 'p', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(54, 79, 37, 156, 157, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/q', 'com_contact', 'Q', 'q', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(55, 80, 37, 158, 159, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/r', 'com_contact', 'R', 'r', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(56, 81, 37, 160, 161, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/s', 'com_contact', 'S', 's', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(57, 82, 37, 162, 163, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/t', 'com_contact', 'T', 't', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(58, 83, 37, 164, 165, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/u', 'com_contact', 'U', 'u', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(59, 84, 37, 166, 167, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/v', 'com_contact', 'V', 'v', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(60, 85, 37, 168, 169, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/w', 'com_contact', 'W', 'w', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(61, 86, 37, 170, 171, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/x', 'com_contact', 'X', 'x', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(62, 87, 37, 172, 173, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/y', 'com_contact', 'Y', 'y', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(63, 88, 37, 174, 175, 6, 'meetings/root/sample-data-contact/shop-site/fruit-encyclopedia/z', 'com_contact', 'Z', 'z', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(77, 169, 1, 185, 186, 3, 'meetings/root/uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 234, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(79, 179, 1, 187, 196, 3, 'meetings/root/blog', 'com_content', 'Blog', 'blog', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 234, '2015-02-02 06:12:46', 0, '0000-00-00 00:00:00', 0, '*', 1),
(80, 191, 79, 188, 189, 4, 'meetings/root/blog/news', 'com_content', 'News', 'news', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 234, '2015-02-02 18:42:14', 864, '2015-02-02 18:42:30', 0, '*', 1),
(81, 192, 79, 190, 191, 4, 'meetings/root/blog/tutorial', 'com_content', 'Tutorial', 'tutorial', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 234, '2015-02-02 18:42:46', 0, '0000-00-00 00:00:00', 0, '*', 1),
(82, 193, 79, 192, 193, 4, 'meetings/root/blog/review', 'com_content', 'Review', 'review', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 234, '2015-02-02 18:42:56', 0, '0000-00-00 00:00:00', 0, '*', 1),
(83, 194, 79, 194, 195, 4, 'meetings/root/blog/updates', 'com_content', 'Updates', 'updates', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 234, '2015-02-02 18:43:15', 0, '0000-00-00 00:00:00', 0, '*', 1),
(84, 272, 1, 197, 198, 3, 'meetings/root/general', 'com_tmea_programs', 'General', 'general', '', '<p>All Programs</p>', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 234, '2017-07-20 05:00:18', 234, '2017-07-26 08:57:19', 0, '*', 1),
(85, 302, 1, 199, 202, 3, 'meetings/root/', 'com_jevents', 'DEFAULT', '', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"catcolor\":\"#CCCCFF\",\"admin\":0,\"overlaps\":0,\"image\":\"\"}', '', '', '', 234, '2017-09-04 23:02:05', 0, '2017-09-04 23:02:05', 0, '*', 1),
(86, 303, 85, 200, 201, 4, 'meetings', 'com_jevents', 'Meetings', 'meetings', '', '<p>All Meetings</p>', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\",\"catcolour\":\"\",\"overlaps\":\"0\",\"admin\":\"234\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 234, '2017-09-04 23:07:31', 234, '2017-09-04 23:18:19', 0, '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_contact_details`
--

DROP TABLE IF EXISTS `tol2j_contact_details`;
CREATE TABLE `tol2j_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` mediumtext COLLATE utf8mb4_unicode_ci,
  `suburb` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `misc` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_con` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `webpage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sortname2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sortname3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_contact_details`
--

INSERT INTO `tol2j_contact_details` (`id`, `name`, `alias`, `con_position`, `address`, `suburb`, `state`, `country`, `postcode`, `telephone`, `fax`, `misc`, `image`, `email_to`, `default_con`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`, `user_id`, `catid`, `access`, `mobile`, `webpage`, `sortname1`, `sortname2`, `sortname3`, `language`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `featured`, `xreference`, `publish_up`, `publish_down`, `version`, `hits`) VALUES
(1, 'Contact Name Here', 'name', 'Position', 'Street Address', 'Suburb', 'State', 'Country', 'Zip Code', 'Telephone', 'Fax', '<p>Information about or by the contact.</p>', 'images/powered_by.png', 'email@example.com', 1, 1, 0, '0000-00-00 00:00:00', 1, '{\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"presentation_style\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"1\",\"linka_name\":\"Twitter\",\"linka\":\"http:\\/\\/twitter.com\\/joomla\",\"linkb_name\":\"YouTube\",\"linkb\":\"http:\\/\\/www.youtube.com\\/user\\/joomla\",\"linkc_name\":\"Facebook\",\"linkc\":\"http:\\/\\/www.facebook.com\\/joomla\",\"linkd_name\":\"FriendFeed\",\"linkd\":\"http:\\/\\/friendfeed.com\\/joomla\",\"linke_name\":\"Scribed\",\"linke\":\"http:\\/\\/www.scribd.com\\/people\\/view\\/504592-joomla\",\"contact_layout\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\"}', 0, 16, 1, '', '', 'last', 'first', 'middle', 'en-GB', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 11),
(2, 'Webmaster', 'webmaster', '', '', '', '', '', '', '', '', '', '', 'webmaster@example.com', 0, 1, 0, '0000-00-00 00:00:00', 1, '{\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"presentation_style\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"\",\"linka_name\":\"\",\"linka\":\"\",\"linkb_name\":\"\",\"linkb\":\"\",\"linkc_name\":\"\",\"linkc\":\"\",\"linkd_name\":\"\",\"linkd\":\"\",\"linke_name\":\"\",\"linke\":\"\",\"show_email_form\":\"1\",\"show_email_copy\":\"1\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"\",\"redirect\":\"\"}', 0, 34, 1, '', '', '', '', '', 'en-GB', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 2),
(3, 'Owner', 'owner', '', '', '', '', '', '', '', '', '<p>I\'m the owner of this store.</p>', '', '', 0, 1, 0, '0000-00-00 00:00:00', 2, '{\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"presentation_style\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"\",\"linka_name\":\"\",\"linka\":\"\",\"linkb_name\":\"\",\"linkb\":\"\",\"linkc_name\":\"\",\"linkc\":\"\",\"linkd_name\":\"\",\"linkd\":\"\",\"linke_name\":\"\",\"linke\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\"}', 0, 36, 1, '', '', '', '', '', '*', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(4, 'Buyer', 'buyer', '', '', '', '', '', '', '', '', '<p>I am in charge of buying fruit. If you sell good fruit, contact me.</p>', '', '', 0, 1, 0, '0000-00-00 00:00:00', 1, '{\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"presentation_style\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"0\",\"linka_name\":\"\",\"linka\":\"\",\"linkb_name\":\"\",\"linkb\":\"\",\"linkc_name\":\"\",\"linkc\":\"\",\"linkd_name\":\"\",\"linkd\":\"\",\"linke_name\":\"\",\"linke\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\"}', 0, 36, 1, '', '', '', '', '', '*', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 2),
(5, 'Bananas', 'bananas', 'Scientific Name: Musa', 'Image Credit: Enzik\r\nRights: Creative Commons Share Alike Unported 3.0\r\nSource: http://commons.wikimedia.org/wiki/File:Bananas_-_Morocco.jpg', '', 'Type: Herbaceous', 'Large Producers: India, China, Brasil', '', '', '', '<p>Bananas are a great source of potassium.</p>\r\n<p> </p>', 'images/sampledata/fruitshop/bananas_2.jpg', '', 0, 1, 0, '0000-00-00 00:00:00', 1, '{\"show_contact_category\":\"show_with_link\",\"show_contact_list\":\"\",\"presentation_style\":\"plain\",\"show_name\":\"\",\"show_position\":\"1\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"1\",\"show_postcode\":\"\",\"show_country\":\"1\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"1\",\"linka_name\":\"Wikipedia: Banana English\",\"linka\":\"http:\\/\\/en.wikipedia.org\\/wiki\\/Banana\",\"linkb_name\":\"Wikipedia:  \\u0939\\u093f\\u0928\\u094d\\u0926\\u0940 \\u0915\\u0947\\u0932\\u093e\",\"linkb\":\"http:\\/\\/hi.wikipedia.org\\/wiki\\/%E0%A4%95%E0%A5%87%E0%A4%B2%E0%A4%BE\",\"linkc_name\":\"Wikipedia:Banana Portugu\\u00eas\",\"linkc\":\"http:\\/\\/pt.wikipedia.org\\/wiki\\/Banana\",\"linkd_name\":\"Wikipedia: \\u0411\\u0430\\u043d\\u0430\\u043d  \\u0420\\u0443\\u0441\\u0441\\u043a\\u0438\\u0439\",\"linkd\":\"http:\\/\\/ru.wikipedia.org\\/\\u0411\\u0430\\u043d\\u0430\\u043d\",\"linke_name\":\"\",\"linke\":\"\",\"contact_layout\":\"beez5:encyclopedia\"}', 0, 39, 1, '', '', '', '', '', '*', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(6, 'Apples', 'apples', 'Scientific Name: Malus domestica', 'Image Credit: Fievet\r\nRights: Public Domain\r\nSource: http://commons.wikimedia.org/wiki/File:Pommes_vertes.JPG', '', 'Family: Rosaceae', 'Large: Producers: China, United States', '', '', '', '<p>Apples are a versatile fruit, used for eating, cooking, and preserving.</p>\r\n<p>There are more that 7500 different kinds of apples grown around the world.</p>', 'images/sampledata/fruitshop/apple.jpg', '', 0, 1, 0, '0000-00-00 00:00:00', 1, '{\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"presentation_style\":\"plain\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"1\",\"linka_name\":\"Wikipedia: Apples English\",\"linka\":\"http:\\/\\/en.wikipedia.org\\/wiki\\/Apple\",\"linkb_name\":\"Wikipedia: Manzana Espa\\u00f1ol \",\"linkb\":\"http:\\/\\/es.wikipedia.org\\/wiki\\/Manzana\",\"linkc_name\":\"Wikipedia: \\u82f9\\u679c \\u4e2d\\u6587\",\"linkc\":\"http:\\/\\/zh.wikipedia.org\\/zh\\/\\u82f9\\u679c\",\"linkd_name\":\"Wikipedia: Tofaa Kiswahili\",\"linkd\":\"http:\\/\\/sw.wikipedia.org\\/wiki\\/Tofaa\",\"linke_name\":\"\",\"linke\":\"\",\"contact_layout\":\"beez5:encyclopedia\"}', 0, 38, 1, '', '', '', '', '', '*', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1),
(7, 'Tamarind', 'tamarind', 'Scientific Name: Tamarindus indica', 'Image Credit: Franz Eugen Köhler, Köhler\'s Medizinal-Pflanzen \r\nRights: Public Domain\r\nSource:http://commons.wikimedia.org/wiki/File:Koeh-134.jpg', '', 'Family: Fabaceae', 'Large Producers: India, United States', '', '', '', '<p>Tamarinds are a versatile fruit used around the world. In its young form it is used in hot sauces; ripened it is the basis for many refreshing drinks.</p>\r\n<p> </p>', 'images/sampledata/fruitshop/tamarind.jpg', '', 0, 1, 0, '0000-00-00 00:00:00', 1, '{\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"presentation_style\":\"plain\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"1\",\"linka_name\":\"Wikipedia: Tamarind English\",\"linka\":\"http:\\/\\/en.wikipedia.org\\/wiki\\/Tamarind\",\"linkb_name\":\"Wikipedia: \\u09a4\\u09c7\\u0981\\u09a4\\u09c1\\u09b2  \\u09ac\\u09be\\u0982\\u09b2\\u09be  \",\"linkb\":\"http:\\/\\/bn.wikipedia.org\\/wiki\\/\\u09a4\\u09c7\\u0981\\u09a4\\u09c1\\u09b2 \",\"linkc_name\":\"Wikipedia: Tamarinier Fran\\u00e7ais\",\"linkc\":\"http:\\/\\/fr.wikipedia.org\\/wiki\\/Tamarinier\",\"linkd_name\":\"Wikipedia:Tamaline lea faka-Tonga\",\"linkd\":\"http:\\/\\/to.wikipedia.org\\/wiki\\/Tamaline\",\"linke_name\":\"\",\"linke\":\"\",\"contact_layout\":\"beez5:encyclopedia\"}', 0, 57, 1, '', '', '', '', '', '*', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(8, 'Shop Address', 'shop-address', '', '', 'Our City', 'Our Province', 'Our Country', '', '555-555-5555', '', '<p>Here are directions for how to get to our shop.</p>', '', '', 0, 1, 0, '0000-00-00 00:00:00', 1, '{\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"presentation_style\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"\",\"linka_name\":\"\",\"linka\":\"\",\"linkb_name\":\"\",\"linkb\":\"\",\"linkc_name\":\"\",\"linkc\":\"\",\"linkd_name\":\"\",\"linkd\":\"\",\"linke_name\":\"\",\"linke\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\"}', 0, 35, 1, '', '', '', '', '', '*', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_content`
--

DROP TABLE IF EXISTS `tol2j_content`;
CREATE TABLE `tol2j_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `introtext` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fulltext` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribs` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_content`
--

INSERT INTO `tol2j_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 89, 'Administrator Components', 'administrator-components', '<p>All components are also used in the administrator area of your website. In addition to the ones listed here, there are components in the administrator that do not have direct front end displays, but do help shape your site. The most important ones for most users are</p>\r\n<ul>\r\n<li>Media Manager</li>\r\n<li>Extensions Manager</li>\r\n<li>Menu Manager</li>\r\n<li>Global Configuration</li>\r\n<li>Banners</li>\r\n<li>Redirect</li>\r\n</ul>\r\n<hr title=\"Media Manager\" alt=\"Media Manager\" class=\"system-pagebreak\" style=\"color: gray; border: 1px dashed gray;\" />\r\n<p> </p>\r\n<h3>Media Manager</h3>\r\n<p>The media manager component lets you upload and insert images into content throughout your site. Optionally, you can enable the flash uploader which will allow you to to upload multiple images. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Content_Media_Manager\">Help</a></p>\r\n<hr title=\"Extensions Manager\" alt=\"Extensions Manager\" class=\"system-pagebreak\" style=\"color: gray; border: 1px dashed gray;\" />\r\n<h3>Extensions Manager</h3>\r\n<p>The extensions manager lets you install, update, uninstall and manage all of your extensions. The extensions manager has been extensively redesigned, although the core install and uninstall functionality remains the same as in Joomla! 1.5. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Extensions_Extension_Manager_Install\">Help</a></p>\r\n<hr title=\"Menu Manager\" alt=\"Menu Manager\" class=\"system-pagebreak\" style=\"color: gray; border: 1px dashed gray;\" />\r\n<h3>Menu Manager</h3>\r\n<p>The menu manager lets you create the menus you see displayed on your site. It also allows you to assign modules and template styles to specific menu links. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Menus_Menu_Manager\">Help</a></p>\r\n<hr title=\"Global Configuration\" alt=\"Global Configuration\" class=\"system-pagebreak\" style=\"color: gray; border: 1px dashed gray;\" />\r\n<h3>Global Configuration</h3>\r\n<p>The global configuration is where the site administrator configures things such as whether search engine friendly urls are enabled, the site meta data (descriptive text used by search engines and indexers) and other functions. For many beginning users simply leaving the settings on default is a good way to begin, although when your site is ready for the public you will want to change the meta data to match its content. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Site_Global_Configuration\">Help</a></p>\r\n<hr title=\"Banners\" alt=\"Banners\" class=\"system-pagebreak\" style=\"color: gray; border: 1px dashed gray;\" />\r\n<h3>Banners</h3>\r\n<p>The banners component provides a simple way to display a rotating image in a module and, if you wish to have advertising, a way to track the number of times an image is viewed and clicked. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Components_Banners_Banners_Edit\">Help</a></p>\r\n<hr title=\"Redirect\" class=\"system-pagebreak\" />\r\n<h3><br />Redirect</h3>\r\n<p>The redirect component is used to manage broken links that produce Page Not Found (404) errors. If enabled it will allow you to redirect broken links to specific pages. It can also be used to manage migration related URL changes. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Components_Redirect_Manager\">Help</a></p>', '', 1, 21, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":\"\",\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":\"\",\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":\"\",\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 7, '', '', 1, 7, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(8, 104, 'Beginners', 'beginners', '<p>If this is your first Joomla! site or your first web site, you have come to the right place. Joomla will help you get your website up and running quickly and easily.</p>\r\n<p>Start off using your site by logging in using the administrator account you created when you installed Joomla.</p>\r\n', '\r\n<p>Explore the articles and other resources right here on your site data to learn more about how Joomla works. (When you\'re done reading, you can delete or archive all of this.) You will also probably want to visit the Beginners\' Areas of the <a href=\"http://docs.joomla.org/Beginners\">Joomla documentation</a> and <a href=\"http://forum.joomla.org\">support forums</a>.</p>\r\n<p>You\'ll also want to sign up for the Joomla Security Mailing list and the Announcements mailing list. For inspiration visit the <a href=\"http://community.joomla.org/showcase/\">Joomla! Site Showcase</a> to see an amazing array of ways people use Joomla to tell their stories on the web.</p>\r\n<p>The basic Joomla installation will let you get a great site up and running, but when you are ready for more features the power of Joomla is in the creative ways that developers have extended it to do all kinds of things. Visit the <a href=\"http://extensions.joomla.org/\">Joomla! Extensions Directory</a> to see thousands of extensions that can do almost anything you could want on a website. Can\'t find what you need? You may want to find a Joomla professional in the <a href=\"http://resources.joomla.org/\">Joomla! Resource Directory</a>.</p>\r\n<p>Want to learn more? Consider attending a <a href=\"http://community.joomla.org/events.html\">Joomla! Day</a> or other event or joining a local <a href=\"http://community.joomla.org/user-groups.html\">Joomla! Users Group</a>. Can\'t find one near you? Start one yourself.</p>', -2, 19, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":\"\",\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":\"\",\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":\"\",\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 4, '', '', 1, 3, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 1, '*', ''),
(9, 105, 'Contacts', 'contact', '<p>The contact component provides a way to provide contact forms and information for your site or to create a complex directory that can be used for many different purposes. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Components_Contacts_Contacts\">Help</a></p>', '', -2, 21, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '', '', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\"}', 1, 2, '', '', 1, 1, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(10, 106, 'Content', 'content', '<p>The content component (com_content) is what you use to write articles. It is extremely flexible and has the largest number of built in views. Articles can be created and edited from the front end, making content the easiest component to use to create your site content. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Content_Article_Manager\">Help</a></p>', '', -2, 21, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '', '', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\"}', 1, 1, '', '', 1, 3, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(21, 119, 'Getting Help', 'getting-help', '<p> There are lots of places you can get help with Joomla!. In many places in your site administrator you will see the help icon. Click on this for more information about the options and functions of items on your screen. Other places to get help are:</p>\r\n<ul>\r\n<li><a href=\"http://forum.joomla.org\">Support Forums</a></li>\r\n<li><a href=\"http://docs.joomla.org\">Documentation</a></li>\r\n<li><a href=\"http://resources.joomla.org\">Professionals</a></li>\r\n<li><a href=\"http://shop.joomla.org/amazoncom-bookstores.html\">Books</a></li>\r\n</ul>', '', -2, 19, '2011-01-01 00:00:01', 234, 'Joomla', '2012-09-25 07:39:17', 123, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"0\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 8, '', '', 1, 5, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(22, 120, 'Getting Started', 'getting-started', '<p>It\'s easy to get started creating your website. Knowing some of the basics will help.</p>\r\n<h3>What is a Content Management System?</h3>\r\n<p>A content management system is software that allows you to create and manage webpages easily by separating the creation of your content from the mechanics required to present it on the web.</p>\r\n<p>In this site, the content is stored in a <em>database</em>. The look and feel are created by a <em>template</em>. The Joomla! software brings together the template and the content to create web pages.</p>\r\n<h3>Site and Administrator</h3>\r\n<p>Your site actually has two separate sites. The site (also called the front end) is what visitors to your site will see. The administrator (also called the back end) is only used by people managing your site. You can access the administrator by clicking the \"Site Administrator\" link on the \"This Site\" menu or by adding /administrator to the end of you domain name.</p>\r\n<p>Log in to the administrator using the username and password created during the installation of Joomla.</p>\r\n<h3>Logging in</h3>\r\n<p>To login to the front end of your site use the login form or the login menu link on the \"This Site\" menu. Use the user name and password that were created as part of the installation process. Once logged-in you will be able to create and edit articles.</p>\r\n<p>In managing your site, you will be able to create content that only logged-in users are able to see.</p>\r\n<h3>Creating an article</h3>\r\n<p>Once you are logged-in, a new menu will be visible. To create a new article, click on the \"submit article\" link on that menu.</p>\r\n<p>The new article interface gives you a lot of options, but all you need to do is add a title and put something in the content area. To make it easy to find, set the state to published and put it in the Joomla category.</p>\r\n<div>You can edit an existing article by clicking on the edit icon (this only displays to users who have the right to edit).</div>\r\n<h3>Learn more</h3>\r\n<p>There is much more to learn about how to use Joomla! to create the web site you envision. You can learn much more at the <a href=\"http://docs.joomla.org\">Joomla! documentation site</a> and on the<a href=\"http://forum.joomla.org\"> Joomla! forums</a>.</p>', '', -2, 19, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":\"\",\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":\"\",\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":\"\",\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 9, '', '', 1, 68, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(24, 122, 'Joomla!', 'joomla', '<p>Congratulations! You have a Joomla site! Joomla makes it easy to build a website just the way you want it and keep it simple to update and maintain.</p>\r\n<p>Joomla is a flexible and powerful platform, whether you are building a small site for yourself or a huge site with hundreds of thousands of visitors. Joomla is open source, which means you can make it work just the way you want it to.</p>\r\n<p>The content in this installation of Joomla has been designed to give you an in depth tour of Joomla\'s features.</p>', '', -2, 19, '2011-01-01 00:00:01', 234, 'Joomla', '2012-09-25 12:19:00', 123, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"0\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 3, 2, '', '', 1, 10, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 1, '*', ''),
(32, 130, 'Options', 'options', '<p>As you make your Joomla! site you will control the details of the display using <em>options</em> also referred to as <em>parameter</em><strong>s</strong>. Options control everything from whether the author\'s name is displayed to who can view what to the number of items shown on a list.</p>\r\n<p>Default options for each component are changed using the Options button on the component toolbar.</p>\r\n<p>Options can also be set on an individual item, such as an article or contact and in menu links.</p>\r\n<p>If you are happy with how your site looks, it is fine to leave all of the options set to the defaults that were created when your site was installed. As you become more experienced with Joomla you will use options more.</p>\r\n<p> </p>', '', 1, 19, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":\"\",\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":\"\",\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":\"\",\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 10, '', '', 1, 8, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(35, 133, 'Professionals', 'professionals', '<p>Joomla! 3 continues development of the Joomla Platform and CMS as a powerful and flexible way to bring your vision of the web to reality. With the new administrator interface and adoption of Twitter Bootstrap, the ability to control its look and the management of extensions is now complete.</p>\r\n', '\r\n<p>Working with multiple template styles and overrides for the same views, creating the design you want is easier than it has ever been. Limiting support to PHP 5.3.10 and above makes Joomla lighter and faster than ever. </p>\r\n<p>The separation of the Joomla! Platform project from the Joomla! CMS project makes continuous development of new, powerful APIs and continuous improvement of existing APIs possible while maintaining the stability of the CMS that millions of webmasters and professionals rely upon.</p>', -2, 19, '2011-01-01 00:00:01', 234, 'Joomla', '2012-09-25 07:14:30', 123, 0, '0000-00-00 00:00:00', '2011-01-09 16:41:13', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"0\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 5, '', '', 1, 43, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 1, '*', ''),
(38, 136, 'Sample Sites', 'sample-sites', '<p>Your installation includes sample data, designed to show you some of the options you have for building your website. In addition to information about Joomla! there are two sample \"sites within a site\" designed to help you get started with building your own site.</p>\r\n<p>The first site is a simple site about <a href=\"index.php?Itemid=243\">Australian Parks</a>. It shows how you can quickly and easily build a personal site with just the building blocks that are part of Joomla. It includes a personal blog, weblinks, and a very simple image gallery.</p>\r\n<p>The second site is slightly more complex and represents what you might do if you are building a site for a small business, in this case a <a href=\"index.php/welcome.html\"></a><a href=\"index.php?Itemid=429\">Fruit Shop</a>.</p>\r\n<p>In building either style site, or something completely different, you will probably want to add <a href=\"http://extensions.joomla.org\">extensions</a> and either create or purchase your own template. Many Joomla users start by modifying the <a href=\"http://docs.joomla.org/How_do_you_modify_a_template%3F\">templates</a> that come with the core distribution so that they include special images and other design elements that relate to their site\'s focus.</p>', '', -2, 19, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":\"\",\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":\"\",\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":\"\",\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 11, '', '', 1, 15, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(39, 137, 'Search', 'search-component', '<p>Joomla! 2.5 offers two search options.</p>\r\n<p>The Basic Search component provides basic search functionality for the information contained in your core components. Many extensions can also be searched by the search component. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Components_Search\">Help</a></p>\r\n<p>The Smart Search component offers searching similar to that found in major search engines. Smart Search is disabled by default. If you choose to enable it you will need to take several steps. First, enable the Smart Search Plugin in the plugin manager. Then, if you are using the Basic Search Module replace it with the Smart Search Module. Finally, if you have already created content, go to the Smart Search component in your site administrator and click the Index icon. Once indexing of your content is complete, Smart Search will be ready to use. Help.</p>', '', 1, 21, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":\"\",\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":\"\",\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":\"\",\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 3, '', '', 1, 16, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(47, 145, 'The Joomla! Community', 'the-joomla-community', '<p>Joomla means All Together, and it is a community of people all working and having fun together that makes Joomla possible. Thousands of people each year participate in the Joomla community, and we hope you will be one of them.</p>\r\n<p>People with all kinds of skills, of all skill levels and from around the world are welcome to join in. Participate in the <a href=\"http://joomla.org\">Joomla.org</a> family of websites (the<a href=\"http://forum.joomla.org\"> forum </a>is a great place to start). Come to a <a href=\"http://community.joomla.org/events.html\">Joomla! event</a>. Join or start a <a href=\"http://community.joomla.org/user-groups.html\">Joomla! Users Group</a>. Whether you are a developer, site administrator, designer, end user or fan, there are ways for you to participate and contribute.</p>', '', -2, 19, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":\"\",\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":\"\",\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":\"\",\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 3, '', '', 1, 2, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(48, 146, 'The Joomla! Project', 'the-joomla-project', '<p>The Joomla Project consists of all of the people who make and support the Joomla Web Platform and Content Management System.</p>\r\n<p>Our mission is to provide a flexible platform for digital publishing and collaboration.</p>\r\n<p>The core values are:</p>\r\n<ul>\r\n<li>Freedom</li>\r\n<li>Equality</li>\r\n<li>Trust</li>\r\n<li>Community</li>\r\n<li>Collaboration</li>\r\n<li>Usability</li>\r\n</ul>\r\n<p>In our vision, we see:</p>\r\n<ul>\r\n<li>People publishing and collaborating in their communities and around the world</li>\r\n<li>Software that is free, secure, and high-quality</li>\r\n<li>A community that is enjoyable and rewarding to participate in</li>\r\n<li>People around the world using their preferred languages</li>\r\n<li>A project that acts autonomously</li>\r\n<li>A project that is socially responsible</li>\r\n<li>A project dedicated to maintaining the trust of its users</li>\r\n</ul>\r\n<p>There are millions of users around the world and thousands of people who contribute to the Joomla Project. They work in three main groups: the Production Working Group, responsible for everything that goes into software and documentation; the Community Working Group, responsible for creating a nurturing the community; and Open Source Matters, the non profit organization responsible for managing legal, financial and organizational issues.</p>\r\n<p>Joomla is a free and open source project, which uses the GNU General Public License version 2 or later.</p>', '', -2, 19, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":\"\",\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":\"\",\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":\"\",\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 1, '', '', 1, 4, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(50, 148, 'Upgraders', 'upgraders', '<p>If you are an experienced Joomla! user, this Joomla site will seem very familiar but also very different. The biggest change is the new administrator interface and the adoption of responsive design. Hundreds of other improvements have been made.</p>\r\n<p> </p>\r\n<p> </p>', '', -2, 19, '2011-01-01 00:00:01', 234, 'Joomla', '2012-09-25 07:12:10', 123, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"0\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 3, 6, '', '', 1, 3, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 1, '*', ''),
(52, 150, 'Users', 'users-component', '<p>The users extension lets your site visitors register, login and logout, change their passwords and other information, and recover lost passwords. In the administrator it allows you to create, block and manage users and create user groups and access levels. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Users_User_Manager\">Help</a></p>\r\n<p>Please note that some of the user views will not display if you are not logged-in to the site.</p>', '', 1, 21, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '', '', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\"}', 1, 5, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(53, 151, 'Using Joomla!', 'using-joomla', '<p>With Joomla you can create anything from a simple personal website to a complex ecommerce or social site with millions of visitors.</p>\r\n<p>This section of the sample data provides you with a brief introduction to Joomla concepts and reference material to help you understand how Joomla works.</p>\r\n<p><em>When you no longer need the sample data, you can can simply unpublish the sample data category found within each extension in the site administrator or you may completely delete each item and all of the categories. </em></p>', '', -2, 19, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":\"\",\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":\"\",\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":\"\",\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 7, '', '', 1, 9, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(60, 158, 'News Feeds', 'news-feeds', '<p>News Feeds (com_newsfeeds) provides a way to organize and present news feeds. News feeds are a way that you present information from another site on your site. For example, the joomla.org website has numerous feeds that you can incorporate on your site. You an use menus to present a single feed, a list of feeds in a category, or a list of all feed categories. <a href=\"http://help.joomla.org/proxy/index.php?option=com_help&amp;keyref=Help31:Components_Newsfeeds_Feeds\">Help</a></p>', '', -2, 21, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":\"\",\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":\"\",\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":\"\",\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 4, '', '', 1, 9, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(67, 168, 'What\'s New in 1.5?', 'whats-new-in-15', '<p>This article deliberately archived as an example.</p><p>As with previous releases, Joomla! provides a unified and easy-to-use framework for delivering content for Web sites of all kinds. To support the changing nature of the Internet and emerging Web technologies, Joomla! required substantial restructuring of its core functionality and we also used this effort to simplify many challenges within the current user interface. Joomla! 1.5 has many new features.</p>\r\n<p style=\"margin-bottom: 0in;\">In Joomla! 1.5, you\'\'ll notice:</p>\r\n<ul>\r\n<li>Substantially improved usability, manageability, and scalability far beyond the original Mambo foundations</li>\r\n<li>Expanded accessibility to support internationalisation, double-byte characters and right-to-left support for Arabic, Farsi, and Hebrew languages among others</li>\r\n<li>Extended integration of external applications through Web services</li>\r\n<li>Enhanced content delivery, template and presentation capabilities to support accessibility standards and content delivery to any destination</li>\r\n<li>A more sustainable and flexible framework for Component and Extension developers</li>\r\n<li>Backward compatibility with previous releases of Components, Templates, Modules, and other Extensions</li>\r\n</ul>', '', 2, 9, '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:01', '0000-00-00 00:00:00', '', '', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_readmore\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"page_title\":\"\",\"alternative_readmore\":\"\",\"layout\":\"\"}', 1, 0, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(71, 195, 'Doner spare ribs pastrami shank', 'doner-spare-ribs-pastrami-shank', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>\r\n', '\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>', -2, 79, '2015-02-02 18:56:08', 234, '', '2015-02-02 20:27:55', 864, 0, '0000-00-00 00:00:00', '2015-02-02 18:56:08', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"post_format\":\"status\",\"gallery\":\"\",\"audio\":\"\",\"video\":\"\",\"link_title\":\"\",\"link_url\":\"\",\"quote_text\":\"\",\"quote_author\":\"\",\"post_status\":\"<blockquote class=\\\"twitter-tweet\\\" lang=\\\"en\\\"><p>Published a new blog entry One Month Extra for all JoomShaper Members in News. <a href=\\\"http:\\/\\/t.co\\/2pQYdykKy8\\\">http:\\/\\/t.co\\/2pQYdykKy8<\\/a><\\/p>&mdash; JoomShaper (@joomshaper) <a href=\\\"https:\\/\\/twitter.com\\/joomshaper\\/status\\/562210375480139777\\\">February 2, 2015<\\/a><\\/blockquote>\\r\\n<script async src=\\\"\\/\\/platform.twitter.com\\/widgets.js\\\" charset=\\\"utf-8\\\"><\\/script>\"}', 4, 6, '', '', 1, 19, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(72, 196, 'Jerky shank chicken boudin', 'jerky-shank-chicken-boudin', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>\r\n', '\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>', -2, 79, '2015-02-02 20:29:50', 234, '', '2015-02-02 20:41:51', 864, 0, '0000-00-00 00:00:00', '2015-02-02 20:29:50', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"post_format\":\"video\",\"gallery\":\"\",\"audio\":\"\",\"video\":\"http:\\/\\/vimeo.com\\/43426940\",\"link_title\":\"\",\"link_url\":\"\",\"quote_text\":\"\",\"quote_author\":\"\",\"post_status\":\"\"}', 4, 5, '', '', 1, 41, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(73, 197, 'Pellentesque Habitant Morbi Tristique', 'leberkas-tail-swine-pork', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>\r\n', '\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>', -2, 79, '2015-02-02 20:29:50', 234, '', '2015-11-02 10:12:48', 47, 0, '0000-00-00 00:00:00', '2015-02-02 20:29:50', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/blog\\/blog01.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"images\\/blog\\/blog01.jpg\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"spfeatured_image\":\"\",\"post_format\":\"standard\",\"gallery\":\"\",\"audio\":\"\",\"video\":\"\",\"link_title\":\"\",\"link_url\":\"\",\"quote_text\":\"\",\"quote_author\":\"\",\"post_status\":\"\"}', 5, 4, '', '', 1, 97, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '');
INSERT INTO `tol2j_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(74, 198, 'Meatball kevin beef ribs shoulder', 'meatball-kevin-beef-ribs-shoulder', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>\r\n', '\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>', -2, 79, '2015-02-02 20:29:50', 234, '', '2015-02-02 20:53:42', 864, 0, '0000-00-00 00:00:00', '2015-02-02 20:29:50', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"post_format\":\"gallery\",\"gallery\":\"{\\\"gallery_images\\\":[\\\"images\\/blog\\/blog06.jpg\\\",\\\"images\\/blog\\/blog05.jpg\\\",\\\"images\\/blog\\/blog04.jpg\\\"]}\",\"audio\":\"\",\"video\":\"\",\"link_title\":\"\",\"link_url\":\"\",\"quote_text\":\"\",\"quote_author\":\"\",\"post_status\":\"\"}', 3, 3, '', '', 1, 38, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(75, 199, '5 Effective Email Unsubscribe Pages', '5-effective-email-unsubscribe-pages', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>\r\n', '\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>', -2, 79, '2015-02-02 20:29:50', 234, '', '2015-02-02 20:58:11', 864, 0, '0000-00-00 00:00:00', '2015-02-02 20:29:50', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"post_format\":\"audio\",\"gallery\":\"\",\"audio\":\"<iframe width=\\\"100%\\\" height=\\\"450\\\" scrolling=\\\"no\\\" frameborder=\\\"no\\\" src=\\\"https:\\/\\/w.soundcloud.com\\/player\\/?url=https%3A\\/\\/api.soundcloud.com\\/tracks\\/28830162&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\\\"><\\/iframe>\",\"video\":\"\",\"link_title\":\"\",\"link_url\":\"\",\"quote_text\":\"\",\"quote_author\":\"\",\"post_status\":\"\"}', 4, 2, '', '', 1, 22, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(76, 200, 'Who Actually Clicks on Banner Ads?', 'who-actually-clicks-on-banner-ads', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>\r\n', '\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>', -2, 79, '2015-02-02 20:29:50', 234, '', '2015-02-02 21:05:00', 864, 0, '0000-00-00 00:00:00', '2015-02-02 20:29:50', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"post_format\":\"link\",\"gallery\":\"\",\"audio\":\"\",\"video\":\"\",\"link_title\":\"Responive Joomla Templates\",\"link_url\":\"http:\\/\\/www.joomshaper.com\\/joomla-templates\",\"quote_text\":\"\",\"quote_author\":\"\",\"post_status\":\"\"}', 3, 1, '', '', 1, 38, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(77, 201, 'See the new Miss Universe get her crown', 'see-the-new-miss-universe-get-her-crown', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>\r\n', '\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>', -2, 79, '2015-02-02 20:29:50', 234, '', '2015-02-02 21:05:57', 864, 0, '0000-00-00 00:00:00', '2015-02-02 20:29:50', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\",\"post_format\":\"quote\",\"gallery\":\"\",\"audio\":\"\",\"video\":\"\",\"link_title\":\"\",\"link_url\":\"\",\"quote_text\":\"Pork meatball ground round prosciutto. Sirloin bresaola ball tip shank tail porchetta pork boudin filet mignon flank jowl salami. Filet mignon bresaola pork boudin capicola prosciutto. Frankfurter chicken leberkas drumstick ball tip turducken rump spare ribs meatball. Tail salami pork loin ham. Drumstick flank porchetta, hamburger ham swine biltong chicken pancetta. Spare ribs prosciutto t-bone.\",\"quote_author\":\"- John Doe\",\"post_status\":\"\"}', 2, 0, '', '', 1, 43, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_contentitem_tag_map`
--

DROP TABLE IF EXISTS `tol2j_contentitem_tag_map`;
CREATE TABLE `tol2j_contentitem_tag_map` (
  `type_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_content_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_content_frontpage`
--

DROP TABLE IF EXISTS `tol2j_content_frontpage`;
CREATE TABLE `tol2j_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_content_frontpage`
--

INSERT INTO `tol2j_content_frontpage` (`content_id`, `ordering`) VALUES
(8, 2),
(24, 1),
(35, 4),
(50, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_content_rating`
--

DROP TABLE IF EXISTS `tol2j_content_rating`;
CREATE TABLE `tol2j_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_content_rating`
--

INSERT INTO `tol2j_content_rating` (`content_id`, `rating_sum`, `rating_count`, `lastip`) VALUES
(72, 5, 1, '::1'),
(73, 5, 1, '::1'),
(74, 4, 1, '::1'),
(75, 4, 1, '::1'),
(77, 5, 1, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_content_types`
--

DROP TABLE IF EXISTS `tol2j_content_types`;
CREATE TABLE `tol2j_content_types` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rules` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_mappings` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `router` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'JSON string for com_contenthistory options'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_content_types`
--

INSERT INTO `tol2j_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{\"special\":{\"dbtable\":\"#__content\",\"key\":\"id\",\"type\":\"Content\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"introtext\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"attribs\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"asset_id\"}, \"special\":{\"fulltext\":\"fulltext\"}}', 'ContentHelperRoute::getArticleRoute', '{\"formFile\":\"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(2, 'Weblink', 'com_weblinks.weblink', '{\"special\":{\"dbtable\":\"#__weblinks\",\"key\":\"id\",\"type\":\"Weblink\",\"prefix\":\"WeblinksTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{}}', 'WeblinksHelperRoute::getWeblinkRoute', '{\"formFile\":\"administrator\\/components\\/com_weblinks\\/models\\/forms\\/weblink.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"featured\",\"images\"], \"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(3, 'Contact', 'com_contact.contact', '{\"special\":{\"dbtable\":\"#__contact_details\",\"key\":\"id\",\"type\":\"Contact\",\"prefix\":\"ContactTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"address\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"image\", \"core_urls\":\"webpage\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"con_position\":\"con_position\",\"suburb\":\"suburb\",\"state\":\"state\",\"country\":\"country\",\"postcode\":\"postcode\",\"telephone\":\"telephone\",\"fax\":\"fax\",\"misc\":\"misc\",\"email_to\":\"email_to\",\"default_con\":\"default_con\",\"user_id\":\"user_id\",\"mobile\":\"mobile\",\"sortname1\":\"sortname1\",\"sortname2\":\"sortname2\",\"sortname3\":\"sortname3\"}}', 'ContactHelperRoute::getContactRoute', '{\"formFile\":\"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml\",\"hideFields\":[\"default_con\",\"checked_out\",\"checked_out_time\",\"version\",\"xreference\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[ {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ] }'),
(4, 'Newsfeed', 'com_newsfeeds.newsfeed', '{\"special\":{\"dbtable\":\"#__newsfeeds\",\"key\":\"id\",\"type\":\"Newsfeed\",\"prefix\":\"NewsfeedsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"numarticles\":\"numarticles\",\"cache_time\":\"cache_time\",\"rtl\":\"rtl\"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{\"formFile\":\"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(5, 'User', 'com_users.user', '{\"special\":{\"dbtable\":\"#__users\",\"key\":\"id\",\"type\":\"User\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"username\",\"core_created_time\":\"registerdate\",\"core_modified_time\":\"lastvisitDate\",\"core_body\":\"null\", \"core_hits\":\"null\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"access\":\"null\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"null\", \"core_language\":\"null\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"null\", \"core_ordering\":\"null\", \"core_metakey\":\"null\", \"core_metadesc\":\"null\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{}}', 'UsersHelperRoute::getUserRoute', ''),
(6, 'Article Category', 'com_content.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContentHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(7, 'Contact Category', 'com_contact.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContactHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(8, 'Newsfeeds Category', 'com_newsfeeds.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(9, 'Weblinks Category', 'com_weblinks.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'WeblinksHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(10, 'Tag', 'com_tags.tag', '{\"special\":{\"dbtable\":\"#__tags\",\"key\":\"tag_id\",\"type\":\"Tag\",\"prefix\":\"TagsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\"}}', 'TagsHelperRoute::getTagRoute', '{\"formFile\":\"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"lft\", \"rgt\", \"level\", \"path\", \"urls\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(11, 'Banner', 'com_banners.banner', '{\"special\":{\"dbtable\":\"#__banners\",\"key\":\"id\",\"type\":\"Banner\",\"prefix\":\"BannersTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"null\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"imptotal\":\"imptotal\", \"impmade\":\"impmade\", \"clicks\":\"clicks\", \"clickurl\":\"clickurl\", \"custombannercode\":\"custombannercode\", \"cid\":\"cid\", \"purchase_type\":\"purchase_type\", \"track_impressions\":\"track_impressions\", \"track_clicks\":\"track_clicks\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"reset\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"imptotal\", \"impmade\", \"reset\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"cid\",\"targetTable\":\"#__banner_clients\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(12, 'Banners Category', 'com_banners.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(13, 'Banner Client', 'com_banners.client', '{\"special\":{\"dbtable\":\"#__banner_clients\",\"key\":\"id\",\"type\":\"Client\",\"prefix\":\"BannersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\"], \"ignoreChanges\":[\"checked_out\", \"checked_out_time\"], \"convertToInt\":[], \"displayLookup\":[]}'),
(14, 'User Notes', 'com_users.note', '{\"special\":{\"dbtable\":\"#__user_notes\",\"key\":\"id\",\"type\":\"Note\",\"prefix\":\"UsersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\"], \"convertToInt\":[\"publish_up\", \"publish_down\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(15, 'User Notes Category', 'com_users.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_core_log_searches`
--

DROP TABLE IF EXISTS `tol2j_core_log_searches`;
CREATE TABLE `tol2j_core_log_searches` (
  `search_term` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_countries`
--

DROP TABLE IF EXISTS `tol2j_countries`;
CREATE TABLE `tol2j_countries` (
  `id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `country_name` varchar(255) NOT NULL COMMENT 'Country Name',
  `country_data_manager` int(255) NOT NULL COMMENT 'Country Data Manager',
  `country_brief` varchar(1200) NOT NULL COMMENT 'Country Brief',
  `banner_image` varchar(255) NOT NULL COMMENT 'Banner Image',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `access` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadata` text NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_countries`
--

INSERT INTO `tol2j_countries` (`id`, `asset_id`, `country_name`, `country_data_manager`, `country_brief`, `banner_image`, `alias`, `ordering`, `published`, `checked_out`, `checked_out_time`, `created`, `created_by`, `modified`, `modified_by`, `publish_up`, `publish_down`, `images`, `access`, `params`, `metadata`, `metakey`, `metadesc`) VALUES
(1, 268, 'Kenya', 234, 'Brief about Kenya. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'images/Flag_of_Kenya.png', 'kenya', 1, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-11 22:16:35', 234, '2017-07-20 04:42:30', '0000-00-00 00:00:00', '', 12, '', '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(2, 274, 'Uganda', 0, 'Uganda Details. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'images/Flag_of_Uganda.png', 'uganda', 2, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-11 22:17:28', 234, '2017-07-20 05:47:29', '0000-00-00 00:00:00', '', 16, '', '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(3, 275, 'Tanzania', 0, 'Tanzania Details. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'images/Flag_of_Tanzania.png', 'tanzania', 3, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-11 22:17:13', 234, '2017-07-20 05:48:02', '0000-00-00 00:00:00', '', 15, '', '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(4, 276, 'Burundi', 238, 'Burundi brief. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'images/Flag_of_Burundi.png', 'burundi', 4, 1, 234, '2017-10-31 03:09:47', '2017-07-21 00:00:00', 234, '2017-10-31 03:09:34', 234, '2017-07-21 04:18:49', '0000-00-00 00:00:00', '', 11, '', '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(5, 283, 'Rwanda', 0, 'Rwanda Details. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'images/Flag_of_Rwanda.png', 'rwanda', 5, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-11 22:16:47', 234, '2017-07-20 05:47:29', '0000-00-00 00:00:00', '', 13, '', '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(6, 284, 'South Sudan', 0, 'South Sudan Details. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'images/Flag_of_South_Sudan.png', 'south-sudan', 6, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-11 22:17:01', 234, '2017-07-20 05:47:29', '0000-00-00 00:00:00', '', 14, '', '{\"robots\":\"\",\"rights\":\"\"}', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_country_contacts`
--

DROP TABLE IF EXISTS `tol2j_country_contacts`;
CREATE TABLE `tol2j_country_contacts` (
  `id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `contact_title` varchar(255) NOT NULL COMMENT 'Contact Title',
  `country` varchar(255) NOT NULL COMMENT 'country',
  `contact_location` varchar(255) NOT NULL COMMENT 'Contact Location',
  `main_email_address` varchar(266) NOT NULL COMMENT 'Main Email',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `access` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_country_contacts`
--

INSERT INTO `tol2j_country_contacts` (`id`, `asset_id`, `contact_title`, `country`, `contact_location`, `main_email_address`, `alias`, `ordering`, `published`, `checked_out`, `checked_out_time`, `created`, `created_by`, `modified`, `modified_by`, `publish_up`, `publish_down`, `images`, `access`, `params`) VALUES
(1, 269, 'Burundi', '4', 'Burundi', 'Burundi@nn.com', 'burundi', 1, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-18 08:17:47', 234, '2017-07-20 04:43:06', '0000-00-00 00:00:00', '', 11, ''),
(2, 277, 'Naivasha', '1', 'Naivasha', 'main@nn.com', 'naivasha', 2, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-18 08:18:10', 234, '2017-07-20 04:43:06', '0000-00-00 00:00:00', '', 12, ''),
(3, 278, 'Kampala', '2', 'Kampala', 'main@nn.com', 'kampala', 3, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-18 08:18:01', 234, '2017-07-20 04:43:06', '0000-00-00 00:00:00', '', 16, ''),
(4, 332, 'Rwanda', '5', 'Rwanda', 'rwanda@tmea.org', 'rwanda', 4, 1, 0, '0000-00-00 00:00:00', '2017-09-05 00:00:00', 234, '2017-09-18 08:18:23', 234, '2017-09-05 02:22:30', '0000-00-00 00:00:00', '', 13, ''),
(5, 338, 'arusha_tepp', '3', 'arusha_tepp', 'arusha_tepp@tmea.org', 'arusha-tepp', 5, 1, 0, '0000-00-00 00:00:00', '2017-10-30 00:00:00', 234, '2017-10-30 21:10:09', 234, '2017-10-30 20:34:02', '0000-00-00 00:00:00', '', 15, '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_categories`
--

DROP TABLE IF EXISTS `tol2j_docman_categories`;
CREATE TABLE `tol2j_docman_categories` (
  `docman_category_id` int(11) NOT NULL,
  `uuid` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(512) NOT NULL DEFAULT '',
  `params` text,
  `access` int(11) NOT NULL DEFAULT '1',
  `access_raw` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_docman_categories`
--

INSERT INTO `tol2j_docman_categories` (`docman_category_id`, `uuid`, `title`, `slug`, `description`, `image`, `params`, `access`, `access_raw`, `enabled`, `locked_on`, `locked_by`, `created_on`, `created_by`, `modified_on`, `modified_by`, `asset_id`) VALUES
(13, '40ec0544-5b1b-4309-99d1-a2b0ac41d04c', 'Board', 'board', '<p>Board Member Files</p>', '', '{\"icon\":\"folder\"}', -4, -4, 1, '0000-00-00 00:00:00', 0, '2017-09-12 23:18:20', 234, '2017-10-30 18:08:20', 234, 315),
(14, 'aba24112-d5be-4db0-953f-d962206be5e0', 'Council', 'council', '<p>Council Member Files</p>', '', '{\"icon\":\"folder\"}', -5, -5, 1, '0000-00-00 00:00:00', 0, '2017-09-13 01:18:59', 234, '2017-10-30 18:08:05', 234, 316),
(15, '4a10e7ad-f5a3-4ced-b7b3-c7be5bcc9e06', 'Financial Reports', 'financial-reports', '<p>Financial report files</p>', '', '{\"icon\":\"folder\"}', -6, -6, 1, '0000-00-00 00:00:00', 0, '2017-09-13 01:20:28', 234, '2017-10-30 18:07:48', 234, 317),
(17, '08ff6cc7-8dc4-4fdd-840a-a0d245f96ec8', 'Uganda', 'uganda', '<p>Uganda\'s files related to programmes, projects and anything else relevant.</p>', '', '{\"icon\":\"folder\"}', -7, -7, 1, '0000-00-00 00:00:00', 0, '2017-09-12 23:45:57', 234, '2017-10-30 18:07:25', 234, 322),
(18, '4f516f1a-9f30-4347-82cc-da80dd34638e', 'Tanzania', 'tanzania', '<p><span style=\"font-size: 12.16px;\">Tanzania\'s files related to programmes, projects and anything else relevant.</span></p>', '', '{\"icon\":\"folder\"}', -8, -8, 1, '0000-00-00 00:00:00', 0, '2017-09-12 23:46:52', 234, '2017-10-30 18:07:03', 234, 323),
(19, '700b30c8-a3f0-4e0c-88d3-2b83faa8ae8c', 'Burundi', 'burundi', '<p><span style=\"font-size: 12.16px;\">Burundi\'s files related to programmes, projects and anything else relevant.</span></p>', '', '{\"icon\":\"folder\"}', -9, -9, 1, '0000-00-00 00:00:00', 0, '2017-09-12 23:47:34', 234, '2017-10-30 21:16:39', 234, 324),
(20, '277959f7-f530-4551-a29f-244f2bb7d9e4', 'Rwanda', 'rwanda', '<p><span style=\"font-size: 12.16px;\">Rwanda\'s files related to programmes, projects and anything else relevant.</span></p>', '', '{\"icon\":\"folder\"}', -10, -10, 1, '0000-00-00 00:00:00', 0, '2017-09-12 23:48:21', 234, '2017-10-30 18:05:16', 234, 325),
(21, '4fb86052-e38a-4cb7-ae2e-540466edbef9', 'South Sudan', 'south-sudan', '<p><span style=\"font-size: 12.16px;\">South Sudan\'s files related to programmes, projects and anything else relevant.</span></p>', '', '{\"icon\":\"folder\"}', -11, -11, 1, '0000-00-00 00:00:00', 0, '2017-09-12 23:49:12', 234, '2017-10-30 18:06:04', 234, 326),
(22, '43656cae-c57a-4d17-b079-e7195b7a6533', 'Kenya', 'kenya', NULL, '', '{\"icon\":\"folder\"}', -13, -13, 1, '0000-00-00 00:00:00', 0, '2017-09-14 09:50:17', 234, '2017-10-30 22:25:39', 234, 333);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_category_folders`
--

DROP TABLE IF EXISTS `tol2j_docman_category_folders`;
CREATE TABLE `tol2j_docman_category_folders` (
  `docman_category_id` int(11) UNSIGNED NOT NULL,
  `folder` varchar(4096) NOT NULL DEFAULT '',
  `automatic` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_docman_category_folders`
--

INSERT INTO `tol2j_docman_category_folders` (`docman_category_id`, `folder`, `automatic`) VALUES
(13, 'board', 1),
(14, 'council', 1),
(15, 'financial-reports', 1),
(17, 'uganda', 1),
(18, 'tanzania', 1),
(19, 'burundi', 1),
(20, 'rwanda', 1),
(21, 'south-sudan', 1),
(22, 'kenya', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_category_orderings`
--

DROP TABLE IF EXISTS `tol2j_docman_category_orderings`;
CREATE TABLE `tol2j_docman_category_orderings` (
  `docman_category_id` int(11) UNSIGNED NOT NULL,
  `title` int(11) NOT NULL DEFAULT '0',
  `custom` int(11) NOT NULL DEFAULT '0',
  `created_on` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_docman_category_orderings`
--

INSERT INTO `tol2j_docman_category_orderings` (`docman_category_id`, `title`, `custom`, `created_on`) VALUES
(13, 1, 1, 1),
(14, 3, 2, 7),
(15, 4, 3, 8),
(17, 9, 4, 2),
(18, 8, 5, 3),
(19, 2, 6, 4),
(20, 6, 7, 5),
(21, 7, 8, 6),
(22, 5, 9, 9);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_category_relations`
--

DROP TABLE IF EXISTS `tol2j_docman_category_relations`;
CREATE TABLE `tol2j_docman_category_relations` (
  `ancestor_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `descendant_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_docman_category_relations`
--

INSERT INTO `tol2j_docman_category_relations` (`ancestor_id`, `descendant_id`, `level`) VALUES
(13, 13, 0),
(14, 14, 0),
(15, 15, 0),
(17, 17, 0),
(18, 18, 0),
(19, 19, 0),
(20, 20, 0),
(21, 21, 0),
(22, 22, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_documents`
--

DROP TABLE IF EXISTS `tol2j_docman_documents`;
CREATE TABLE `tol2j_docman_documents` (
  `docman_document_id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `docman_category_id` bigint(20) NOT NULL,
  `description` longtext,
  `image` varchar(512) NOT NULL DEFAULT '',
  `storage_type` varchar(64) NOT NULL DEFAULT '',
  `storage_path` varchar(512) NOT NULL DEFAULT '',
  `hits` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `access` int(11) NOT NULL DEFAULT '0',
  `publish_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unpublish_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `params` text,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_docman_documents`
--

INSERT INTO `tol2j_docman_documents` (`docman_document_id`, `uuid`, `title`, `slug`, `docman_category_id`, `description`, `image`, `storage_type`, `storage_path`, `hits`, `enabled`, `access`, `publish_on`, `unpublish_on`, `locked_on`, `locked_by`, `created_on`, `created_by`, `modified_on`, `modified_by`, `params`, `asset_id`, `ordering`) VALUES
(43, 'c4ce720a-061b-4986-82d8-030da0ad7288', 'TRADEMARK EASTAFRICA TEST DOCUMENT (Board)', 'trademark-eastafrica-test-document-board', 13, NULL, '', 'file', 'board/TRADEMARK EASTAFRICA TEST DOCUMENT (Board).docx', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2017-09-13 00:02:06', 234, '0000-00-00 00:00:00', 0, '{\"icon\":\"document\"}', 327, 1),
(44, 'eab390f5-0465-43ed-a8bd-5d6a7cacf942', 'Burundi', 'burundi', 19, NULL, '', 'file', 'burundi/Burundi.pdf', 6, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2017-09-13 00:07:34', 234, '0000-00-00 00:00:00', 0, '{\"icon\":\"pdf\"}', 328, 2),
(47, 'cf00c879-d2ca-4dd0-979a-01b23dbcc3fc', 'TRADEMARK EASTAFRICA TEST DOCUMENT (Council)', 'trademark-eastafrica-test-document-council', 14, NULL, '', 'file', 'council/TRADEMARK EASTAFRICA TEST DOCUMENT (Council).docx', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2017-09-13 01:51:40', 234, '0000-00-00 00:00:00', 0, '{\"icon\":\"document\"}', 331, 3),
(48, 'f0061d42-77b2-44fe-b591-0aa57a674475', 'Projectplan sample2', 'projectplan-sample2', 22, '<p>Kenya</p>', '', 'file', 'kenya/projectplan-sample2.xls', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2017-10-30 20:08:34', 234, '0000-00-00 00:00:00', 0, '{\"icon\":\"spreadsheet\"}', 337, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_files`
--

DROP TABLE IF EXISTS `tol2j_docman_files`;
CREATE TABLE `tol2j_docman_files` (
  `docman_file_id` bigint(20) UNSIGNED NOT NULL,
  `folder` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) NOT NULL,
  `parameters` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_docman_files`
--

INSERT INTO `tol2j_docman_files` (`docman_file_id`, `folder`, `name`, `modified_on`, `modified_by`, `created_on`, `created_by`, `parameters`) VALUES
(42, 'board', 'TRADEMARK EASTAFRICA TEST DOCUMENT (Board).docx', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(43, 'burundi', 'Burundi.pdf', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(45, 'council', 'TRADEMARK EASTAFRICA TEST DOCUMENT (Council).docx', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(46, 'kenya', 'projectplan-sample2.xls', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tol2j_docman_file_counts`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `tol2j_docman_file_counts`;
CREATE TABLE `tol2j_docman_file_counts` (
);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_folders`
--

DROP TABLE IF EXISTS `tol2j_docman_folders`;
CREATE TABLE `tol2j_docman_folders` (
  `docman_folder_id` bigint(20) UNSIGNED NOT NULL,
  `folder` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) NOT NULL,
  `parameters` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_docman_folders`
--

INSERT INTO `tol2j_docman_folders` (`docman_folder_id`, `folder`, `name`, `modified_on`, `modified_by`, `created_on`, `created_by`, `parameters`) VALUES
(13, '', 'board', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(14, '', 'council', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(15, '', 'financial-reports', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(18, '', 'uganda', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(19, '', 'tanzania', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(20, '', 'burundi', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(21, '', 'rwanda', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(22, '', 'south-sudan', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL),
(23, '', 'kenya', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_levels`
--

DROP TABLE IF EXISTS `tol2j_docman_levels`;
CREATE TABLE `tol2j_docman_levels` (
  `docman_level_id` bigint(20) UNSIGNED NOT NULL,
  `entity` char(36) DEFAULT NULL,
  `groups` varchar(1024) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_docman_levels`
--

INSERT INTO `tol2j_docman_levels` (`docman_level_id`, `entity`, `groups`) VALUES
(1, '7955170c-fe43-40ca-815f-861f479a40e2', '10'),
(2, '068b9b7a-fde2-43be-8cd3-d7e160e4d32a', '10,11'),
(3, '7dbe2fb5-b2ad-463e-a8cc-5413a1343e68', '11'),
(4, '40ec0544-5b1b-4309-99d1-a2b0ac41d04c', '10,35,36,37'),
(5, 'aba24112-d5be-4db0-953f-d962206be5e0', '10,11,35,36,37'),
(6, '4a10e7ad-f5a3-4ced-b7b3-c7be5bcc9e06', '10,11,35,36,37'),
(7, '08ff6cc7-8dc4-4fdd-840a-a0d245f96ec8', '10,11,15,35,36,37'),
(8, '4f516f1a-9f30-4347-82cc-da80dd34638e', '10,11,18,35,36,37'),
(9, '700b30c8-a3f0-4e0c-88d3-2b83faa8ae8c', '10,11,25,28,35,36,37'),
(10, '277959f7-f530-4551-a29f-244f2bb7d9e4', '10,11,19,35,36,37'),
(11, '4fb86052-e38a-4cb7-ae2e-540466edbef9', '10,11,21,35,36,37'),
(12, '8840dbde-865f-46ab-b036-68002fb516c8', '10,11,12'),
(13, '43656cae-c57a-4d17-b079-e7195b7a6533', '10,11,30,31,32,35,36,37,44');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_mimetypes`
--

DROP TABLE IF EXISTS `tol2j_docman_mimetypes`;
CREATE TABLE `tol2j_docman_mimetypes` (
  `mimetype` varchar(255) NOT NULL,
  `extension` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_docman_mimetypes`
--

INSERT INTO `tol2j_docman_mimetypes` (`mimetype`, `extension`) VALUES
('application/andrew-inset', 'ez'),
('application/applixware', 'aw'),
('application/atom+xml', 'atom'),
('application/atomcat+xml', 'atomcat'),
('application/atomsvc+xml', 'atomsvc'),
('application/ccxml+xml', 'ccxml'),
('application/cdmi-capability', 'cdmia'),
('application/cdmi-container', 'cdmic'),
('application/cdmi-domain', 'cdmid'),
('application/cdmi-object', 'cdmio'),
('application/cdmi-queue', 'cdmiq'),
('application/cu-seeme', 'cu'),
('application/davmount+xml', 'davmount'),
('application/docbook+xml', 'dbk'),
('application/dssc+der', 'dssc'),
('application/dssc+xml', 'xdssc'),
('application/ecmascript', 'ecma'),
('application/emma+xml', 'emma'),
('application/epub+zip', 'epub'),
('application/exi', 'exi'),
('application/font-tdpfr', 'pfr'),
('application/font-woff', 'woff'),
('application/gml+xml', 'gml'),
('application/gpx+xml', 'gpx'),
('application/gxf', 'gxf'),
('application/hyperstudio', 'stk'),
('application/inkml+xml', 'ink'),
('application/inkml+xml', 'inkml'),
('application/ipfix', 'ipfix'),
('application/java-archive', 'jar'),
('application/java-serialized-object', 'ser'),
('application/java-vm', 'class'),
('application/javascript', 'js'),
('application/json', 'json'),
('application/jsonml+json', 'jsonml'),
('application/lost+xml', 'lostxml'),
('application/mac-binhex40', 'hqx'),
('application/mac-compactpro', 'cpt'),
('application/mads+xml', 'mads'),
('application/marc', 'mrc'),
('application/marcxml+xml', 'mrcx'),
('application/mathematica', 'ma'),
('application/mathematica', 'mb'),
('application/mathematica', 'nb'),
('application/mathml+xml', 'mathml'),
('application/mbox', 'mbox'),
('application/mediaservercontrol+xml', 'mscml'),
('application/metalink+xml', 'metalink'),
('application/metalink4+xml', 'meta4'),
('application/mets+xml', 'mets'),
('application/mods+xml', 'mods'),
('application/mp21', 'm21'),
('application/mp21', 'mp21'),
('application/mp4', 'mp4s'),
('application/msword', 'doc'),
('application/msword', 'dot'),
('application/mxf', 'mxf'),
('application/octet-stream', 'bin'),
('application/octet-stream', 'bpk'),
('application/octet-stream', 'deploy'),
('application/octet-stream', 'dist'),
('application/octet-stream', 'distz'),
('application/octet-stream', 'dms'),
('application/octet-stream', 'dump'),
('application/octet-stream', 'elc'),
('application/octet-stream', 'lrf'),
('application/octet-stream', 'mar'),
('application/octet-stream', 'pkg'),
('application/octet-stream', 'so'),
('application/oda', 'oda'),
('application/oebps-package+xml', 'opf'),
('application/ogg', 'ogx'),
('application/omdoc+xml', 'omdoc'),
('application/onenote', 'onepkg'),
('application/onenote', 'onetmp'),
('application/onenote', 'onetoc'),
('application/onenote', 'onetoc2'),
('application/oxps', 'oxps'),
('application/patch-ops-error+xml', 'xer'),
('application/pdf', 'pdf'),
('application/pgp-encrypted', 'pgp'),
('application/pgp-signature', 'asc'),
('application/pgp-signature', 'sig'),
('application/pics-rules', 'prf'),
('application/pkcs10', 'p10'),
('application/pkcs7-mime', 'p7c'),
('application/pkcs7-mime', 'p7m'),
('application/pkcs7-signature', 'p7s'),
('application/pkcs8', 'p8'),
('application/pkix-attr-cert', 'ac'),
('application/pkix-cert', 'cer'),
('application/pkix-crl', 'crl'),
('application/pkix-pkipath', 'pkipath'),
('application/pkixcmp', 'pki'),
('application/pls+xml', 'pls'),
('application/postscript', 'ai'),
('application/postscript', 'eps'),
('application/postscript', 'ps'),
('application/prs.cww', 'cww'),
('application/pskc+xml', 'pskcxml'),
('application/rdf+xml', 'rdf'),
('application/reginfo+xml', 'rif'),
('application/relax-ng-compact-syntax', 'rnc'),
('application/resource-lists+xml', 'rl'),
('application/resource-lists-diff+xml', 'rld'),
('application/rls-services+xml', 'rs'),
('application/rpki-ghostbusters', 'gbr'),
('application/rpki-manifest', 'mft'),
('application/rpki-roa', 'roa'),
('application/rsd+xml', 'rsd'),
('application/rss+xml', 'rss'),
('application/rtf', 'rtf'),
('application/sbml+xml', 'sbml'),
('application/scvp-cv-request', 'scq'),
('application/scvp-cv-response', 'scs'),
('application/scvp-vp-request', 'spq'),
('application/scvp-vp-response', 'spp'),
('application/sdp', 'sdp'),
('application/set-payment-initiation', 'setpay'),
('application/set-registration-initiation', 'setreg'),
('application/shf+xml', 'shf'),
('application/smil+xml', 'smi'),
('application/smil+xml', 'smil'),
('application/sparql-query', 'rq'),
('application/sparql-results+xml', 'srx'),
('application/srgs', 'gram'),
('application/srgs+xml', 'grxml'),
('application/sru+xml', 'sru'),
('application/ssdl+xml', 'ssdl'),
('application/ssml+xml', 'ssml'),
('application/tei+xml', 'tei'),
('application/tei+xml', 'teicorpus'),
('application/thraud+xml', 'tfi'),
('application/timestamped-data', 'tsd'),
('application/vnd.3gpp.pic-bw-large', 'plb'),
('application/vnd.3gpp.pic-bw-small', 'psb'),
('application/vnd.3gpp.pic-bw-var', 'pvb'),
('application/vnd.3gpp2.tcap', 'tcap'),
('application/vnd.3m.post-it-notes', 'pwn'),
('application/vnd.accpac.simply.aso', 'aso'),
('application/vnd.accpac.simply.imp', 'imp'),
('application/vnd.acucobol', 'acu'),
('application/vnd.acucorp', 'acutc'),
('application/vnd.acucorp', 'atc'),
('application/vnd.adobe.air-application-installer-package+zip', 'air'),
('application/vnd.adobe.formscentral.fcdt', 'fcdt'),
('application/vnd.adobe.fxp', 'fxp'),
('application/vnd.adobe.fxp', 'fxpl'),
('application/vnd.adobe.xdp+xml', 'xdp'),
('application/vnd.adobe.xfdf', 'xfdf'),
('application/vnd.ahead.space', 'ahead'),
('application/vnd.airzip.filesecure.azf', 'azf'),
('application/vnd.airzip.filesecure.azs', 'azs'),
('application/vnd.amazon.ebook', 'azw'),
('application/vnd.americandynamics.acc', 'acc'),
('application/vnd.amiga.ami', 'ami'),
('application/vnd.android.package-archive', 'apk'),
('application/vnd.anser-web-certificate-issue-initiation', 'cii'),
('application/vnd.anser-web-funds-transfer-initiation', 'fti'),
('application/vnd.antix.game-component', 'atx'),
('application/vnd.apple.installer+xml', 'mpkg'),
('application/vnd.apple.mpegurl', 'm3u8'),
('application/vnd.aristanetworks.swi', 'swi'),
('application/vnd.astraea-software.iota', 'iota'),
('application/vnd.audiograph', 'aep'),
('application/vnd.blueice.multipass', 'mpm'),
('application/vnd.bmi', 'bmi'),
('application/vnd.businessobjects', 'rep'),
('application/vnd.chemdraw+xml', 'cdxml'),
('application/vnd.chipnuts.karaoke-mmd', 'mmd'),
('application/vnd.cinderella', 'cdy'),
('application/vnd.claymore', 'cla'),
('application/vnd.cloanto.rp9', 'rp9'),
('application/vnd.clonk.c4group', 'c4d'),
('application/vnd.clonk.c4group', 'c4f'),
('application/vnd.clonk.c4group', 'c4g'),
('application/vnd.clonk.c4group', 'c4p'),
('application/vnd.clonk.c4group', 'c4u'),
('application/vnd.cluetrust.cartomobile-config', 'c11amc'),
('application/vnd.cluetrust.cartomobile-config-pkg', 'c11amz'),
('application/vnd.commonspace', 'csp'),
('application/vnd.contact.cmsg', 'cdbcmsg'),
('application/vnd.cosmocaller', 'cmc'),
('application/vnd.crick.clicker', 'clkx'),
('application/vnd.crick.clicker.keyboard', 'clkk'),
('application/vnd.crick.clicker.palette', 'clkp'),
('application/vnd.crick.clicker.template', 'clkt'),
('application/vnd.crick.clicker.wordbank', 'clkw'),
('application/vnd.criticaltools.wbs+xml', 'wbs'),
('application/vnd.ctc-posml', 'pml'),
('application/vnd.cups-ppd', 'ppd'),
('application/vnd.curl.car', 'car'),
('application/vnd.curl.pcurl', 'pcurl'),
('application/vnd.dart', 'dart'),
('application/vnd.data-vision.rdz', 'rdz'),
('application/vnd.dece.data', 'uvd'),
('application/vnd.dece.data', 'uvf'),
('application/vnd.dece.data', 'uvvd'),
('application/vnd.dece.data', 'uvvf'),
('application/vnd.dece.ttml+xml', 'uvt'),
('application/vnd.dece.ttml+xml', 'uvvt'),
('application/vnd.dece.unspecified', 'uvvx'),
('application/vnd.dece.unspecified', 'uvx'),
('application/vnd.dece.zip', 'uvvz'),
('application/vnd.dece.zip', 'uvz'),
('application/vnd.denovo.fcselayout-link', 'fe_launch'),
('application/vnd.dna', 'dna'),
('application/vnd.dolby.mlp', 'mlp'),
('application/vnd.dpgraph', 'dpg'),
('application/vnd.dreamfactory', 'dfac'),
('application/vnd.ds-keypoint', 'kpxx'),
('application/vnd.dvb.ait', 'ait'),
('application/vnd.dvb.service', 'svc'),
('application/vnd.dynageo', 'geo'),
('application/vnd.ecowin.chart', 'mag'),
('application/vnd.enliven', 'nml'),
('application/vnd.epson.esf', 'esf'),
('application/vnd.epson.msf', 'msf'),
('application/vnd.epson.quickanime', 'qam'),
('application/vnd.epson.salt', 'slt'),
('application/vnd.epson.ssf', 'ssf'),
('application/vnd.eszigno3+xml', 'es3'),
('application/vnd.eszigno3+xml', 'et3'),
('application/vnd.ezpix-album', 'ez2'),
('application/vnd.ezpix-package', 'ez3'),
('application/vnd.fdf', 'fdf'),
('application/vnd.fdsn.mseed', 'mseed'),
('application/vnd.fdsn.seed', 'dataless'),
('application/vnd.fdsn.seed', 'seed'),
('application/vnd.flographit', 'gph'),
('application/vnd.fluxtime.clip', 'ftc'),
('application/vnd.framemaker', 'book'),
('application/vnd.framemaker', 'fm'),
('application/vnd.framemaker', 'frame'),
('application/vnd.framemaker', 'maker'),
('application/vnd.frogans.fnc', 'fnc'),
('application/vnd.frogans.ltf', 'ltf'),
('application/vnd.fsc.weblaunch', 'fsc'),
('application/vnd.fujitsu.oasys', 'oas'),
('application/vnd.fujitsu.oasys2', 'oa2'),
('application/vnd.fujitsu.oasys3', 'oa3'),
('application/vnd.fujitsu.oasysgp', 'fg5'),
('application/vnd.fujitsu.oasysprs', 'bh2'),
('application/vnd.fujixerox.ddd', 'ddd'),
('application/vnd.fujixerox.docuworks', 'xdw'),
('application/vnd.fujixerox.docuworks.binder', 'xbd'),
('application/vnd.fuzzysheet', 'fzs'),
('application/vnd.genomatix.tuxedo', 'txd'),
('application/vnd.geogebra.file', 'ggb'),
('application/vnd.geogebra.tool', 'ggt'),
('application/vnd.geometry-explorer', 'gex'),
('application/vnd.geometry-explorer', 'gre'),
('application/vnd.geonext', 'gxt'),
('application/vnd.geoplan', 'g2w'),
('application/vnd.geospace', 'g3w'),
('application/vnd.gmx', 'gmx'),
('application/vnd.google-earth.kml+xml', 'kml'),
('application/vnd.google-earth.kmz', 'kmz'),
('application/vnd.grafeq', 'gqf'),
('application/vnd.grafeq', 'gqs'),
('application/vnd.groove-account', 'gac'),
('application/vnd.groove-help', 'ghf'),
('application/vnd.groove-identity-message', 'gim'),
('application/vnd.groove-injector', 'grv'),
('application/vnd.groove-tool-message', 'gtm'),
('application/vnd.groove-tool-template', 'tpl'),
('application/vnd.groove-vcard', 'vcg'),
('application/vnd.hal+xml', 'hal'),
('application/vnd.handheld-entertainment+xml', 'zmm'),
('application/vnd.hbci', 'hbci'),
('application/vnd.hhe.lesson-player', 'les'),
('application/vnd.hp-hpgl', 'hpgl'),
('application/vnd.hp-hpid', 'hpid'),
('application/vnd.hp-hps', 'hps'),
('application/vnd.hp-jlyt', 'jlt'),
('application/vnd.hp-pcl', 'pcl'),
('application/vnd.hp-pclxl', 'pclxl'),
('application/vnd.hydrostatix.sof-data', 'sfd-hdstx'),
('application/vnd.ibm.minipay', 'mpy'),
('application/vnd.ibm.modcap', 'afp'),
('application/vnd.ibm.modcap', 'list3820'),
('application/vnd.ibm.modcap', 'listafp'),
('application/vnd.ibm.rights-management', 'irm'),
('application/vnd.ibm.secure-container', 'sc'),
('application/vnd.iccprofile', 'icc'),
('application/vnd.iccprofile', 'icm'),
('application/vnd.igloader', 'igl'),
('application/vnd.immervision-ivp', 'ivp'),
('application/vnd.immervision-ivu', 'ivu'),
('application/vnd.insors.igm', 'igm'),
('application/vnd.intercon.formnet', 'xpw'),
('application/vnd.intercon.formnet', 'xpx'),
('application/vnd.intergeo', 'i2g'),
('application/vnd.intu.qbo', 'qbo'),
('application/vnd.intu.qfx', 'qfx'),
('application/vnd.ipunplugged.rcprofile', 'rcprofile'),
('application/vnd.irepository.package+xml', 'irp'),
('application/vnd.is-xpr', 'xpr'),
('application/vnd.isac.fcs', 'fcs'),
('application/vnd.jam', 'jam'),
('application/vnd.jcp.javame.midlet-rms', 'rms'),
('application/vnd.jisp', 'jisp'),
('application/vnd.joost.joda-archive', 'joda'),
('application/vnd.kahootz', 'ktr'),
('application/vnd.kahootz', 'ktz'),
('application/vnd.kde.karbon', 'karbon'),
('application/vnd.kde.kchart', 'chrt'),
('application/vnd.kde.kformula', 'kfo'),
('application/vnd.kde.kivio', 'flw'),
('application/vnd.kde.kontour', 'kon'),
('application/vnd.kde.kpresenter', 'kpr'),
('application/vnd.kde.kpresenter', 'kpt'),
('application/vnd.kde.kspread', 'ksp'),
('application/vnd.kde.kword', 'kwd'),
('application/vnd.kde.kword', 'kwt'),
('application/vnd.kenameaapp', 'htke'),
('application/vnd.kidspiration', 'kia'),
('application/vnd.kinar', 'kne'),
('application/vnd.kinar', 'knp'),
('application/vnd.koan', 'skd'),
('application/vnd.koan', 'skm'),
('application/vnd.koan', 'skp'),
('application/vnd.koan', 'skt'),
('application/vnd.kodak-descriptor', 'sse'),
('application/vnd.las.las+xml', 'lasxml'),
('application/vnd.llamagraphics.life-balance.desktop', 'lbd'),
('application/vnd.llamagraphics.life-balance.exchange+xml', 'lbe'),
('application/vnd.lotus-1-2-3', '123'),
('application/vnd.lotus-approach', 'apr'),
('application/vnd.lotus-freelance', 'pre'),
('application/vnd.lotus-notes', 'nsf'),
('application/vnd.lotus-organizer', 'org'),
('application/vnd.lotus-screencam', 'scm'),
('application/vnd.lotus-wordpro', 'lwp'),
('application/vnd.macports.portpkg', 'portpkg'),
('application/vnd.mcd', 'mcd'),
('application/vnd.medcalcdata', 'mc1'),
('application/vnd.mediastation.cdkey', 'cdkey'),
('application/vnd.mfer', 'mwf'),
('application/vnd.mfmp', 'mfm'),
('application/vnd.micrografx.flo', 'flo'),
('application/vnd.micrografx.igx', 'igx'),
('application/vnd.mif', 'mif'),
('application/vnd.mobius.daf', 'daf'),
('application/vnd.mobius.dis', 'dis'),
('application/vnd.mobius.mbk', 'mbk'),
('application/vnd.mobius.mqy', 'mqy'),
('application/vnd.mobius.msl', 'msl'),
('application/vnd.mobius.plc', 'plc'),
('application/vnd.mobius.txf', 'txf'),
('application/vnd.mophun.application', 'mpn'),
('application/vnd.mophun.certificate', 'mpc'),
('application/vnd.mozilla.xul+xml', 'xul'),
('application/vnd.ms-artgalry', 'cil'),
('application/vnd.ms-cab-compressed', 'cab'),
('application/vnd.ms-excel', 'xla'),
('application/vnd.ms-excel', 'xlc'),
('application/vnd.ms-excel', 'xlm'),
('application/vnd.ms-excel', 'xls'),
('application/vnd.ms-excel', 'xlt'),
('application/vnd.ms-excel', 'xlw'),
('application/vnd.ms-excel.addin.macroenabled.12', 'xlam'),
('application/vnd.ms-excel.sheet.binary.macroenabled.12', 'xlsb'),
('application/vnd.ms-excel.sheet.macroenabled.12', 'xlsm'),
('application/vnd.ms-excel.template.macroenabled.12', 'xltm'),
('application/vnd.ms-fontobject', 'eot'),
('application/vnd.ms-htmlhelp', 'chm'),
('application/vnd.ms-ims', 'ims'),
('application/vnd.ms-lrm', 'lrm'),
('application/vnd.ms-officetheme', 'thmx'),
('application/vnd.ms-pki.seccat', 'cat'),
('application/vnd.ms-pki.stl', 'stl'),
('application/vnd.ms-powerpoint', 'pot'),
('application/vnd.ms-powerpoint', 'pps'),
('application/vnd.ms-powerpoint', 'ppt'),
('application/vnd.ms-powerpoint.addin.macroenabled.12', 'ppam'),
('application/vnd.ms-powerpoint.presentation.macroenabled.12', 'pptm'),
('application/vnd.ms-powerpoint.slide.macroenabled.12', 'sldm'),
('application/vnd.ms-powerpoint.slideshow.macroenabled.12', 'ppsm'),
('application/vnd.ms-powerpoint.template.macroenabled.12', 'potm'),
('application/vnd.ms-project', 'mpp'),
('application/vnd.ms-project', 'mpt'),
('application/vnd.ms-word.document.macroenabled.12', 'docm'),
('application/vnd.ms-word.template.macroenabled.12', 'dotm'),
('application/vnd.ms-works', 'wcm'),
('application/vnd.ms-works', 'wdb'),
('application/vnd.ms-works', 'wks'),
('application/vnd.ms-works', 'wps'),
('application/vnd.ms-wpl', 'wpl'),
('application/vnd.ms-xpsdocument', 'xps'),
('application/vnd.mseq', 'mseq'),
('application/vnd.musician', 'mus'),
('application/vnd.muvee.style', 'msty'),
('application/vnd.mynfc', 'taglet'),
('application/vnd.neurolanguage.nlu', 'nlu'),
('application/vnd.nitf', 'nitf'),
('application/vnd.nitf', 'ntf'),
('application/vnd.noblenet-directory', 'nnd'),
('application/vnd.noblenet-sealer', 'nns'),
('application/vnd.noblenet-web', 'nnw'),
('application/vnd.nokia.n-gage.data', 'ngdat'),
('application/vnd.nokia.n-gage.symbian.install', 'n-gage'),
('application/vnd.nokia.radio-preset', 'rpst'),
('application/vnd.nokia.radio-presets', 'rpss'),
('application/vnd.novadigm.edm', 'edm'),
('application/vnd.novadigm.edx', 'edx'),
('application/vnd.novadigm.ext', 'ext'),
('application/vnd.oasis.opendocument.chart', 'odc'),
('application/vnd.oasis.opendocument.chart-template', 'otc'),
('application/vnd.oasis.opendocument.database', 'odb'),
('application/vnd.oasis.opendocument.formula', 'odf'),
('application/vnd.oasis.opendocument.formula-template', 'odft'),
('application/vnd.oasis.opendocument.graphics', 'odg'),
('application/vnd.oasis.opendocument.graphics-template', 'otg'),
('application/vnd.oasis.opendocument.image', 'odi'),
('application/vnd.oasis.opendocument.image-template', 'oti'),
('application/vnd.oasis.opendocument.presentation', 'odp'),
('application/vnd.oasis.opendocument.presentation-template', 'otp'),
('application/vnd.oasis.opendocument.spreadsheet', 'ods'),
('application/vnd.oasis.opendocument.spreadsheet-template', 'ots'),
('application/vnd.oasis.opendocument.text', 'odt'),
('application/vnd.oasis.opendocument.text-master', 'odm'),
('application/vnd.oasis.opendocument.text-template', 'ott'),
('application/vnd.oasis.opendocument.text-web', 'oth'),
('application/vnd.olpc-sugar', 'xo'),
('application/vnd.oma.dd2+xml', 'dd2'),
('application/vnd.openofficeorg.extension', 'oxt'),
('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'pptx'),
('application/vnd.openxmlformats-officedocument.presentationml.slide', 'sldx'),
('application/vnd.openxmlformats-officedocument.presentationml.slideshow', 'ppsx'),
('application/vnd.openxmlformats-officedocument.presentationml.template', 'potx'),
('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'xlsx'),
('application/vnd.openxmlformats-officedocument.spreadsheetml.template', 'xltx'),
('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'docx'),
('application/vnd.openxmlformats-officedocument.wordprocessingml.template', 'dotx'),
('application/vnd.osgeo.mapguide.package', 'mgp'),
('application/vnd.osgi.dp', 'dp'),
('application/vnd.osgi.subsystem', 'esa'),
('application/vnd.palm', 'oprc'),
('application/vnd.palm', 'pdb'),
('application/vnd.palm', 'pqa'),
('application/vnd.pawaafile', 'paw'),
('application/vnd.pg.format', 'str'),
('application/vnd.pg.osasli', 'ei6'),
('application/vnd.picsel', 'efif'),
('application/vnd.pmi.widget', 'wg'),
('application/vnd.pocketlearn', 'plf'),
('application/vnd.powerbuilder6', 'pbd'),
('application/vnd.previewsystems.box', 'box'),
('application/vnd.proteus.magazine', 'mgz'),
('application/vnd.publishare-delta-tree', 'qps'),
('application/vnd.pvi.ptid1', 'ptid'),
('application/vnd.quark.quarkxpress', 'qwd'),
('application/vnd.quark.quarkxpress', 'qwt'),
('application/vnd.quark.quarkxpress', 'qxb'),
('application/vnd.quark.quarkxpress', 'qxd'),
('application/vnd.quark.quarkxpress', 'qxl'),
('application/vnd.quark.quarkxpress', 'qxt'),
('application/vnd.realvnc.bed', 'bed'),
('application/vnd.recordare.musicxml', 'mxl'),
('application/vnd.recordare.musicxml+xml', 'musicxml'),
('application/vnd.rig.cryptonote', 'cryptonote'),
('application/vnd.rim.cod', 'cod'),
('application/vnd.rn-realmedia', 'rm'),
('application/vnd.rn-realmedia-vbr', 'rmvb'),
('application/vnd.route66.link66+xml', 'link66'),
('application/vnd.sailingtracker.track', 'st'),
('application/vnd.seemail', 'see'),
('application/vnd.sema', 'sema'),
('application/vnd.semd', 'semd'),
('application/vnd.semf', 'semf'),
('application/vnd.shana.informed.formdata', 'ifm'),
('application/vnd.shana.informed.formtemplate', 'itp'),
('application/vnd.shana.informed.interchange', 'iif'),
('application/vnd.shana.informed.package', 'ipk'),
('application/vnd.simtech-mindmapper', 'twd'),
('application/vnd.simtech-mindmapper', 'twds'),
('application/vnd.smaf', 'mmf'),
('application/vnd.smart.teacher', 'teacher'),
('application/vnd.solent.sdkm+xml', 'sdkd'),
('application/vnd.solent.sdkm+xml', 'sdkm'),
('application/vnd.spotfire.dxp', 'dxp'),
('application/vnd.spotfire.sfs', 'sfs'),
('application/vnd.stardivision.calc', 'sdc'),
('application/vnd.stardivision.draw', 'sda'),
('application/vnd.stardivision.impress', 'sdd'),
('application/vnd.stardivision.math', 'smf'),
('application/vnd.stardivision.writer', 'sdw'),
('application/vnd.stardivision.writer', 'vor'),
('application/vnd.stardivision.writer-global', 'sgl'),
('application/vnd.stepmania.package', 'smzip'),
('application/vnd.stepmania.stepchart', 'sm'),
('application/vnd.sun.xml.calc', 'sxc'),
('application/vnd.sun.xml.calc.template', 'stc'),
('application/vnd.sun.xml.draw', 'sxd'),
('application/vnd.sun.xml.draw.template', 'std'),
('application/vnd.sun.xml.impress', 'sxi'),
('application/vnd.sun.xml.impress.template', 'sti'),
('application/vnd.sun.xml.math', 'sxm'),
('application/vnd.sun.xml.writer', 'sxw'),
('application/vnd.sun.xml.writer.global', 'sxg'),
('application/vnd.sun.xml.writer.template', 'stw'),
('application/vnd.sus-calendar', 'sus'),
('application/vnd.sus-calendar', 'susp'),
('application/vnd.svd', 'svd'),
('application/vnd.symbian.install', 'sis'),
('application/vnd.symbian.install', 'sisx'),
('application/vnd.syncml+xml', 'xsm'),
('application/vnd.syncml.dm+wbxml', 'bdm'),
('application/vnd.syncml.dm+xml', 'xdm'),
('application/vnd.tao.intent-module-archive', 'tao'),
('application/vnd.tcpdump.pcap', 'cap'),
('application/vnd.tcpdump.pcap', 'dmp'),
('application/vnd.tcpdump.pcap', 'pcap'),
('application/vnd.tmobile-livetv', 'tmo'),
('application/vnd.trid.tpt', 'tpt'),
('application/vnd.triscape.mxs', 'mxs'),
('application/vnd.trueapp', 'tra'),
('application/vnd.ufdl', 'ufd'),
('application/vnd.ufdl', 'ufdl'),
('application/vnd.uiq.theme', 'utz'),
('application/vnd.umajin', 'umj'),
('application/vnd.unity', 'unityweb'),
('application/vnd.uoml+xml', 'uoml'),
('application/vnd.vcx', 'vcx'),
('application/vnd.visio', 'vsd'),
('application/vnd.visio', 'vss'),
('application/vnd.visio', 'vst'),
('application/vnd.visio', 'vsw'),
('application/vnd.visionary', 'vis'),
('application/vnd.vsf', 'vsf'),
('application/vnd.wap.wbxml', 'wbxml'),
('application/vnd.wap.wmlc', 'wmlc'),
('application/vnd.wap.wmlscriptc', 'wmlsc'),
('application/vnd.webturbo', 'wtb'),
('application/vnd.wolfram.player', 'nbp'),
('application/vnd.wordperfect', 'wpd'),
('application/vnd.wqd', 'wqd'),
('application/vnd.wt.stf', 'stf'),
('application/vnd.xara', 'xar'),
('application/vnd.xfdl', 'xfdl'),
('application/vnd.yamaha.hv-dic', 'hvd'),
('application/vnd.yamaha.hv-script', 'hvs'),
('application/vnd.yamaha.hv-voice', 'hvp'),
('application/vnd.yamaha.openscoreformat', 'osf'),
('application/vnd.yamaha.openscoreformat.osfpvg+xml', 'osfpvg'),
('application/vnd.yamaha.smaf-audio', 'saf'),
('application/vnd.yamaha.smaf-phrase', 'spf'),
('application/vnd.yellowriver-custom-menu', 'cmp'),
('application/vnd.zul', 'zir'),
('application/vnd.zul', 'zirz'),
('application/vnd.zzazz.deck+xml', 'zaz'),
('application/voicexml+xml', 'vxml'),
('application/widget', 'wgt'),
('application/winhlp', 'hlp'),
('application/wsdl+xml', 'wsdl'),
('application/wspolicy+xml', 'wspolicy'),
('application/x-7z-compressed', '7z'),
('application/x-abiword', 'abw'),
('application/x-ace-compressed', 'ace'),
('application/x-apple-diskimage', 'dmg'),
('application/x-authorware-bin', 'aab'),
('application/x-authorware-bin', 'u32'),
('application/x-authorware-bin', 'vox'),
('application/x-authorware-bin', 'x32'),
('application/x-authorware-map', 'aam'),
('application/x-authorware-seg', 'aas'),
('application/x-bcpio', 'bcpio'),
('application/x-bittorrent', 'torrent'),
('application/x-blorb', 'blb'),
('application/x-blorb', 'blorb'),
('application/x-bzip', 'bz'),
('application/x-bzip2', 'boz'),
('application/x-bzip2', 'bz2'),
('application/x-cbr', 'cb7'),
('application/x-cbr', 'cba'),
('application/x-cbr', 'cbr'),
('application/x-cbr', 'cbt'),
('application/x-cbr', 'cbz'),
('application/x-cdlink', 'vcd'),
('application/x-cfs-compressed', 'cfs'),
('application/x-chat', 'chat'),
('application/x-chess-pgn', 'pgn'),
('application/x-conference', 'nsc'),
('application/x-cpio', 'cpio'),
('application/x-csh', 'csh'),
('application/x-debian-package', 'deb'),
('application/x-debian-package', 'udeb'),
('application/x-dgc-compressed', 'dgc'),
('application/x-director', 'cct'),
('application/x-director', 'cst'),
('application/x-director', 'cxt'),
('application/x-director', 'dcr'),
('application/x-director', 'dir'),
('application/x-director', 'dxr'),
('application/x-director', 'fgd'),
('application/x-director', 'swa'),
('application/x-director', 'w3d'),
('application/x-doom', 'wad'),
('application/x-dtbncx+xml', 'ncx'),
('application/x-dtbook+xml', 'dtb'),
('application/x-dtbresource+xml', 'res'),
('application/x-dvi', 'dvi'),
('application/x-envoy', 'evy'),
('application/x-eva', 'eva'),
('application/x-font-bdf', 'bdf'),
('application/x-font-ghostscript', 'gsf'),
('application/x-font-linux-psf', 'psf'),
('application/x-font-otf', 'otf'),
('application/x-font-pcf', 'pcf'),
('application/x-font-snf', 'snf'),
('application/x-font-ttf', 'ttc'),
('application/x-font-ttf', 'ttf'),
('application/x-font-type1', 'afm'),
('application/x-font-type1', 'pfa'),
('application/x-font-type1', 'pfb'),
('application/x-font-type1', 'pfm'),
('application/x-freearc', 'arc'),
('application/x-futuresplash', 'spl'),
('application/x-gca-compressed', 'gca'),
('application/x-glulx', 'ulx'),
('application/x-gnumeric', 'gnumeric'),
('application/x-gramps-xml', 'gramps'),
('application/x-gtar', 'gtar'),
('application/x-gzip', 'gz'),
('application/x-hdf', 'hdf'),
('application/x-install-instructions', 'install'),
('application/x-iso9660-image', 'iso'),
('application/x-java-jnlp-file', 'jnlp'),
('application/x-latex', 'latex'),
('application/x-lzh-compressed', 'lha'),
('application/x-lzh-compressed', 'lzh'),
('application/x-mie', 'mie'),
('application/x-mobipocket-ebook', 'mobi'),
('application/x-mobipocket-ebook', 'prc'),
('application/x-ms-application', 'application'),
('application/x-ms-shortcut', 'lnk'),
('application/x-ms-wmd', 'wmd'),
('application/x-ms-wmz', 'wmz'),
('application/x-ms-xbap', 'xbap'),
('application/x-msaccess', 'mdb'),
('application/x-msbinder', 'obd'),
('application/x-mscardfile', 'crd'),
('application/x-msclip', 'clp'),
('application/x-msdownload', 'bat'),
('application/x-msdownload', 'com'),
('application/x-msdownload', 'dll'),
('application/x-msdownload', 'exe'),
('application/x-msdownload', 'msi'),
('application/x-msmediaview', 'm13'),
('application/x-msmediaview', 'm14'),
('application/x-msmediaview', 'mvb'),
('application/x-msmetafile', 'emf'),
('application/x-msmetafile', 'emz'),
('application/x-msmetafile', 'wmf'),
('application/x-msmetafile', 'wmz'),
('application/x-msmoney', 'mny'),
('application/x-mspublisher', 'pub'),
('application/x-msschedule', 'scd'),
('application/x-msterminal', 'trm'),
('application/x-mswrite', 'wri'),
('application/x-netcdf', 'cdf'),
('application/x-netcdf', 'nc'),
('application/x-nzb', 'nzb'),
('application/x-pkcs12', 'p12'),
('application/x-pkcs12', 'pfx'),
('application/x-pkcs7-certificates', 'p7b'),
('application/x-pkcs7-certificates', 'spc'),
('application/x-pkcs7-certreqresp', 'p7r'),
('application/x-rar-compressed', 'rar'),
('application/x-research-info-systems', 'ris'),
('application/x-sh', 'sh'),
('application/x-shar', 'shar'),
('application/x-shockwave-flash', 'swf'),
('application/x-silverlight-app', 'xap'),
('application/x-sql', 'sql'),
('application/x-stuffit', 'sit'),
('application/x-stuffitx', 'sitx'),
('application/x-subrip', 'srt'),
('application/x-sv4cpio', 'sv4cpio'),
('application/x-sv4crc', 'sv4crc'),
('application/x-t3vm-image', 't3'),
('application/x-tads', 'gam'),
('application/x-tar', 'tar'),
('application/x-tcl', 'tcl'),
('application/x-tex', 'tex'),
('application/x-tex-tfm', 'tfm'),
('application/x-texinfo', 'texi'),
('application/x-texinfo', 'texinfo'),
('application/x-tgif', 'obj'),
('application/x-ustar', 'ustar'),
('application/x-wais-source', 'src'),
('application/x-x509-ca-cert', 'crt'),
('application/x-x509-ca-cert', 'der'),
('application/x-xfig', 'fig'),
('application/x-xliff+xml', 'xlf'),
('application/x-xpinstall', 'xpi'),
('application/x-xz', 'xz'),
('application/x-zmachine', 'z1'),
('application/x-zmachine', 'z2'),
('application/x-zmachine', 'z3'),
('application/x-zmachine', 'z4'),
('application/x-zmachine', 'z5'),
('application/x-zmachine', 'z6'),
('application/x-zmachine', 'z7'),
('application/x-zmachine', 'z8'),
('application/xaml+xml', 'xaml'),
('application/xcap-diff+xml', 'xdf'),
('application/xenc+xml', 'xenc'),
('application/xhtml+xml', 'xht'),
('application/xhtml+xml', 'xhtml'),
('application/xml', 'xml'),
('application/xml', 'xsl'),
('application/xml-dtd', 'dtd'),
('application/xop+xml', 'xop'),
('application/xproc+xml', 'xpl'),
('application/xslt+xml', 'xslt'),
('application/xspf+xml', 'xspf'),
('application/xv+xml', 'mxml'),
('application/xv+xml', 'xhvml'),
('application/xv+xml', 'xvm'),
('application/xv+xml', 'xvml'),
('application/yang', 'yang'),
('application/yin+xml', 'yin'),
('application/zip', 'zip'),
('audio/adpcm', 'adp'),
('audio/basic', 'au'),
('audio/basic', 'snd'),
('audio/midi', 'kar'),
('audio/midi', 'mid'),
('audio/midi', 'midi'),
('audio/midi', 'rmi'),
('audio/mp4', 'mp4a'),
('audio/mpeg', 'm2a'),
('audio/mpeg', 'm3a'),
('audio/mpeg', 'mp2'),
('audio/mpeg', 'mp2a'),
('audio/mpeg', 'mp3'),
('audio/mpeg', 'mpga'),
('audio/ogg', 'oga'),
('audio/ogg', 'ogg'),
('audio/ogg', 'spx'),
('audio/s3m', 's3m'),
('audio/silk', 'sil'),
('audio/vnd.dece.audio', 'uva'),
('audio/vnd.dece.audio', 'uvva'),
('audio/vnd.digital-winds', 'eol'),
('audio/vnd.dra', 'dra'),
('audio/vnd.dts', 'dts'),
('audio/vnd.dts.hd', 'dtshd'),
('audio/vnd.lucent.voice', 'lvp'),
('audio/vnd.ms-playready.media.pya', 'pya'),
('audio/vnd.nuera.ecelp4800', 'ecelp4800'),
('audio/vnd.nuera.ecelp7470', 'ecelp7470'),
('audio/vnd.nuera.ecelp9600', 'ecelp9600'),
('audio/vnd.rip', 'rip'),
('audio/webm', 'weba'),
('audio/x-aac', 'aac'),
('audio/x-aiff', 'aif'),
('audio/x-aiff', 'aifc'),
('audio/x-aiff', 'aiff'),
('audio/x-caf', 'caf'),
('audio/x-flac', 'flac'),
('audio/x-matroska', 'mka'),
('audio/x-mpegurl', 'm3u'),
('audio/x-ms-wax', 'wax'),
('audio/x-ms-wma', 'wma'),
('audio/x-pn-realaudio', 'ra'),
('audio/x-pn-realaudio', 'ram'),
('audio/x-pn-realaudio-plugin', 'rmp'),
('audio/x-wav', 'wav'),
('audio/xm', 'xm'),
('chemical/x-cdx', 'cdx'),
('chemical/x-cif', 'cif'),
('chemical/x-cmdf', 'cmdf'),
('chemical/x-cml', 'cml'),
('chemical/x-csml', 'csml'),
('chemical/x-xyz', 'xyz'),
('image/bmp', 'bmp'),
('image/cgm', 'cgm'),
('image/g3fax', 'g3'),
('image/gif', 'gif'),
('image/ief', 'ief'),
('image/jpeg', 'jpe'),
('image/jpeg', 'jpeg'),
('image/jpeg', 'jpg'),
('image/ktx', 'ktx'),
('image/png', 'png'),
('image/prs.btif', 'btif'),
('image/sgi', 'sgi'),
('image/svg+xml', 'svg'),
('image/svg+xml', 'svgz'),
('image/tiff', 'tif'),
('image/tiff', 'tiff'),
('image/vnd.adobe.photoshop', 'psd'),
('image/vnd.dece.graphic', 'uvg'),
('image/vnd.dece.graphic', 'uvi'),
('image/vnd.dece.graphic', 'uvvg'),
('image/vnd.dece.graphic', 'uvvi'),
('image/vnd.djvu', 'djv'),
('image/vnd.djvu', 'djvu'),
('image/vnd.dvb.subtitle', 'sub'),
('image/vnd.dwg', 'dwg'),
('image/vnd.dxf', 'dxf'),
('image/vnd.fastbidsheet', 'fbs'),
('image/vnd.fpx', 'fpx'),
('image/vnd.fst', 'fst'),
('image/vnd.fujixerox.edmics-mmr', 'mmr'),
('image/vnd.fujixerox.edmics-rlc', 'rlc'),
('image/vnd.ms-modi', 'mdi'),
('image/vnd.ms-photo', 'wdp'),
('image/vnd.net-fpx', 'npx'),
('image/vnd.wap.wbmp', 'wbmp'),
('image/vnd.xiff', 'xif'),
('image/webp', 'webp'),
('image/x-3ds', '3ds'),
('image/x-cmu-raster', 'ras'),
('image/x-cmx', 'cmx'),
('image/x-freehand', 'fh'),
('image/x-freehand', 'fh4'),
('image/x-freehand', 'fh5'),
('image/x-freehand', 'fh7'),
('image/x-freehand', 'fhc'),
('image/x-icon', 'ico'),
('image/x-mrsid-image', 'sid'),
('image/x-pcx', 'pcx'),
('image/x-pict', 'pct'),
('image/x-pict', 'pic'),
('image/x-portable-anymap', 'pnm'),
('image/x-portable-bitmap', 'pbm'),
('image/x-portable-graymap', 'pgm'),
('image/x-portable-pixmap', 'ppm'),
('image/x-rgb', 'rgb'),
('image/x-tga', 'tga'),
('image/x-xbitmap', 'xbm'),
('image/x-xpixmap', 'xpm'),
('image/x-xwindowdump', 'xwd'),
('message/rfc822', 'eml'),
('message/rfc822', 'mime'),
('model/iges', 'iges'),
('model/iges', 'igs'),
('model/mesh', 'mesh'),
('model/mesh', 'msh'),
('model/mesh', 'silo'),
('model/vnd.collada+xml', 'dae'),
('model/vnd.dwf', 'dwf'),
('model/vnd.gdl', 'gdl'),
('model/vnd.gtw', 'gtw'),
('model/vnd.mts', 'mts'),
('model/vnd.vtu', 'vtu'),
('model/vrml', 'vrml'),
('model/vrml', 'wrl'),
('model/x3d+binary', 'x3db'),
('model/x3d+binary', 'x3dbz'),
('model/x3d+vrml', 'x3dv'),
('model/x3d+vrml', 'x3dvz'),
('model/x3d+xml', 'x3d'),
('model/x3d+xml', 'x3dz'),
('text/cache-manifest', 'appcache'),
('text/calendar', 'ics'),
('text/calendar', 'ifb'),
('text/css', 'css'),
('text/csv', 'csv'),
('text/html', 'htm'),
('text/html', 'html'),
('text/n3', 'n3'),
('text/plain', 'conf'),
('text/plain', 'def'),
('text/plain', 'in'),
('text/plain', 'list'),
('text/plain', 'log'),
('text/plain', 'text'),
('text/plain', 'txt'),
('text/prs.lines.tag', 'dsc'),
('text/richtext', 'rtx'),
('text/sgml', 'sgm'),
('text/sgml', 'sgml'),
('text/tab-separated-values', 'tsv'),
('text/troff', 'man'),
('text/troff', 'me'),
('text/troff', 'ms'),
('text/troff', 'roff'),
('text/troff', 't'),
('text/troff', 'tr'),
('text/turtle', 'ttl'),
('text/uri-list', 'uri'),
('text/uri-list', 'uris'),
('text/uri-list', 'urls'),
('text/vcard', 'vcard'),
('text/vnd.curl', 'curl'),
('text/vnd.curl.dcurl', 'dcurl'),
('text/vnd.curl.mcurl', 'mcurl'),
('text/vnd.curl.scurl', 'scurl'),
('text/vnd.dvb.subtitle', 'sub'),
('text/vnd.fly', 'fly'),
('text/vnd.fmi.flexstor', 'flx'),
('text/vnd.graphviz', 'gv'),
('text/vnd.in3d.3dml', '3dml'),
('text/vnd.in3d.spot', 'spot'),
('text/vnd.sun.j2me.app-descriptor', 'jad'),
('text/vnd.wap.wml', 'wml'),
('text/vnd.wap.wmlscript', 'wmls'),
('text/x-asm', 'asm'),
('text/x-asm', 's'),
('text/x-c', 'c'),
('text/x-c', 'cc'),
('text/x-c', 'cpp'),
('text/x-c', 'cxx'),
('text/x-c', 'dic'),
('text/x-c', 'h'),
('text/x-c', 'hh'),
('text/x-fortran', 'f'),
('text/x-fortran', 'f77'),
('text/x-fortran', 'f90'),
('text/x-fortran', 'for'),
('text/x-java-source', 'java'),
('text/x-nfo', 'nfo'),
('text/x-opml', 'opml'),
('text/x-pascal', 'p'),
('text/x-pascal', 'pas'),
('text/x-setext', 'etx'),
('text/x-sfv', 'sfv'),
('text/x-uuencode', 'uu'),
('text/x-vcalendar', 'vcs'),
('text/x-vcard', 'vcf'),
('video/3gpp', '3gp'),
('video/3gpp2', '3g2'),
('video/h261', 'h261'),
('video/h263', 'h263'),
('video/h264', 'h264'),
('video/jpeg', 'jpgv'),
('video/jpm', 'jpgm'),
('video/jpm', 'jpm'),
('video/mj2', 'mj2'),
('video/mj2', 'mjp2'),
('video/mp4', 'mp4'),
('video/mp4', 'mp4v'),
('video/mp4', 'mpg4'),
('video/mpeg', 'm1v'),
('video/mpeg', 'm2v'),
('video/mpeg', 'mpe'),
('video/mpeg', 'mpeg'),
('video/mpeg', 'mpg'),
('video/ogg', 'ogv'),
('video/quicktime', 'mov'),
('video/quicktime', 'qt'),
('video/vnd.dece.hd', 'uvh'),
('video/vnd.dece.hd', 'uvvh'),
('video/vnd.dece.mobile', 'uvm'),
('video/vnd.dece.mobile', 'uvvm'),
('video/vnd.dece.pd', 'uvp'),
('video/vnd.dece.pd', 'uvvp'),
('video/vnd.dece.sd', 'uvs'),
('video/vnd.dece.sd', 'uvvs'),
('video/vnd.dece.video', 'uvv'),
('video/vnd.dece.video', 'uvvv'),
('video/vnd.dvb.file', 'dvb'),
('video/vnd.fvt', 'fvt'),
('video/vnd.mpegurl', 'm4u'),
('video/vnd.mpegurl', 'mxu'),
('video/vnd.ms-playready.media.pyv', 'pyv'),
('video/vnd.uvvu.mp4', 'uvu'),
('video/vnd.uvvu.mp4', 'uvvu'),
('video/vnd.vivo', 'viv'),
('video/webm', 'webm'),
('video/x-f4v', 'f4v'),
('video/x-fli', 'fli'),
('video/x-flv', 'flv'),
('video/x-m4v', 'm4v'),
('video/x-matroska', 'mk3d'),
('video/x-matroska', 'mks'),
('video/x-matroska', 'mkv'),
('video/x-mng', 'mng'),
('video/x-ms-asf', 'asf'),
('video/x-ms-asf', 'asx'),
('video/x-ms-vob', 'vob'),
('video/x-ms-wm', 'wm'),
('video/x-ms-wmv', 'wmv'),
('video/x-ms-wmx', 'wmx'),
('video/x-ms-wvx', 'wvx'),
('video/x-msvideo', 'avi'),
('video/x-sgi-movie', 'movie'),
('video/x-smv', 'smv'),
('x-conference/x-cooltalk', 'ice');

-- --------------------------------------------------------

--
-- Stand-in structure for view `tol2j_docman_nodes`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `tol2j_docman_nodes`;
CREATE TABLE `tol2j_docman_nodes` (
`docman_folder_id` bigint(20) unsigned
,`folder` varchar(255)
,`name` varchar(255)
,`modified_on` datetime
,`modified_by` bigint(20)
,`created_on` datetime
,`created_by` bigint(20)
,`parameters` text
,`type` varchar(6)
);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_tags`
--

DROP TABLE IF EXISTS `tol2j_docman_tags`;
CREATE TABLE `tol2j_docman_tags` (
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `count` int(11) DEFAULT '0',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `locked_by` int(10) UNSIGNED DEFAULT NULL,
  `locked_on` datetime DEFAULT NULL,
  `params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_docman_tags`
--

INSERT INTO `tol2j_docman_tags` (`tag_id`, `title`, `slug`, `count`, `created_by`, `created_on`, `modified_by`, `modified_on`, `locked_by`, `locked_on`, `params`) VALUES
(1, 'board_paper', 'board-paper', 0, 234, '2016-09-29 05:04:38', NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_docman_tags_relations`
--

DROP TABLE IF EXISTS `tol2j_docman_tags_relations`;
CREATE TABLE `tol2j_docman_tags_relations` (
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `row` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_extensions`
--

DROP TABLE IF EXISTS `tol2j_extensions`;
CREATE TABLE `tol2j_extensions` (
  `extension_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_extensions`
--

INSERT INTO `tol2j_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{\"name\":\"com_mailto\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MAILTO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mailto\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{\"name\":\"com_wrapper\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{\"name\":\"com_admin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_ADMIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{\"name\":\"com_banners\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"banners\"}', '{\"purchase_type\":\"3\",\"track_impressions\":\"0\",\"track_clicks\":\"0\",\"metakey_prefix\":\"\",\"save_history\":\"1\",\"history_limit\":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{\"name\":\"com_cache\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CACHE_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{\"name\":\"com_categories\",\"type\":\"component\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{\"name\":\"com_checkin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CHECKIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{\"name\":\"com_contact\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '{\"show_contact_category\":\"hide\",\"save_history\":\"1\",\"history_limit\":10,\"show_contact_list\":\"0\",\"presentation_style\":\"sliders\",\"show_name\":\"1\",\"show_position\":\"1\",\"show_email\":\"0\",\"show_street_address\":\"1\",\"show_suburb\":\"1\",\"show_state\":\"1\",\"show_postcode\":\"1\",\"show_country\":\"1\",\"show_telephone\":\"1\",\"show_mobile\":\"1\",\"show_fax\":\"1\",\"show_webpage\":\"1\",\"show_misc\":\"1\",\"show_image\":\"1\",\"image\":\"\",\"allow_vcard\":\"0\",\"show_articles\":\"0\",\"show_profile\":\"0\",\"show_links\":\"0\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"contact_icons\":\"0\",\"icon_address\":\"\",\"icon_email\":\"\",\"icon_telephone\":\"\",\"icon_mobile\":\"\",\"icon_fax\":\"\",\"icon_misc\":\"\",\"show_headings\":\"1\",\"show_position_headings\":\"1\",\"show_email_headings\":\"0\",\"show_telephone_headings\":\"1\",\"show_mobile_headings\":\"0\",\"show_fax_headings\":\"0\",\"allow_vcard_headings\":\"0\",\"show_suburb_headings\":\"1\",\"show_state_headings\":\"1\",\"show_country_headings\":\"1\",\"show_email_form\":\"1\",\"show_email_copy\":\"1\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"0\",\"redirect\":\"\",\"show_category_crumb\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{\"name\":\"com_cpanel\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CPANEL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{\"name\":\"com_installer\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_INSTALLER_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{\"name\":\"com_languages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"administrator\":\"en-GB\",\"site\":\"en-GB\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{\"name\":\"com_login\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{\"name\":\"com_media\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '{\"upload_extensions\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\",\"upload_maxsize\":\"10\",\"file_path\":\"images\",\"image_path\":\"images\",\"restrict_uploads\":\"1\",\"allowed_media_usergroup\":\"3\",\"check_mime\":\"1\",\"image_extensions\":\"bmp,gif,jpg,png\",\"ignore_extensions\":\"\",\"upload_mime\":\"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip\",\"upload_mime_illegal\":\"text\\/html\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{\"name\":\"com_menus\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MENUS_XML_DESCRIPTION\",\"group\":\"\"}', '{\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{\"name\":\"com_messages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MESSAGES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{\"name\":\"com_modules\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MODULES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{\"name\":\"com_newsfeeds\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"newsfeed_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_feed_image\":\"1\",\"show_feed_description\":\"1\",\"show_item_description\":\"1\",\"feed_character_count\":\"0\",\"feed_display_order\":\"des\",\"float_first\":\"right\",\"float_second\":\"right\",\"show_tags\":\"1\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"0\",\"show_subcat_desc\":\"1\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_headings\":\"1\",\"show_articles\":\"0\",\"show_link\":\"1\",\"show_pagination\":\"1\",\"show_pagination_results\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{\"name\":\"com_plugins\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_PLUGINS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{\"name\":\"com_search\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"search\"}', '{\"enabled\":\"0\",\"search_phrases\":\"1\",\"search_areas\":\"1\",\"show_date\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{\"name\":\"com_templates\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"template_positions_display\":\"1\",\"upload_limit\":\"2\",\"image_formats\":\"gif,bmp,jpg,jpeg,png\",\"source_formats\":\"txt,less,ini,xml,js,php,css\",\"font_formats\":\"woff,ttf,otf\",\"compressed_formats\":\"zip\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{\"name\":\"com_content\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"article_layout\":\"_:default\",\"show_title\":\"1\",\"link_titles\":\"1\",\"show_intro\":\"1\",\"info_block_position\":\"0\",\"info_block_show_title\":\"1\",\"show_category\":\"1\",\"link_category\":\"1\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_author\":\"1\",\"link_author\":\"1\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"1\",\"show_item_navigation\":\"1\",\"show_vote\":\"1\",\"show_readmore\":\"1\",\"show_readmore_title\":\"0\",\"readmore_limit\":\"100\",\"show_tags\":\"1\",\"show_icons\":\"0\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_hits\":\"1\",\"show_noauth\":\"0\",\"urls_position\":\"0\",\"show_publishing_options\":\"1\",\"show_article_options\":\"1\",\"save_history\":\"1\",\"history_limit\":10,\"show_urls_images_frontend\":\"0\",\"show_urls_images_backend\":\"1\",\"targeta\":0,\"targetb\":0,\"targetc\":0,\"float_intro\":\"left\",\"float_fulltext\":\"left\",\"category_layout\":\"_:blog\",\"show_category_heading_title_text\":\"1\",\"show_category_title\":\"0\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"0\",\"show_empty_categories\":\"0\",\"show_no_articles\":\"1\",\"show_subcat_desc\":\"0\",\"show_cat_num_articles\":\"0\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"0\",\"show_cat_num_articles_cat\":\"1\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"1\",\"num_links\":\"0\",\"multi_column_order\":\"0\",\"show_subcategory_content\":\"0\",\"show_pagination_limit\":\"1\",\"filter_field\":\"hide\",\"show_headings\":\"1\",\"list_show_date\":\"0\",\"date_format\":\"\",\"list_show_hits\":\"1\",\"list_show_author\":\"1\",\"orderby_pri\":\"order\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"0\",\"show_featured\":\"show\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\",\"feed_show_readmore\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{\"name\":\"com_config\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONFIG_XML_DESCRIPTION\",\"group\":\"\"}', '{\"filters\":{\"1\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"2\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"10\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"34\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"41\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"20\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"30\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"19\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"21\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"18\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"15\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"42\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"43\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"11\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"33\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"35\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"38\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"49\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"44\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"46\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"48\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"47\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"45\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"36\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"39\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"25\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"31\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"23\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"24\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"22\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"16\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"40\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"28\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"32\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"26\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"27\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"29\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"17\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"37\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"8\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{\"name\":\"com_redirect\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{\"name\":\"com_users\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_USERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"users\"}', '{\"allowUserRegistration\":\"0\",\"new_usertype\":\"2\",\"guest_usergroup\":\"1\",\"sendpassword\":\"0\",\"useractivation\":\"1\",\"mail_to_admin\":\"0\",\"captcha\":\"\",\"frontend_userparams\":\"1\",\"site_language\":\"0\",\"change_login_name\":\"0\",\"reset_count\":\"10\",\"reset_time\":\"1\",\"minimum_length\":\"4\",\"minimum_integers\":\"0\",\"minimum_symbols\":\"0\",\"minimum_uppercase\":\"0\",\"save_history\":\"1\",\"history_limit\":5,\"mailSubjectPrefix\":\"\",\"mailBodySuffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{\"name\":\"com_finder\",\"type\":\"component\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '{\"show_description\":\"1\",\"description_length\":255,\"allow_empty_query\":\"0\",\"show_url\":\"1\",\"show_advanced\":\"1\",\"expand_advanced\":\"0\",\"show_date_filters\":\"0\",\"highlight_terms\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\",\"batch_size\":\"50\",\"memory_table_limit\":30000,\"title_multiplier\":\"1.7\",\"text_multiplier\":\"0.7\",\"meta_multiplier\":\"1.2\",\"path_multiplier\":\"2.0\",\"misc_multiplier\":\"0.3\",\"stemmer\":\"snowball\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{\"name\":\"com_joomlaupdate\",\"type\":\"component\",\"creationDate\":\"February 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"COM_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{\"name\":\"com_tags\",\"type\":\"component\",\"creationDate\":\"December 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"COM_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"tag_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_tag_title\":\"0\",\"tag_list_show_tag_image\":\"0\",\"tag_list_show_tag_description\":\"0\",\"tag_list_image\":\"\",\"show_tag_num_items\":\"0\",\"tag_list_orderby\":\"title\",\"tag_list_orderby_direction\":\"ASC\",\"show_headings\":\"0\",\"tag_list_show_date\":\"0\",\"tag_list_show_item_image\":\"0\",\"tag_list_show_item_description\":\"0\",\"tag_list_item_maximum_characters\":0,\"return_any_or_all\":\"1\",\"include_children\":\"0\",\"maximum\":200,\"tag_list_language_filter\":\"all\",\"tags_layout\":\"_:default\",\"all_tags_orderby\":\"title\",\"all_tags_orderby_direction\":\"ASC\",\"all_tags_show_tag_image\":\"0\",\"all_tags_show_tag_descripion\":\"0\",\"all_tags_tag_maximum_characters\":20,\"all_tags_show_tag_hits\":\"0\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"tag_field_ajax_mode\":\"1\",\"show_feed_link\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{\"name\":\"com_contenthistory\",\"type\":\"component\",\"creationDate\":\"May 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_CONTENTHISTORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contenthistory\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 1, '{\"name\":\"com_ajax\",\"type\":\"component\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_AJAX_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ajax\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{\"name\":\"com_postinstall\",\"type\":\"component\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_POSTINSTALL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 'phputf8', 'library', 'phputf8', '', 0, 1, 1, 1, '{\"name\":\"phputf8\",\"type\":\"library\",\"creationDate\":\"2006\",\"author\":\"Harry Fuecks\",\"copyright\":\"Copyright various authors\",\"authorEmail\":\"hfuecks@gmail.com\",\"authorUrl\":\"http:\\/\\/sourceforge.net\\/projects\\/phputf8\",\"version\":\"0.5\",\"description\":\"LIB_PHPUTF8_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phputf8\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 'Joomla! Platform', 'library', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"Joomla! Platform\",\"type\":\"library\",\"creationDate\":\"2008\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"https:\\/\\/www.joomla.org\",\"version\":\"13.1\",\"description\":\"LIB_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"mediaversion\":\"ee11ad826670bc2e7bdeffb8dabb612b\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 'IDNA Convert', 'library', 'idna_convert', '', 0, 1, 1, 1, '{\"name\":\"IDNA Convert\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"phlyLabs\",\"copyright\":\"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de\",\"authorEmail\":\"phlymail@phlylabs.de\",\"authorUrl\":\"http:\\/\\/phlylabs.de\",\"version\":\"0.8.0\",\"description\":\"LIB_IDNA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"idna_convert\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{\"name\":\"FOF\",\"type\":\"library\",\"creationDate\":\"2015-04-22 13:15:32\",\"author\":\"Nicholas K. Dionysopoulos \\/ Akeeba Ltd\",\"copyright\":\"(C)2011-2015 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"2.4.3\",\"description\":\"LIB_FOF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fof\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 'PHPass', 'library', 'phpass', '', 0, 1, 1, 1, '{\"name\":\"PHPass\",\"type\":\"library\",\"creationDate\":\"2004-2006\",\"author\":\"Solar Designer\",\"copyright\":\"\",\"authorEmail\":\"solar@openwall.com\",\"authorUrl\":\"http:\\/\\/www.openwall.com\\/phpass\\/\",\"version\":\"0.3\",\"description\":\"LIB_PHPASS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpass\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_archive\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_archive\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_popular\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{\"name\":\"mod_banners\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_banners\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{\"name\":\"mod_breadcrumbs\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BREADCRUMBS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_breadcrumbs\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{\"name\":\"mod_footer\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FOOTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_footer\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_news\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_news\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{\"name\":\"mod_random_image\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RANDOM_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_random_image\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{\"name\":\"mod_related_items\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RELATED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_related_items\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{\"name\":\"mod_search\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_search\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{\"name\":\"mod_stats\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{\"name\":\"mod_syndicate\",\"type\":\"module\",\"creationDate\":\"May 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SYNDICATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_syndicate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_users_latest\",\"type\":\"module\",\"creationDate\":\"December 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_USERS_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_users_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{\"name\":\"mod_whosonline\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WHOSONLINE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_whosonline\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{\"name\":\"mod_wrapper\",\"type\":\"module\",\"creationDate\":\"October 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_category\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_category\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_categories\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{\"name\":\"mod_languages\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"MOD_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_languages\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{\"name\":\"mod_finder\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FINDER_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{\"name\":\"mod_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{\"name\":\"mod_logged\",\"type\":\"module\",\"creationDate\":\"January 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGGED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_logged\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"March 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{\"name\":\"mod_popular\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{\"name\":\"mod_quickicon\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_QUICKICON_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_quickicon\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{\"name\":\"mod_status\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_status\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{\"name\":\"mod_submenu\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SUBMENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_submenu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{\"name\":\"mod_title\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TITLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_title\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{\"name\":\"mod_toolbar\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TOOLBAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_toolbar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{\"name\":\"mod_multilangstatus\",\"type\":\"module\",\"creationDate\":\"September 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MULTILANGSTATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_multilangstatus\"}', '{\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{\"name\":\"mod_version\",\"type\":\"module\",\"creationDate\":\"January 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_VERSION_XML_DESCRIPTION\",\"group\":\"\"}', '{\"format\":\"short\",\"product\":\"1\",\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{\"name\":\"mod_stats_admin\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats_admin\"}', '{\"serverinfo\":\"0\",\"siteinfo\":\"0\",\"counter\":\"0\",\"increase\":\"0\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_popular\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_popular\"}', '{\"maximum\":\"5\",\"timeframe\":\"alltime\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_similar\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_SIMILAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_similar\"}', '{\"maximum\":\"5\",\"matchtype\":\"any\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_gmail\",\"type\":\"plugin\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_GMAIL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"gmail\"}', '{\"applysuffix\":\"0\",\"suffix\":\"\",\"verifypeer\":\"1\",\"user_blacklist\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{\"name\":\"plg_authentication_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_ldap\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LDAP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ldap\"}', '{\"host\":\"\",\"port\":\"389\",\"use_ldapV3\":\"0\",\"negotiate_tls\":\"0\",\"no_referrals\":\"0\",\"auth_method\":\"bind\",\"base_dn\":\"\",\"search_string\":\"\",\"users_dn\":\"\",\"username\":\"admin\",\"password\":\"bobby7\",\"ldap_fullname\":\"fullName\",\"ldap_email\":\"mail\",\"ldap_uid\":\"uid\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0);
INSERT INTO `tol2j_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(403, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_contact\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.2\",\"description\":\"PLG_CONTENT_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_emailcloak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"emailcloak\"}', '{\"mode\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_loadmodule\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOADMODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"loadmodule\"}', '{\"style\":\"xhtml\"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '{\"title\":\"1\",\"multipage_toc\":\"1\",\"showall\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagenavigation\",\"type\":\"plugin\",\"creationDate\":\"January 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_PAGENAVIGATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagenavigation\"}', '{\"position\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_vote\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_VOTE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"vote\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_codemirror\",\"type\":\"plugin\",\"creationDate\":\"28 March 2011\",\"author\":\"Marijn Haverbeke\",\"copyright\":\"Copyright (C) 2014 by Marijn Haverbeke <marijnh@gmail.com> and others\",\"authorEmail\":\"marijnh@gmail.com\",\"authorUrl\":\"http:\\/\\/codemirror.net\\/\",\"version\":\"5.17.0\",\"description\":\"PLG_CODEMIRROR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"codemirror\"}', '{\"lineNumbers\":\"1\",\"lineWrapping\":\"1\",\"matchTags\":\"1\",\"matchBrackets\":\"1\",\"marker-gutter\":\"1\",\"autoCloseTags\":\"1\",\"autoCloseBrackets\":\"1\",\"autoFocus\":\"1\",\"theme\":\"default\",\"tabmode\":\"indent\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_none\",\"type\":\"plugin\",\"creationDate\":\"September 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_NONE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"none\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{\"name\":\"plg_editors_tinymce\",\"type\":\"plugin\",\"creationDate\":\"2005-2016\",\"author\":\"Ephox Corporation\",\"copyright\":\"Ephox Corporation\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"http:\\/\\/www.tinymce.com\",\"version\":\"4.4.0\",\"description\":\"PLG_TINY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tinymce\"}', '{\"mode\":\"1\",\"skin\":\"0\",\"mobile\":\"0\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"extended_elements\":\"\",\"html_height\":\"550\",\"html_width\":\"750\",\"resizing\":\"1\",\"element_path\":\"1\",\"fonts\":\"1\",\"paste\":\"1\",\"searchreplace\":\"1\",\"insertdate\":\"1\",\"colors\":\"1\",\"table\":\"1\",\"smilies\":\"1\",\"hr\":\"1\",\"link\":\"1\",\"media\":\"1\",\"print\":\"1\",\"directionality\":\"1\",\"fullscreen\":\"1\",\"alignment\":\"1\",\"visualchars\":\"1\",\"visualblocks\":\"1\",\"nonbreaking\":\"1\",\"template\":\"1\",\"blockquote\":\"1\",\"wordcount\":\"1\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"inlinepopups\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_article\",\"type\":\"plugin\",\"creationDate\":\"October 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_ARTICLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"article\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_image\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"image\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_readmore\",\"type\":\"plugin\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_READMORE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"readmore\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_categories\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_contacts\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_content\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_languagefilter\",\"type\":\"plugin\",\"creationDate\":\"July 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagefilter\"}', '{\"detect_browser\":\"0\",\"automatic_change\":\"1\",\"item_associations\":\"1\",\"remove_default_prefix\":\"0\",\"lang_cookie\":\"0\",\"alternate_meta\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_p3p\",\"type\":\"plugin\",\"creationDate\":\"September 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_P3P_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"p3p\"}', '{\"headers\":\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_cache\",\"type\":\"plugin\",\"creationDate\":\"February 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CACHE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cache\"}', '{\"browsercache\":\"0\",\"cachetime\":\"15\"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_debug\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_DEBUG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"debug\"}', '{\"profile\":\"1\",\"queries\":\"1\",\"memory\":\"1\",\"language_files\":\"1\",\"language_strings\":\"1\",\"strip-first\":\"1\",\"strip-prefix\":\"\",\"strip-suffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_log\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"log\"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_redirect\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"redirect\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_remember\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_REMEMBER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"remember\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_sef\",\"type\":\"plugin\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sef\"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_logout\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logout\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_contactcreator\",\"type\":\"plugin\",\"creationDate\":\"August 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTACTCREATOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contactcreator\"}', '{\"autowebpage\":\"\",\"category\":\"34\",\"autopublish\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{\"name\":\"plg_user_joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"strong_passwords\":\"1\",\"autoregister\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_profile\",\"type\":\"plugin\",\"creationDate\":\"January 2008\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_PROFILE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"profile\"}', '{\"register-require_address1\":\"1\",\"register-require_address2\":\"1\",\"register-require_city\":\"1\",\"register-require_region\":\"1\",\"register-require_country\":\"1\",\"register-require_postal_code\":\"1\",\"register-require_phone\":\"1\",\"register-require_website\":\"1\",\"register-require_favoritebook\":\"1\",\"register-require_aboutme\":\"1\",\"register-require_tos\":\"1\",\"register-require_dob\":\"1\",\"profile-require_address1\":\"1\",\"profile-require_address2\":\"1\",\"profile-require_city\":\"1\",\"profile-require_region\":\"1\",\"profile-require_country\":\"1\",\"profile-require_postal_code\":\"1\",\"profile-require_phone\":\"1\",\"profile-require_website\":\"1\",\"profile-require_favoritebook\":\"1\",\"profile-require_aboutme\":\"1\",\"profile-require_tos\":\"1\",\"profile-require_dob\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{\"name\":\"plg_extension_joomla\",\"type\":\"plugin\",\"creationDate\":\"May 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_languagecode\",\"type\":\"plugin\",\"creationDate\":\"November 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagecode\"}', '{\"ar-aa\":\"\",\"en-gb\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_joomlaupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomlaupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_extensionupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"extensionupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{\"name\":\"plg_captcha_recaptcha\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.0\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha\"}', '{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_highlight\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{\"name\":\"plg_content_finder\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_FINDER_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_categories\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_contacts\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_content\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_tags\",\"type\":\"plugin\",\"creationDate\":\"February 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_totp\",\"type\":\"plugin\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"totp\"}', '{\"section\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{\"name\":\"plg_authentication_cookie\",\"type\":\"plugin\",\"creationDate\":\"July 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_COOKIE_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_yubikey\",\"type\":\"plugin\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"yubikey\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_tags\",\"type\":\"plugin\",\"creationDate\":\"March 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"search_limit\":\"50\",\"show_tagged_items\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(452, 'plg_system_updatenotification', 'plugin', 'updatenotification', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_updatenotification\",\"type\":\"plugin\",\"creationDate\":\"May 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_UPDATENOTIFICATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"updatenotification\"}', '{\"lastrun\":1509444460}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(453, 'plg_editors-xtd_module', 'plugin', 'module', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_module\",\"type\":\"plugin\",\"creationDate\":\"October 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_MODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"module\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(454, 'plg_system_stats', 'plugin', 'stats', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_stats\",\"type\":\"plugin\",\"creationDate\":\"November 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"stats\"}', '{\"mode\":1,\"lastrun\":1509429589,\"unique_id\":\"67e29b79898ccbf829924b216a424e0d993c5547\",\"interval\":12}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(455, 'plg_installer_packageinstaller', 'plugin', 'packageinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"plg_installer_packageinstaller\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_PACKAGEINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"packageinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(456, 'PLG_INSTALLER_FOLDERINSTALLER', 'plugin', 'folderinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_FOLDERINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_FOLDERINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"folderinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(457, 'PLG_INSTALLER_URLINSTALLER', 'plugin', 'urlinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_URLINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_URLINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"urlinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(503, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{\"name\":\"beez3\",\"type\":\"template\",\"creationDate\":\"25 November 2009\",\"author\":\"Angie Radtke\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"a.radtke@derauftritt.de\",\"authorUrl\":\"http:\\/\\/www.der-auftritt.de\",\"version\":\"3.1.0\",\"description\":\"TPL_BEEZ3_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"navposition\":\"center\",\"templatecolor\":\"nature\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{\"name\":\"hathor\",\"type\":\"template\",\"creationDate\":\"May 2010\",\"author\":\"Andrea Tarr\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"3.0.0\",\"description\":\"TPL_HATHOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"showSiteName\":\"0\",\"colourChoice\":\"0\",\"boldText\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{\"name\":\"protostar\",\"type\":\"template\",\"creationDate\":\"4\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_PROTOSTAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{\"name\":\"isis\",\"type\":\"template\",\"creationDate\":\"3\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_ISIS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"en-GB site language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"en-GB administrator language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"files_joomla\",\"type\":\"file\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2016 Open Source Matters. All rights reserved\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"FILES_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(802, 'English (en-GB) Language Pack', 'package', 'pkg_en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB) Language Pack\",\"type\":\"package\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2.1\",\"description\":\"en-GB language pack\",\"group\":\"\",\"filename\":\"pkg_en-GB\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 'Helix3 - Ajax', 'plugin', 'helix3', 'ajax', 0, 1, 1, 0, '{\"name\":\"Helix3 - Ajax\",\"type\":\"plugin\",\"creationDate\":\"Jan 2015\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.2\",\"description\":\"Helix3 Framework - Joomla Template Framework by JoomShaper\",\"group\":\"\",\"filename\":\"helix3\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 'System - Helix3 Framework', 'plugin', 'helix3', 'system', 0, 1, 1, 0, '{\"name\":\"System - Helix3 Framework\",\"type\":\"plugin\",\"creationDate\":\"Jan 2015\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.2\",\"description\":\"Helix3 Framework - Joomla Template Framework by JoomShaper\",\"group\":\"\",\"filename\":\"helix3\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10003, 'shaper_helix3', 'template', 'shaper_helix3', '', 0, 1, 1, 0, '{\"name\":\"shaper_helix3\",\"type\":\"template\",\"creationDate\":\"Jan 2016\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2016 JoomShaper.com. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"http:\\/\\/www.joomshaper.com\",\"version\":\"1.4\",\"description\":\"Shaper Helix3 - Starter Template of Helix3 framework\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"sticky_header\":\"1\",\"boxed_layout\":\"0\",\"logo_type\":\"image\",\"logo_position\":\"logo\",\"body_bg_repeat\":\"inherit\",\"body_bg_size\":\"inherit\",\"body_bg_attachment\":\"inherit\",\"body_bg_position\":\"0 0\",\"enabled_copyright\":\"1\",\"copyright_position\":\"footer1\",\"copyright\":\"\\u00a9 2015 Your Company. All Rights Reserved. Designed By JoomShaper\",\"show_social_icons\":\"1\",\"social_position\":\"top1\",\"enable_contactinfo\":\"1\",\"contact_position\":\"top2\",\"contact_phone\":\"+228 872 4444\",\"contact_email\":\"contact@email.com\",\"comingsoon_mode\":\"0\",\"comingsoon_title\":\"Coming Soon Title\",\"comingsoon_date\":\"5-10-2018\",\"comingsoon_content\":\"Coming soon content\",\"preset\":\"preset1\",\"preset1_bg\":\"#ffffff\",\"preset1_text\":\"#000000\",\"preset1_major\":\"#22b8f0\",\"preset2_bg\":\"#f2f2f2\",\"preset2_text\":\"#666666\",\"preset2_major\":\"#22b8f0\",\"preset3_bg\":\"#f2f2f2\",\"preset3_text\":\"#666666\",\"preset3_major\":\"#22b8f0\",\"menu\":\"mainmenu\",\"menu_type\":\"1\",\"menu_animation\":\"1\",\"enable_body_font\":\"0\",\"enable_h1_font\":\"0\",\"enable_h2_font\":\"0\",\"enable_h3_font\":\"0\",\"enable_h4_font\":\"0\",\"enable_h5_font\":\"0\",\"enable_h6_font\":\"0\",\"enable_navigation_font\":\"0\",\"enable_custom_font\":\"0\",\"compress_css\":\"0\",\"compress_js\":\"0\",\"lessoption\":\"0\",\"show_post_format\":\"1\",\"commenting_engine\":\"disqus\",\"disqus_devmode\":\"0\",\"intensedebate_acc\":\"\",\"fb_width\":\"500\",\"fb_cpp\":\"10\",\"comments_count\":\"1\",\"social_share\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 'SP Page Builder', 'component', 'com_sppagebuilder', '', 1, 1, 0, 0, '{\"name\":\"SP Page Builder\",\"type\":\"component\",\"creationDate\":\"Sep 2014\",\"author\":\"JoomShaper\",\"copyright\":\"Copyright @ 2010 - 2016 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"http:\\/\\/www.joomshaper.com\",\"version\":\"1.3\",\"description\":\"Most powerful drag and drop page builder for Joomla 3.4 or later.\",\"group\":\"\",\"filename\":\"sppagebuilder\"}', '{\"fontawesome\":\"1\",\"disableanimatecss\":\"0\",\"disablecss\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 'mod_sppagebuilder_icons', 'module', 'mod_sppagebuilder_icons', '', 1, 1, 2, 0, '{\"name\":\"mod_sppagebuilder_icons\",\"type\":\"module\",\"creationDate\":\"August 2014\",\"author\":\"JoomShaper\",\"copyright\":\"Copyright (C) 2010 - 2016 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.0.2\",\"description\":\"MOD_SPPAGEBUILDER_ICONS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_sppagebuilder_icons\"}', '[]', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10006, 'mod_sppagebuilder_admin_menu', 'module', 'mod_sppagebuilder_admin_menu', '', 1, 1, 2, 0, '{\"name\":\"mod_sppagebuilder_admin_menu\",\"type\":\"module\",\"creationDate\":\"August 2014\",\"author\":\"JoomShaper\",\"copyright\":\"Copyright (C) 2010 - 2016 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.0.2\",\"description\":\"MOD_SPPAGEBUILDER_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_sppagebuilder_admin_menu\"}', '[]', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10007, 'SP Simple Portfolio', 'component', 'com_spsimpleportfolio', '', 1, 1, 0, 0, '{\"name\":\"SP Simple Portfolio\",\"type\":\"component\",\"creationDate\":\"December 2015\",\"author\":\"JoomShaper\",\"copyright\":\"Copyright (c) 2010- 2015 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"http:\\/\\/www.joomshaper.com\",\"version\":\"1.3\",\"description\":\"Simple Portfolio Component for Joomla 3.3+\",\"group\":\"\",\"filename\":\"spsimpleportfolio\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10008, 'SP Simple Portfolio Module', 'module', 'mod_spsimpleportfolio', '', 0, 1, 0, 0, '{\"name\":\"SP Simple Portfolio Module\",\"type\":\"module\",\"creationDate\":\"December 2014\",\"author\":\"JoomShaper\",\"copyright\":\"Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.2\",\"description\":\"Module to display latest item from SP Simple Portfolio\",\"group\":\"\",\"filename\":\"mod_spsimpleportfolio\"}', '{\"show_filter\":\"1\",\"layout_type\":\"default\",\"columns\":\"3\",\"thumbnail_type\":\"masonry\",\"limit\":\"12\",\"cache\":\"1\",\"cache_time\":\"900\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10014, 'plg_system_joomlatools', 'plugin', 'joomlatools', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_joomlatools\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2016 Timble CVBA (http:\\/\\/www.timble.net)\",\"authorEmail\":\"support@joomlatools.com\",\"authorUrl\":\"www.joomlatools.com\",\"version\":\"3.0.0\",\"description\":\"\",\"group\":\"\",\"filename\":\"joomlatools\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10015, 'com_docman', 'component', 'com_docman', '', 1, 1, 0, 0, '{\"name\":\"com_docman\",\"type\":\"component\",\"creationDate\":\"January 2016\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2011 - 2014 Timble CVBA (http:\\/\\/www.timble.net)\",\"authorEmail\":\"support@joomlatools.com\",\"authorUrl\":\"www.joomlatools.com\",\"version\":\"3.0.0-rc.1\",\"description\":\"\",\"group\":\"\",\"filename\":\"docman\"}', '{\"can_edit_own\":1,\"can_delete_own\":1,\"can_create_tag\":1,\"automatic_document_creation\":1,\"automatic_category_creation\":\"1\",\"automatic_humanized_titles\":1,\"default_owner\":\"234\",\"_action\":\"apply\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10016, 'plg_editors-xtd_doclink', 'plugin', 'doclink', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_doclink\",\"type\":\"plugin\",\"creationDate\":\"January 2016\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2011 - 2014 Timble CVBA (http:\\/\\/www.timble.net)\",\"authorEmail\":\"support@joomlatools.com\",\"authorUrl\":\"www.joomlatools.com\",\"version\":\"3.0.0\",\"description\":\"PLG_DOCLINK_DESCRIPTION\",\"group\":\"\",\"filename\":\"doclink\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10017, 'plg_search_docman', 'plugin', 'docman', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_docman\",\"type\":\"plugin\",\"creationDate\":\"January 2016\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2011 - 2014 Timble CVBA (http:\\/\\/www.timble.net)\",\"authorEmail\":\"support@joomlatools.com\",\"authorUrl\":\"www.joomlatools.com\",\"version\":\"3.0.0\",\"description\":\"\",\"group\":\"\",\"filename\":\"docman\"}', '{\"search_limit\":\"50\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10018, 'plg_finder_docman', 'plugin', 'docman', 'finder', 0, 0, 1, 0, '{\"name\":\"plg_finder_docman\",\"type\":\"plugin\",\"creationDate\":\"January 2016\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2014 Timble CVBA\",\"authorEmail\":\"info@joomlatools.eu\",\"authorUrl\":\"www.joomlatools.eu\",\"version\":\"3.0.0\",\"description\":\"\",\"group\":\"\",\"filename\":\"docman\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10019, 'plg_content_doclink', 'plugin', 'doclink', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_doclink\",\"type\":\"plugin\",\"creationDate\":\"January 2016\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2011 - 2014 Timble CVBA (http:\\/\\/www.timble.net)\",\"authorEmail\":\"support@joomlatools.com\",\"authorUrl\":\"www.joomlatools.com\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_DOCLINK_DESCRIPTION\",\"group\":\"\",\"filename\":\"doclink\"}', '{\"show_icon\":\"1\",\"show_size\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10020, 'plg_system_scheduler', 'plugin', 'scheduler', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_scheduler\",\"type\":\"plugin\",\"creationDate\":\"September 2015\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2015 Timble CVBA (http:\\/\\/www.timble.net)\",\"authorEmail\":\"support@joomlatools.com\",\"authorUrl\":\"www.joomlatools.com\",\"version\":\"1.0.0\",\"description\":\"\",\"group\":\"\",\"filename\":\"scheduler\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10021, 'plg_system_joomlatoolsupdater', 'plugin', 'joomlatoolsupdater', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_joomlatoolsupdater\",\"type\":\"plugin\",\"creationDate\":\"June 2016\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2016 Timble CVBA\",\"authorEmail\":\"info@joomlatools.com\",\"authorUrl\":\"www.joomlatools.com\",\"version\":\"1.0.0\",\"description\":\"\",\"group\":\"\",\"filename\":\"joomlatoolsupdater\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10022, 'mod_docman_documents', 'module', 'mod_docman_documents', '', 0, 1, 0, 0, '{\"name\":\"mod_docman_documents\",\"type\":\"module\",\"creationDate\":\"January 2016\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2011 - 2014 Timble CVBA (http:\\/\\/www.timble.net)\",\"authorEmail\":\"support@joomlatools.com\",\"authorUrl\":\"www.joomlatools.com\",\"version\":\"3.0.0\",\"description\":\"MOD_DOCMAN_DOCUMENTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_docman_documents\"}', '{\"page\":\"\",\"category\":\"\",\"tag\":\"\",\"own\":\"0\",\"limit\":\"10\",\"sort\":\"reverse_created_on\",\"include_child_categories\":\"1\",\"show_icon\":\"1\",\"show_category\":\"0\",\"show_created\":\"0\",\"show_size\":\"0\",\"show_hits\":\"0\",\"show_recent\":\"1\",\"show_popular\":\"1\",\"download_in_blank_page\":\"0\",\"track_downloads\":\"1\",\"link_to_download\":\"0\",\"days_for_new\":\"7\",\"hits_for_popular\":\"100\",\"layout\":\"_:default.html\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10023, 'mod_docman_categories', 'module', 'mod_docman_categories', '', 0, 1, 0, 0, '{\"name\":\"mod_docman_categories\",\"type\":\"module\",\"creationDate\":\"January 2016\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2011 - 2014 Timble CVBA (http:\\/\\/www.timble.net)\",\"authorEmail\":\"support@joomlatools.com\",\"authorUrl\":\"www.joomlatools.com\",\"version\":\"3.0.0\",\"description\":\"MOD_DOCMAN_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_docman_categories\"}', '{\"page\":\"\",\"parent\":\"\",\"own\":\"0\",\"level\":\"\",\"limit\":\"10\",\"sort\":\"custom\",\"show_icon\":\"1\",\"layout\":\"_:default.html\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10025, 'com_komento', 'component', 'com_komento', '', 1, 1, 0, 0, '{\"name\":\"com_komento\",\"type\":\"component\",\"creationDate\":\"25th April 2016\",\"author\":\"Stack Ideas Sdn Bhd\",\"copyright\":\"Copyright 2009 - 2016 StackIdeas. All rights reserved.\",\"authorEmail\":\"support@stackideas.com\",\"authorUrl\":\"http:\\/\\/www.stackideas.com\",\"version\":\"2.0.9\",\"description\":\"\\n\\t\\tKomento is a Joomla! comment component.\\n\\t\",\"group\":\"\",\"filename\":\"komento\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10026, 'Content - Komento', 'plugin', 'komento', 'content', 0, 1, 1, 0, '{\"name\":\"Content - Komento\",\"type\":\"plugin\",\"creationDate\":\"25th April 2016\",\"author\":\"Stack Ideas Sdn Bhd\",\"copyright\":\"Copyright 2009 - 2016 StackIdeas. All rights reserved.\",\"authorEmail\":\"support@stackideas.com\",\"authorUrl\":\"http:\\/\\/www.stackideas.com\",\"version\":\"1.0\",\"description\":\"This plugin adds a discussion\\/comment interface at the bottom of the article\",\"group\":\"\",\"filename\":\"komento\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10027, 'System - Komento', 'plugin', 'komento', 'system', 0, 1, 1, 0, '{\"name\":\"System - Komento\",\"type\":\"plugin\",\"creationDate\":\"19th April 2016\",\"author\":\"Stack Ideas Sdn Bhd\",\"copyright\":\"Copyright 2009 - 2016 StackIdeas. All rights reserved.\",\"authorEmail\":\"support@stackideas.com\",\"authorUrl\":\"http:\\/\\/www.stackideas.com\",\"version\":\"1.0\",\"description\":\"This plugin adds a discussion\\/comment interface at the bottom of the article\",\"group\":\"\",\"filename\":\"komento\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10028, 'User - Komento Users', 'plugin', 'komentousers', 'user', 0, 1, 1, 0, '{\"name\":\"User - Komento Users\",\"type\":\"plugin\",\"creationDate\":\"19th April 2016\",\"author\":\"Stack Ideas Sdn Bhd\",\"copyright\":\"Copyright 2009 - 2016 StackIdeas. All rights reserved.\",\"authorEmail\":\"support@stackideas.com\",\"authorUrl\":\"http:\\/\\/www.stackideas.com\",\"version\":\"1.0.0\",\"description\":\"Komento user plugin. This plugin is responsible to delete user\'s related records.\",\"group\":\"\",\"filename\":\"komentousers\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10029, 'Komento Activities', 'module', 'mod_komento_activities', '', 0, 1, 0, 0, '{\"name\":\"Komento Activities\",\"type\":\"module\",\"creationDate\":\"19th April 2016\",\"author\":\"Stack Ideas Sdn Bhd\",\"copyright\":\"Copyright 2009 - 2016 StackIdeas. All rights reserved.\",\"authorEmail\":\"support@stackideas.com\",\"authorUrl\":\"http:\\/\\/www.stackideas.com\",\"version\":\"1.0.4\",\"description\":\"Display activities from Komento\",\"group\":\"\",\"filename\":\"mod_komento_activities\"}', '{\"limit\":\"5\",\"component\":\"all\",\"includelikes\":\"1\",\"includecomments\":\"1\",\"includereplies\":\"1\",\"showcomment\":\"1\",\"maxcommentlength\":\"100\",\"maxtitlelength\":\"30\",\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `tol2j_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(10030, 'Komento Comments', 'module', 'mod_komento_comments', '', 0, 1, 0, 0, '{\"name\":\"Komento Comments\",\"type\":\"module\",\"creationDate\":\"19th April 2016\",\"author\":\"Stack Ideas Sdn Bhd\",\"copyright\":\"Copyright 2009 - 2016 StackIdeas. All rights reserved.\",\"authorEmail\":\"support@stackideas.com\",\"authorUrl\":\"http:\\/\\/www.stackideas.com\",\"version\":\"1.0.7\",\"description\":\"Display comments from Komento\",\"group\":\"\",\"filename\":\"mod_komento_comments\"}', '{\"limit\":\"5\",\"component\":\"all\",\"filter\":\"all\",\"category\":\"\",\"articleId\":\"\",\"userId\":\"\",\"sort\":\"latest\",\"random\":\"0\",\"filtersticked\":\"0\",\"showtitle\":\"1\",\"showcomponent\":\"1\",\"showavatar\":\"1\",\"showauthor\":\"1\",\"maxcommentlength\":\"100\",\"maxtitlelength\":\"30\",\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10031, 'mod_user_section', 'module', 'mod_user_section', '', 0, 1, 0, 0, '{\"name\":\"mod_user_section\",\"type\":\"module\",\"creationDate\":\"2016 october 12\",\"author\":\"Unknown\",\"copyright\":\"\",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"MOD_USER_SECTION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_user_section\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10032, 'plg_docman_notify', 'plugin', 'notify', 'docman', 0, 1, 1, 0, '{\"name\":\"plg_docman_notify\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomlatools\",\"copyright\":\"Copyright (C) 2011 - 2016 Timble CVBA (http:\\/\\/www.timble.net)\",\"authorEmail\":\"support@joomlatools.com\",\"authorUrl\":\"www.joomlatools.com\",\"version\":\"2.1.0\",\"description\":\"PLG_DOCMAN_NOTIFY_DESCRIPTION\",\"group\":\"\",\"filename\":\"notify\"}', '{\"notify_document\":\"1\",\"notify_document_group\":\"0\",\"notify_category\":\"1\",\"notify_add\":\"1\",\"notify_edit\":\"1\",\"notify_delete\":\"1\",\"notify_submit\":\"1\",\"notify_publish\":\"1\",\"notify_assign\":\"1\",\"notify_download\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10033, 'com_tmea_countries', 'component', 'com_tmea_countries', '', 1, 1, 0, 0, '{\"name\":\"com_tmea_countries\",\"type\":\"component\",\"creationDate\":\"2017 july 20\",\"author\":\"Unknown\",\"copyright\":\"\",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"COM_TMEA_COUNTRIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tmea_countries\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10034, 'com_tmea_programs', 'component', 'com_tmea_programs', '', 1, 1, 0, 0, '{\"name\":\"com_tmea_programs\",\"type\":\"component\",\"creationDate\":\"2017 july 20\",\"author\":\"Michael Buluma\",\"copyright\":\"\",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"COM_TMEA_PROGRAMS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tmea_programs\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10039, 'JEvents', 'component', 'com_jevents', '', 1, 1, 0, 0, '{\"name\":\"JEvents\",\"type\":\"component\",\"creationDate\":\"September 2015\",\"author\":\"GWE Systems Ltd \",\"copyright\":\"(C) 2009-2015 GWE Systems Ltd, 2006-2008 JEvents Project Group\",\"authorEmail\":\"\",\"authorUrl\":\"http:\\/\\/www.jevents.net\",\"version\":\"3.2.21\",\"description\":\"Events, meetings and more\",\"group\":\"\",\"filename\":\"manifest\"}', '{\"com_difficulty\":\"3\",\"clubcode\":\"\",\"com_calViewName\":\"flat\",\"darktemplate\":\"0\",\"com_dateformat\":\"1\",\"com_calHeadline\":\"comp\",\"com_calUseIconic\":\"1\",\"iconstoshow\":[\"byyear\",\"bymonth\",\"byweek\",\"byday\",\"search\"],\"com_navbarcolor\":\"green\",\"com_earliestyear\":\"2017\",\"com_latestyear\":\"2020\",\"com_starday\":\"0\",\"com_print_icon_view\":\"1\",\"com_email_icon_view\":\"1\",\"fixjquery\":\"1\",\"bootstrapcss\":\"1\",\"bootstrapjs\":\"1\",\"mergemenus\":\"0\",\"show_adminpanel\":\"1\",\"icaltimezonelive\":\"\",\"regexsearch\":\"1\",\"catseparator\":\"|\",\"jevadmin\":\"234\",\"authorisedonly\":\"1\",\"jevpublishown\":\"0\",\"category_allow_deny\":\"1\",\"rules\":{\"core.manage\":{\"1\":\"\",\"2\":\"\",\"10\":\"\",\"34\":\"\",\"41\":\"\",\"20\":\"\",\"30\":\"\",\"19\":\"\",\"21\":\"\",\"18\":\"\",\"15\":\"\",\"42\":\"\",\"43\":\"\",\"11\":\"\",\"33\":\"\",\"35\":\"\",\"38\":\"\",\"49\":\"\",\"44\":\"\",\"46\":\"\",\"48\":\"\",\"47\":\"\",\"45\":\"\",\"36\":\"\",\"39\":\"\",\"25\":\"\",\"31\":\"\",\"23\":\"\",\"24\":\"\",\"22\":\"\",\"16\":\"\",\"40\":\"\",\"28\":\"\",\"32\":\"\",\"26\":\"\",\"27\":\"\",\"29\":\"\",\"17\":\"\",\"37\":\"\",\"8\":\"1\"},\"core.create\":{\"1\":\"\",\"2\":\"\",\"10\":\"\",\"34\":\"\",\"41\":\"\",\"20\":\"\",\"30\":\"\",\"19\":\"\",\"21\":\"\",\"18\":\"\",\"15\":\"\",\"42\":\"\",\"43\":\"\",\"11\":\"\",\"33\":\"\",\"35\":\"\",\"38\":\"\",\"49\":\"\",\"44\":\"\",\"46\":\"\",\"48\":\"\",\"47\":\"\",\"45\":\"\",\"36\":\"\",\"39\":\"\",\"25\":\"\",\"31\":\"\",\"23\":\"\",\"24\":\"\",\"22\":\"\",\"16\":\"\",\"40\":\"\",\"28\":\"\",\"32\":\"\",\"26\":\"\",\"27\":\"\",\"29\":\"\",\"17\":\"\",\"37\":\"\",\"8\":\"1\"},\"core.edit.own\":{\"1\":\"\",\"2\":\"\",\"10\":\"\",\"34\":\"\",\"41\":\"\",\"20\":\"\",\"30\":\"\",\"19\":\"\",\"21\":\"\",\"18\":\"\",\"15\":\"\",\"42\":\"\",\"43\":\"\",\"11\":\"\",\"33\":\"\",\"35\":\"\",\"38\":\"\",\"49\":\"\",\"44\":\"\",\"46\":\"\",\"48\":\"\",\"47\":\"\",\"45\":\"\",\"36\":\"\",\"39\":\"\",\"25\":\"\",\"31\":\"\",\"23\":\"\",\"24\":\"\",\"22\":\"\",\"16\":\"\",\"40\":\"\",\"28\":\"\",\"32\":\"\",\"26\":\"\",\"27\":\"\",\"29\":\"\",\"17\":\"\",\"37\":\"\",\"8\":\"1\"},\"core.edit\":{\"1\":\"\",\"2\":\"\",\"10\":\"\",\"34\":\"\",\"41\":\"\",\"20\":\"\",\"30\":\"\",\"19\":\"\",\"21\":\"\",\"18\":\"\",\"15\":\"\",\"42\":\"\",\"43\":\"\",\"11\":\"\",\"33\":\"\",\"35\":\"\",\"38\":\"\",\"49\":\"\",\"44\":\"\",\"46\":\"\",\"48\":\"\",\"47\":\"\",\"45\":\"\",\"36\":\"\",\"39\":\"\",\"25\":\"\",\"31\":\"\",\"23\":\"\",\"24\":\"\",\"22\":\"\",\"16\":\"\",\"40\":\"\",\"28\":\"\",\"32\":\"\",\"26\":\"\",\"27\":\"\",\"29\":\"\",\"17\":\"\",\"37\":\"\",\"8\":\"1\"},\"core.edit.state\":{\"1\":\"\",\"2\":\"\",\"10\":\"\",\"34\":\"\",\"41\":\"\",\"20\":\"\",\"30\":\"\",\"19\":\"\",\"21\":\"\",\"18\":\"\",\"15\":\"\",\"42\":\"\",\"43\":\"\",\"11\":\"\",\"33\":\"\",\"35\":\"\",\"38\":\"\",\"49\":\"\",\"44\":\"\",\"46\":\"\",\"48\":\"\",\"47\":\"\",\"45\":\"\",\"36\":\"\",\"39\":\"\",\"25\":\"\",\"31\":\"\",\"23\":\"\",\"24\":\"\",\"22\":\"\",\"16\":\"\",\"40\":\"\",\"28\":\"\",\"32\":\"\",\"26\":\"\",\"27\":\"\",\"29\":\"\",\"17\":\"\",\"37\":\"\",\"8\":\"1\"},\"core.deleteall\":{\"1\":\"\",\"2\":\"\",\"10\":\"\",\"34\":\"\",\"41\":\"\",\"20\":\"\",\"30\":\"\",\"19\":\"\",\"21\":\"\",\"18\":\"\",\"15\":\"\",\"42\":\"\",\"43\":\"\",\"11\":\"\",\"33\":\"\",\"35\":\"\",\"38\":\"\",\"49\":\"\",\"44\":\"\",\"46\":\"\",\"48\":\"\",\"47\":\"\",\"45\":\"\",\"36\":\"\",\"39\":\"\",\"25\":\"\",\"31\":\"\",\"23\":\"\",\"24\":\"\",\"22\":\"\",\"16\":\"\",\"40\":\"\",\"28\":\"\",\"32\":\"\",\"26\":\"\",\"27\":\"\",\"29\":\"\",\"17\":\"\",\"37\":\"\",\"8\":\"1\"},\"core.admin\":{\"1\":\"\",\"2\":\"\",\"10\":\"\",\"34\":\"\",\"41\":\"\",\"20\":\"\",\"30\":\"\",\"19\":\"\",\"21\":\"\",\"18\":\"\",\"15\":\"\",\"42\":\"\",\"43\":\"\",\"11\":\"\",\"33\":\"\",\"35\":\"\",\"38\":\"\",\"49\":\"\",\"44\":\"\",\"46\":\"\",\"48\":\"\",\"47\":\"\",\"45\":\"\",\"36\":\"\",\"39\":\"\",\"25\":\"\",\"31\":\"\",\"23\":\"\",\"24\":\"\",\"22\":\"\",\"16\":\"\",\"40\":\"\",\"28\":\"\",\"32\":\"\",\"26\":\"\",\"27\":\"\",\"29\":\"\",\"17\":\"\",\"37\":\"\",\"8\":\"1\"}},\"com_calUseStdTime\":\"1\",\"com_editdateformat\":\"Y-m-d\",\"disablerepeats\":\"0\",\"defaultcat\":\"0\",\"forcepopupcalendar\":\"1\",\"com_calForceCatColorEventForm\":\"2\",\"editpopup\":\"0\",\"popupw\":\"800\",\"popuph\":\"500\",\"com_single_pane_edit\":\"0\",\"timebeforedescription\":\"0\",\"multiday\":\"1\",\"com_show_editor_buttons\":\"0\",\"com_editor_button_exceptions\":\"pagebreak,readmore\",\"com_notifyboth\":\"0\",\"com_notifyallevents\":\"0\",\"com_notifyauthor\":\"0\",\"showpriority\":\"0\",\"checkconflicts\":\"0\",\"skipreferrer\":\"0\",\"defaultstarttime\":\"08:00\",\"defaultendtime\":\"17:00\",\"multicategory\":\"0\",\"blocktoplevelcategories\":\"0\",\"bootstrapchosen\":\"1\",\"allowraw\":\"0\",\"com_edit_toolbar\":\"0\",\"notifymessage\":\"<p>Title : {TITLE} {DESCRIPTION} Event submitted from [ {LIVESITE} ] by [ {AUTHOR} ] View this event : {VIEWLINK} Edit this event : {EDITLINK} Manage your events : <a href=\\\"{MANAGEEVENTS}\\\">Manage Events<\\/a><\\/p>\",\"editreturnto\":\"day.listevents\",\"com_byview\":\"1\",\"com_mailview\":\"1\",\"com_hitsview\":\"1\",\"com_repeatview\":\"1\",\"contact_display_name\":\"0\",\"redirect_detail\":\"0\",\"shownavbar_detail\":\"1\",\"com_calCutTitle\":\"15\",\"com_calMaxDisplay\":\"15\",\"com_calDisplayStarttime\":\"1\",\"com_calShowLegend\":\"1\",\"com_enableToolTip\":\"1\",\"tooltiptype\":\"bootstrap\",\"com_calTTBackground\":\"1\",\"com_calEventListRowsPpg\":\"10\",\"showyearpast\":\"1\",\"com_showrepeats\":\"1\",\"showyeardate\":\"1\",\"year_show_noev_found\":\"0\",\"icaltimezone\":\"\",\"icalkey\":\"SECRET_PHRASE\",\"showicalicon\":\"0\",\"disableicalexport\":\"0\",\"icalmultiday\":\"0\",\"icalmultiday24h\":\"0\",\"feimport\":\"0\",\"allowedit\":\"0\",\"icalformatted\":\"0\",\"com_rss_cache\":\"1\",\"com_rss_cache_time\":\"3600\",\"com_rss_count\":\"5\",\"com_rss_live_bookmarks\":\"1\",\"com_rss_modid\":\"0\",\"com_rss_title\":\"JEvents RSS Feed for Joomla\",\"com_rss_description\":\"Powered by JEvents!\",\"com_rss_limit_text\":\"0\",\"com_rss_text_length\":\"20\",\"com_rss_logo\":\"\",\"com_cache\":\"0\",\"newsef\":\"0\",\"com_blockRobots\":\"1\",\"robotprior\":\"-1 month\",\"robotpost\":\"+1 month\",\"redirectrobots\":\"0\",\"robotmenuitem\":\"\",\"blockall\":\"0\",\"largeDataSetLimit\":\"100000\",\"modcal_DispLastMonth\":\"NO\",\"modcal_DispLastMonthDays\":\"0\",\"modcal_DispNextMonth\":\"NO\",\"modcal_DispNextMonthDays\":\"0\",\"modcal_LinkCloaking\":\"0\",\"modlatest_MaxEvents\":\"10\",\"modlatest_Mode\":\"0\",\"modlatest_Days\":\"5\",\"startnow\":\"0\",\"modlatest_NoRepeat\":\"1\",\"modlatest_DispYear\":\"0\",\"modlatest_multiday\":\"0\",\"modlatest_DispLinks\":\"1\",\"modlatest_DisDateStyle\":\"0\",\"modlatest_DisTitleStyle\":\"0\",\"modlatest_LinkToCal\":\"0\",\"modlatest_SortReverse\":\"0\",\"modlatest_LinkCloaking\":\"0\",\"modlatest_CustFmtStr\":\"${eventDate}[!a: - ${endDate(%I:%M%p)}]<br\\/>${title}\",\"modlatest_RSS\":\"0\",\"modlatest_contentplugins\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10040, 'JEvents Calendar', 'module', 'mod_jevents_cal', '', 0, 1, 0, 0, '{\"name\":\"JEvents Calendar\",\"type\":\"module\",\"creationDate\":\"September 2015\",\"author\":\"GWE Systems Ltd\",\"copyright\":\"(C) 2009-2015 GWE Systems Ltd, 2006-2008 JEvents Project Group\",\"authorEmail\":\"\",\"authorUrl\":\"http:\\/\\/www.jevents.net\",\"version\":\"3.2.21\",\"description\":\"Shows up to 3 different monthly calendar for JEvents component\",\"group\":\"\",\"filename\":\"mod_jevents_cal\"}', '{\"@spacer\":\"JEV_LATEST_EXTRAS_TAB_LABEL\",\"com_calViewName\":\"\",\"target_itemid\":\"\",\"catidnew\":\"\",\"modcal_useLocalParam\":\"0\",\"showtooltips\":\"0\",\"noeventcheck\":\"0\",\"ignorecatfilter\":\"0\",\"ignorefiltermodule\":\"1\",\"minical_showlink\":\"1\",\"minical_prevyear\":\"1\",\"minical_prevmonth\":\"1\",\"minical_actmonth\":\"1\",\"minical_actyear\":\"1\",\"minical_nextmonth\":\"1\",\"minical_nextyear\":\"1\",\"minical_usedate\":\"0\",\"modcal_DispLastMonth\":\"NO\",\"modcal_DispLastMonthDays\":\"0\",\"modcal_DispNextMonth\":\"NO\",\"modcal_DispNextMonthDays\":\"0\",\"emptydaylinks\":\"1\",\"extras0\":\"\",\"extras1\":\"\",\"extras2\":\"\",\"extras3\":\"\",\"extras4\":\"\",\"extras5\":\"\",\"extras6\":\"\",\"extras7\":\"\",\"extras8\":\"\",\"extras9\":\"\",\"extras10\":\"\",\"extras11\":\"\",\"extras12\":\"\",\"extras13\":\"\",\"moduleclass_sfx\":\"\",\"inc_ec_css\":\"1\",\"cache\":\"1\",\"mod_cal_width\":\"\",\"mod_cal_height\":\"\",\"mod_cal_rowheight\":\"\",\"modcal_LinkCloaking\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10041, 'JEvents Legend', 'module', 'mod_jevents_legend', '', 0, 1, 0, 0, '{\"name\":\"JEvents Legend\",\"type\":\"module\",\"creationDate\":\"September 2015\",\"author\":\"GWE Systems Ltd\",\"copyright\":\"(C) 2009-2015 GWE Systems Ltd, 2006-2008 JEvents Project Group\",\"authorEmail\":\"\",\"authorUrl\":\"http:\\/\\/www.jevents.net\",\"version\":\"3.2.21\",\"description\":\"Shows legend for JEvents categories\",\"group\":\"\",\"filename\":\"mod_jevents_legend\"}', '{\"cache\":\"0\",\"moduleclass_sfx\":\"\",\"show_admin\":\"0\",\"modlegend_inccss\":\"1\",\"nonjeventsdisable\":\"1\",\"target_itemid\":\"\",\"catidnew\":\"\",\"hideinactivekids\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10042, 'JEvents Latest Events', 'module', 'mod_jevents_latest', '', 0, 1, 0, 0, '{\"name\":\"JEvents Latest Events\",\"type\":\"module\",\"creationDate\":\"September 2015\",\"author\":\"GWE Systems Ltd\",\"copyright\":\"(C) 2009-2015 GWE Systems Ltd, 2006-2008 JEvents Project Group\",\"authorEmail\":\"\",\"authorUrl\":\"http:\\/\\/www.jevents.net\",\"version\":\"3.2.21\",\"description\":\"Show latest events for Events component\",\"group\":\"\",\"filename\":\"mod_jevents_latest\"}', '{\"@spacer\":\"ADDITIONAL_CONSTRAINTS\",\"com_calViewName\":\"\",\"cache\":\"0\",\"contentplugins\":\"0\",\"moduleclass_sfx\":\"\",\"catidnew\":\"\",\"ignorecatfilter\":\"0\",\"ignorefiltermodule\":\"0\",\"target_itemid\":\"\",\"modlatest_inccss\":\"1\",\"layout\":\"\",\"modlatest_useLocalParam\":\"1\",\"CustomFromTemplate\":\"1\",\"modlatest_CustFmtStr\":\"<span class=\\\"icon-calendar\\\"><\\/span>${startDate(%d %b %Y)}\\\\n<span class=\\\"icon-time\\\"><\\/span>${startDate(%I:%M%p)}[!a: - ${endDate(%I:%M%p)}]\\\\n<span class=\\\"icon-hand-right\\\"><\\/span>${title}\",\"modlatest_customcss\":\"\",\"modlatest_MaxEvents\":\"10\",\"modlatest_Mode\":\"3\",\"modlatest_Days\":\"30\",\"startnow\":\"0\",\"pastonly\":\"0\",\"modlatest_NoRepeat\":\"0\",\"modlatest_multiday\":\"0\",\"modlatest_DispLinks\":\"1\",\"modlatest_DispYear\":\"0\",\"modlatest_NoEvents\":\"1\",\"modlatest_DisDateStyle\":\"0\",\"modlatest_DisTitleStyle\":\"0\",\"modlatest_LinkToCal\":\"0\",\"modlatest_LinkCloaking\":\"0\",\"modlatest_SortReverse\":\"0\",\"modlatest_RSS\":\"0\",\"modlatest_rss_title\":\"\",\"modlatest_rss_description\":\"\",\"modlatest_templatetop\":\"\",\"modlatest_templaterow\":\"\",\"modlatest_templatebottom\":\"\",\"bootstrapcss\":\"1\",\"extras0\":\"\",\"extras1\":\"\",\"extras2\":\"\",\"extras3\":\"\",\"extras4\":\"\",\"extras5\":\"\",\"extras6\":\"\",\"extras7\":\"\",\"extras8\":\"\",\"extras9\":\"\",\"extras10\":\"\",\"extras11\":\"\",\"extras12\":\"\",\"extras13\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10043, 'JEvents Filter', 'module', 'mod_jevents_filter', '', 0, 1, 0, 0, '{\"name\":\"JEvents Filter\",\"type\":\"module\",\"creationDate\":\"September 2015\",\"author\":\"GWE Systems Ltd\",\"copyright\":\"(C) 2009-2015 GWE Systems Ltd\",\"authorEmail\":\"\",\"authorUrl\":\"http:\\/\\/www.jevents.net\",\"version\":\"3.2.21\",\"description\":\"Shows JEvents Filter\",\"group\":\"\",\"filename\":\"mod_jevents_filter\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10044, 'JEvents CustomModule', 'module', 'mod_jevents_custom', '', 0, 1, 0, 0, '{\"name\":\"JEvents CustomModule\",\"type\":\"module\",\"creationDate\":\"September 2015\",\"author\":\"GWE Systems Ltd\",\"copyright\":\"(C) 2013-2015 GWE Systems Ltd\",\"authorEmail\":\"\",\"authorUrl\":\"http:\\/\\/www.jevents.net\",\"version\":\"3.2.21\",\"description\":\"Displays data from customised event detail or event list layouts\",\"group\":\"\",\"filename\":\"mod_jevents_custom\"}', '[]', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10045, 'JEvents View Switcher', 'module', 'mod_jevents_switchview', '', 0, 1, 0, 0, '{\"name\":\"JEvents View Switcher\",\"type\":\"module\",\"creationDate\":\"September 2015\",\"author\":\"GWE Systems Ltd\",\"copyright\":\"(C) 2009-2015 GWE Systems Ltd, 2006-2008 JEvents Project Group\",\"authorEmail\":\"\",\"authorUrl\":\"http:\\/\\/www.jevents.net\",\"version\":\"3.2.21\",\"description\":\"Switch views for JEvents component and modules\",\"group\":\"\",\"filename\":\"mod_jevents_switchview\"}', '{\"cache\":\"0\",\"moduleclass_sfx\":\"\",\"show_preview\":\"0\",\"target_itemid\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10046, 'plg_finder_jevents', 'plugin', 'jevents', 'finder', 0, 0, 1, 0, '{\"name\":\"plg_finder_jevents\",\"type\":\"plugin\",\"creationDate\":\"September 2015\",\"author\":\"Geraint Edwards\",\"copyright\":\"(C) 2010-2015 GWE Systems Ltd. All rights reserved.\",\"authorEmail\":\"\",\"authorUrl\":\"www.gwesystems.com\",\"version\":\"3.2.21\",\"description\":\"PLG_FINDER_JEVENTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"jevents\"}', '{\"target_itemid\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10047, 'Search - JEvents', 'plugin', 'eventsearch', 'search', 0, 1, 1, 0, '{\"name\":\"Search - JEvents\",\"type\":\"plugin\",\"creationDate\":\"September 2015\",\"author\":\"JEvents Project Group\",\"copyright\":\"(C) 2008-2015 GWE Systems Ltd, 2006-2008 JEvents Project Group\",\"authorEmail\":\"\",\"authorUrl\":\"www.jevents.net\",\"version\":\"3.2.21\",\"description\":\"Events Search is a plugin to allow searching events of the Events component.\",\"group\":\"\",\"filename\":\"eventsearch\"}', '{\"search_limit\":\"50\",\"date_format\":\"%Y-%m-%d\",\"target_itemid\":\"\",\"all_language_search\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10048, 'JEvents - Core Content Plugin', 'plugin', 'jevents', 'content', 0, 1, 1, 0, '{\"name\":\"JEvents - Core Content Plugin\",\"type\":\"plugin\",\"creationDate\":\"January 2015\",\"author\":\"Tony Partridge\",\"copyright\":\"(C) 2012-2015 GWE Systems Ltd\",\"authorEmail\":\"\",\"authorUrl\":\"www.gwesystems.com\",\"version\":\"3.2.21\",\"description\":\"JEvents Plugin - Events, Meetings, Calendars and More\",\"group\":\"\",\"filename\":\"jevents\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10049, 'googl Shortener', 'library', 'googl', '', 0, 1, 1, 0, '{\"name\":\"googl Shortener\",\"type\":\"library\",\"creationDate\":\"January 2015\",\"author\":\"Sebastian Wyder package by Carlos Camara\",\"copyright\":\"(C) 2010-2015 GWE Systems Ltd. All rights reserved.\",\"authorEmail\":\"carcam@jevents.net\",\"authorUrl\":\"www.gwesystems.com\",\"version\":\"3.2.21\",\"description\":\"Googl Shortener library created by Sebastian Wyder (https:\\/\\/github.com\\/sebi\\/googl-php) and package by Carlos C\\u00e1mara (http:\\/\\/www.jevents.net)\",\"group\":\"\",\"filename\":\"googl\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10050, 'JEvents Package', 'package', 'pkg_jevents', '', 0, 1, 1, 0, '{\"name\":\"JEvents Package\",\"type\":\"package\",\"creationDate\":\"September 2015\",\"author\":\"Geraint Edwards\",\"copyright\":\"(C) 2012-2015 GWE Systems Ltd\",\"authorEmail\":\"\",\"authorUrl\":\"www.gwesystems.com\",\"version\":\"3.2.21\",\"description\":\"JEvents  - Events, Meetings, Calendars and More\",\"group\":\"\",\"filename\":\"pkg_jevents\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_extman_extensions_bkp`
--

DROP TABLE IF EXISTS `tol2j_extman_extensions_bkp`;
CREATE TABLE `tol2j_extman_extensions_bkp` (
  `extman_extension_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `identifier` varchar(128) NOT NULL,
  `joomla_extension_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(32) NOT NULL,
  `manifest` text,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `version` varchar(32) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_files_containers`
--

DROP TABLE IF EXISTS `tol2j_files_containers`;
CREATE TABLE `tol2j_files_containers` (
  `files_container_id` int(11) UNSIGNED NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `parameters` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_files_containers`
--

INSERT INTO `tol2j_files_containers` (`files_container_id`, `slug`, `title`, `path`, `parameters`) VALUES
(1, 'docman-files', 'DOCman', 'joomlatools-files/docman-files', '{\"allowed_extensions\":[\"xml\",\"xlsx\",\"xls\",\"txt\",\"tex\",\"rtf\",\"pptx\",\"ppt\",\"pps\",\"pdf\",\"pages\",\"odt\",\"ods\",\"odp\",\"keynote\",\"key\",\"html\",\"docx\",\"doc\",\"csv\"],\"allowed_mimetypes\":[\"image\\/jpeg\",\"image\\/gif\",\"image\\/png\",\"image\\/bmp\",\"application\\/x-shockwave-flash\",\"application\\/msword\",\"application\\/excel\",\"application\\/pdf\",\"application\\/powerpoint\",\"text\\/plain\",\"application\\/x-zip\",\"application\\/rtf\",\"application\\/vnd.ms-excel\",\"application\\/vnd.ms-powerpoint\",\"application\\/vnd.oasis.opendocument.presentation\",\"application\\/vnd.oasis.opendocument.spreadsheet\",\"application\\/vnd.oasis.opendocument.text\",\"application\\/vnd.openxmlformats-officedocument.presentationml.presentation\",\"application\\/vnd.openxmlformats-officedocument.spreadsheetml.sheet\",\"application\\/vnd.openxmlformats-officedocument.wordprocessingml.document\",\"application\\/x-tex\",\"application\\/xml\",\"text\\/csv\",\"text\\/html\"],\"maximum_size\":0,\"thumbnails\":true}'),
(2, 'docman-icons', 'DOCman Icons', 'joomlatools-files/docman-icons', '{\"allowed_extensions\":[\"bmp\",\"gif\",\"jpeg\",\"jpg\",\"png\"],\"allowed_mimetypes\":[\"image\\/jpeg\",\"image\\/gif\",\"image\\/png\",\"image\\/bmp\"],\"maximum_size\":0,\"thumbnails\":true}'),
(3, 'docman-images', 'DOCman Images', 'joomlatools-files/docman-images', '{\"allowed_extensions\":[\"bmp\",\"gif\",\"jpeg\",\"jpg\",\"png\"],\"allowed_mimetypes\":[\"image\\/jpeg\",\"image\\/gif\",\"image\\/png\",\"image\\/bmp\"],\"maximum_size\":0,\"thumbnails\":false}');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_files_thumbnails`
--

DROP TABLE IF EXISTS `tol2j_files_thumbnails`;
CREATE TABLE `tol2j_files_thumbnails` (
  `files_thumbnail_id` int(11) UNSIGNED NOT NULL,
  `files_container_id` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `thumbnail` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_filters`
--

DROP TABLE IF EXISTS `tol2j_finder_filters`;
CREATE TABLE `tol2j_finder_filters` (
  `filter_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links`
--

DROP TABLE IF EXISTS `tol2j_finder_links`;
CREATE TABLE `tol2j_finder_links` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double UNSIGNED NOT NULL DEFAULT '0',
  `sale_price` double UNSIGNED NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_terms0`
--

DROP TABLE IF EXISTS `tol2j_finder_links_terms0`;
CREATE TABLE `tol2j_finder_links_terms0` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_terms1`
--

DROP TABLE IF EXISTS `tol2j_finder_links_terms1`;
CREATE TABLE `tol2j_finder_links_terms1` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_terms2`
--

DROP TABLE IF EXISTS `tol2j_finder_links_terms2`;
CREATE TABLE `tol2j_finder_links_terms2` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_terms3`
--

DROP TABLE IF EXISTS `tol2j_finder_links_terms3`;
CREATE TABLE `tol2j_finder_links_terms3` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_terms4`
--

DROP TABLE IF EXISTS `tol2j_finder_links_terms4`;
CREATE TABLE `tol2j_finder_links_terms4` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_terms5`
--

DROP TABLE IF EXISTS `tol2j_finder_links_terms5`;
CREATE TABLE `tol2j_finder_links_terms5` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_terms6`
--

DROP TABLE IF EXISTS `tol2j_finder_links_terms6`;
CREATE TABLE `tol2j_finder_links_terms6` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_terms7`
--

DROP TABLE IF EXISTS `tol2j_finder_links_terms7`;
CREATE TABLE `tol2j_finder_links_terms7` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_terms8`
--

DROP TABLE IF EXISTS `tol2j_finder_links_terms8`;
CREATE TABLE `tol2j_finder_links_terms8` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_terms9`
--

DROP TABLE IF EXISTS `tol2j_finder_links_terms9`;
CREATE TABLE `tol2j_finder_links_terms9` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_termsa`
--

DROP TABLE IF EXISTS `tol2j_finder_links_termsa`;
CREATE TABLE `tol2j_finder_links_termsa` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_termsb`
--

DROP TABLE IF EXISTS `tol2j_finder_links_termsb`;
CREATE TABLE `tol2j_finder_links_termsb` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_termsc`
--

DROP TABLE IF EXISTS `tol2j_finder_links_termsc`;
CREATE TABLE `tol2j_finder_links_termsc` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_termsd`
--

DROP TABLE IF EXISTS `tol2j_finder_links_termsd`;
CREATE TABLE `tol2j_finder_links_termsd` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_termse`
--

DROP TABLE IF EXISTS `tol2j_finder_links_termse`;
CREATE TABLE `tol2j_finder_links_termse` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_links_termsf`
--

DROP TABLE IF EXISTS `tol2j_finder_links_termsf`;
CREATE TABLE `tol2j_finder_links_termsf` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_taxonomy`
--

DROP TABLE IF EXISTS `tol2j_finder_taxonomy`;
CREATE TABLE `tol2j_finder_taxonomy` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `access` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tol2j_finder_taxonomy`
--

INSERT INTO `tol2j_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_taxonomy_map`
--

DROP TABLE IF EXISTS `tol2j_finder_taxonomy_map`;
CREATE TABLE `tol2j_finder_taxonomy_map` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_terms`
--

DROP TABLE IF EXISTS `tol2j_finder_terms`;
CREATE TABLE `tol2j_finder_terms` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_terms_common`
--

DROP TABLE IF EXISTS `tol2j_finder_terms_common`;
CREATE TABLE `tol2j_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tol2j_finder_terms_common`
--

INSERT INTO `tol2j_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('a', 'en'),
('about', 'en'),
('about', 'en'),
('after', 'en'),
('after', 'en'),
('ago', 'en'),
('ago', 'en'),
('all', 'en'),
('all', 'en'),
('am', 'en'),
('am', 'en'),
('an', 'en'),
('an', 'en'),
('and', 'en'),
('and', 'en'),
('ani', 'en'),
('ani', 'en'),
('any', 'en'),
('any', 'en'),
('are', 'en'),
('are', 'en'),
('aren\'t', 'en'),
('aren\'t', 'en'),
('as', 'en'),
('as', 'en'),
('at', 'en'),
('at', 'en'),
('be', 'en'),
('be', 'en'),
('but', 'en'),
('but', 'en'),
('by', 'en'),
('by', 'en'),
('for', 'en'),
('for', 'en'),
('from', 'en'),
('from', 'en'),
('get', 'en'),
('get', 'en'),
('go', 'en'),
('go', 'en'),
('how', 'en'),
('how', 'en'),
('if', 'en'),
('if', 'en'),
('in', 'en'),
('in', 'en'),
('into', 'en'),
('into', 'en'),
('is', 'en'),
('is', 'en'),
('isn\'t', 'en'),
('isn\'t', 'en'),
('it', 'en'),
('it', 'en'),
('its', 'en'),
('its', 'en'),
('me', 'en'),
('me', 'en'),
('more', 'en'),
('more', 'en'),
('most', 'en'),
('most', 'en'),
('must', 'en'),
('must', 'en'),
('my', 'en'),
('my', 'en'),
('new', 'en'),
('new', 'en'),
('no', 'en'),
('no', 'en'),
('none', 'en'),
('none', 'en'),
('not', 'en'),
('not', 'en'),
('noth', 'en'),
('noth', 'en'),
('nothing', 'en'),
('nothing', 'en'),
('of', 'en'),
('of', 'en'),
('off', 'en'),
('off', 'en'),
('often', 'en'),
('often', 'en'),
('old', 'en'),
('old', 'en'),
('on', 'en'),
('on', 'en'),
('onc', 'en'),
('onc', 'en'),
('once', 'en'),
('once', 'en'),
('onli', 'en'),
('onli', 'en'),
('only', 'en'),
('only', 'en'),
('or', 'en'),
('or', 'en'),
('other', 'en'),
('other', 'en'),
('our', 'en'),
('our', 'en'),
('ours', 'en'),
('ours', 'en'),
('out', 'en'),
('out', 'en'),
('over', 'en'),
('over', 'en'),
('page', 'en'),
('page', 'en'),
('she', 'en'),
('she', 'en'),
('should', 'en'),
('should', 'en'),
('small', 'en'),
('small', 'en'),
('so', 'en'),
('so', 'en'),
('some', 'en'),
('some', 'en'),
('than', 'en'),
('than', 'en'),
('thank', 'en'),
('thank', 'en'),
('that', 'en'),
('that', 'en'),
('the', 'en'),
('the', 'en'),
('their', 'en'),
('their', 'en'),
('theirs', 'en'),
('theirs', 'en'),
('them', 'en'),
('them', 'en'),
('then', 'en'),
('then', 'en'),
('there', 'en'),
('there', 'en'),
('these', 'en'),
('these', 'en'),
('they', 'en'),
('they', 'en'),
('this', 'en'),
('this', 'en'),
('those', 'en'),
('those', 'en'),
('thus', 'en'),
('thus', 'en'),
('time', 'en'),
('time', 'en'),
('times', 'en'),
('times', 'en'),
('to', 'en'),
('to', 'en'),
('too', 'en'),
('too', 'en'),
('true', 'en'),
('true', 'en'),
('under', 'en'),
('under', 'en'),
('until', 'en'),
('until', 'en'),
('up', 'en'),
('up', 'en'),
('upon', 'en'),
('upon', 'en'),
('use', 'en'),
('use', 'en'),
('user', 'en'),
('user', 'en'),
('users', 'en'),
('users', 'en'),
('veri', 'en'),
('veri', 'en'),
('version', 'en'),
('version', 'en'),
('very', 'en'),
('very', 'en'),
('via', 'en'),
('via', 'en'),
('want', 'en'),
('want', 'en'),
('was', 'en'),
('was', 'en'),
('way', 'en'),
('way', 'en'),
('were', 'en'),
('were', 'en'),
('what', 'en'),
('what', 'en'),
('when', 'en'),
('when', 'en'),
('where', 'en'),
('where', 'en'),
('whi', 'en'),
('whi', 'en'),
('which', 'en'),
('which', 'en'),
('who', 'en'),
('who', 'en'),
('whom', 'en'),
('whom', 'en'),
('whose', 'en'),
('whose', 'en'),
('why', 'en'),
('why', 'en'),
('wide', 'en'),
('wide', 'en'),
('will', 'en'),
('will', 'en'),
('with', 'en'),
('with', 'en'),
('within', 'en'),
('within', 'en'),
('without', 'en'),
('without', 'en'),
('would', 'en'),
('would', 'en'),
('yes', 'en'),
('yes', 'en'),
('yet', 'en'),
('yet', 'en'),
('you', 'en'),
('you', 'en'),
('your', 'en'),
('your', 'en'),
('yours', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_tokens`
--

DROP TABLE IF EXISTS `tol2j_finder_tokens`;
CREATE TABLE `tol2j_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '1',
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_tokens_aggregate`
--

DROP TABLE IF EXISTS `tol2j_finder_tokens_aggregate`;
CREATE TABLE `tol2j_finder_tokens_aggregate` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `term_weight` float UNSIGNED NOT NULL,
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `context_weight` float UNSIGNED NOT NULL,
  `total_weight` float UNSIGNED NOT NULL,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_finder_types`
--

DROP TABLE IF EXISTS `tol2j_finder_types`;
CREATE TABLE `tol2j_finder_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tol2j_finder_types`
--

INSERT INTO `tol2j_finder_types` (`id`, `title`, `mime`) VALUES
(1, 'Tag', ''),
(2, 'Category', ''),
(4, 'Contact', ''),
(5, 'Article', ''),
(6, 'News Feed', '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jevents_catmap`
--

DROP TABLE IF EXISTS `tol2j_jevents_catmap`;
CREATE TABLE `tol2j_jevents_catmap` (
  `evid` int(12) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT '1',
  `ordering` int(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jevents_exception`
--

DROP TABLE IF EXISTS `tol2j_jevents_exception`;
CREATE TABLE `tol2j_jevents_exception` (
  `ex_id` int(12) NOT NULL,
  `rp_id` int(12) NOT NULL DEFAULT '0',
  `eventid` int(12) NOT NULL DEFAULT '1',
  `eventdetail_id` int(12) NOT NULL DEFAULT '0',
  `exception_type` int(2) NOT NULL DEFAULT '0',
  `startrepeat` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `oldstartrepeat` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tempfield` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jevents_filtermap`
--

DROP TABLE IF EXISTS `tol2j_jevents_filtermap`;
CREATE TABLE `tol2j_jevents_filtermap` (
  `fid` int(12) NOT NULL,
  `userid` int(12) NOT NULL DEFAULT '0',
  `filters` text NOT NULL,
  `md5` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jevents_icsfile`
--

DROP TABLE IF EXISTS `tol2j_jevents_icsfile`;
CREATE TABLE `tol2j_jevents_icsfile` (
  `ics_id` int(12) NOT NULL,
  `srcURL` varchar(255) NOT NULL DEFAULT '',
  `label` varchar(30) NOT NULL DEFAULT '',
  `filename` varchar(120) NOT NULL DEFAULT '',
  `icaltype` tinyint(3) NOT NULL DEFAULT '0',
  `isdefault` tinyint(3) NOT NULL DEFAULT '0',
  `ignoreembedcat` tinyint(3) NOT NULL DEFAULT '0',
  `state` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(100) NOT NULL DEFAULT '',
  `modified_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `refreshed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `autorefresh` tinyint(3) NOT NULL DEFAULT '0',
  `overlaps` tinyint(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_jevents_icsfile`
--

INSERT INTO `tol2j_jevents_icsfile` (`ics_id`, `srcURL`, `label`, `filename`, `icaltype`, `isdefault`, `ignoreembedcat`, `state`, `access`, `catid`, `created`, `created_by`, `created_by_alias`, `modified_by`, `refreshed`, `autorefresh`, `overlaps`) VALUES
(1, '', 'Default', 'Initial ICS File', 2, 1, 0, 1, 1, 0, '0000-00-00 00:00:00', 0, '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jevents_repetition`
--

DROP TABLE IF EXISTS `tol2j_jevents_repetition`;
CREATE TABLE `tol2j_jevents_repetition` (
  `rp_id` int(12) NOT NULL,
  `eventid` int(12) NOT NULL DEFAULT '1',
  `eventdetail_id` int(12) NOT NULL DEFAULT '0',
  `duplicatecheck` varchar(32) NOT NULL DEFAULT '',
  `startrepeat` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `endrepeat` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_jevents_repetition`
--

INSERT INTO `tol2j_jevents_repetition` (`rp_id`, `eventid`, `eventdetail_id`, `duplicatecheck`, `startrepeat`, `endrepeat`) VALUES
(1, 1, 1, 'c66b0b6b23cc3561b86196a8c91b3df5', '2017-08-30 08:00:00', '2017-08-30 17:00:00'),
(2, 2, 2, 'dafa33fc3f72056889c67a41b2ea7c3c', '2017-08-30 08:00:00', '2017-08-30 17:00:00'),
(3, 3, 3, 'a834561d6a7fb6f3f026c8d351ff7904', '2017-09-11 08:00:00', '2017-09-11 17:00:00'),
(5, 4, 4, '1c0aee5c61208710fc10a834f08cccf7', '2017-09-11 00:00:00', '2017-09-11 23:59:59'),
(6, 6, 6, '987718eae983f32878b9fee4dfd5e06d', '2017-11-08 20:00:00', '2017-11-08 21:00:00'),
(7, 7, 7, '8b15bfcf28471af0394c43ce817b1980', '2017-09-15 08:00:00', '2017-09-15 10:00:00'),
(8, 5, 5, '4c2caf78513ec0caa382d25c25cf2417', '2017-10-01 00:00:00', '2017-10-01 23:59:59'),
(9, 5, 5, '93c0811765c86b209fe00f3e9ed061f1', '2017-11-01 00:00:00', '2017-11-01 23:59:59'),
(10, 5, 5, '9ed5c0bde46289ca767478c89ac7460b', '2017-12-01 00:00:00', '2017-12-01 23:59:59'),
(11, 5, 5, '43c0a8bc10a6eb3a30348d7959943207', '2018-01-01 00:00:00', '2018-01-01 23:59:59'),
(12, 5, 5, 'f3042605098bb9615a7b1371a1338b8a', '2018-02-01 00:00:00', '2018-02-01 23:59:59'),
(13, 5, 5, '378d885974743f0d8e1af214303de110', '2018-03-01 00:00:00', '2018-03-01 23:59:59'),
(14, 5, 5, '4999fee8866062353b4fdcbaba5cecb3', '2018-04-01 00:00:00', '2018-04-01 23:59:59'),
(15, 5, 5, '547b6222b9ad0e016c04bba44d29100b', '2018-05-01 00:00:00', '2018-05-01 23:59:59'),
(16, 5, 5, '62476744445b7815896782decd72e1cb', '2018-06-01 00:00:00', '2018-06-01 23:59:59'),
(17, 5, 5, '6dc6451a6ff053d1b4ae618abab2e63a', '2018-07-01 00:00:00', '2018-07-01 23:59:59'),
(18, 5, 5, '7e976603296c029cd4e291e62aab4cb4', '2018-08-01 00:00:00', '2018-08-01 23:59:59'),
(19, 5, 5, '41a43f34b905267cbdaffe420b1fafa7', '2018-09-01 00:00:00', '2018-09-01 23:59:59'),
(20, 8, 8, '7318c2f4bb9fcb287d3b27003f7e596b', '2017-10-30 08:00:00', '2017-10-30 17:00:00'),
(21, 8, 8, 'f13a22728f7e364a7c133b409e482603', '2017-11-30 08:00:00', '2017-11-30 17:00:00'),
(22, 8, 8, 'a3abe527dcd3b3f875228314575b9dbe', '2017-12-30 08:00:00', '2017-12-30 17:00:00'),
(23, 8, 8, 'b42d60a48d7746e8804a2cd830174ab8', '2018-01-30 08:00:00', '2018-01-30 17:00:00'),
(24, 8, 8, '5ac0f164cd3d0bb28c053f4e602337ba', '2018-03-30 08:00:00', '2018-03-30 17:00:00'),
(25, 8, 8, '24819a8182f74a1adb5293bf53575ff7', '2018-04-30 08:00:00', '2018-04-30 17:00:00'),
(26, 8, 8, 'f0c285f485d69a46a78ab534f9c73511', '2018-05-30 08:00:00', '2018-05-30 17:00:00'),
(27, 8, 8, '598fc5409d7dbd65a41aea649b370eb7', '2018-06-30 08:00:00', '2018-06-30 17:00:00'),
(28, 8, 8, 'a823ef1045caddca39394f943b61125b', '2018-07-30 08:00:00', '2018-07-30 17:00:00'),
(29, 8, 8, 'b21655e0b2188910942c68c95cb834cc', '2018-08-30 08:00:00', '2018-08-30 17:00:00'),
(30, 8, 8, 'b699b56143250e1b709712c1df12937f', '2018-09-30 08:00:00', '2018-09-30 17:00:00'),
(31, 8, 8, '902b7b126e13083531ec7dc030006962', '2018-10-30 08:00:00', '2018-10-30 17:00:00'),
(32, 8, 8, 'd210debe7135eb169f67ce2fb50c9fab', '2018-11-30 08:00:00', '2018-11-30 17:00:00'),
(33, 8, 8, 'dcadcf2cfdf912c52ef52a676c7f7ed9', '2018-12-30 08:00:00', '2018-12-30 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jevents_rrule`
--

DROP TABLE IF EXISTS `tol2j_jevents_rrule`;
CREATE TABLE `tol2j_jevents_rrule` (
  `rr_id` int(12) NOT NULL,
  `eventid` int(12) NOT NULL DEFAULT '1',
  `freq` varchar(30) NOT NULL DEFAULT '',
  `until` int(12) NOT NULL DEFAULT '1',
  `untilraw` varchar(30) NOT NULL DEFAULT '',
  `count` int(6) NOT NULL DEFAULT '1',
  `rinterval` int(6) NOT NULL DEFAULT '1',
  `bysecond` varchar(50) NOT NULL DEFAULT '',
  `byminute` varchar(50) NOT NULL DEFAULT '',
  `byhour` varchar(50) NOT NULL DEFAULT '',
  `byday` varchar(50) NOT NULL DEFAULT '',
  `bymonthday` varchar(50) NOT NULL DEFAULT '',
  `byyearday` varchar(100) NOT NULL DEFAULT '',
  `byweekno` varchar(50) NOT NULL DEFAULT '',
  `bymonth` varchar(50) NOT NULL DEFAULT '',
  `bysetpos` varchar(50) NOT NULL DEFAULT '',
  `wkst` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_jevents_rrule`
--

INSERT INTO `tol2j_jevents_rrule` (`rr_id`, `eventid`, `freq`, `until`, `untilraw`, `count`, `rinterval`, `bysecond`, `byminute`, `byhour`, `byday`, `bymonthday`, `byyearday`, `byweekno`, `bymonth`, `bysetpos`, `wkst`) VALUES
(1, 1, 'none', 0, '', 1, 1, '', '', '', '+1WE,+2WE,+3WE,+4WE,+5WE', '', '', '', '', '', ''),
(2, 2, 'none', 0, '', 1, 1, '', '', '', '+1WE,+2WE,+3WE,+4WE,+5WE', '', '', '', '', '', ''),
(3, 3, 'none', 0, '', 1, 1, '', '', '', '+1TU,+2TU,+3TU,+4TU,+5TU', '', '', '', '', '', ''),
(4, 4, 'none', 0, '', 1, 1, '', '', '', '+1TU,+2TU,+3TU,+4TU,+5TU', '', '', '', '', '', ''),
(5, 6, 'none', 0, '', 1, 1, '', '', '', '+1TU,+2TU,+3TU,+4TU,+5TU', '', '', '', '', '', ''),
(6, 7, 'none', 0, '', 1, 1, '', '', '', '+1TU,+2TU,+3TU,+4TU,+5TU', '', '', '', '', '', ''),
(7, 5, 'MONTHLY', 0, '', 12, 1, '', '', '', '', '+1', '', '', '', '', ''),
(8, 8, 'MONTHLY', 0, '', 14, 1, '', '', '', '', '+30', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jevents_translation`
--

DROP TABLE IF EXISTS `tol2j_jevents_translation`;
CREATE TABLE `tol2j_jevents_translation` (
  `translation_id` int(12) NOT NULL,
  `evdet_id` int(12) NOT NULL DEFAULT '0',
  `description` longtext NOT NULL,
  `location` varchar(120) NOT NULL DEFAULT '',
  `summary` longtext NOT NULL,
  `contact` varchar(120) NOT NULL DEFAULT '',
  `extra_info` text NOT NULL,
  `language` varchar(20) NOT NULL DEFAULT '*'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jevents_vevdetail`
--

DROP TABLE IF EXISTS `tol2j_jevents_vevdetail`;
CREATE TABLE `tol2j_jevents_vevdetail` (
  `evdet_id` int(12) NOT NULL,
  `rawdata` longtext NOT NULL,
  `dtstart` int(11) NOT NULL DEFAULT '0',
  `dtstartraw` varchar(30) NOT NULL DEFAULT '',
  `duration` int(11) NOT NULL DEFAULT '0',
  `durationraw` varchar(30) NOT NULL DEFAULT '',
  `dtend` int(11) NOT NULL DEFAULT '0',
  `dtendraw` varchar(30) NOT NULL DEFAULT '',
  `dtstamp` varchar(30) NOT NULL DEFAULT '',
  `class` varchar(10) NOT NULL DEFAULT '',
  `categories` varchar(120) NOT NULL DEFAULT '',
  `color` varchar(20) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `geolon` float NOT NULL DEFAULT '0',
  `geolat` float NOT NULL DEFAULT '0',
  `location` varchar(120) NOT NULL DEFAULT '',
  `priority` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `status` varchar(20) NOT NULL DEFAULT '',
  `summary` longtext NOT NULL,
  `contact` varchar(120) NOT NULL DEFAULT '',
  `organizer` varchar(120) NOT NULL DEFAULT '',
  `url` text NOT NULL,
  `extra_info` text NOT NULL,
  `created` varchar(30) NOT NULL DEFAULT '',
  `sequence` int(11) NOT NULL DEFAULT '1',
  `state` tinyint(3) NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `multiday` tinyint(3) NOT NULL DEFAULT '1',
  `hits` int(11) NOT NULL DEFAULT '0',
  `noendtime` tinyint(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_jevents_vevdetail`
--

INSERT INTO `tol2j_jevents_vevdetail` (`evdet_id`, `rawdata`, `dtstart`, `dtstartraw`, `duration`, `durationraw`, `dtend`, `dtendraw`, `dtstamp`, `class`, `categories`, `color`, `description`, `geolon`, `geolat`, `location`, `priority`, `status`, `summary`, `contact`, `organizer`, `url`, `extra_info`, `created`, `sequence`, `state`, `modified`, `multiday`, `hits`, `noendtime`) VALUES
(1, '', 1504072800, '', 0, '', 1504105200, '', '', '', '', '', '<p>Board meeting</p>', 0, 0, 'TMEA', 0, '', 'Board Meeting', 'Ann', '', '', '', '', 0, 1, '2017-09-05 01:24:15', 1, 0, 0),
(2, '', 1504072800, '', 0, '', 1504105200, '', '', '', '', '', '<p>Board meeting</p>', 0, 0, 'TMEA', 0, '', 'Board Meeting November', 'Ann', '', '', '', '', 0, 1, '2017-09-05 01:24:24', 1, 0, 0),
(3, '', 1505109600, '', 0, '', 1505142000, '', '', '', '', '', '<p>Desc for event</p>', 0, 0, 'TMEA HQ', 0, '', 'Board Meeting', 'Contact Person', '', '', 'Extra info', '', 0, 1, '2017-09-05 01:24:36', 1, 2, 0),
(4, '', 1505080800, '', 0, '', 1505167199, '', '', '', '', '', '<p>Desc for Council Meeting </p>', 0, 0, 'TMEA HQ', 0, '', 'Council Meeting', 'Contact Person', '', '', 'Extra info', '', 0, 1, '2017-09-05 04:06:38', 1, 6, 0),
(5, '', 1505001600, '', 0, '', 1505088000, '', '', '', '', '', '<p>Desc for Council Meeting </p>', 0, 0, 'TMEA HQ', 0, '', 'Council Meeting', 'Contact Person', '', '', 'Extra info', '', 0, 1, '2017-10-30 18:36:00', 1, 1, 0),
(6, '', 1510171200, '', 0, '', 1510174800, '', '', '', '', '', '<p>Dinner Celebration</p>', 0, 0, '', 0, '', 'Dinner', '', '', '', '', '', 0, 1, '2017-10-30 18:40:09', 1, 23, 0),
(7, '', 1505455200, '', 0, '', 1505462400, '', '', '', '', '', '<p>CEO\'s Breakfast</p>', 0, 0, '', 0, '', 'Breakfast', '', '', '', '', '', 0, 1, '2017-09-13 02:20:06', 1, 4, 0),
(8, '', 1509350400, '', 0, '', 1509382800, '', '', '', '', '', '<p>staff meeting </p>', 0, 0, 'TMEA HQ', 0, '', 'Staff Meeting', 'Contact Person', '', '', '', '', 0, 1, '2017-10-30 18:37:07', 1, 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jevents_vevent`
--

DROP TABLE IF EXISTS `tol2j_jevents_vevent`;
CREATE TABLE `tol2j_jevents_vevent` (
  `ev_id` int(12) NOT NULL,
  `icsid` int(12) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '1',
  `uid` varchar(255) NOT NULL DEFAULT '',
  `refreshed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(100) NOT NULL DEFAULT '',
  `modified_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `rawdata` longtext NOT NULL,
  `recurrence_id` varchar(30) NOT NULL DEFAULT '',
  `detail_id` int(12) NOT NULL DEFAULT '0',
  `state` tinyint(3) NOT NULL DEFAULT '1',
  `lockevent` tinyint(3) NOT NULL DEFAULT '0',
  `author_notified` tinyint(3) NOT NULL DEFAULT '0',
  `access` int(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_jevents_vevent`
--

INSERT INTO `tol2j_jevents_vevent` (`ev_id`, `icsid`, `catid`, `uid`, `refreshed`, `created`, `created_by`, `created_by_alias`, `modified_by`, `rawdata`, `recurrence_id`, `detail_id`, `state`, `lockevent`, `author_notified`, `access`) VALUES
(1, 1, 86, '9b4dc4916031fb29ce5dee0a71318d31', '0000-00-00 00:00:00', '2017-08-30 06:52:01', 234, '', 234, 'a:20:{s:3:\"UID\";s:32:\"9b4dc4916031fb29ce5dee0a71318d31\";s:11:\"X-EXTRAINFO\";s:0:\"\";s:8:\"LOCATION\";s:4:\"TMEA\";s:11:\"allDayEvent\";s:3:\"off\";s:7:\"CONTACT\";s:3:\"Ann\";s:11:\"DESCRIPTION\";s:20:\"<p>Board meeting</p>\";s:12:\"publish_down\";s:9:\"2017-8-30\";s:10:\"publish_up\";s:9:\"2017-8-30\";s:13:\"publish_down2\";s:9:\"2017-8-30\";s:11:\"publish_up2\";s:9:\"2017-8-30\";s:7:\"SUMMARY\";s:13:\"Board Meeting\";s:3:\"URL\";s:0:\"\";s:11:\"X-CREATEDBY\";i:234;s:7:\"DTSTART\";i:1504072800;s:5:\"DTEND\";i:1504105200;s:5:\"RRULE\";a:4:{s:4:\"FREQ\";s:4:\"none\";s:5:\"COUNT\";i:1;s:8:\"INTERVAL\";s:1:\"1\";s:5:\"BYDAY\";s:24:\"+1WE,+2WE,+3WE,+4WE,+5WE\";}s:8:\"MULTIDAY\";s:1:\"1\";s:9:\"NOENDTIME\";s:1:\"0\";s:7:\"X-COLOR\";s:0:\"\";s:9:\"LOCKEVENT\";s:1:\"0\";}', '', 1, 1, 0, 0, 0),
(2, 1, 86, '289708882d56c6b7087b7fc0db557e9e', '0000-00-00 00:00:00', '2017-08-30 06:52:19', 234, '', 234, 'a:20:{s:3:\"UID\";s:32:\"289708882d56c6b7087b7fc0db557e9e\";s:11:\"X-EXTRAINFO\";s:0:\"\";s:8:\"LOCATION\";s:4:\"TMEA\";s:11:\"allDayEvent\";s:3:\"off\";s:7:\"CONTACT\";s:3:\"Ann\";s:11:\"DESCRIPTION\";s:20:\"<p>Board meeting</p>\";s:12:\"publish_down\";s:9:\"2017-8-30\";s:10:\"publish_up\";s:9:\"2017-8-30\";s:13:\"publish_down2\";s:9:\"2017-8-30\";s:11:\"publish_up2\";s:9:\"2017-8-30\";s:7:\"SUMMARY\";s:22:\"Board Meeting November\";s:3:\"URL\";s:0:\"\";s:11:\"X-CREATEDBY\";i:234;s:7:\"DTSTART\";i:1504072800;s:5:\"DTEND\";i:1504105200;s:5:\"RRULE\";a:4:{s:4:\"FREQ\";s:4:\"none\";s:5:\"COUNT\";i:1;s:8:\"INTERVAL\";s:1:\"1\";s:5:\"BYDAY\";s:24:\"+1WE,+2WE,+3WE,+4WE,+5WE\";}s:8:\"MULTIDAY\";s:1:\"1\";s:9:\"NOENDTIME\";s:1:\"0\";s:7:\"X-COLOR\";s:0:\"\";s:9:\"LOCKEVENT\";s:1:\"0\";}', '', 2, 1, 0, 0, 0),
(3, 1, 86, 'b58a2bda7ace5fffb3249aeb27774112', '0000-00-00 00:00:00', '2017-09-05 01:08:55', 234, '', 234, 'a:20:{s:3:\"UID\";s:32:\"b58a2bda7ace5fffb3249aeb27774112\";s:11:\"X-EXTRAINFO\";s:10:\"Extra info\";s:8:\"LOCATION\";s:7:\"TMEA HQ\";s:11:\"allDayEvent\";s:3:\"off\";s:7:\"CONTACT\";s:14:\"Contact Person\";s:11:\"DESCRIPTION\";s:21:\"<p>Desc for event</p>\";s:12:\"publish_down\";s:9:\"2017-9-11\";s:10:\"publish_up\";s:9:\"2017-9-11\";s:13:\"publish_down2\";s:9:\"2017-9-11\";s:11:\"publish_up2\";s:9:\"2017-9-11\";s:7:\"SUMMARY\";s:13:\"Board Meeting\";s:3:\"URL\";s:0:\"\";s:11:\"X-CREATEDBY\";i:234;s:7:\"DTSTART\";i:1505109600;s:5:\"DTEND\";i:1505142000;s:5:\"RRULE\";a:4:{s:4:\"FREQ\";s:4:\"none\";s:5:\"COUNT\";i:1;s:8:\"INTERVAL\";s:1:\"1\";s:5:\"BYDAY\";s:24:\"+1TU,+2TU,+3TU,+4TU,+5TU\";}s:8:\"MULTIDAY\";s:1:\"1\";s:9:\"NOENDTIME\";s:1:\"0\";s:7:\"X-COLOR\";s:0:\"\";s:9:\"LOCKEVENT\";s:1:\"0\";}', '', 3, 1, 0, 0, 0),
(4, 1, 86, 'e7dcae1247089e67a5470a518301dd97', '0000-00-00 00:00:00', '2017-09-05 01:18:57', 234, '', 234, 'a:20:{s:3:\"UID\";s:32:\"e7dcae1247089e67a5470a518301dd97\";s:11:\"X-EXTRAINFO\";s:10:\"Extra info\";s:8:\"LOCATION\";s:7:\"TMEA HQ\";s:11:\"allDayEvent\";s:2:\"on\";s:7:\"CONTACT\";s:14:\"Contact Person\";s:11:\"DESCRIPTION\";s:33:\"<p>Desc for Council Meeting </p>\";s:12:\"publish_down\";s:9:\"2017-9-11\";s:10:\"publish_up\";s:9:\"2017-9-11\";s:13:\"publish_down2\";s:9:\"2017-9-11\";s:11:\"publish_up2\";s:9:\"2017-9-11\";s:7:\"SUMMARY\";s:15:\"Council Meeting\";s:3:\"URL\";s:0:\"\";s:11:\"X-CREATEDBY\";i:234;s:7:\"DTSTART\";i:1505080800;s:5:\"DTEND\";i:1505167199;s:5:\"RRULE\";a:4:{s:4:\"FREQ\";s:4:\"none\";s:5:\"COUNT\";i:1;s:8:\"INTERVAL\";s:1:\"1\";s:5:\"BYDAY\";s:24:\"+1TU,+2TU,+3TU,+4TU,+5TU\";}s:8:\"MULTIDAY\";s:1:\"1\";s:9:\"NOENDTIME\";s:1:\"0\";s:7:\"X-COLOR\";s:0:\"\";s:9:\"LOCKEVENT\";s:1:\"0\";}', '', 4, 1, 0, 0, 8),
(5, 1, 86, 'e7dcae1247089e67a5470a518301dd99', '0000-00-00 00:00:00', '2017-09-05 01:18:57', 234, '', 234, 'a:20:{s:3:\"UID\";s:32:\"e7dcae1247089e67a5470a518301dd99\";s:11:\"X-EXTRAINFO\";s:10:\"Extra info\";s:8:\"LOCATION\";s:7:\"TMEA HQ\";s:11:\"allDayEvent\";s:3:\"off\";s:7:\"CONTACT\";s:14:\"Contact Person\";s:11:\"DESCRIPTION\";s:33:\"<p>Desc for Council Meeting </p>\";s:12:\"publish_down\";s:9:\"2017-9-11\";s:10:\"publish_up\";s:9:\"2017-9-10\";s:13:\"publish_down2\";s:9:\"2017-9-11\";s:11:\"publish_up2\";s:9:\"2017-9-10\";s:7:\"SUMMARY\";s:15:\"Council Meeting\";s:3:\"URL\";s:0:\"\";s:11:\"X-CREATEDBY\";i:234;s:7:\"DTSTART\";i:1505001600;s:5:\"DTEND\";i:1505088000;s:5:\"RRULE\";a:4:{s:4:\"FREQ\";s:7:\"MONTHLY\";s:5:\"COUNT\";i:12;s:8:\"INTERVAL\";s:1:\"1\";s:10:\"BYMONTHDAY\";s:2:\"+1\";}s:8:\"MULTIDAY\";s:1:\"1\";s:9:\"NOENDTIME\";s:1:\"0\";s:7:\"X-COLOR\";s:0:\"\";s:9:\"LOCKEVENT\";s:1:\"0\";}', '', 5, 1, 0, 0, 8),
(6, 1, 85, '0573fd65c093a18e502b9814b46d7c1f', '0000-00-00 00:00:00', '2017-09-12 22:40:13', 234, '', 234, 'a:20:{s:3:\"UID\";s:32:\"0573fd65c093a18e502b9814b46d7c1f\";s:11:\"X-EXTRAINFO\";s:0:\"\";s:8:\"LOCATION\";s:0:\"\";s:11:\"allDayEvent\";s:3:\"off\";s:7:\"CONTACT\";s:0:\"\";s:11:\"DESCRIPTION\";s:25:\"<p>Dinner Celebration</p>\";s:12:\"publish_down\";s:9:\"2017-11-8\";s:10:\"publish_up\";s:9:\"2017-11-8\";s:13:\"publish_down2\";s:9:\"2017-11-8\";s:11:\"publish_up2\";s:9:\"2017-11-8\";s:7:\"SUMMARY\";s:6:\"Dinner\";s:3:\"URL\";s:0:\"\";s:11:\"X-CREATEDBY\";i:234;s:7:\"DTSTART\";i:1510171200;s:5:\"DTEND\";i:1510174800;s:5:\"RRULE\";a:4:{s:4:\"FREQ\";s:4:\"none\";s:5:\"COUNT\";i:1;s:8:\"INTERVAL\";s:1:\"1\";s:5:\"BYDAY\";s:24:\"+1TU,+2TU,+3TU,+4TU,+5TU\";}s:8:\"MULTIDAY\";s:1:\"1\";s:9:\"NOENDTIME\";s:1:\"0\";s:7:\"X-COLOR\";s:0:\"\";s:9:\"LOCKEVENT\";s:1:\"0\";}', '', 6, 1, 0, 0, 12),
(7, 1, 85, 'f504eaefbb8cc72f513dfb9757198e48', '0000-00-00 00:00:00', '2017-09-12 23:15:06', 234, '', 234, 'a:20:{s:3:\"UID\";s:32:\"f504eaefbb8cc72f513dfb9757198e48\";s:11:\"X-EXTRAINFO\";s:0:\"\";s:8:\"LOCATION\";s:0:\"\";s:11:\"allDayEvent\";s:3:\"off\";s:7:\"CONTACT\";s:0:\"\";s:11:\"DESCRIPTION\";s:22:\"<p>CEO\'s Breakfast</p>\";s:12:\"publish_down\";s:9:\"2017-9-15\";s:10:\"publish_up\";s:9:\"2017-9-15\";s:13:\"publish_down2\";s:9:\"2017-9-15\";s:11:\"publish_up2\";s:9:\"2017-9-15\";s:7:\"SUMMARY\";s:9:\"Breakfast\";s:3:\"URL\";s:0:\"\";s:11:\"X-CREATEDBY\";i:234;s:7:\"DTSTART\";i:1505455200;s:5:\"DTEND\";i:1505462400;s:5:\"RRULE\";a:4:{s:4:\"FREQ\";s:4:\"none\";s:5:\"COUNT\";i:1;s:8:\"INTERVAL\";s:1:\"1\";s:5:\"BYDAY\";s:24:\"+1TU,+2TU,+3TU,+4TU,+5TU\";}s:8:\"MULTIDAY\";s:1:\"1\";s:9:\"NOENDTIME\";s:1:\"0\";s:7:\"X-COLOR\";s:0:\"\";s:9:\"LOCKEVENT\";s:1:\"0\";}', '', 7, 1, 0, 0, 11),
(8, 1, 86, '5122580fcb83b298793a46881b008570', '0000-00-00 00:00:00', '2017-10-30 18:37:07', 234, '', 234, 'a:20:{s:3:\"UID\";s:32:\"5122580fcb83b298793a46881b008570\";s:11:\"X-EXTRAINFO\";s:0:\"\";s:8:\"LOCATION\";s:7:\"TMEA HQ\";s:11:\"allDayEvent\";s:3:\"off\";s:7:\"CONTACT\";s:14:\"Contact Person\";s:11:\"DESCRIPTION\";s:22:\"<p>staff meeting </p>\";s:12:\"publish_down\";s:10:\"2017-10-30\";s:10:\"publish_up\";s:10:\"2017-10-30\";s:13:\"publish_down2\";s:10:\"2017-10-30\";s:11:\"publish_up2\";s:10:\"2017-10-30\";s:7:\"SUMMARY\";s:13:\"Staff Meeting\";s:3:\"URL\";s:0:\"\";s:11:\"X-CREATEDBY\";i:234;s:7:\"DTSTART\";i:1509350400;s:5:\"DTEND\";i:1509382800;s:5:\"RRULE\";a:4:{s:4:\"FREQ\";s:7:\"MONTHLY\";s:5:\"COUNT\";i:14;s:8:\"INTERVAL\";s:1:\"1\";s:10:\"BYMONTHDAY\";s:3:\"+30\";}s:8:\"MULTIDAY\";s:1:\"1\";s:9:\"NOENDTIME\";s:1:\"0\";s:7:\"X-COLOR\";s:0:\"\";s:9:\"LOCKEVENT\";s:1:\"0\";}', '', 8, 1, 0, 0, 17);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jev_defaults`
--

DROP TABLE IF EXISTS `tol2j_jev_defaults`;
CREATE TABLE `tol2j_jev_defaults` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `subject` text NOT NULL,
  `value` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `language` varchar(20) NOT NULL DEFAULT '*',
  `catid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_jev_users`
--

DROP TABLE IF EXISTS `tol2j_jev_users`;
CREATE TABLE `tol2j_jev_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(2) NOT NULL DEFAULT '0',
  `canuploadimages` tinyint(2) NOT NULL DEFAULT '0',
  `canuploadmovies` tinyint(2) NOT NULL DEFAULT '0',
  `cancreate` tinyint(2) NOT NULL DEFAULT '0',
  `canedit` tinyint(2) NOT NULL DEFAULT '0',
  `canpublishown` tinyint(2) NOT NULL DEFAULT '0',
  `candeleteown` tinyint(2) NOT NULL DEFAULT '0',
  `canpublishall` tinyint(2) NOT NULL DEFAULT '0',
  `candeleteall` tinyint(2) NOT NULL DEFAULT '0',
  `cancreateown` tinyint(2) NOT NULL DEFAULT '0',
  `cancreateglobal` tinyint(2) NOT NULL DEFAULT '0',
  `eventslimit` int(11) NOT NULL DEFAULT '0',
  `extraslimit` int(11) NOT NULL DEFAULT '0',
  `categories` varchar(255) NOT NULL DEFAULT '',
  `calendars` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_jev_users`
--

INSERT INTO `tol2j_jev_users` (`id`, `user_id`, `published`, `canuploadimages`, `canuploadmovies`, `cancreate`, `canedit`, `canpublishown`, `candeleteown`, `canpublishall`, `candeleteall`, `cancreateown`, `cancreateglobal`, `eventslimit`, `extraslimit`, `categories`, `calendars`, `created`) VALUES
(1, 234, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 'all', 'all', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_acl`
--

DROP TABLE IF EXISTS `tol2j_komento_acl`;
CREATE TABLE `tol2j_komento_acl` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cid` varchar(255) NOT NULL,
  `component` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `rules` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_komento_acl`
--

INSERT INTO `tol2j_komento_acl` (`id`, `cid`, `component`, `type`, `rules`) VALUES
(1, '1', 'com_content', 'usergroup', '{\"read_comment\":true,\"read_others_comment\":true,\"read_stickies\":true,\"read_lovies\":true,\"add_comment\":true,\"like_comment\":false,\"report_comment\":false,\"share_comment\":true,\"reply_comment\":false,\"upload_attachment\":false,\"download_attachment\":true,\"edit_own_comment\":false,\"delete_own_comment\":false,\"delete_own_attachment\":false,\"author_edit_comment\":false,\"author_delete_comment\":false,\"author_publish_comment\":false,\"author_unpublish_comment\":false,\"author_stick_comment\":false,\"author_delete_attachment\":false,\"edit_all_comment\":false,\"delete_all_comment\":false,\"publish_all_comment\":false,\"unpublish_all_comment\":false,\"stick_all_comment\":false,\"delete_all_attachment\":false}'),
(2, '9', 'com_content', 'usergroup', '{\"read_comment\":true,\"read_others_comment\":true,\"read_stickies\":true,\"read_lovies\":true,\"add_comment\":true,\"like_comment\":false,\"report_comment\":false,\"share_comment\":true,\"reply_comment\":false,\"upload_attachment\":false,\"download_attachment\":true,\"edit_own_comment\":false,\"delete_own_comment\":false,\"delete_own_attachment\":false,\"author_edit_comment\":false,\"author_delete_comment\":false,\"author_publish_comment\":false,\"author_unpublish_comment\":false,\"author_stick_comment\":false,\"author_delete_attachment\":false,\"edit_all_comment\":false,\"delete_all_comment\":false,\"publish_all_comment\":false,\"unpublish_all_comment\":false,\"stick_all_comment\":false,\"delete_all_attachment\":false}'),
(3, '2', 'com_content', 'usergroup', '{\"read_comment\":true,\"read_others_comment\":true,\"read_stickies\":true,\"read_lovies\":true,\"add_comment\":true,\"like_comment\":true,\"report_comment\":true,\"share_comment\":true,\"reply_comment\":true,\"upload_attachment\":true,\"download_attachment\":true,\"edit_own_comment\":false,\"delete_own_comment\":false,\"delete_own_attachment\":false,\"author_edit_comment\":false,\"author_delete_comment\":false,\"author_publish_comment\":false,\"author_unpublish_comment\":false,\"author_stick_comment\":false,\"author_delete_attachment\":false,\"edit_all_comment\":false,\"delete_all_comment\":false,\"publish_all_comment\":false,\"unpublish_all_comment\":false,\"stick_all_comment\":false,\"delete_all_attachment\":false}'),
(4, '6', 'com_content', 'usergroup', '{\"read_comment\":true,\"read_others_comment\":true,\"read_stickies\":true,\"read_lovies\":true,\"add_comment\":true,\"like_comment\":true,\"report_comment\":true,\"share_comment\":true,\"reply_comment\":true,\"upload_attachment\":true,\"download_attachment\":true,\"edit_own_comment\":true,\"delete_own_comment\":true,\"delete_own_attachment\":true,\"author_edit_comment\":false,\"author_delete_comment\":false,\"author_publish_comment\":false,\"author_unpublish_comment\":false,\"author_stick_comment\":false,\"author_delete_attachment\":false,\"edit_all_comment\":false,\"delete_all_comment\":false,\"publish_all_comment\":false,\"unpublish_all_comment\":false,\"stick_all_comment\":false,\"delete_all_attachment\":false}'),
(5, '7', 'com_content', 'usergroup', '{\"read_comment\":true,\"read_others_comment\":true,\"read_stickies\":true,\"read_lovies\":true,\"add_comment\":true,\"like_comment\":true,\"report_comment\":true,\"share_comment\":true,\"reply_comment\":true,\"upload_attachment\":true,\"download_attachment\":true,\"edit_own_comment\":true,\"delete_own_comment\":true,\"delete_own_attachment\":true,\"author_edit_comment\":false,\"author_delete_comment\":false,\"author_publish_comment\":false,\"author_unpublish_comment\":false,\"author_stick_comment\":false,\"author_delete_attachment\":false,\"edit_all_comment\":true,\"delete_all_comment\":true,\"publish_all_comment\":true,\"unpublish_all_comment\":true,\"stick_all_comment\":true,\"delete_all_attachment\":true}'),
(6, '3', 'com_content', 'usergroup', '{\"read_comment\":true,\"read_others_comment\":true,\"read_stickies\":true,\"read_lovies\":true,\"add_comment\":true,\"like_comment\":true,\"report_comment\":true,\"share_comment\":true,\"reply_comment\":true,\"upload_attachment\":true,\"download_attachment\":true,\"edit_own_comment\":true,\"delete_own_comment\":false,\"delete_own_attachment\":false,\"author_edit_comment\":true,\"author_delete_comment\":true,\"author_publish_comment\":true,\"author_unpublish_comment\":true,\"author_stick_comment\":true,\"author_delete_attachment\":true,\"edit_all_comment\":false,\"delete_all_comment\":false,\"publish_all_comment\":false,\"unpublish_all_comment\":false,\"stick_all_comment\":false,\"delete_all_attachment\":false}'),
(7, '4', 'com_content', 'usergroup', '{\"read_comment\":true,\"read_others_comment\":true,\"read_stickies\":true,\"read_lovies\":true,\"add_comment\":true,\"like_comment\":true,\"report_comment\":true,\"share_comment\":true,\"reply_comment\":true,\"upload_attachment\":true,\"download_attachment\":true,\"edit_own_comment\":true,\"delete_own_comment\":false,\"delete_own_attachment\":false,\"author_edit_comment\":true,\"author_delete_comment\":true,\"author_publish_comment\":true,\"author_unpublish_comment\":true,\"author_stick_comment\":true,\"author_delete_attachment\":true,\"edit_all_comment\":false,\"delete_all_comment\":false,\"publish_all_comment\":false,\"unpublish_all_comment\":false,\"stick_all_comment\":false,\"delete_all_attachment\":false}'),
(8, '5', 'com_content', 'usergroup', '{\"read_comment\":true,\"read_others_comment\":true,\"read_stickies\":true,\"read_lovies\":true,\"add_comment\":true,\"like_comment\":true,\"report_comment\":true,\"share_comment\":true,\"reply_comment\":true,\"upload_attachment\":true,\"download_attachment\":true,\"edit_own_comment\":true,\"delete_own_comment\":false,\"delete_own_attachment\":false,\"author_edit_comment\":true,\"author_delete_comment\":true,\"author_publish_comment\":true,\"author_unpublish_comment\":true,\"author_stick_comment\":true,\"author_delete_attachment\":true,\"edit_all_comment\":false,\"delete_all_comment\":false,\"publish_all_comment\":true,\"unpublish_all_comment\":true,\"stick_all_comment\":true,\"delete_all_attachment\":false}'),
(9, '8', 'com_content', 'usergroup', '{\"read_comment\":true,\"read_others_comment\":true,\"read_stickies\":true,\"read_lovies\":true,\"add_comment\":true,\"like_comment\":true,\"report_comment\":true,\"share_comment\":true,\"reply_comment\":true,\"upload_attachment\":true,\"download_attachment\":true,\"edit_own_comment\":true,\"delete_own_comment\":true,\"delete_own_attachment\":true,\"author_edit_comment\":true,\"author_delete_comment\":true,\"author_publish_comment\":true,\"author_unpublish_comment\":true,\"author_stick_comment\":true,\"author_delete_attachment\":true,\"edit_all_comment\":true,\"delete_all_comment\":true,\"publish_all_comment\":true,\"unpublish_all_comment\":true,\"stick_all_comment\":true,\"delete_all_attachment\":true}');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_actions`
--

DROP TABLE IF EXISTS `tol2j_komento_actions`;
CREATE TABLE `tol2j_komento_actions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(20) NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL,
  `action_by` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `actioned` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_activities`
--

DROP TABLE IF EXISTS `tol2j_komento_activities`;
CREATE TABLE `tol2j_komento_activities` (
  `id` bigint(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `comment_id` bigint(20) NOT NULL,
  `uid` bigint(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_komento_activities`
--

INSERT INTO `tol2j_komento_activities` (`id`, `type`, `comment_id`, `uid`, `created`, `published`) VALUES
(1, 'comment', 2, 235, '2016-09-30 05:40:13', 1),
(2, 'comment', 3, 235, '2016-09-30 05:40:36', 1),
(3, 'comment', 4, 235, '2016-09-30 05:40:53', 1),
(4, 'comment', 5, 235, '2016-09-30 05:58:15', 1),
(5, 'comment', 6, 236, '2016-10-07 11:42:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_cache`
--

DROP TABLE IF EXISTS `tol2j_komento_cache`;
CREATE TABLE `tol2j_komento_cache` (
  `id` int(11) NOT NULL,
  `last_purge` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_komento_cache`
--

INSERT INTO `tol2j_komento_cache` (`id`, `last_purge`) VALUES
(1, '2016-09-30 06:15:02');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_captcha`
--

DROP TABLE IF EXISTS `tol2j_komento_captcha`;
CREATE TABLE `tol2j_komento_captcha` (
  `id` int(11) NOT NULL,
  `response` varchar(5) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_comments`
--

DROP TABLE IF EXISTS `tol2j_komento_comments`;
CREATE TABLE `tol2j_komento_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `component` varchar(255) NOT NULL,
  `cid` bigint(20) UNSIGNED NOT NULL,
  `comment` text,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT '',
  `url` varchar(255) DEFAULT '',
  `ip` varchar(255) DEFAULT '',
  `created_by` bigint(20) UNSIGNED DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) UNSIGNED DEFAULT '0',
  `modified` datetime DEFAULT '0000-00-00 00:00:00',
  `deleted_by` bigint(20) UNSIGNED DEFAULT '0',
  `deleted` datetime DEFAULT '0000-00-00 00:00:00',
  `flag` tinyint(1) DEFAULT '0',
  `published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `publish_up` datetime DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime DEFAULT '0000-00-00 00:00:00',
  `sticked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `sent` tinyint(1) DEFAULT '0',
  `parent_id` int(11) UNSIGNED DEFAULT '0',
  `lft` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `rgt` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `depth` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `address` text,
  `params` text,
  `ratings` int(11) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_komento_comments`
--

INSERT INTO `tol2j_komento_comments` (`id`, `component`, `cid`, `comment`, `name`, `title`, `email`, `url`, `ip`, `created_by`, `created`, `modified_by`, `modified`, `deleted_by`, `deleted`, `flag`, `published`, `publish_up`, `publish_down`, `sticked`, `sent`, `parent_id`, `lft`, `rgt`, `depth`, `latitude`, `longitude`, `address`, `params`, `ratings`) VALUES
(0, 'com_jevents', 8, 'comment about staff meeting', 'CEO', '', 'ceo@tmea.org', '', '::1', 244, '2017-10-31 07:14:43', 0, '2017-10-31 07:14:43', 0, '0000-00-00 00:00:00', 0, 1, '2017-10-31 07:14:43', '0000-00-00 00:00:00', 0, 0, 0, 1, 2, 0, '', '', '', NULL, 0),
(1, 'com_docman', 1, 'Why is this not in the model?', 'Michael', '', 'mbuluma@gbc.co.ke', 'http://gbc.co.k', '::1', 0, '2016-09-29 06:00:04', 0, '2016-09-29 06:00:04', 0, '0000-00-00 00:00:00', 0, 1, '2016-09-29 06:00:04', '0000-00-00 00:00:00', 0, 0, 0, 1, 2, 0, '', '', '', NULL, 0),
(2, 'com_docman', 8, 'muhahahaha', 'Michael', '', 'bulumaknight@gmail.com', '', '197.232.33.130', 235, '2016-09-30 05:40:13', 0, '2016-09-30 05:40:13', 0, '0000-00-00 00:00:00', 0, 1, '2016-09-30 05:40:13', '0000-00-00 00:00:00', 0, 0, 0, 1, 2, 0, '', '', '', NULL, 0),
(3, 'com_docman', 8, 'hello what is this', 'Michael', '', 'bulumaknight@gmail.com', '', '197.232.33.130', 235, '2016-09-30 05:40:36', 0, '2016-09-30 05:40:36', 0, '0000-00-00 00:00:00', 0, 1, '2016-09-30 05:40:36', '0000-00-00 00:00:00', 0, 0, 0, 3, 4, 0, '', '', '', NULL, 0),
(4, 'com_docman', 8, 'ha. im a ninja', 'Michael', '', 'bulumaknight@gmail.com', '', '197.232.33.130', 235, '2016-09-30 05:40:53', 0, '2016-09-30 05:40:53', 0, '0000-00-00 00:00:00', 0, 1, '2016-09-30 05:40:53', '0000-00-00 00:00:00', 0, 0, 0, 5, 6, 0, '', '', '', NULL, 0),
(5, 'com_docman', 7, 'Please let me catch up', 'Michael', '', 'bulumaknight@gmail.com', '', '197.232.33.130', 235, '2016-09-30 05:58:15', 0, '2016-09-30 05:58:15', 0, '0000-00-00 00:00:00', 0, 1, '2016-09-30 05:58:15', '0000-00-00 00:00:00', 0, 0, 0, 1, 2, 0, '', '', '', NULL, 0),
(6, 'com_docman', 27, 'This is a test comment....', 'Francis Karuga', '', 'wamsfrank@gmail.com', '', '197.232.33.130', 236, '2016-10-07 11:42:07', 0, '2016-10-07 11:42:07', 0, '0000-00-00 00:00:00', 0, 1, '2016-10-07 11:42:07', '0000-00-00 00:00:00', 0, 0, 0, 1, 2, 0, '', '', '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_configs`
--

DROP TABLE IF EXISTS `tol2j_komento_configs`;
CREATE TABLE `tol2j_komento_configs` (
  `component` varchar(255) NOT NULL,
  `params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_komento_configs`
--

INSERT INTO `tol2j_komento_configs` (`component`, `params`) VALUES
('com_komento_comments_columns', '{\"column_comment\":1,\"column_published\":1,\"column_sticked\":1,\"column_link\":1,\"column_edit\":1,\"column_component\":1,\"column_article\":1,\"column_date\":1,\"column_author\":1,\"column_id\":1}'),
('com_komento_pending_columns', '{\"column_comment\":1,\"column_published\":1,\"column_link\":1,\"column_edit\":1,\"column_component\":1,\"column_article\":1,\"column_date\":1,\"column_author\":1,\"column_id\":1}'),
('com_komento_reports_columns', '{\"column_comment\":1,\"column_published\":1,\"column_link\":1,\"column_edit\":1,\"column_component\":1,\"column_article\":1,\"column_date\":1,\"column_author\":1,\"column_id\":1}'),
('com_komento', '{\"profile_enable\":\"1\",\"profile_tab_comments\":\"1\",\"profile_tab_activities\":\"1\",\"profile_tab_popular\":\"1\",\"profile_tab_sticked\":\"1\",\"profile_activities_comments\":\"1\",\"profile_activities_replies\":\"1\",\"profile_activities_likes\":\"1\",\"name_type\":\"default\",\"layout_avatar_integration\":\"gravatar\",\"layout_phpbb_path\":\"\",\"layout_phpbb_url\":\"\",\"enable_schema\":\"1\",\"database_clearcaptchaonpageload\":\"0\",\"enable_inline_reply\":\"1\",\"enable_ajax_permalink\":\"1\",\"enable_ajax_load_list\":\"0\",\"enable_ajax_load_stickies\":\"1\",\"enforce_live_stickies\":\"1\",\"enable_ajax_load_lovies\":\"1\",\"enforce_live_lovies\":\"1\",\"enable_shorten_link\":\"1\",\"parent_preload\":\"0\",\"enable_js_form_validation\":\"1\",\"enable_live_form_validation\":\"1\",\"enable_admin_mode\":\"0\",\"enable_warning_messages\":\"1\",\"enable_language_fallback\":\"0\",\"disable_mailq\":\"0\",\"komento_environment\":\"static\",\"komento_mode\":\"compressed\"}'),
('com_content', '{\"enable_komento\":1,\"disable_komento_on_tmpl_component\":\"0\",\"allowed_categories_mode\":\"0\",\"allowed_categories\":\"0\",\"enable_orphanitem_convert\":\"1\",\"orphanitem_ownership\":\"0\",\"enable_login_form\":\"1\",\"login_provider\":\"joomla\",\"enable_moderation\":\"1\",\"requires_moderation\":\"1\",\"subscription_auto\":\"0\",\"subscription_confirmation\":\"0\",\"enable_ratings\":\"1\",\"enable_rss\":\"1\",\"rss_max_items\":\"10\",\"pagebreak_load\":\"last\",\"antispam_akismet\":\"0\",\"antispam_akismet_key\":\"\",\"antispam_akismet_trackback\":\"\",\"antispam_flood_control\":\"0\",\"antispam_flood_interval\":\"10\",\"antispam_min_length_enable\":\"0\",\"antispam_min_length\":\"10\",\"antispam_max_length_enable\":\"0\",\"antispam_max_length\":\"300\",\"filter_word\":\"0\",\"filter_word_text\":\"\",\"antispam_captcha_enable\":\"0\",\"antispam_captcha_type\":\"0\",\"show_captcha\":\"1\",\"antispam_recaptcha_ssl\":\"\",\"antispam_recaptcha_public_key\":\"\",\"antispam_recaptcha_private_key\":\"\",\"antispam_recaptcha_theme\":\"\",\"antispam_recaptcha_lang\":\"\",\"layout_theme\":\"wireframe\",\"enable_responsive\":\"1\",\"tabbed_comments\":\"1\",\"max_threaded_level\":\"5\",\"enable_threaded\":\"1\",\"thread_indentation\":\"60\",\"show_sort_buttons\":\"1\",\"default_sort\":\"oldest\",\"load_previous\":\"1\",\"max_comments_per_page\":\"10\",\"layout_template_override\":\"1\",\"layout_component_override\":\"1\",\"layout_inherit_kuro_css\":\"1\",\"layout_css_admin\":\"kmt-comment-item-admin\",\"layout_css_author\":\"kmt-comment-item-author\",\"layout_css_registered\":\"kmt-comment-item-registered\",\"layout_css_public\":\"kmt-comment-item-public\",\"enable_lapsed_time\":\"1\",\"layout_avatar_enable\":\"1\",\"enable_guest_link\":\"1\",\"enable_permalink\":\"1\",\"enable_share\":\"1\",\"enable_likes\":\"1\",\"enable_reply\":\"1\",\"enable_report\":\"1\",\"enable_location\":\"1\",\"enable_info\":\"1\",\"enable_syntax_highlighting\":\"1\",\"enable_id\":\"0\",\"enable_reply_reference\":\"1\",\"enable_rank_bar\":\"1\",\"name_type\":\"default\",\"guest_label\":\"1\",\"auto_hyperlink\":\"1\",\"links_nofollow\":\"0\",\"datetime_permalink\":\"0\",\"date_format\":\"l, M j Y g:i:sa\",\"allow_video\":\"1\",\"bbcode_video_width\":\"350\",\"bbcode_video_height\":\"350\",\"max_image_width\":\"300\",\"max_image_height\":\"300\",\"layout_frontpage_comment\":\"1\",\"layout_frontpage_readmore\":\"1\",\"layout_frontpage_readmore_use_joomla\":\"0\",\"layout_frontpage_hits\":\"1\",\"layout_frontpage_alignment\":\"right\",\"layout_frontpage_preview\":\"1\",\"preview_count\":\"3\",\"preview_sort\":\"latest\",\"preview_sticked_only\":\"0\",\"preview_parent_only\":\"1\",\"preview_comment_length\":\"300\",\"form_editor\":\"bbcode\",\"form_position\":\"0\",\"form_toggle_button\":\"0\",\"autohide_form_notification\":\"1\",\"form_show_moderate_message\":\"1\",\"scroll_to_comment\":\"1\",\"show_location\":\"1\",\"enable_bbcode\":\"1\",\"enable_subscription\":\"1\",\"show_tnc\":\"1\",\"tnc_text\":\"Before submitting the comment, you agree that:\\n\\na. To accept full responsibility for the comment that you submit.\\nb. To use this function only for lawful purposes.\\nc. Not to post defamatory, abusive, offensive, racist, sexist, threatening, vulgar, obscene, hateful or otherwise inappropriate comments, or to post comments which will constitute a criminal offense or give rise to civil liability.\\nd. Not to post or make available any material which is protected by copyright, trade mark or other proprietary right without the express permission of the owner of the copyright, trade mark or any other proprietary right.\\ne. To evaluate for yourself the accuracy of any opinion, advice or other content.\",\"show_name\":\"1\",\"show_email\":\"1\",\"show_website\":\"1\",\"require_name\":\"1\",\"require_email\":\"0\",\"require_website\":\"0\",\"enable_email_regex\":\"1\",\"email_regex\":[\"%5CS%2B%40%5CS%2B\"],\"enable_website_regex\":\"1\",\"website_regex\":[\"%28http%3A%2F%2F%7Cftp%3A%2F%2F%7Cwww%29%5CS%2B\"],\"bbcode_bold\":\"1\",\"bbcode_italic\":\"1\",\"bbcode_underline\":\"1\",\"bbcode_link\":\"1\",\"bbcode_picture\":\"1\",\"bbcode_video\":\"1\",\"bbcode_bulletlist\":\"1\",\"bbcode_numericlist\":\"1\",\"bbcode_bullet\":\"1\",\"bbcode_quote\":\"1\",\"bbcode_code\":\"1\",\"bbcode_clean\":\"1\",\"bbcode_smile\":\"1\",\"bbcode_happy\":\"1\",\"bbcode_surprised\":\"1\",\"bbcode_tongue\":\"1\",\"bbcode_unhappy\":\"1\",\"bbcode_wink\":\"1\",\"smileycode\":[],\"smileypath\":[],\"layout_avatar_integration\":\"gravatar\",\"use_komento_profile\":\"1\",\"easysocial_profile_popbox\":\"0\",\"gravatar_default_avatar\":\"mm\",\"layout_phpbb_path\":\"\",\"layout_phpbb_url\":\"\",\"share_facebook\":\"1\",\"share_twitter\":\"1\",\"share_googleplus\":\"0\",\"share_linkedin\":\"0\",\"share_tumblr\":\"0\",\"share_digg\":\"0\",\"share_delicious\":\"0\",\"share_reddit\":\"0\",\"share_stumbleupon\":\"0\",\"enable_conversation_bar\":\"1\",\"conversation_bar_include_guest\":\"0\",\"conversation_bar_max_authors\":\"10\",\"enable_stickies\":\"1\",\"max_stickies\":\"5\",\"enable_lovies\":\"1\",\"minimum_likes_lovies\":\"0\",\"max_lovies\":\"5\",\"syntaxhighlighter_theme\":\"default\",\"notification_sendmailonpageload\":\"1\",\"notification_sendmailinhtml\":\"1\",\"notification_enable\":\"1\",\"notification_event_new_comment\":\"1\",\"notification_event_new_reply\":\"1\",\"notification_event_new_pending\":\"1\",\"notification_event_reported_comment\":\"1\",\"notification_to_admins\":\"1\",\"notification_to_author\":\"1\",\"notification_to_subscribers\":\"1\",\"notification_to_usergroup_comment\":\"0\",\"notification_to_usergroup_reply\":\"0\",\"notification_to_usergroup_pending\":\"0\",\"notification_to_usergroup_reported\":\"0\",\"notification_es_enable\":\"0\",\"notification_es_event_new_comment\":\"0\",\"notification_es_event_new_reply\":\"0\",\"notification_es_event_new_like\":\"0\",\"notification_es_to_author\":\"0\",\"notification_es_to_usergroup_comment\":\"0\",\"notification_es_to_usergroup_reply\":\"0\",\"notification_es_to_usergroup_like\":\"0\",\"notification_es_to_participant\":\"0\",\"activities_comment\":\"1\",\"activities_reply\":\"1\",\"activities_like\":\"1\",\"jomsocial_enable_comment\":\"0\",\"jomsocial_enable_reply\":\"0\",\"jomsocial_enable_like\":\"0\",\"jomsocial_comment_length\":\"250\",\"jomsocial_enable_userpoints\":\"0\",\"enable_aup\":\"0\",\"enable_discuss_points\":\"0\",\"enable_discuss_log\":\"0\",\"enable_easysocial_points\":\"0\",\"enable_easysocial_badges\":\"0\",\"enable_easysocial_stream_comment\":\"0\",\"enable_easysocial_stream_like\":\"0\",\"enable_easysocial_sync_comment\":\"0\",\"enable_easysocial_sync_like\":\"0\",\"upload_enable\":\"1\",\"upload_path\":\"\",\"attachment_layout\":\"icon\",\"upload_allowed_extension\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,rar,txt,xcf,xls,zip\",\"upload_max_size\":\"2\",\"upload_max_file\":\"3\",\"upload_image_preview\":\"1\",\"upload_image_fancybox\":\"1\",\"upload_image_overlay\":\"1\",\"trigger_method\":\"component\",\"enable_live_notification\":\"0\",\"live_notification_interval\":\"180\"}'),
('com_docman', '{\"enable_komento\":\"1\",\"disable_komento_on_tmpl_component\":\"0\",\"enable_ratings\":\"0\",\"enable_orphanitem_convert\":\"1\",\"orphanitem_ownership\":\"0\",\"allowed_categories_mode\":\"0\",\"enable_moderation\":\"1\",\"requires_moderation\":[\"1\"],\"subscription_auto\":\"0\",\"subscription_confirmation\":\"0\",\"enable_rss\":\"1\",\"rss_max_items\":\"10\",\"antispam_akismet\":\"0\",\"antispam_akismet_key\":\"\",\"antispam_flood_control\":\"0\",\"antispam_flood_interval\":\"10\",\"antispam_min_length_enable\":\"0\",\"antispam_min_length\":\"10\",\"antispam_max_length_enable\":\"0\",\"antispam_max_length\":\"300\",\"filter_word\":\"0\",\"filter_word_text\":\"\",\"blacklist_ip\":\"\",\"antispam_captcha_enable\":\"0\",\"antispam_captcha_type\":\"0\",\"show_captcha\":[\"1\"],\"antispam_recaptcha_ssl\":\"0\",\"antispam_recaptcha_public_key\":\"\",\"antispam_recaptcha_private_key\":\"\",\"antispam_recaptcha_theme\":\"clean\",\"antispam_recaptcha_lang\":\"en\",\"layout_theme\":\"cleo\",\"enable_responsive\":\"1\",\"tabbed_comments\":\"1\",\"max_threaded_level\":\"5\",\"enable_threaded\":\"1\",\"thread_indentation\":\"60\",\"show_sort_buttons\":\"1\",\"default_sort\":\"oldest\",\"load_previous\":\"1\",\"max_comments_per_page\":\"10\",\"layout_template_override\":\"0\",\"layout_component_override\":\"0\",\"layout_inherit_kuro_css\":\"1\",\"layout_css_admin\":\"kmt-comment-item-admin\",\"layout_css_registered\":\"kmt-comment-item-registered\",\"layout_css_author\":\"kmt-comment-item-author\",\"layout_css_public\":\"kmt-comment-item-public\",\"name_type\":\"default\",\"guest_label\":\"1\",\"auto_hyperlink\":\"1\",\"links_nofollow\":\"0\",\"datetime_permalink\":\"0\",\"date_format\":\"l, M j Y g:i:sa\",\"max_image_width\":\"300\",\"max_image_height\":\"300\",\"allow_video\":\"1\",\"bbcode_video_width\":\"350\",\"layout_frontpage_comment\":\"1\",\"layout_frontpage_readmore\":\"1\",\"layout_frontpage_hits\":\"1\",\"layout_frontpage_alignment\":\"right\",\"layout_frontpage_preview\":\"1\",\"preview_count\":\"3\",\"preview_sort\":\"latest\",\"preview_sticked_only\":\"0\",\"preview_parent_only\":\"1\",\"preview_comment_length\":\"300\",\"enable_lapsed_time\":\"1\",\"layout_avatar_enable\":\"1\",\"enable_guest_link\":\"1\",\"enable_permalink\":\"1\",\"enable_share\":\"1\",\"enable_likes\":\"1\",\"enable_reply\":\"1\",\"enable_report\":\"1\",\"enable_location\":\"1\",\"enable_info\":\"1\",\"enable_syntax_highlighting\":\"1\",\"enable_id\":\"0\",\"enable_reply_reference\":\"1\",\"enable_rank_bar\":\"1\",\"enable_live_notification\":\"0\",\"live_notification_interval\":\"180\",\"form_position\":\"0\",\"enable_login_form\":\"1\",\"login_provider\":\"joomla\",\"form_toggle_button\":\"0\",\"autohide_form_notification\":\"1\",\"form_show_moderate_message\":\"1\",\"scroll_to_comment\":\"1\",\"show_location\":\"1\",\"enable_bbcode\":\"1\",\"enable_subscription\":\"1\",\"show_tnc\":[\"1\"],\"tnc_text\":\"Before submitting the comment, you agree that:\\r\\n\\r\\na. To accept full responsibility for the comment that you submit.\\r\\nb. To use this function only for lawful purposes.\\r\\nc. Not to post defamatory, abusive, offensive, racist, sexist, threatening, vulgar, obscene, hateful or otherwise inappropriate comments, or to post comments which will constitute a criminal offense or give rise to civil liability.\\r\\nd. Not to post or make available any material which is protected by copyright, trade mark or other proprietary right without the express permission of the owner of the copyright, trade mark or any other proprietary right.\\r\\ne. To evaluate for yourself the accuracy of any opinion, advice or other content.\",\"show_name\":\"1\",\"show_email\":\"1\",\"show_website\":\"1\",\"require_name\":\"1\",\"require_email\":\"0\",\"require_website\":\"0\",\"enable_email_regex\":\"1\",\"email_regex\":[\"%5CS%2B%40%5CS%2B\"],\"enable_website_regex\":\"1\",\"website_regex\":[\"%28http%3A%2F%2F%7Cftp%3A%2F%2F%7Cwww%29%5CS%2B\"],\"layout_avatar_integration\":\"gravatar\",\"use_komento_profile\":\"1\",\"easysocial_profile_popbox\":\"0\",\"gravatar_default_avatar\":\"mm\",\"layout_phpbb_path\":\"\",\"layout_phpbb_url\":\"\",\"bbcode_bold\":\"1\",\"bbcode_italic\":\"1\",\"bbcode_underline\":\"1\",\"bbcode_link\":\"1\",\"bbcode_picture\":\"1\",\"bbcode_video\":\"1\",\"bbcode_bulletlist\":\"1\",\"bbcode_numericlist\":\"1\",\"bbcode_bullet\":\"1\",\"bbcode_quote\":\"1\",\"bbcode_code\":\"1\",\"bbcode_clean\":\"1\",\"bbcode_smile\":\"1\",\"bbcode_happy\":\"1\",\"bbcode_surprised\":\"1\",\"bbcode_tongue\":\"1\",\"bbcode_unhappy\":\"1\",\"bbcode_wink\":\"1\",\"smileycode\":[],\"smileypath\":[],\"share_facebook\":\"1\",\"share_twitter\":\"1\",\"share_googleplus\":\"1\",\"share_linkedin\":\"1\",\"share_tumblr\":\"0\",\"share_digg\":\"0\",\"share_delicious\":\"0\",\"share_reddit\":\"0\",\"share_stumbleupon\":\"0\",\"enable_conversation_bar\":\"1\",\"conversation_bar_max_authors\":\"10\",\"conversation_bar_include_guest\":\"0\",\"enable_stickies\":\"1\",\"max_stickies\":\"5\",\"enable_lovies\":\"1\",\"minimum_likes_lovies\":\"0\",\"max_lovies\":\"5\",\"syntaxhighlighter_theme\":\"default\",\"notification_enable\":\"1\",\"notification_sendmailonpageload\":\"1\",\"notification_sendmailinhtml\":\"1\",\"notification_event_new_comment\":\"1\",\"notification_event_new_reply\":\"1\",\"notification_event_new_pending\":\"1\",\"notification_event_reported_comment\":\"1\",\"notification_to_author\":\"1\",\"notification_to_subscribers\":\"1\",\"notification_to_usergroup_comment\":[\"8\"],\"notification_to_usergroup_reply\":[\"8\"],\"notification_to_usergroup_pending\":[\"8\"],\"notification_to_usergroup_reported\":[\"8\"],\"activities_comment\":\"1\",\"activities_reply\":\"1\",\"activities_like\":\"1\",\"upload_enable\":\"1\",\"upload_path\":\"\",\"upload_allowed_extension\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,rar,txt,xcf,xls,zip\",\"upload_max_file\":\"3\",\"upload_max_size\":\"2\",\"upload_image_preview\":\"1\",\"upload_image_fancybox\":\"1\",\"upload_image_overlay\":\"1\",\"trigger_method\":\"component\",\"target\":\"com_docman\",\"tnc\":\"\",\"allowed_categories\":[],\"notification_es_to_usergroup_comment\":[],\"notification_es_to_usergroup_reply\":[],\"notification_es_to_usergroup_like\":[]}'),
('com_komento_comments_columns', '{\"column_comment\":1,\"column_published\":1,\"column_sticked\":1,\"column_link\":1,\"column_edit\":1,\"column_component\":1,\"column_article\":1,\"column_date\":1,\"column_author\":1,\"column_id\":1}'),
('com_komento_pending_columns', '{\"column_comment\":1,\"column_published\":1,\"column_link\":1,\"column_edit\":1,\"column_component\":1,\"column_article\":1,\"column_date\":1,\"column_author\":1,\"column_id\":1}'),
('com_komento_reports_columns', '{\"column_comment\":1,\"column_published\":1,\"column_link\":1,\"column_edit\":1,\"column_component\":1,\"column_article\":1,\"column_date\":1,\"column_author\":1,\"column_id\":1}'),
('com_komento', '{\"profile_enable\":\"1\",\"profile_tab_comments\":\"1\",\"profile_tab_activities\":\"1\",\"profile_tab_popular\":\"1\",\"profile_tab_sticked\":\"1\",\"profile_activities_comments\":\"1\",\"profile_activities_replies\":\"1\",\"profile_activities_likes\":\"1\",\"name_type\":\"default\",\"layout_avatar_integration\":\"gravatar\",\"layout_phpbb_path\":\"\",\"layout_phpbb_url\":\"\",\"enable_schema\":\"1\",\"database_clearcaptchaonpageload\":\"0\",\"enable_inline_reply\":\"1\",\"enable_ajax_permalink\":\"1\",\"enable_ajax_load_list\":\"0\",\"enable_ajax_load_stickies\":\"1\",\"enforce_live_stickies\":\"1\",\"enable_ajax_load_lovies\":\"1\",\"enforce_live_lovies\":\"1\",\"enable_shorten_link\":\"1\",\"parent_preload\":\"0\",\"enable_js_form_validation\":\"1\",\"enable_live_form_validation\":\"1\",\"enable_admin_mode\":\"0\",\"enable_warning_messages\":\"1\",\"enable_language_fallback\":\"0\",\"disable_mailq\":\"0\",\"komento_environment\":\"static\",\"komento_mode\":\"compressed\"}'),
('com_content', '{\"enable_komento\":1,\"disable_komento_on_tmpl_component\":\"0\",\"allowed_categories_mode\":\"0\",\"allowed_categories\":\"0\",\"enable_orphanitem_convert\":\"1\",\"orphanitem_ownership\":\"0\",\"enable_login_form\":\"1\",\"login_provider\":\"joomla\",\"enable_moderation\":\"1\",\"requires_moderation\":\"1\",\"subscription_auto\":\"0\",\"subscription_confirmation\":\"0\",\"enable_ratings\":\"1\",\"enable_rss\":\"1\",\"rss_max_items\":\"10\",\"pagebreak_load\":\"last\",\"antispam_akismet\":\"0\",\"antispam_akismet_key\":\"\",\"antispam_akismet_trackback\":\"\",\"antispam_flood_control\":\"0\",\"antispam_flood_interval\":\"10\",\"antispam_min_length_enable\":\"0\",\"antispam_min_length\":\"10\",\"antispam_max_length_enable\":\"0\",\"antispam_max_length\":\"300\",\"filter_word\":\"0\",\"filter_word_text\":\"\",\"antispam_captcha_enable\":\"0\",\"antispam_captcha_type\":\"0\",\"show_captcha\":\"1\",\"antispam_recaptcha_ssl\":\"\",\"antispam_recaptcha_public_key\":\"\",\"antispam_recaptcha_private_key\":\"\",\"antispam_recaptcha_theme\":\"\",\"antispam_recaptcha_lang\":\"\",\"layout_theme\":\"wireframe\",\"enable_responsive\":\"1\",\"tabbed_comments\":\"1\",\"max_threaded_level\":\"5\",\"enable_threaded\":\"1\",\"thread_indentation\":\"60\",\"show_sort_buttons\":\"1\",\"default_sort\":\"oldest\",\"load_previous\":\"1\",\"max_comments_per_page\":\"10\",\"layout_template_override\":\"1\",\"layout_component_override\":\"1\",\"layout_inherit_kuro_css\":\"1\",\"layout_css_admin\":\"kmt-comment-item-admin\",\"layout_css_author\":\"kmt-comment-item-author\",\"layout_css_registered\":\"kmt-comment-item-registered\",\"layout_css_public\":\"kmt-comment-item-public\",\"enable_lapsed_time\":\"1\",\"layout_avatar_enable\":\"1\",\"enable_guest_link\":\"1\",\"enable_permalink\":\"1\",\"enable_share\":\"1\",\"enable_likes\":\"1\",\"enable_reply\":\"1\",\"enable_report\":\"1\",\"enable_location\":\"1\",\"enable_info\":\"1\",\"enable_syntax_highlighting\":\"1\",\"enable_id\":\"0\",\"enable_reply_reference\":\"1\",\"enable_rank_bar\":\"1\",\"name_type\":\"default\",\"guest_label\":\"1\",\"auto_hyperlink\":\"1\",\"links_nofollow\":\"0\",\"datetime_permalink\":\"0\",\"date_format\":\"l, M j Y g:i:sa\",\"allow_video\":\"1\",\"bbcode_video_width\":\"350\",\"bbcode_video_height\":\"350\",\"max_image_width\":\"300\",\"max_image_height\":\"300\",\"layout_frontpage_comment\":\"1\",\"layout_frontpage_readmore\":\"1\",\"layout_frontpage_readmore_use_joomla\":\"0\",\"layout_frontpage_hits\":\"1\",\"layout_frontpage_alignment\":\"right\",\"layout_frontpage_preview\":\"1\",\"preview_count\":\"3\",\"preview_sort\":\"latest\",\"preview_sticked_only\":\"0\",\"preview_parent_only\":\"1\",\"preview_comment_length\":\"300\",\"form_editor\":\"bbcode\",\"form_position\":\"0\",\"form_toggle_button\":\"0\",\"autohide_form_notification\":\"1\",\"form_show_moderate_message\":\"1\",\"scroll_to_comment\":\"1\",\"show_location\":\"1\",\"enable_bbcode\":\"1\",\"enable_subscription\":\"1\",\"show_tnc\":\"1\",\"tnc_text\":\"Before submitting the comment, you agree that:\\n\\na. To accept full responsibility for the comment that you submit.\\nb. To use this function only for lawful purposes.\\nc. Not to post defamatory, abusive, offensive, racist, sexist, threatening, vulgar, obscene, hateful or otherwise inappropriate comments, or to post comments which will constitute a criminal offense or give rise to civil liability.\\nd. Not to post or make available any material which is protected by copyright, trade mark or other proprietary right without the express permission of the owner of the copyright, trade mark or any other proprietary right.\\ne. To evaluate for yourself the accuracy of any opinion, advice or other content.\",\"show_name\":\"1\",\"show_email\":\"1\",\"show_website\":\"1\",\"require_name\":\"1\",\"require_email\":\"0\",\"require_website\":\"0\",\"enable_email_regex\":\"1\",\"email_regex\":[\"%5CS%2B%40%5CS%2B\"],\"enable_website_regex\":\"1\",\"website_regex\":[\"%28http%3A%2F%2F%7Cftp%3A%2F%2F%7Cwww%29%5CS%2B\"],\"bbcode_bold\":\"1\",\"bbcode_italic\":\"1\",\"bbcode_underline\":\"1\",\"bbcode_link\":\"1\",\"bbcode_picture\":\"1\",\"bbcode_video\":\"1\",\"bbcode_bulletlist\":\"1\",\"bbcode_numericlist\":\"1\",\"bbcode_bullet\":\"1\",\"bbcode_quote\":\"1\",\"bbcode_code\":\"1\",\"bbcode_clean\":\"1\",\"bbcode_smile\":\"1\",\"bbcode_happy\":\"1\",\"bbcode_surprised\":\"1\",\"bbcode_tongue\":\"1\",\"bbcode_unhappy\":\"1\",\"bbcode_wink\":\"1\",\"smileycode\":[],\"smileypath\":[],\"layout_avatar_integration\":\"gravatar\",\"use_komento_profile\":\"1\",\"easysocial_profile_popbox\":\"0\",\"gravatar_default_avatar\":\"mm\",\"layout_phpbb_path\":\"\",\"layout_phpbb_url\":\"\",\"share_facebook\":\"1\",\"share_twitter\":\"1\",\"share_googleplus\":\"0\",\"share_linkedin\":\"0\",\"share_tumblr\":\"0\",\"share_digg\":\"0\",\"share_delicious\":\"0\",\"share_reddit\":\"0\",\"share_stumbleupon\":\"0\",\"enable_conversation_bar\":\"1\",\"conversation_bar_include_guest\":\"0\",\"conversation_bar_max_authors\":\"10\",\"enable_stickies\":\"1\",\"max_stickies\":\"5\",\"enable_lovies\":\"1\",\"minimum_likes_lovies\":\"0\",\"max_lovies\":\"5\",\"syntaxhighlighter_theme\":\"default\",\"notification_sendmailonpageload\":\"1\",\"notification_sendmailinhtml\":\"1\",\"notification_enable\":\"1\",\"notification_event_new_comment\":\"1\",\"notification_event_new_reply\":\"1\",\"notification_event_new_pending\":\"1\",\"notification_event_reported_comment\":\"1\",\"notification_to_admins\":\"1\",\"notification_to_author\":\"1\",\"notification_to_subscribers\":\"1\",\"notification_to_usergroup_comment\":\"0\",\"notification_to_usergroup_reply\":\"0\",\"notification_to_usergroup_pending\":\"0\",\"notification_to_usergroup_reported\":\"0\",\"notification_es_enable\":\"0\",\"notification_es_event_new_comment\":\"0\",\"notification_es_event_new_reply\":\"0\",\"notification_es_event_new_like\":\"0\",\"notification_es_to_author\":\"0\",\"notification_es_to_usergroup_comment\":\"0\",\"notification_es_to_usergroup_reply\":\"0\",\"notification_es_to_usergroup_like\":\"0\",\"notification_es_to_participant\":\"0\",\"activities_comment\":\"1\",\"activities_reply\":\"1\",\"activities_like\":\"1\",\"jomsocial_enable_comment\":\"0\",\"jomsocial_enable_reply\":\"0\",\"jomsocial_enable_like\":\"0\",\"jomsocial_comment_length\":\"250\",\"jomsocial_enable_userpoints\":\"0\",\"enable_aup\":\"0\",\"enable_discuss_points\":\"0\",\"enable_discuss_log\":\"0\",\"enable_easysocial_points\":\"0\",\"enable_easysocial_badges\":\"0\",\"enable_easysocial_stream_comment\":\"0\",\"enable_easysocial_stream_like\":\"0\",\"enable_easysocial_sync_comment\":\"0\",\"enable_easysocial_sync_like\":\"0\",\"upload_enable\":\"1\",\"upload_path\":\"\",\"attachment_layout\":\"icon\",\"upload_allowed_extension\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,rar,txt,xcf,xls,zip\",\"upload_max_size\":\"2\",\"upload_max_file\":\"3\",\"upload_image_preview\":\"1\",\"upload_image_fancybox\":\"1\",\"upload_image_overlay\":\"1\",\"trigger_method\":\"component\",\"enable_live_notification\":\"0\",\"live_notification_interval\":\"180\"}'),
('com_docman', '{\"enable_komento\":\"1\",\"disable_komento_on_tmpl_component\":\"0\",\"enable_ratings\":\"0\",\"enable_orphanitem_convert\":\"1\",\"orphanitem_ownership\":\"0\",\"allowed_categories_mode\":\"0\",\"enable_moderation\":\"1\",\"requires_moderation\":[\"1\"],\"subscription_auto\":\"0\",\"subscription_confirmation\":\"0\",\"enable_rss\":\"1\",\"rss_max_items\":\"10\",\"antispam_akismet\":\"0\",\"antispam_akismet_key\":\"\",\"antispam_flood_control\":\"0\",\"antispam_flood_interval\":\"10\",\"antispam_min_length_enable\":\"0\",\"antispam_min_length\":\"10\",\"antispam_max_length_enable\":\"0\",\"antispam_max_length\":\"300\",\"filter_word\":\"0\",\"filter_word_text\":\"\",\"blacklist_ip\":\"\",\"antispam_captcha_enable\":\"0\",\"antispam_captcha_type\":\"0\",\"show_captcha\":[\"1\"],\"antispam_recaptcha_ssl\":\"0\",\"antispam_recaptcha_public_key\":\"\",\"antispam_recaptcha_private_key\":\"\",\"antispam_recaptcha_theme\":\"clean\",\"antispam_recaptcha_lang\":\"en\",\"layout_theme\":\"cleo\",\"enable_responsive\":\"1\",\"tabbed_comments\":\"1\",\"max_threaded_level\":\"5\",\"enable_threaded\":\"1\",\"thread_indentation\":\"60\",\"show_sort_buttons\":\"1\",\"default_sort\":\"oldest\",\"load_previous\":\"1\",\"max_comments_per_page\":\"10\",\"layout_template_override\":\"0\",\"layout_component_override\":\"0\",\"layout_inherit_kuro_css\":\"1\",\"layout_css_admin\":\"kmt-comment-item-admin\",\"layout_css_registered\":\"kmt-comment-item-registered\",\"layout_css_author\":\"kmt-comment-item-author\",\"layout_css_public\":\"kmt-comment-item-public\",\"name_type\":\"default\",\"guest_label\":\"1\",\"auto_hyperlink\":\"1\",\"links_nofollow\":\"0\",\"datetime_permalink\":\"0\",\"date_format\":\"l, M j Y g:i:sa\",\"max_image_width\":\"300\",\"max_image_height\":\"300\",\"allow_video\":\"1\",\"bbcode_video_width\":\"350\",\"layout_frontpage_comment\":\"1\",\"layout_frontpage_readmore\":\"1\",\"layout_frontpage_hits\":\"1\",\"layout_frontpage_alignment\":\"right\",\"layout_frontpage_preview\":\"1\",\"preview_count\":\"3\",\"preview_sort\":\"latest\",\"preview_sticked_only\":\"0\",\"preview_parent_only\":\"1\",\"preview_comment_length\":\"300\",\"enable_lapsed_time\":\"1\",\"layout_avatar_enable\":\"1\",\"enable_guest_link\":\"1\",\"enable_permalink\":\"1\",\"enable_share\":\"1\",\"enable_likes\":\"1\",\"enable_reply\":\"1\",\"enable_report\":\"1\",\"enable_location\":\"1\",\"enable_info\":\"1\",\"enable_syntax_highlighting\":\"1\",\"enable_id\":\"0\",\"enable_reply_reference\":\"1\",\"enable_rank_bar\":\"1\",\"enable_live_notification\":\"0\",\"live_notification_interval\":\"180\",\"form_position\":\"0\",\"enable_login_form\":\"1\",\"login_provider\":\"joomla\",\"form_toggle_button\":\"0\",\"autohide_form_notification\":\"1\",\"form_show_moderate_message\":\"1\",\"scroll_to_comment\":\"1\",\"show_location\":\"1\",\"enable_bbcode\":\"1\",\"enable_subscription\":\"1\",\"show_tnc\":[\"1\"],\"tnc_text\":\"Before submitting the comment, you agree that:\\r\\n\\r\\na. To accept full responsibility for the comment that you submit.\\r\\nb. To use this function only for lawful purposes.\\r\\nc. Not to post defamatory, abusive, offensive, racist, sexist, threatening, vulgar, obscene, hateful or otherwise inappropriate comments, or to post comments which will constitute a criminal offense or give rise to civil liability.\\r\\nd. Not to post or make available any material which is protected by copyright, trade mark or other proprietary right without the express permission of the owner of the copyright, trade mark or any other proprietary right.\\r\\ne. To evaluate for yourself the accuracy of any opinion, advice or other content.\",\"show_name\":\"1\",\"show_email\":\"1\",\"show_website\":\"1\",\"require_name\":\"1\",\"require_email\":\"0\",\"require_website\":\"0\",\"enable_email_regex\":\"1\",\"email_regex\":[\"%5CS%2B%40%5CS%2B\"],\"enable_website_regex\":\"1\",\"website_regex\":[\"%28http%3A%2F%2F%7Cftp%3A%2F%2F%7Cwww%29%5CS%2B\"],\"layout_avatar_integration\":\"gravatar\",\"use_komento_profile\":\"1\",\"easysocial_profile_popbox\":\"0\",\"gravatar_default_avatar\":\"mm\",\"layout_phpbb_path\":\"\",\"layout_phpbb_url\":\"\",\"bbcode_bold\":\"1\",\"bbcode_italic\":\"1\",\"bbcode_underline\":\"1\",\"bbcode_link\":\"1\",\"bbcode_picture\":\"1\",\"bbcode_video\":\"1\",\"bbcode_bulletlist\":\"1\",\"bbcode_numericlist\":\"1\",\"bbcode_bullet\":\"1\",\"bbcode_quote\":\"1\",\"bbcode_code\":\"1\",\"bbcode_clean\":\"1\",\"bbcode_smile\":\"1\",\"bbcode_happy\":\"1\",\"bbcode_surprised\":\"1\",\"bbcode_tongue\":\"1\",\"bbcode_unhappy\":\"1\",\"bbcode_wink\":\"1\",\"smileycode\":[],\"smileypath\":[],\"share_facebook\":\"1\",\"share_twitter\":\"1\",\"share_googleplus\":\"1\",\"share_linkedin\":\"1\",\"share_tumblr\":\"0\",\"share_digg\":\"0\",\"share_delicious\":\"0\",\"share_reddit\":\"0\",\"share_stumbleupon\":\"0\",\"enable_conversation_bar\":\"1\",\"conversation_bar_max_authors\":\"10\",\"conversation_bar_include_guest\":\"0\",\"enable_stickies\":\"1\",\"max_stickies\":\"5\",\"enable_lovies\":\"1\",\"minimum_likes_lovies\":\"0\",\"max_lovies\":\"5\",\"syntaxhighlighter_theme\":\"default\",\"notification_enable\":\"1\",\"notification_sendmailonpageload\":\"1\",\"notification_sendmailinhtml\":\"1\",\"notification_event_new_comment\":\"1\",\"notification_event_new_reply\":\"1\",\"notification_event_new_pending\":\"1\",\"notification_event_reported_comment\":\"1\",\"notification_to_author\":\"1\",\"notification_to_subscribers\":\"1\",\"notification_to_usergroup_comment\":[\"8\"],\"notification_to_usergroup_reply\":[\"8\"],\"notification_to_usergroup_pending\":[\"8\"],\"notification_to_usergroup_reported\":[\"8\"],\"activities_comment\":\"1\",\"activities_reply\":\"1\",\"activities_like\":\"1\",\"upload_enable\":\"1\",\"upload_path\":\"\",\"upload_allowed_extension\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,rar,txt,xcf,xls,zip\",\"upload_max_file\":\"3\",\"upload_max_size\":\"2\",\"upload_image_preview\":\"1\",\"upload_image_fancybox\":\"1\",\"upload_image_overlay\":\"1\",\"trigger_method\":\"component\",\"target\":\"com_docman\",\"tnc\":\"\",\"allowed_categories\":[],\"notification_es_to_usergroup_comment\":[],\"notification_es_to_usergroup_reply\":[],\"notification_es_to_usergroup_like\":[]}'),
('com_jevents', '{\"enable_komento\":\"1\",\"disable_komento_on_tmpl_component\":\"0\",\"enable_ratings\":\"0\",\"enable_orphanitem_convert\":\"1\",\"orphanitem_ownership\":\"0\",\"allowed_categories_mode\":\"0\",\"enable_moderation\":\"1\",\"requires_moderation\":[\"1\"],\"subscription_auto\":\"0\",\"subscription_confirmation\":\"0\",\"enable_rss\":\"1\",\"rss_max_items\":\"10\",\"antispam_akismet\":\"0\",\"antispam_akismet_key\":\"\",\"antispam_flood_control\":\"0\",\"antispam_flood_interval\":\"10\",\"antispam_min_length_enable\":\"0\",\"antispam_min_length\":\"10\",\"antispam_max_length_enable\":\"0\",\"antispam_max_length\":\"300\",\"filter_word\":\"0\",\"filter_word_text\":\"\",\"blacklist_ip\":\"\",\"antispam_captcha_enable\":\"0\",\"antispam_captcha_type\":\"0\",\"show_captcha\":[\"1\"],\"antispam_recaptcha_ssl\":\"0\",\"antispam_recaptcha_public_key\":\"\",\"antispam_recaptcha_private_key\":\"\",\"antispam_recaptcha_theme\":\"clean\",\"antispam_recaptcha_lang\":\"en\",\"layout_theme\":\"wireframe\",\"enable_responsive\":\"1\",\"tabbed_comments\":\"1\",\"max_threaded_level\":\"5\",\"enable_threaded\":\"1\",\"thread_indentation\":\"60\",\"show_sort_buttons\":\"1\",\"default_sort\":\"oldest\",\"load_previous\":\"1\",\"max_comments_per_page\":\"10\",\"layout_template_override\":\"1\",\"layout_component_override\":\"1\",\"layout_inherit_kuro_css\":\"1\",\"layout_css_admin\":\"kmt-comment-item-admin\",\"layout_css_registered\":\"kmt-comment-item-registered\",\"layout_css_author\":\"kmt-comment-item-author\",\"layout_css_public\":\"kmt-comment-item-public\",\"name_type\":\"default\",\"guest_label\":\"1\",\"auto_hyperlink\":\"1\",\"links_nofollow\":\"0\",\"datetime_permalink\":\"0\",\"date_format\":\"l, M j Y g:i:sa\",\"max_image_width\":\"300\",\"max_image_height\":\"300\",\"allow_video\":\"1\",\"bbcode_video_width\":\"350\",\"layout_frontpage_comment\":\"1\",\"layout_frontpage_readmore\":\"1\",\"layout_frontpage_hits\":\"1\",\"layout_frontpage_alignment\":\"right\",\"layout_frontpage_preview\":\"1\",\"preview_count\":\"3\",\"preview_sort\":\"latest\",\"preview_sticked_only\":\"0\",\"preview_parent_only\":\"1\",\"preview_comment_length\":\"300\",\"enable_lapsed_time\":\"1\",\"layout_avatar_enable\":\"1\",\"enable_guest_link\":\"1\",\"enable_permalink\":\"1\",\"enable_share\":\"1\",\"enable_likes\":\"1\",\"enable_reply\":\"1\",\"enable_report\":\"1\",\"enable_location\":\"1\",\"enable_info\":\"1\",\"enable_syntax_highlighting\":\"1\",\"enable_id\":\"0\",\"enable_reply_reference\":\"1\",\"enable_rank_bar\":\"1\",\"enable_live_notification\":\"0\",\"live_notification_interval\":\"180\",\"form_position\":\"0\",\"enable_login_form\":\"1\",\"login_provider\":\"joomla\",\"form_toggle_button\":\"0\",\"autohide_form_notification\":\"1\",\"form_show_moderate_message\":\"1\",\"scroll_to_comment\":\"1\",\"show_location\":\"1\",\"enable_bbcode\":\"1\",\"enable_subscription\":\"1\",\"show_tnc\":[\"1\"],\"tnc_text\":\"Before submitting the comment, you agree that:\\r\\n\\r\\na. To accept full responsibility for the comment that you submit.\\r\\nb. To use this function only for lawful purposes.\\r\\nc. Not to post defamatory, abusive, offensive, racist, sexist, threatening, vulgar, obscene, hateful or otherwise inappropriate comments, or to post comments which will constitute a criminal offense or give rise to civil liability.\\r\\nd. Not to post or make available any material which is protected by copyright, trade mark or other proprietary right without the express permission of the owner of the copyright, trade mark or any other proprietary right.\\r\\ne. To evaluate for yourself the accuracy of any opinion, advice or other content.\",\"show_name\":\"1\",\"show_email\":\"1\",\"show_website\":\"1\",\"require_name\":\"1\",\"require_email\":\"0\",\"require_website\":\"0\",\"enable_email_regex\":\"1\",\"email_regex\":[\"%5CS%2B%40%5CS%2B\"],\"enable_website_regex\":\"1\",\"website_regex\":[\"%28http%3A%2F%2F%7Cftp%3A%2F%2F%7Cwww%29%5CS%2B\"],\"layout_avatar_integration\":\"gravatar\",\"use_komento_profile\":\"1\",\"easysocial_profile_popbox\":\"0\",\"gravatar_default_avatar\":\"mm\",\"layout_phpbb_path\":\"\",\"layout_phpbb_url\":\"\",\"bbcode_bold\":\"1\",\"bbcode_italic\":\"1\",\"bbcode_underline\":\"1\",\"bbcode_link\":\"1\",\"bbcode_picture\":\"1\",\"bbcode_video\":\"1\",\"bbcode_bulletlist\":\"1\",\"bbcode_numericlist\":\"1\",\"bbcode_bullet\":\"1\",\"bbcode_quote\":\"1\",\"bbcode_code\":\"1\",\"bbcode_clean\":\"1\",\"bbcode_smile\":\"1\",\"bbcode_happy\":\"1\",\"bbcode_surprised\":\"1\",\"bbcode_tongue\":\"1\",\"bbcode_unhappy\":\"1\",\"bbcode_wink\":\"1\",\"smileycode\":[],\"smileypath\":[],\"share_facebook\":\"1\",\"share_twitter\":\"1\",\"share_googleplus\":\"0\",\"share_linkedin\":\"0\",\"share_tumblr\":\"0\",\"share_digg\":\"0\",\"share_delicious\":\"0\",\"share_reddit\":\"0\",\"share_stumbleupon\":\"0\",\"enable_conversation_bar\":\"1\",\"conversation_bar_max_authors\":\"10\",\"conversation_bar_include_guest\":\"0\",\"enable_stickies\":\"1\",\"max_stickies\":\"5\",\"enable_lovies\":\"1\",\"minimum_likes_lovies\":\"0\",\"max_lovies\":\"5\",\"syntaxhighlighter_theme\":\"default\",\"notification_enable\":\"1\",\"notification_sendmailonpageload\":\"1\",\"notification_sendmailinhtml\":\"1\",\"notification_event_new_comment\":\"1\",\"notification_event_new_reply\":\"1\",\"notification_event_new_pending\":\"1\",\"notification_event_reported_comment\":\"1\",\"notification_to_author\":\"1\",\"notification_to_subscribers\":\"1\",\"activities_comment\":\"1\",\"activities_reply\":\"1\",\"activities_like\":\"1\",\"upload_enable\":\"1\",\"upload_path\":\"\",\"upload_allowed_extension\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,rar,txt,xcf,xls,zip\",\"upload_max_file\":\"3\",\"upload_max_size\":\"2\",\"upload_image_preview\":\"1\",\"upload_image_fancybox\":\"1\",\"upload_image_overlay\":\"1\",\"trigger_method\":\"component\",\"target\":\"com_jevents\",\"tnc\":\"\",\"allowed_categories\":[],\"notification_to_usergroup_comment\":[],\"notification_to_usergroup_reply\":[],\"notification_to_usergroup_pending\":[],\"notification_to_usergroup_reported\":[],\"notification_es_to_usergroup_comment\":[],\"notification_es_to_usergroup_reply\":[],\"notification_es_to_usergroup_like\":[]}');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_hashkeys`
--

DROP TABLE IF EXISTS `tol2j_komento_hashkeys`;
CREATE TABLE `tol2j_komento_hashkeys` (
  `id` bigint(11) NOT NULL,
  `uid` bigint(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `key` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_ipfilter`
--

DROP TABLE IF EXISTS `tol2j_komento_ipfilter`;
CREATE TABLE `tol2j_komento_ipfilter` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `component` varchar(255) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `rules` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_mailq`
--

DROP TABLE IF EXISTS `tol2j_komento_mailq`;
CREATE TABLE `tol2j_komento_mailq` (
  `id` int(11) NOT NULL,
  `mailfrom` varchar(255) DEFAULT NULL,
  `fromname` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `created` datetime NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'text',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_komento_mailq`
--

INSERT INTO `tol2j_komento_mailq` (`id`, `mailfrom`, `fromname`, `recipient`, `subject`, `body`, `created`, `type`, `status`) VALUES
(1, 'mbuluma@gbc.co.ke', 'TMEA Extranet', 'mbuluma@gbc.co.ke', 'A new comment is posted (CRA KAM MEETING Minutes 200416)', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title></title>\n</head>\n\n<body style=\"margin:0;padding:0;background:#ddd;\">\n	<div style=\"width:100%;background:#ddd;margin:0;padding:50px 0 80px;color:#798796;font-family:\'Lucida Grande\',Tahoma,Arial;font-size:12px;\">\n\n	<center style=\"display:block;padding:30px 0\">\n		<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:590px;background:#fff;border:1px solid #b5bbc1;border-bottom-color:#9ba3ab;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;\">\n			<tbody>\n				<tr>\n					<td style=\"padding:20px;border-bottom:1px solid #b5bbc1;;background:#f5f5f5;border-radius:3px 3px 0 0;-moz-border-radius:3px 3px 0 0;-webkit-border-radius:3px 3px 0 0;\">\n						Good day Super User,\n						<br /><br />\n						A new comment is posted on:						<br />\n						<a href=\"http://gbc.me.ke/tmea/index.php/dms/board/7-cra-kam-meeting-minutes-200416\" style=\"font-weight:bold;color:#477fda;text-decoration:none;font-size:16px;line-height:20px\">CRA KAM MEETING Minutes 200416</a>\n						<br />\n						<br />\n						Below is a snippet from the comment that has been posted.					</td>\n				</tr>\n				<tr>\n					<td style=\"padding:15px 20px;line-height:1.5;color:#555;font-family:\'Lucida Grande\',Tahoma,Arial;font-size:12px;text-align:left\">\n						<div style=\"display:inline-block;width:100%;padding-bottom:20px;\">\n							<img src=\"http://www.gravatar.com/avatar/a41d8256fc7412f5eb8891555a2e41ce?s=100&amp;d=mm\" width=\"50\" style=\"float:left;width:50px;height:auto;border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px;\" />\n							<div style=\"margin-left:60px\">\n								<span style=\"font-weight:bold;color:#477fda;text-decoration:none\">Michael</span>\n								<span style=\"color:#999\">- Posted on Friday, Sep 30 2016 5:58:15am</span>\n								<div style=\"font-size:12px;margin-top:3px;\">\n									Please let me catch up								</div>\n							</div>\n						</div>\n\n						<div style=\"color:#555;clear:both;border-top:1px solid #ddd;padding:20px 0 10px\">\n							<a href=\"http://gbc.me.ke/tmea/index.php/dms/board/7-cra-kam-meeting-minutes-200416#kmt-5\" target=\"_blank\" style=\"display:inline-block;padding:5px 15px;background:#fc0;border:1px solid #caa200;border-bottom-color:#977900;color:#534200;text-shadow:0 1px 0 #ffe684;font-weight:bold;box-shadow:inset 0 1px 0 #ffe064;-moz-box-shadow:inset 0 1px 0 #ffe064;-webkit-box-shadow:inset 0 1px 0 #ffe064;border-radius:2px;moz-border-radius:2px;-webkit-border-radius:2px;text-decoration:none!important\">\n								View Comment &nbsp; &raquo;							</a>\n						</div>\n					</td>\n				</tr>\n			</tbody>\n		</table>\n\n			</center>\n\n	</div>\n</body>\n</html>\n', '2016-09-30 05:58:15', 'html', 1),
(2, 'mbuluma@gbc.co.ke', 'TMEA Extranet', 'mbuluma@gbc.co.ke', 'A new comment is posted (TRADEMARK EASTAFRICA TEST DOCUMENT)', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n<title></title>\n</head>\n\n<body style=\"margin:0;padding:0;background:#ddd;\">\n	<div style=\"width:100%;background:#ddd;margin:0;padding:50px 0 80px;color:#798796;font-family:\'Lucida Grande\',Tahoma,Arial;font-size:12px;\">\n\n	<center style=\"display:block;padding:30px 0\">\n		<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:590px;background:#fff;border:1px solid #b5bbc1;border-bottom-color:#9ba3ab;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;\">\n			<tbody>\n				<tr>\n					<td style=\"padding:20px;border-bottom:1px solid #b5bbc1;;background:#f5f5f5;border-radius:3px 3px 0 0;-moz-border-radius:3px 3px 0 0;-webkit-border-radius:3px 3px 0 0;\">\n						Good day Super User,\n						<br /><br />\n						A new comment is posted on:						<br />\n						<a href=\"http://gbc.me.ke/tmea/index.php?option=com_docman&amp;view=document&amp;alias=27-trademark-eastafrica-test-document-1&amp;category_slug=council&amp;Itemid=524\" style=\"font-weight:bold;color:#477fda;text-decoration:none;font-size:16px;line-height:20px\">TRADEMARK EASTAFRICA TEST DOCUMENT</a>\n						<br />\n						<br />\n						Below is a snippet from the comment that has been posted.					</td>\n				</tr>\n				<tr>\n					<td style=\"padding:15px 20px;line-height:1.5;color:#555;font-family:\'Lucida Grande\',Tahoma,Arial;font-size:12px;text-align:left\">\n						<div style=\"display:inline-block;width:100%;padding-bottom:20px;\">\n							<img src=\"http://www.gravatar.com/avatar/a262ad7501757daeb2c3f888bd6b3699?s=100&amp;d=mm\" width=\"50\" style=\"float:left;width:50px;height:auto;border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px;\" />\n							<div style=\"margin-left:60px\">\n								<span style=\"font-weight:bold;color:#477fda;text-decoration:none\">Francis Karuga</span>\n								<span style=\"color:#999\">- Posted on Friday, Oct 7 2016 11:42:07am</span>\n								<div style=\"font-size:12px;margin-top:3px;\">\n									This is a test comment....								</div>\n							</div>\n						</div>\n\n						<div style=\"color:#555;clear:both;border-top:1px solid #ddd;padding:20px 0 10px\">\n							<a href=\"http://gbc.me.ke/tmea/index.php?option=com_docman&amp;view=document&amp;alias=27-trademark-eastafrica-test-document-1&amp;category_slug=council&amp;Itemid=524#kmt-6\" target=\"_blank\" style=\"display:inline-block;padding:5px 15px;background:#fc0;border:1px solid #caa200;border-bottom-color:#977900;color:#534200;text-shadow:0 1px 0 #ffe684;font-weight:bold;box-shadow:inset 0 1px 0 #ffe064;-moz-box-shadow:inset 0 1px 0 #ffe064;-webkit-box-shadow:inset 0 1px 0 #ffe064;border-radius:2px;moz-border-radius:2px;-webkit-border-radius:2px;text-decoration:none!important\">\n								View Comment &nbsp; &raquo;							</a>\n						</div>\n					</td>\n				</tr>\n			</tbody>\n		</table>\n\n			</center>\n\n	</div>\n</body>\n</html>\n', '2016-10-07 11:42:07', 'html', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_subscription`
--

DROP TABLE IF EXISTS `tol2j_komento_subscription`;
CREATE TABLE `tol2j_komento_subscription` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(20) NOT NULL,
  `component` varchar(255) NOT NULL,
  `cid` bigint(20) UNSIGNED NOT NULL,
  `userid` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_komento_subscription`
--

INSERT INTO `tol2j_komento_subscription` (`id`, `type`, `component`, `cid`, `userid`, `fullname`, `email`, `created`, `published`) VALUES
(1, 'comment', 'com_docman', 1, 0, 'Michael', 'mbuluma@gbc.co.ke', '2016-09-29 06:00:04', 1),
(2, 'comment', 'com_docman', 27, 236, 'Francis Karuga', 'wamsfrank@gmail.com', '2016-10-14 07:31:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_komento_uploads`
--

DROP TABLE IF EXISTS `tol2j_komento_uploads`;
CREATE TABLE `tol2j_komento_uploads` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `filename` text NOT NULL,
  `hashname` text NOT NULL,
  `path` text,
  `created` datetime NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT '0',
  `published` tinyint(1) NOT NULL,
  `mime` text NOT NULL,
  `size` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_languages`
--

DROP TABLE IF EXISTS `tol2j_languages`;
CREATE TABLE `tol2j_languages` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(11) NOT NULL,
  `lang_code` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_native` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sef` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_languages`
--

INSERT INTO `tol2j_languages` (`lang_id`, `asset_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 0, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_menu`
--

DROP TABLE IF EXISTS `tol2j_menu`;
CREATE TABLE `tol2j_menu` (
  `id` int(11) NOT NULL,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_menu`
--

INSERT INTO `tol2j_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 207, 0, '*', 0),
(3, 'menu', 'com_banners', 'Banners', '', 'com-jevents/Banners/Banners', 'index.php?option=com_banners', 'component', 0, 2, 3, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 217, 218, 0, '*', 1),
(4, 'menu', 'com_banners_categories', 'Categories', '', 'com-jevents/Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 0, 2, 3, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 219, 220, 0, '*', 1),
(5, 'menu', 'com_banners_clients', 'Clients', '', 'com-jevents/Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 0, 2, 3, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 221, 222, 0, '*', 1),
(6, 'menu', 'com_banners_tracks', 'Tracks', '', 'com-jevents/Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 0, 2, 3, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 223, 224, 0, '*', 1),
(7, 'menu', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 0, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 71, 76, 0, '*', 1),
(8, 'menu', 'com_contact_contacts', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 0, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 72, 73, 0, '*', 1),
(9, 'menu', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 0, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 74, 75, 0, '*', 1),
(10, 'menu', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 0, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 77, 80, 0, '*', 1),
(11, 'menu', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 78, 79, 0, '*', 1),
(13, 'menu', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 81, 86, 0, '*', 1),
(14, 'menu', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 82, 83, 0, '*', 1),
(15, 'menu', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 0, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 84, 85, 0, '*', 1),
(16, 'menu', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 0, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 93, 94, 0, '*', 1),
(17, 'menu', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 0, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 91, 92, 0, '*', 1),
(21, 'menu', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 0, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 89, 90, 0, '*', 1),
(22, 'menu', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 0, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 87, 88, 0, '*', 1),
(201, 'usermenu', 'Your Profile', 'your-profile', '', 'your-profile', 'index.php?option=com_users&view=profile', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 2, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 95, 96, 0, '*', 0),
(229, 'mainmenu', 'Single Contact', 'single-contact', '', 'contact-component/single-contact', 'index.php?option=com_contact&view=contact&id=1', 'component', 0, 270, 2, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"presentation_style\":\"\",\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"show_tags\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_links\":\"\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 164, 165, 0, '*', 0),
(251, 'mainmenu', 'Contact Categories', 'contact-categories', '', 'contact-component/contact-categories', 'index.php?option=com_contact&view=categories&id=16', 'component', 0, 270, 2, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_base_description\":\"\",\"categories_description\":\"\",\"maxLevelcat\":\"\",\"show_empty_categories_cat\":\"\",\"show_subcat_desc_cat\":\"\",\"show_cat_items_cat\":\"\",\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"\",\"show_subcat_desc\":\"\",\"show_cat_items\":\"\",\"filter_field\":\"\",\"show_pagination_limit\":\"\",\"show_headings\":\"\",\"show_position_headings\":\"\",\"show_email_headings\":\"\",\"show_telephone_headings\":\"\",\"show_mobile_headings\":\"\",\"show_fax_headings\":\"\",\"show_suburb_headings\":\"\",\"show_state_headings\":\"\",\"show_country_headings\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"presentation_style\":\"sliders\",\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_links\":\"1\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\",\"show_feed_link\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 160, 161, 0, '*', 0),
(252, 'mainmenu', 'News Feed Categories', 'new-feed-categories', '', 'joomla/other-components/news-feeds-component/new-feed-categories', 'index.php?option=com_newsfeeds&view=categories&id=0', 'component', 0, 267, 4, 17, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_base_description\":\"\",\"categories_description\":\"\",\"maxLevelcat\":\"\",\"show_empty_categories_cat\":\"\",\"show_subcat_desc_cat\":\"\",\"show_cat_items_cat\":\"\",\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_subcat_desc\":\"\",\"show_cat_items\":\"\",\"filter_field\":\"\",\"show_pagination_limit\":\"\",\"show_headings\":\"\",\"show_articles\":\"\",\"show_link\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"show_feed_image\":\"\",\"show_feed_description\":\"\",\"show_item_description\":\"\",\"feed_character_count\":\"0\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 56, 57, 0, '*', 0),
(253, 'mainmenu', 'News Feed Category', 'news-feed-category', '', 'joomla/other-components/news-feeds-component/news-feed-category', 'index.php?option=com_newsfeeds&view=category&id=17', 'component', 0, 267, 4, 17, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"\",\"show_subcat_desc\":\"\",\"show_cat_items\":\"\",\"filter_field\":\"\",\"show_pagination_limit\":\"\",\"show_headings\":\"\",\"show_articles\":\"\",\"show_link\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"show_feed_image\":\"\",\"show_feed_description\":\"\",\"show_item_description\":\"\",\"feed_character_count\":\"0\",\"feed_display_order\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 60, 61, 0, '*', 0),
(254, 'mainmenu', 'Single News Feed', 'single-news-feed', '', 'joomla/other-components/news-feeds-component/single-news-feed', 'index.php?option=com_newsfeeds&view=newsfeed&id=4', 'component', 0, 267, 4, 17, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_feed_image\":\"\",\"show_feed_description\":\"\",\"show_item_description\":\"\",\"show_tags\":\"\",\"feed_character_count\":\"0\",\"feed_display_order\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 58, 59, 0, '*', 0),
(255, 'mainmenu', 'Search', 'search', '', 'joomla/other-components/search', 'index.php?option=com_search&view=search', 'component', 0, 268, 3, 19, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"search_areas\":\"1\",\"show_date\":\"1\",\"searchphrase\":\"0\",\"ordering\":\"newest\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 67, 68, 0, '*', 0),
(257, 'mainmenu', 'Single Article', 'single-article', '', 'joomla/content-component/single-article', 'index.php?option=com_content&view=article&id=73', 'component', 0, 266, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 43, 44, 0, '*', 0),
(259, 'mainmenu', 'Article Category Blog', 'article-category-blog', '', 'joomla/content-component/article-category-blog', 'index.php?option=com_content&view=category&layout=blog&id=79', 'component', 0, 266, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"layout_type\":\"blog\",\"show_category_heading_title_text\":\"\",\"show_category_title\":\"\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"show_cat_tags\":\"\",\"page_subheading\":\"\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"\",\"order_date\":\"\",\"show_pagination\":\"2\",\"show_pagination_results\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"1\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 45, 46, 0, '*', 0),
(260, 'mainmenu', 'Article Category List', 'article-category-list', '', 'joomla/content-component/article-category-list', 'index.php?option=com_content&view=category&id=19', 'component', 0, 266, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_category_heading_title\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"show_cat_tags\":\"\",\"page_subheading\":\"\",\"show_pagination_limit\":\"\",\"filter_field\":\"\",\"show_headings\":\"\",\"list_show_date\":\"\",\"date_format\":\"\",\"list_show_hits\":\"\",\"list_show_author\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"alpha\",\"order_date\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"display_num\":\"10\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 47, 48, 0, '*', 0),
(262, 'mainmenu', 'Featured Articles', 'featured-articles', '', 'joomla/content-component/featured-articles', 'index.php?option=com_content&view=featured', 'component', 0, 266, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"featured_categories\":[\"\"],\"layout_type\":\"blog\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"1\",\"orderby_pri\":\"\",\"orderby_sec\":\"front\",\"order_date\":\"\",\"show_pagination\":\"2\",\"show_pagination_results\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"1\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 49, 50, 0, '*', 0),
(263, 'mainmenu', 'Submit Article', 'submit-article', '', 'joomla/content-component/submit-article', 'index.php?option=com_content&view=form&layout=edit', 'component', 0, 266, 3, 22, 0, '0000-00-00 00:00:00', 0, 3, '', 0, '{\"enable_category\":\"0\",\"catid\":\"19\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 51, 52, 0, '*', 0),
(266, 'mainmenu', 'Content Component', 'content-component', '', 'joomla/content-component', '#', 'url', 0, 280, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 42, 53, 0, '*', 0),
(267, 'mainmenu', 'News Feeds Component', 'news-feeds-component', '', 'joomla/other-components/news-feeds-component', 'index.php?option=com_content&view=article&id=60', 'component', 0, 268, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"Newsfeeds Categories View \",\"show_page_heading\":1,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"left\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 55, 62, 0, '*', 0),
(268, 'mainmenu', 'Other Components', 'other-components', '', 'joomla/other-components', '#', 'url', 0, 280, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 54, 69, 0, '*', 0),
(270, 'usermenu', 'Get In Touch...', 'contact-component', '', 'contact-component', '#', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu-anchor_rel\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 159, 168, 0, '*', 0),
(271, 'mainmenu', 'User Profile', 'users-component', '', 'users-component', '#', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu-anchor_rel\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"271\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 173, 182, 0, '*', 0),
(275, 'mainmenu', 'Contact Single Category', 'contact-single-category', '', 'contact-component/contact-single-category', 'index.php?option=com_contact&view=category&catid=26&id=36', 'component', 0, 270, 2, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"\",\"show_subcat_desc\":\"\",\"show_cat_items\":\"\",\"filter_field\":\"\",\"show_pagination_limit\":\"\",\"show_headings\":\"\",\"show_position_headings\":\"\",\"show_email_headings\":\"\",\"show_telephone_headings\":\"\",\"show_mobile_headings\":\"\",\"show_fax_headings\":\"\",\"show_suburb_headings\":\"\",\"show_state_headings\":\"\",\"show_country_headings\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"initial_sort\":\"\",\"presentation_style\":\"sliders\",\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_links\":\"1\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\",\"show_feed_link\":\"1\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 162, 163, 0, '*', 0),
(276, 'mainmenu', 'Search Components', 'search-component', '', 'joomla/other-components/search-component', 'index.php?option=com_content&view=article&id=39', 'component', 0, 268, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"left\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 63, 64, 0, '*', 0),
(278, 'mainmenu', 'Blog', 'blog', '', 'blog', 'index.php?option=com_content&view=category&layout=blog&id=79', 'component', 0, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"layout_type\":\"blog\",\"show_category_heading_title_text\":\"\",\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"show_cat_tags\":\"\",\"page_subheading\":\"\",\"num_leading_articles\":\"\",\"num_intro_articles\":\"\",\"num_columns\":\"\",\"num_links\":\"\",\"multi_column_order\":\"\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"\",\"order_date\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"show_title\":\"1\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"278\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 15, 22, 0, '*', 0),
(279, 'mainmenu', 'Pages', 'pages', '', 'pages', '#', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 7, 12, 0, '*', 0),
(280, 'mainmenu', 'Joomla!', 'joomla', '', 'joomla', '#', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menulayout\":\"{\\\"width\\\":800,\\\"menuItem\\\":4,\\\"menuAlign\\\":\\\"full\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":3,\\\"menuParentId\\\":\\\"266\\\",\\\"moduleId\\\":\\\"\\\"},{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":3,\\\"menuParentId\\\":\\\"270\\\",\\\"moduleId\\\":\\\"\\\"},{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":3,\\\"menuParentId\\\":\\\"271\\\",\\\"moduleId\\\":\\\"\\\"},{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":3,\\\"menuParentId\\\":\\\"268\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"1\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 41, 70, 0, '*', 0),
(402, 'mainmenu', 'Login Form', 'login-form', '', 'users-component/login-form', 'index.php?option=com_users&view=login', 'component', 1, 271, 2, 25, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"loginredirectchoice\":\"1\",\"login_redirect_url\":\"\",\"login_redirect_menuitem\":\"\",\"logindescription_show\":\"1\",\"login_description\":\"Login text\",\"login_image\":\"images\\/logo-200x90.png\",\"logoutredirectchoice\":\"1\",\"logout_redirect_url\":\"\",\"logout_redirect_menuitem\":\"402\",\"logoutdescription_show\":\"0\",\"logout_description\":\"\",\"logout_image\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"Log In\",\"show_page_heading\":\"1\",\"page_heading\":\"Log In\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 174, 175, 0, '*', 0),
(403, 'mainmenu', 'User Profile', 'user-profile', '', 'users-component/user-profile', 'index.php?option=com_users&view=profile', 'component', 1, 271, 2, 25, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"0\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 176, 177, 0, '*', 0),
(404, 'mainmenu', 'Edit User Profile', 'edit-user-profile', '', 'users-component/edit-user-profile', 'index.php?option=com_users&view=profile&layout=edit', 'component', 1, 271, 2, 25, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"0\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 178, 179, 0, '*', 0),
(405, 'mainmenu', 'Registration Form', 'registration-form', '', 'users-component/registration-form', 'index.php?option=com_users&view=registration', 'component', 1, 271, 2, 25, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 180, 181, 0, '*', 0),
(437, 'mainmenu', 'Home', 'home', '', 'home', 'index.php?option=com_sppagebuilder&view=page&id=1', 'component', 0, 1, 1, 10004, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"0\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 1, 6, 0, '*', 0),
(449, 'usermenu', 'Submit an Article', 'submit-an-article', '', 'submit-an-article', 'index.php?option=com_content&view=form&layout=edit', 'component', 0, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 3, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 97, 98, 0, '*', 0),
(450, 'usermenu', 'Submit a Web Link', 'submit-a-web-link', '', 'submit-a-web-link', 'index.php?option=com_weblinks&view=form&layout=edit', 'component', 0, 1, 1, 21, 0, '0000-00-00 00:00:00', 0, 3, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 99, 100, 0, '*', 0),
(452, 'mainmenu', 'Featured Contacts', 'featured-contacts', '', 'contact-component/featured-contacts', 'index.php?option=com_contact&view=featured&id=16', 'component', 0, 270, 2, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_pagination_limit\":\"\",\"show_headings\":\"\",\"show_position_headings\":\"\",\"show_email_headings\":\"\",\"show_telephone_headings\":\"\",\"show_mobile_headings\":\"\",\"show_fax_headings\":\"\",\"show_suburb_headings\":\"\",\"show_state_headings\":\"\",\"show_country_headings\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"presentation_style\":\"sliders\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_links\":\"1\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":1,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 166, 167, 0, '*', 0),
(466, 'mainmenu', 'Smart Search', 'smart-search', '', 'joomla/other-components/smart-search', 'index.php?option=com_finder&view=search', 'component', 0, 268, 3, 27, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_date_filters\":\"\",\"show_advanced\":\"\",\"expand_advanced\":\"\",\"show_description\":\"\",\"description_length\":255,\"show_url\":\"\",\"show_pagination_limit\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"allow_empty_query\":\"0\",\"show_suggested_query\":\"1\",\"show_explained_query\":\"1\",\"sort_order\":\"\",\"sort_direction\":\"\",\"show_feed\":\"0\",\"show_feed_text\":\"0\",\"show_feed_link\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 65, 66, 0, '*', 0),
(471, 'menu', 'com_tags', 'com-tags', '', 'com-tags', 'index.php?option=com_tags', 'component', 0, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 101, 102, 0, '', 1),
(472, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 0, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 103, 104, 0, '*', 1),
(475, 'partners', 'JoomShaper', 'joomshaper', '', 'joomshaper', 'http://www.joomshaper.com/', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 105, 106, 0, '*', 0),
(476, 'partners', 'Themeum', 'themeum', '', 'themeum', 'http://www.themeum.com/', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 107, 108, 0, '*', 0),
(477, 'partners', 'ShapeBootstrap', 'shapebootstrap', '', 'shapebootstrap', 'http://shapebootstrap.net/', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 109, 110, 0, '*', 0),
(479, 'partners', 'Page Builder', 'page-builder', '', 'page-builder', 'http://www.joomshaper.com/page-builder', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 111, 112, 0, '*', 0),
(481, 'quicklink', 'Helix 3 Framework', 'helix-framework', '', 'helix-framework', 'http://www.joomshaper.com/helix3', 'url', -2, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"481\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 113, 114, 0, '*', 0),
(482, 'quicklink', 'Documentation', 'documentation', '', 'documentation', 'http://www.joomshaper.com/documentation/', 'url', -2, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 115, 116, 0, '*', 0),
(483, 'quicklink', 'Demo Page Builder ', 'demo-pagebuilder', '', 'demo-pagebuilder', 'http://demo.joomshaper.com/page-builder/', 'url', -2, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 117, 118, 0, '*', 0),
(484, 'quicklink', 'Plans & Pricing', 'plan-pricing', '', 'plan-pricing', 'http://www.joomshaper.com/join-now', 'url', -2, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 119, 120, 0, '*', 0),
(487, 'mainmenu', 'Coming Soon', '2015-02-02-15-01-12', '', 'pages/2015-02-02-15-01-12', '?tmpl=comingsoon', 'url', 0, 279, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"487\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 8, 9, 0, '*', 0),
(488, 'mainmenu', '404 Page', '2015-02-02-15-05-34', '', 'pages/2015-02-02-15-05-34', 'index.php?option=com_404', 'url', 0, 279, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 10, 11, 0, '*', 0),
(489, 'mainmenu', 'Portfolio', 'portfolio', '', 'home/portfolio', 'index.php?option=com_sppagebuilder&view=page&id=6', 'component', 0, 437, 2, 10004, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 4, 5, 0, '*', 0),
(490, 'mainmenu', 'Corporate', 'corporate', '', 'home/corporate', 'index.php?option=com_sppagebuilder&view=page&id=2', 'component', 0, 437, 2, 10004, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 2, 3, 0, '*', 0),
(492, 'mainmenu', 'Blog With Right Sidebar', 'blog-with-right-sidebar', '', 'blog/blog-with-right-sidebar', 'index.php?option=com_content&view=category&layout=blog&id=79', 'component', 0, 278, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"layout_type\":\"blog\",\"show_category_heading_title_text\":\"\",\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"show_cat_tags\":\"\",\"page_subheading\":\"\",\"num_leading_articles\":\"\",\"num_intro_articles\":\"\",\"num_columns\":\"\",\"num_links\":\"\",\"multi_column_order\":\"\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"\",\"order_date\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"show_title\":\"1\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 16, 17, 0, '*', 0),
(493, 'mainmenu', 'Blog With Left Sidebar', 'blog-with-left-sidebar', '', 'blog/blog-with-left-sidebar', 'index.php?option=com_content&view=category&layout=blog&id=79', 'component', 0, 278, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"layout_type\":\"blog\",\"show_category_heading_title_text\":\"\",\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"show_cat_tags\":\"\",\"page_subheading\":\"\",\"num_leading_articles\":\"\",\"num_intro_articles\":\"\",\"num_columns\":\"\",\"num_links\":\"\",\"multi_column_order\":\"\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"\",\"order_date\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"show_title\":\"1\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 18, 19, 0, '*', 0);
INSERT INTO `tol2j_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(494, 'mainmenu', 'Standard Post Format', 'standard-post-format', '', 'post-formats/standard-post-format', 'index.php?option=com_content&view=article&id=73', 'component', 0, 503, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 24, 25, 0, '*', 0),
(495, 'mainmenu', 'Video Post Format', 'video-post-format', '', 'post-formats/video-post-format', 'index.php?option=com_content&view=article&id=72', 'component', 0, 503, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 26, 27, 0, '*', 0),
(496, 'mainmenu', 'Gallery Post Format', 'gallery-post-format', '', 'post-formats/gallery-post-format', 'index.php?option=com_content&view=article&id=74', 'component', 0, 503, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 30, 31, 0, '*', 0),
(497, 'mainmenu', 'Audio Post Format', 'audio-post-format', '', 'post-formats/audio-post-format', 'index.php?option=com_content&view=article&id=75', 'component', 0, 503, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 28, 29, 0, '*', 0),
(498, 'mainmenu', 'Image Post Format', 'image-post-format', '', 'post-formats/image-post-format', 'index.php?option=com_content&view=article&id=73', 'component', 0, 503, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 38, 39, 0, '*', 0),
(499, 'mainmenu', 'Link Post Format', 'link-post-format', '', 'post-formats/link-post-format', 'index.php?option=com_content&view=article&id=76', 'component', 0, 503, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 32, 33, 0, '*', 0),
(500, 'mainmenu', 'Status Post Format', 'status-post-format', '', 'post-formats/status-post-format', 'index.php?option=com_content&view=article&id=71', 'component', 0, 503, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 36, 37, 0, '*', 0),
(501, 'mainmenu', 'Quote Post Format', 'quote-post-format', '', 'post-formats/quote-post-format', 'index.php?option=com_content&view=article&id=77', 'component', 0, 503, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 34, 35, 0, '*', 0),
(502, 'mainmenu', 'Portfolio', 'portfolio-2', '', 'portfolio-2', 'index.php?option=com_spsimpleportfolio&view=items', 'component', 0, 1, 1, 10007, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_filter\":\"1\",\"layout_type\":\"gallery_nospace\",\"columns\":\"4\",\"thumbnail_type\":\"square\",\"limit\":\"12\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"502\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 13, 14, 0, '*', 0),
(503, 'mainmenu', 'Post Formats', 'post-formats', '', 'post-formats', '#', 'url', 0, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"503\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 23, 40, 0, '*', 0),
(504, 'mainmenu', 'Article Category Blog', 'article-category-blog-2', '', 'blog/article-category-blog-2', 'index.php?option=com_content&view=category&layout=blog&id=79', 'component', 0, 278, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"layout_type\":\"blog\",\"show_category_heading_title_text\":\"\",\"show_category_title\":\"\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"show_cat_tags\":\"\",\"page_subheading\":\"\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"\",\"order_date\":\"\",\"show_pagination\":\"2\",\"show_pagination_results\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"1\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"1\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 20, 21, 0, '*', 0),
(515, 'main', 'COM_SPSIMPLEPORTFOLIO', 'com-spsimpleportfolio', '', 'com-spsimpleportfolio', 'index.php?option=com_spsimpleportfolio', 'component', 0, 1, 1, 10007, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 121, 122, 0, '', 1),
(516, 'main', 'COM_SPPAGEBUILDER', 'com-sppagebuilder', '', 'com-sppagebuilder', 'index.php?option=com_sppagebuilder', 'component', 0, 1, 1, 10004, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 123, 124, 0, '', 1),
(518, 'main', 'COM_DOCMAN', 'com-docman', '', 'com-docman', 'index.php?option=com_docman', 'component', 0, 1, 1, 10015, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 125, 136, 0, '', 1),
(519, 'main', 'COM_DOCMAN_SUBMENU_DOCUMENTS', 'com-docman-submenu-documents', '', 'com-docman/com-docman-submenu-documents', 'index.php?option=com_docman&view=documents', 'component', 0, 518, 2, 10015, 0, '0000-00-00 00:00:00', 0, 1, 'class:article', 0, '{}', 126, 127, 0, '', 1),
(520, 'main', 'COM_DOCMAN_SUBMENU_CATEGORIES', 'com-docman-submenu-categories', '', 'com-docman/com-docman-submenu-categories', 'index.php?option=com_docman&view=categories', 'component', 0, 518, 2, 10015, 0, '0000-00-00 00:00:00', 0, 1, 'class:category', 0, '{}', 128, 129, 0, '', 1),
(521, 'main', 'COM_DOCMAN_SUBMENU_TAGS', 'com-docman-submenu-tags', '', 'com-docman/com-docman-submenu-tags', 'index.php?option=com_docman&view=tags', 'component', 0, 518, 2, 10015, 0, '0000-00-00 00:00:00', 0, 1, 'class:media', 0, '{}', 130, 131, 0, '', 1),
(522, 'main', 'COM_DOCMAN_SUBMENU_FILES', 'com-docman-submenu-files', '', 'com-docman/com-docman-submenu-files', 'index.php?option=com_docman&view=files', 'component', 0, 518, 2, 10015, 0, '0000-00-00 00:00:00', 0, 1, 'class:media', 0, '{}', 132, 133, 0, '', 1),
(523, 'main', 'COM_DOCMAN_SUBMENU_USERS', 'com-docman-submenu-users', '', 'com-docman/com-docman-submenu-users', 'index.php?option=com_docman&view=users', 'component', 0, 518, 2, 10015, 0, '0000-00-00 00:00:00', 0, 1, 'class:users', 0, '{}', 134, 135, 0, '', 1),
(524, 'left-nav', 'Document Management', 'dms', '', 'dms', 'index.php?option=com_docman&view=tree&layout=default&own=0', 'component', 1, 1, 1, 10015, 0, '0000-00-00 00:00:00', 0, 2, ' ', 10, '{\"limit\":\"20\",\"sort_documents\":\"title\",\"document_title_link\":\"download\",\"show_document_search\":\"1\",\"show_document_sort_limit\":\"1\",\"show_document_title\":\"1\",\"show_document_description\":\"1\",\"show_document_icon\":\"1\",\"show_document_image\":\"1\",\"show_document_recent\":\"1\",\"show_document_popular\":\"1\",\"show_document_created\":\"1\",\"show_document_created_by\":\"1\",\"show_document_modified\":\"1\",\"show_document_filename\":\"1\",\"show_document_size\":\"1\",\"show_document_hits\":\"1\",\"show_document_extension\":\"1\",\"track_downloads\":\"1\",\"show_documents_header\":\"1\",\"download_in_blank_page\":\"1\",\"force_download\":\"0\",\"preview_with_gdocs\":\"0\",\"days_for_new\":\"7\",\"hits_for_popular\":\"100\",\"sort_categories\":\"title\",\"show_category_title\":\"1\",\"show_icon\":\"1\",\"show_image\":\"1\",\"show_description\":\"1\",\"show_subcategories\":\"1\",\"show_categories_header\":\"1\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"#345487\",\"page_title_bg_image\":\"\"}', 137, 138, 0, '*', 0),
(525, 'main', 'Komento', 'komento', '', 'komento', 'index.php?option=com_komento', 'component', 0, 1, 1, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_komento/assets/images/komento-favicon.png', 0, '{}', 139, 158, 0, '', 1),
(526, 'main', 'COM_KOMENTO_MENU_COMMENTS', 'com-komento-menu-comments', '', 'komento/com-komento-menu-comments', 'index.php?option=com_komento&view=comments', 'component', 0, 525, 2, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_komento/assets/images/comments-favicon.png', 0, '{}', 140, 141, 0, '', 1),
(527, 'main', 'COM_KOMENTO_MENU_PENDINGS', 'com-komento-menu-pendings', '', 'komento/com-komento-menu-pendings', 'index.php?option=com_komento&view=pending', 'component', 0, 525, 2, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_komento/assets/images/pending-favicon.png', 0, '{}', 142, 143, 0, '', 1),
(528, 'main', 'COM_KOMENTO_MENU_REPORTS', 'com-komento-menu-reports', '', 'komento/com-komento-menu-reports', 'index.php?option=com_komento&view=reports', 'component', 0, 525, 2, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_komento/assets/images/reports-favicon.png', 0, '{}', 144, 145, 0, '', 1),
(529, 'main', 'COM_KOMENTO_MENU_SUBSCRIBERS', 'com-komento-menu-subscribers', '', 'komento/com-komento-menu-subscribers', 'index.php?option=com_komento&view=subscribers', 'component', 0, 525, 2, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_komento/assets/images/subscribers-favicon.png', 0, '{}', 146, 147, 0, '', 1),
(530, 'main', 'COM_KOMENTO_MENU_INTEGRATIONS', 'com-komento-menu-integrations', '', 'komento/com-komento-menu-integrations', 'index.php?option=com_komento&view=integrations', 'component', 0, 525, 2, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_komento/assets/images/integrations-favicon.png', 0, '{}', 148, 149, 0, '', 1),
(531, 'main', 'COM_KOMENTO_MENU_CONFIGURATION', 'com-komento-menu-configuration', '', 'komento/com-komento-menu-configuration', 'index.php?option=com_komento&view=system', 'component', 0, 525, 2, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_komento/assets/images/system-favicon.png', 0, '{}', 150, 151, 0, '', 1),
(532, 'main', 'COM_KOMENTO_MENU_ACL', 'com-komento-menu-acl', '', 'komento/com-komento-menu-acl', 'index.php?option=com_komento&view=acl', 'component', 0, 525, 2, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_komento/assets/images/acls-favicon.png', 0, '{}', 152, 153, 0, '', 1),
(533, 'main', 'COM_KOMENTO_MENU_MIGRATORS', 'com-komento-menu-migrators', '', 'komento/com-komento-menu-migrators', 'index.php?option=com_komento&view=migrators', 'component', 0, 525, 2, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_komento/assets/images/migrators-favicon.png', 0, '{}', 154, 155, 0, '', 1),
(534, 'main', 'COM_KOMENTO_MENU_MAILQ', 'com-komento-menu-mailq', '', 'komento/com-komento-menu-mailq', 'index.php?option=com_komento&view=mailq', 'component', 0, 525, 2, 10025, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_komento/assets/images/mailq-favicon.png', 0, '{}', 156, 157, 0, '', 1),
(535, 'mainmenu', 'DMS Home', 'dms-home', '', 'dms-home', 'index.php?option=com_docman&view=flat&layout=table&category_children=1&own=0', 'component', 0, 1, 1, 10015, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"limit\":\"0\",\"sort\":\"tbl.title\",\"document_title_link\":\"details\",\"show_document_search\":\"1\",\"show_document_sort_limit\":\"0\",\"show_pagination\":\"1\",\"show_document_title\":\"1\",\"show_document_description\":\"1\",\"show_document_icon\":\"1\",\"show_document_image\":\"1\",\"show_document_recent\":\"1\",\"show_document_popular\":\"1\",\"show_document_category\":\"1\",\"show_document_created\":\"1\",\"show_document_created_by\":\"1\",\"show_document_modified\":\"1\",\"show_document_filename\":\"1\",\"show_document_size\":\"1\",\"show_document_hits\":\"1\",\"show_document_extension\":\"1\",\"track_downloads\":\"1\",\"download_in_blank_page\":\"0\",\"force_download\":\"0\",\"preview_with_gdocs\":\"0\",\"days_for_new\":\"7\",\"hits_for_popular\":\"100\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 169, 170, 0, '*', 0),
(536, 'mainmenu', 'DMS Custom', 'dms-costom', '', 'dms-costom', 'index.php?option=com_sppagebuilder&view=page&id=8', 'component', 0, 1, 1, 10004, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"536\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 171, 172, 0, '*', 0),
(537, 'quicklink', 'Help', 'help', '', 'help', 'index.php?option=com_sppagebuilder&view=page&id=9', 'component', 1, 1, 1, 10004, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"dropdown_position\":\"right\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 183, 184, 0, '*', 0),
(538, 'main', 'Tmea_countries', 'tmea-countries', '', 'tmea-countries', 'index.php?option=com_tmea_countries', 'component', 0, 1, 1, 10033, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 185, 190, 0, '', 1),
(539, 'main', 'Countries', 'countries', '', 'tmea-countries/countries', 'index.php?option=com_tmea_countries&view=countries', 'component', 0, 538, 2, 10033, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 186, 187, 0, '', 1),
(540, 'main', 'Contacts', 'contacts', '', 'tmea-countries/contacts', 'index.php?option=com_tmea_countries&view=contacts', 'component', 0, 538, 2, 10033, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 188, 189, 0, '', 1),
(541, 'left-nav', 'Countries', 'countries', '', 'countries', 'index.php?option=com_tmea_countries&view=countries', 'component', 1, 1, 1, 10033, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"show_filters\":\"0\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"541\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 191, 192, 1, '*', 0),
(542, 'left-nav', 'Country Contacts', 'country-contacts', '', 'country-contacts', 'index.php?option=com_tmea_countries&view=contacts', 'component', 1, 1, 1, 10033, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"show_filters\":\"1\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"542\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 193, 194, 0, '*', 0),
(543, 'main', 'Tmea_programs', 'tmea-programs', '', 'tmea-programs', 'index.php?option=com_tmea_programs', 'component', 0, 1, 1, 10034, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 195, 200, 0, '', 1),
(544, 'main', 'programs', 'programs', '', 'tmea-programs/programs', 'index.php?option=com_tmea_programs&view=programs', 'component', 0, 543, 2, 10034, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 196, 197, 0, '', 1),
(545, 'main', 'JCATEGORIES', 'jcategories', '', 'tmea-programs/jcategories', 'index.php?option=com_categories&view=categories&extension=com_tmea_programs', 'component', 0, 543, 2, 10034, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 198, 199, 0, '', 1),
(546, 'left-nav', 'Programs', 'programs', '', 'programs', 'index.php?option=com_tmea_programs&view=programs', 'component', 1, 1, 1, 10034, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"show_filters\":\"1\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"546\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 201, 202, 0, '*', 0),
(549, 'main', 'com_jevents', 'com-jevents', '', 'com-jevents', 'index.php?option=com_jevents', 'component', 0, 1, 1, 10039, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 203, 204, 0, '', 1),
(550, 'left-nav', 'Events Calender', 'events', '', 'events', 'index.php?option=com_jevents&view=month&layout=calendar', 'component', 1, 1, 1, 10039, 0, '0000-00-00 00:00:00', 0, 2, ' ', 0, '{\"com_calViewName\":\"global\",\"darktemplate\":\"0\",\"com_calUseIconic\":\"2\",\"iconstoshow\":[\"byyear\",\"bymonth\",\"byweek\",\"byday\",\"search\"],\"showyearpast\":\"1\",\"overridelayout\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"menulayout\":\"{\\\"width\\\":600,\\\"menuItem\\\":1,\\\"menuAlign\\\":\\\"right\\\",\\\"layout\\\":[{\\\"type\\\":\\\"row\\\",\\\"attr\\\":[{\\\"type\\\":\\\"column\\\",\\\"colGrid\\\":12,\\\"menuParentId\\\":\\\"550\\\",\\\"moduleId\\\":\\\"\\\"}]}]}\",\"megamenu\":\"0\",\"showmenutitle\":\"1\",\"icon\":\"\",\"class\":\"\",\"enable_page_title\":\"0\",\"page_title_alt\":\"\",\"page_subtitle\":\"\",\"page_title_bg_color\":\"\",\"page_title_bg_image\":\"\"}', 205, 206, 0, '*', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_menu_types`
--

DROP TABLE IF EXISTS `tol2j_menu_types`;
CREATE TABLE `tol2j_menu_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(11) NOT NULL,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_menu_types`
--

INSERT INTO `tol2j_menu_types` (`id`, `asset_id`, `menutype`, `title`, `description`) VALUES
(2, 0, 'usermenu', 'User Menu', 'A Menu for logged-in Users'),
(4, 0, 'mainmenu', 'Main Menu', 'Main Menu'),
(8, 0, 'partners', 'Our Partmers', ''),
(9, 0, 'quicklink', 'Quick Link', ''),
(10, 270, 'left-nav', 'Left Nav', 'Left Navigation Menu');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_messages`
--

DROP TABLE IF EXISTS `tol2j_messages`;
CREATE TABLE `tol2j_messages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id_from` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id_to` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_messages_cfg`
--

DROP TABLE IF EXISTS `tol2j_messages_cfg`;
CREATE TABLE `tol2j_messages_cfg` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_modules`
--

DROP TABLE IF EXISTS `tol2j_modules`;
CREATE TABLE `tol2j_modules` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_modules`
--

INSERT INTO `tol2j_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(2, 0, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 0, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{\"count\":\"5\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}', 1, '*'),
(4, 0, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{\"count\":\"5\",\"ordering\":\"c_dsc\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}', 1, '*'),
(8, 0, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 0, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 0, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{\"count\":\"5\",\"name\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}', 1, '*'),
(12, 0, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{\"layout\":\"\",\"moduleclass_sfx\":\"\",\"shownew\":\"1\",\"showhelp\":\"1\",\"cache\":\"0\"}', 1, '*'),
(13, 0, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 0, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 0, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(17, 185, 'Breadcrumbs', '', '', 1, 'breadcrumb', 0, '0000-00-00 00:00:00', '2016-10-06 12:15:03', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 2, 1, '{\"showHere\":\"1\",\"showHome\":\"1\",\"homeText\":\"\",\"showLast\":\"1\",\"separator\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(35, 186, 'Search', '', '', 1, 'offcanvas', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_search', 2, 1, '{\"label\":\"\",\"width\":\"20\",\"text\":\"\",\"button\":\"0\",\"button_pos\":\"right\",\"imagebutton\":\"0\",\"button_text\":\"\",\"opensearch\":\"1\",\"opensearch_title\":\"\",\"set_itemid\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\",\"icon\":\"\",\"background_color\":\"\",\"color\":\"\",\"padding\":\"\",\"margin\":\"\"}', 0, '*'),
(79, 0, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(86, 334, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '2017-10-30 15:56:09', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{\"format\":\"long\",\"product\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 1, '*'),
(91, 181, 'SP Page Builder', '', '', 0, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_sppagebuilder_icons', 1, 1, '', 1, '*'),
(92, 182, 'SP Page Builder Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_sppagebuilder_admin_menu', 1, 1, '', 1, '*'),
(94, 187, 'About Helix', '', '<p>Ball tip biltong pork belly frankfurter shankle jerky leberkas pig kielbasa kay boudin alcatra short loin.</p>\r\n<p>Jowl salami leberkas turkey pork brisket meatball turducken flank bilto porke belly ball tip. pork belly frankf urtane bilto</p>', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(95, 188, 'Latest News', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_articles_latest', 1, 1, '{\"catid\":[\"\"],\"count\":\"3\",\"show_featured\":\"\",\"ordering\":\"c_dsc\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(96, 189, 'Our Partners', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_menu', 1, 1, '{\"menutype\":\"partners\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(97, 190, 'Quick Link', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_menu', 1, 1, '{\"menutype\":\"quicklink\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(98, 202, 'Portfolio Module', '', '', 1, 'pagebuilder', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_spsimpleportfolio', 2, 1, '{\"show_filter\":\"0\",\"layout_type\":\"gallery_nospace\",\"columns\":\"4\",\"thumbnail_type\":\"square\",\"limit\":\"4\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\",\"icon\":\"\",\"background_color\":\"\",\"color\":\"\",\"padding\":\"\",\"margin\":\"\"}', 0, '*'),
(99, 203, 'Latest News', '', '', 2, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_articles_latest', 2, 1, '{\"catid\":[\"79\"],\"count\":\"6\",\"show_featured\":\"\",\"ordering\":\"c_dsc\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\",\"icon\":\"\",\"background_color\":\"\",\"color\":\"\",\"padding\":\"\",\"margin\":\"\"}', 0, '*'),
(100, 204, 'Search', '', '', 1, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_search', 2, 1, '{\"label\":\"\",\"width\":\"20\",\"text\":\"\",\"button\":\"0\",\"button_pos\":\"right\",\"imagebutton\":\"0\",\"button_text\":\"\",\"opensearch\":\"1\",\"opensearch_title\":\"\",\"set_itemid\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(101, 205, 'Information', '', '', 3, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 2, 1, '{\"menutype\":\"partners\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\",\"icon\":\"\",\"background_color\":\"\",\"color\":\"\",\"padding\":\"\",\"margin\":\"\"}', 0, '*'),
(102, 206, 'Search', '', '', 1, 'left', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_search', 2, 1, '{\"label\":\"\",\"width\":\"20\",\"text\":\"\",\"button\":\"0\",\"button_pos\":\"right\",\"imagebutton\":\"0\",\"button_text\":\"\",\"opensearch\":\"1\",\"opensearch_title\":\"\",\"set_itemid\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\",\"icon\":\"\",\"background_color\":\"\",\"color\":\"\",\"padding\":\"\",\"margin\":\"\"}', 0, '*'),
(103, 207, 'Latest News', '', '', 2, 'left', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_latest', 2, 1, '{\"catid\":[\"79\"],\"count\":\"6\",\"show_featured\":\"\",\"ordering\":\"c_dsc\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\",\"icon\":\"\",\"background_color\":\"\",\"color\":\"\",\"padding\":\"\",\"margin\":\"\"}', 0, '*'),
(104, 208, 'Information', '', '', 3, 'left', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 2, 1, '{\"menutype\":\"partners\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\",\"icon\":\"\",\"background_color\":\"\",\"color\":\"\",\"padding\":\"\",\"margin\":\"\"}', 0, '*'),
(105, 209, 'Off Canvas Menu', '', '', 1, 'offcanvas', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 2, 0, '{\"menutype\":\"left-nav\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(107, 211, 'Portfolio Module -  Portfolio Home', '', '', 1, 'pagebuilder', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_spsimpleportfolio', 2, 1, '{\"show_filter\":\"1\",\"layout_type\":\"gallery_nospace\",\"columns\":\"4\",\"thumbnail_type\":\"square\",\"limit\":\"12\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\",\"icon\":\"\",\"background_color\":\"\",\"color\":\"\",\"padding\":\"\",\"margin\":\"\"}', 0, '*'),
(108, 212, 'Recent Documents', '', '', 1, 'recent_documents', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_docman_documents', 2, 0, '{\"page\":\"\",\"own\":\"0\",\"limit\":\"10\",\"sort\":\"reverse_created_on\",\"include_child_categories\":\"1\",\"show_icon\":\"1\",\"show_category\":\"1\",\"show_created\":\"1\",\"show_size\":\"1\",\"show_hits\":\"1\",\"show_recent\":\"1\",\"show_popular\":\"1\",\"download_in_blank_page\":\"1\",\"track_downloads\":\"1\",\"link_to_download\":\"0\",\"days_for_new\":\"7\",\"hits_for_popular\":\"100\",\"layout\":\"_:default.html\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(109, 216, 'Folders', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_docman_categories', 2, 1, '{\"page\":\"\",\"parent\":\"\",\"own\":\"0\",\"level\":\"\",\"limit\":\"10\",\"sort\":\"custom\",\"show_icon\":\"1\",\"layout\":\"_:default.html\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(110, 220, 'Komento Activities', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_komento_activities', 2, 1, '{\"limit\":\"5\",\"component\":\"all\",\"includelikes\":\"1\",\"includecomments\":\"1\",\"includereplies\":\"1\",\"showcomment\":\"1\",\"maxcommentlength\":\"100\",\"maxtitlelength\":\"30\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(111, 221, 'Komento Comments', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_komento_comments', 2, 1, '{\"limit\":\"5\",\"component\":\"all\",\"filter\":\"all\",\"category\":\"\",\"articleId\":\"\",\"userId\":\"\",\"sort\":\"latest\",\"random\":\"0\",\"filtersticked\":\"0\",\"showtitle\":\"1\",\"showcomponent\":\"1\",\"showavatar\":\"1\",\"showauthor\":\"1\",\"maxcommentlength\":\"100\",\"maxtitlelength\":\"30\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(112, 261, 'Menu', '', '', 3, 'left', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 2, 0, '{\"menutype\":\"left-nav\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"side\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(113, 262, ' Help', '', '<h3>Help</h3>\r\n<p>Step by step details on how to use the Extranet and manage Documents.</p>\r\n<p><a href=\"help\">Read More</a></p>', 1, 'left', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 2, 0, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"_help\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(114, 263, 'Footer 1', '', '<p>Copyright 2017 TradeMark East Africa | All Rights Reserved</p>', 1, 'footer1', 234, '2017-10-30 16:00:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"pull-left\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(115, 264, 'Footer 2', '', '<p><a href=\"https://www.trademarkea.com\" target=\"_blank\">TMEA Site</a> | <a href=\"https://www.gbc.co.ke\" target=\"_blank\">Designed by GBC</a></p>', 1, 'footer2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"pull-right\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(116, 265, 'mod_user_section', '', '', 1, 'user_section', 234, '2017-09-12 21:46:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_user_section', 1, 0, '{\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(117, 266, 'Side Logo', '', '<p> </p>\r\n<p><a title=\"TMEA Extranet\" href=\".\"><img src=\"images/logo-200x90_colorshift.png\" alt=\"\" /></a></p>', 1, 'left', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 2, 0, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(119, 296, 'JEvents Calendar', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_jevents_cal', 1, 1, '', 0, '*'),
(120, 297, 'JEvents Legend', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_jevents_legend', 1, 1, '', 0, '*'),
(121, 298, 'JEvents Latest Events', '', '', 1, 'upcoming_events', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_jevents_latest', 1, 0, '{\"com_calViewName\":\"global\",\"cache\":\"0\",\"contentplugins\":\"0\",\"moduleclass_sfx\":\"\",\"ignorecatfilter\":\"0\",\"ignorefiltermodule\":\"0\",\"target_itemid\":\"\",\"modlatest_inccss\":\"1\",\"layout\":\"\",\"modlatest_useLocalParam\":\"1\",\"modlatest_CustFmtStr\":\"<span class=\\\"icon-calendar\\\"><\\/span>${startDate(%d %b %Y)}\\r\\n<span class=\\\"icon-time\\\"><\\/span>${startDate(%I:%M%p)}[!a: - ${endDate(%I:%M%p)}]\\r\\n<span class=\\\"icon-hand-right\\\"><\\/span>${title}\",\"modlatest_customcss\":\"\",\"modlatest_MaxEvents\":\"10\",\"modlatest_Mode\":\"3\",\"modlatest_Days\":\"30\",\"startnow\":\"0\",\"pastonly\":\"0\",\"modlatest_NoRepeat\":\"0\",\"modlatest_multiday\":\"0\",\"modlatest_DispLinks\":\"1\",\"modlatest_DispYear\":\"0\",\"modlatest_NoEvents\":\"1\",\"modlatest_DisDateStyle\":\"0\",\"modlatest_DisTitleStyle\":\"0\",\"modlatest_LinkToCal\":\"0\",\"modlatest_LinkCloaking\":\"0\",\"modlatest_SortReverse\":\"0\",\"modlatest_RSS\":\"0\",\"modlatest_rss_title\":\"\",\"modlatest_rss_description\":\"\",\"modlatest_templatetop\":\"\",\"modlatest_templaterow\":\"\",\"modlatest_templatebottom\":\"\",\"bootstrapcss\":\"1\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(122, 299, 'JEvents Filter', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_jevents_filter', 1, 1, '', 0, '*'),
(123, 300, 'JEvents CustomModule', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_jevents_custom', 1, 1, '', 0, '*'),
(124, 301, 'JEvents View Switcher', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_jevents_switchview', 1, 1, '', 0, '*');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_modules_menu`
--

DROP TABLE IF EXISTS `tol2j_modules_menu`;
CREATE TABLE `tol2j_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_modules_menu`
--

INSERT INTO `tol2j_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 101),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(17, 0),
(20, 0),
(22, 234),
(22, 238),
(22, 242),
(22, 243),
(22, 244),
(22, 296),
(22, 399),
(22, 400),
(25, 294),
(26, -463),
(26, -462),
(26, -433),
(26, -432),
(26, -431),
(26, -430),
(26, -429),
(26, -427),
(26, -400),
(26, -399),
(26, -296),
(26, -244),
(26, -243),
(26, -242),
(26, -238),
(26, -234),
(32, 309),
(35, 0),
(45, 303),
(57, 238),
(57, 427),
(57, 429),
(57, 430),
(57, 431),
(57, 432),
(57, 433),
(57, 462),
(57, 463),
(79, 0),
(86, 0),
(87, 238),
(87, 427),
(87, 429),
(87, 430),
(87, 431),
(87, 432),
(87, 433),
(87, 462),
(87, 463),
(91, 0),
(92, 0),
(94, 0),
(95, 0),
(96, 0),
(97, 0),
(98, 0),
(99, 492),
(100, 492),
(101, 492),
(102, 493),
(103, 493),
(104, 493),
(105, 0),
(107, 0),
(108, -550),
(108, -542),
(108, -537),
(108, -536),
(108, -535),
(108, -524),
(108, -504),
(108, -503),
(108, -502),
(108, -501),
(108, -500),
(108, -499),
(108, -498),
(108, -497),
(108, -496),
(108, -495),
(108, -494),
(108, -493),
(108, -492),
(108, -490),
(108, -489),
(108, -488),
(108, -487),
(108, -479),
(108, -477),
(108, -476),
(108, -475),
(108, -466),
(108, -452),
(108, -450),
(108, -449),
(108, -437),
(108, -405),
(108, -404),
(108, -403),
(108, -402),
(108, -280),
(108, -279),
(108, -278),
(108, -276),
(108, -275),
(108, -271),
(108, -270),
(108, -268),
(108, -267),
(108, -266),
(108, -263),
(108, -262),
(108, -260),
(108, -259),
(108, -257),
(108, -255),
(108, -254),
(108, -253),
(108, -252),
(108, -251),
(108, -229),
(108, -201),
(109, 492),
(109, 493),
(109, 504),
(109, 524),
(112, 0),
(113, 0),
(114, 0),
(115, 0),
(116, 0),
(117, 0),
(121, 541);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_newsfeeds`
--

DROP TABLE IF EXISTS `tol2j_newsfeeds`;
CREATE TABLE `tol2j_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cache_time` int(10) UNSIGNED NOT NULL DEFAULT '3600',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_newsfeeds`
--

INSERT INTO `tol2j_newsfeeds` (`catid`, `id`, `name`, `alias`, `link`, `published`, `numarticles`, `cache_time`, `checked_out`, `checked_out_time`, `ordering`, `rtl`, `access`, `language`, `params`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `xreference`, `publish_up`, `publish_down`, `description`, `version`, `hits`, `images`) VALUES
(17, 1, 'Joomla! Announcements', 'joomla-announcements', 'http://feeds.joomla.org/JoomlaAnnouncements', 1, 5, 3600, 0, '0000-00-00 00:00:00', 1, 1, 1, 'en-GB', '{\"show_feed_image\":\"\",\"show_feed_description\":\"\",\"show_item_description\":\"\",\"feed_character_count\":\"0\",\"newsfeed_layout\":\"\",\"feed_display_order\":\"\"}', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 1, ''),
(17, 2, 'New Joomla! Extensions', 'new-joomla-extensions', 'http://feeds.joomla.org/JoomlaExtensions', 1, 5, 3600, 0, '0000-00-00 00:00:00', 4, 1, 1, 'en-GB', '{\"show_feed_image\":\"\",\"show_feed_description\":\"\",\"show_item_description\":\"\",\"feed_character_count\":\"0\",\"newsfeed_layout\":\"\",\"feed_display_order\":\"\"}', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(17, 3, 'Joomla! Security News', 'joomla-security-news', 'http://feeds.joomla.org/JoomlaSecurityNews', 1, 5, 3600, 0, '0000-00-00 00:00:00', 2, 1, 1, 'en-GB', '{\"show_feed_image\":\"\",\"show_feed_description\":\"\",\"show_item_description\":\"\",\"feed_character_count\":\"0\",\"newsfeed_layout\":\"\",\"feed_display_order\":\"\"}', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(17, 4, 'Joomla! Connect', 'joomla-connect', 'http://feeds.joomla.org/JoomlaConnect', 1, 5, 3600, 0, '0000-00-00 00:00:00', 3, 1, 1, 'en-GB', '{\"show_feed_image\":\"\",\"show_feed_description\":\"\",\"show_item_description\":\"\",\"feed_character_count\":\"0\",\"newsfeed_layout\":\"\",\"feed_display_order\":\"\"}', '2011-01-01 00:00:01', 234, 'Joomla', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_overrider`
--

DROP TABLE IF EXISTS `tol2j_overrider`;
CREATE TABLE `tol2j_overrider` (
  `id` int(10) NOT NULL COMMENT 'Primary Key',
  `constant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_postinstall_messages`
--

DROP TABLE IF EXISTS `tol2j_postinstall_messages`;
CREATE TABLE `tol2j_postinstall_messages` (
  `postinstall_message_id` bigint(20) UNSIGNED NOT NULL,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language_extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_postinstall_messages`
--

INSERT INTO `tol2j_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 0),
(2, 700, 'COM_CPANEL_MSG_EACCELERATOR_TITLE', 'COM_CPANEL_MSG_EACCELERATOR_BODY', 'COM_CPANEL_MSG_EACCELERATOR_BUTTON', 'com_cpanel', 1, 'action', 'admin://components/com_admin/postinstall/eaccelerator.php', 'admin_postinstall_eaccelerator_action', 'admin://components/com_admin/postinstall/eaccelerator.php', 'admin_postinstall_eaccelerator_condition', '3.2.0', 1),
(3, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 0),
(4, 700, 'COM_CPANEL_MSG_PHPVERSION_TITLE', 'COM_CPANEL_MSG_PHPVERSION_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/phpversion.php', 'admin_postinstall_phpversion_condition', '3.2.2', 1),
(5, 700, 'COM_CPANEL_MSG_HTACCESS_TITLE', 'COM_CPANEL_MSG_HTACCESS_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/htaccess.php', 'admin_postinstall_htaccess_condition', '3.4.0', 0),
(6, 700, 'COM_CPANEL_MSG_ROBOTS_TITLE', 'COM_CPANEL_MSG_ROBOTS_BODY', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.3.0', 0),
(7, 700, 'COM_CPANEL_MSG_LANGUAGEACCESS340_TITLE', 'COM_CPANEL_MSG_LANGUAGEACCESS340_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/languageaccess340.php', 'admin_postinstall_languageaccess340_condition', '3.4.1', 1),
(8, 700, 'COM_CPANEL_MSG_STATS_COLLECTION_TITLE', 'COM_CPANEL_MSG_STATS_COLLECTION_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/statscollection.php', 'admin_postinstall_statscollection_condition', '3.5.0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_programs`
--

DROP TABLE IF EXISTS `tol2j_programs`;
CREATE TABLE `tol2j_programs` (
  `id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `catid` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `program_name` varchar(255) NOT NULL COMMENT 'Program Name',
  `country` varchar(255) NOT NULL COMMENT 'Country',
  `program_manager` int(255) NOT NULL COMMENT 'Program Manager',
  `program_documents` int(11) NOT NULL COMMENT 'Program Documents',
  `banner` varchar(250) NOT NULL,
  `project_type` varchar(255) NOT NULL,
  `project_grant` varchar(255) NOT NULL,
  `implementor` varchar(255) NOT NULL,
  `project_description` varchar(1245) NOT NULL,
  `budget` int(11) NOT NULL,
  `enddate` varchar(50) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_programs`
--

INSERT INTO `tol2j_programs` (`id`, `asset_id`, `catid`, `program_name`, `country`, `program_manager`, `program_documents`, `banner`, `project_type`, `project_grant`, `implementor`, `project_description`, `budget`, `enddate`, `alias`, `ordering`, `published`, `checked_out`, `checked_out_time`, `created`, `created_by`, `modified`, `modified_by`, `publish_up`, `publish_down`, `access`, `metadata`, `metakey`, `metadesc`) VALUES
(1, 273, 84, 'Kenya', '1', 234, 0, 'images/Screen-Shot-2017-09-05-at-7.08.39-AM.png', '', '', '', 'Kenya is a major gateway to East Africa and a regional hub for trade, finance,communication and transportation services. The Mombasa Port serves over 200 million people as a critical node for trade for the Northern Corridor and it also serves northern Tanzania, DR Congo and Ethiopia. Evidence shows that many Kenyan businesses are still not sufficiently competitive to take full advantage of this emerging trade opportunities, particularly in terms of the capacity to export.The country, through Vision 2030 aims to transform into industrialising middleincome\r\n\r\nstatus with an annual economic growth rate of 10% driven by an exportled development strategy. Kenya’s key challenge is creation of more productive jobs for an increasing youth population, and attracting private investors who can drive innovation and entrepreneurship. Thus, it is imperative for Kenya to reduce the cost of doing business, which is necessary for a robust private sector.', 0, '', 'kenya', 1, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-14 09:42:17', 234, '2017-07-20 05:00:40', '0000-00-00 00:00:00', 12, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(2, 279, 84, 'Uganda', '2', 239, 0, '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '', 'uganda', 2, 1, 0, '0000-00-00 00:00:00', '2017-07-25 00:00:00', 234, '2017-09-14 09:43:50', 234, '2017-07-25 17:15:15', '0000-00-00 00:00:00', 16, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(3, 280, 84, 'Burundi', '4', 234, 0, '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 56550, '2019-08-30 13:08:21', 'burundi', 3, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-14 09:41:49', 234, '2017-07-20 05:00:40', '0000-00-00 00:00:00', 11, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(4, 281, 84, 'Tanzania', '3', 234, 0, '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '', 'tanzania', 4, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-14 09:43:19', 234, '2017-07-20 05:00:40', '0000-00-00 00:00:00', 15, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(5, 282, 84, 'Rwanda-01', '4', 234, 0, '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '', 'rwanda-01', 5, -2, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-07-25 00:00:00', 234, '2017-07-20 05:00:40', '0000-00-00 00:00:00', 1, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(6, 285, 84, 'Rwanda', '5', 234, 0, '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '', 'rwanda', 6, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-14 09:42:36', 234, '2017-07-20 05:00:40', '0000-00-00 00:00:00', 13, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(7, 286, 84, 'South Sudan', '6', 234, 0, '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '', 'south-sudan', 7, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-14 09:42:47', 234, '2017-07-20 05:00:40', '0000-00-00 00:00:00', 14, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(8, 287, 84, 'Arusha', '3', 234, 0, '', 'Test', 'TZ Gov', 'TZ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 330080, '2018-07-27 13:04:11', 'arusha', 8, 1, 0, '0000-00-00 00:00:00', '2017-07-20 00:00:00', 234, '2017-09-13 03:47:39', 234, '2017-07-20 05:00:40', '0000-00-00 00:00:00', 15, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(9, 288, 84, 'Biz Competitiveness', '4', 240, 0, '', 'Business', 'TMEA', 'TMEA', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 100000, '2019-07-05 13:07:18', 'biz-competitiveness', 9, 1, 0, '0000-00-00 00:00:00', '2017-07-26 00:00:00', 234, '2017-10-30 21:30:18', 234, '2017-07-26 09:01:02', '0000-00-00 00:00:00', 11, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(10, 289, 84, 'TECC', '7', 236, 0, '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '', 'tecc', 10, 1, 0, '0000-00-00 00:00:00', '2017-07-26 00:00:00', 234, '2017-09-14 09:43:35', 234, '2017-07-26 09:01:17', '0000-00-00 00:00:00', 16, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(11, 290, 84, 'Finance', '7', 235, 0, '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '', 'finance', 11, 1, 0, '0000-00-00 00:00:00', '2017-07-26 00:00:00', 234, '2017-09-14 09:42:03', 234, '2017-07-26 09:01:31', '0000-00-00 00:00:00', 13, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(12, 291, 84, 'START (Research & Knowledge)', '7', 238, 0, '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '', 'start-research-knowledge', 12, 1, 0, '0000-00-00 00:00:00', '2017-07-26 00:00:00', 234, '2017-09-14 09:43:02', 234, '2017-07-26 09:01:54', '0000-00-00 00:00:00', 12, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(13, 335, 84, 'TMEA EAC Partnership Programme', '', 251, 0, '', 'Regional', '', 'Arusha', 'TMEA EAC Partnership Programme', 0, '2020-10-09 21:28:02', 'tmea-eac-partnership-programme', 13, 1, 0, '0000-00-00 00:00:00', '2017-10-30 00:00:00', 234, '2017-10-30 20:18:08', 234, '2017-10-30 18:28:44', '0000-00-00 00:00:00', 25, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(14, 339, 84, 'New program', '5', 234, 0, '', 'Regional', '', 'Rwanda', 'Desc here', 0, '2018-10-19 09:23:52', 'new-program', 14, 1, 0, '0000-00-00 00:00:00', '2017-10-31 06:24:11', 234, '0000-00-00 00:00:00', 234, '2017-10-31 06:24:11', '0000-00-00 00:00:00', 13, '{\"robots\":\"\",\"rights\":\"\"}', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_projects`
--

DROP TABLE IF EXISTS `tol2j_projects`;
CREATE TABLE `tol2j_projects` (
  `id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(255) UNSIGNED NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL,
  `parent_program` int(11) NOT NULL,
  `project_type` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_description` varchar(4550) NOT NULL,
  `outcome` varchar(255) NOT NULL,
  `why` varchar(3000) NOT NULL,
  `what` varchar(3000) NOT NULL,
  `how` varchar(3000) NOT NULL,
  `achievements` varchar(3000) NOT NULL,
  `banner` varchar(250) NOT NULL,
  `country` int(11) NOT NULL,
  `project_manager` int(11) NOT NULL,
  `project_grant` varchar(255) NOT NULL,
  `project_budget` varchar(200) NOT NULL,
  `project_implementors` varchar(255) NOT NULL,
  `target` varchar(250) NOT NULL,
  `project_documents` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `state` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `access` int(11) NOT NULL,
  `access_level` int(11) NOT NULL,
  `published` int(11) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL,
  `metadata` text NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='TMEA Projects';

--
-- Dumping data for table `tol2j_projects`
--

INSERT INTO `tol2j_projects` (`id`, `asset_id`, `catid`, `parent_program`, `project_type`, `project_name`, `project_description`, `outcome`, `why`, `what`, `how`, `achievements`, `banner`, `country`, `project_manager`, `project_grant`, `project_budget`, `project_implementors`, `target`, `project_documents`, `start_date`, `end_date`, `created_by`, `created`, `state`, `ordering`, `checked_out`, `checked_out_time`, `access`, `access_level`, `published`, `publish_up`, `publish_down`, `alias`, `modified`, `modified_by`, `metadata`, `metakey`, `metadesc`) VALUES
(1, 0, 84, 1, 'Regional', 'Test', '<p>Test Description</p>', 'Business Competitiveness', '', '', '', '', '', 1, 235, 'Granter', '90000', 'TMEA KE', '', '', '2017-08-01', '2017-08-31', 235, '0000-00-00 00:00:00', 0, 0, 0, '0000-00-00 00:00:00', 12, 0, 1, '2017-08-29 19:10:40', '0000-00-00 00:00:00', 'test', '2017-09-12 17:16:01', 234, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(2, 0, 84, 6, 'Regional', 'One Stop Border Post at Kagitumba (Rwanda) / Mirama Hills (Uganda)', '<p>Improved physical infrastructure that is fit for purpose will contribute to reducing the time to transport goods between Kampala and Kigali, and therefore also contribute to reducing trade costs in East Africa.</p>', 'Business Competitiveness', '<p>The Mirama Hills road route offers a shorter and less difficult route to Rwanda than passing through the Katuna/Gatuna border post. Operationalisation of a one stop border post at Kagitumba and Mirama Hills is expected to decrease the time for goods to be transported between Kampala and Kigali and increase the volume of traffic using this route.</p>', '<p>The project focuses on constructing a one stop border post at the Kagitumba and Mirama Hills border. The project is related to the TMEA supported projects that will provide:</p>\r\n<ul>\r\n<li>Improved IT infrastructure.</li>\r\n<li>Harmonized working procedures on both sides of the border.</li>\r\n<li>Training for border agency staff, freight forwarders and traders.</li>\r\n<li>Construction of the Mirama Hills road.</li>\r\n</ul>', '<p>TMEA is providing funding for the construction of the OSBP and is supporting the lead agencies to implement the project on time and ensure quality. The project is implemented in coordination with other ongoing initiatives of the World Bank, JICA, USAID, the African Development Bank and the European Union.</p>', '', 'images/Screen-Shot-2017-09-05-at-6.43.13-AM.png', 5, 235, 'Grant', '90000', 'Rwanda Revenue Authority and Uganda Revenue Authority', 'Importers and exporters in East Africa', '', '2013-08-01', '2017-08-31', 235, '2017-08-29 00:00:00', 0, 1, 0, '0000-00-00 00:00:00', 12, 0, 1, '2017-08-29 19:10:40', '0000-00-00 00:00:00', 'one-stop-border-post-at-kagitumba-rwanda-mirama-hills-uganda', '2017-10-30 17:54:17', 234, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(3, 0, 84, 1, 'Regional', 'iShamba', '<ul>\r\n<li>Make quality agricultural information accessible for 20 crops and over five breeds of livestock in order to promote best practices, increase productivity and improve access to markets among targeted farmers</li>\r\n<li>Digitize weather information for all counties in Kenya.</li>\r\n<li>Digitize market price information for 42 commodities; thus enabling farmers to check and receive weather and market prices information in real time</li>\r\n<li>Create a platform where input suppliers, veterinary companies and agribusinesses such as grain buyers / processors interact directly with potential customers (farmers).</li>\r\n<li>Design content for marginal areas where farmers are more vulnerable to climatic risks, thereby providing them with access to information specifically tailored to their situation.</li>\r\n</ul>', 'Reduced Barriers to Trade', '<p>Access to quality agricultural information remains a challenge to many Kenyan farmers despite the fact that their ability to increase food production and benefit economically is linked to their ability to adopt new farming practices, solve problems and link to markets. While farmers currently access information through a web of social networks, sometimes this information is limited and cannot help them increase their farm productivity.</p>', '<p><span style=\"line-height: 1.6;\">Kenya’s mobile penetration stands at 82% with an expected rise to 90% before 2030, according to Communication Authority of Kenya. Therefore, mobile phone accessibility in Kenya, especially among the poor, makes it an important tool to pass important information on agriculture, which can enable farmers to improve their incomes.  iShamba, a Kenyan based mobile phone subscription- Farmers Club, was developed to respond to the need of access to information for farmers by utilising the rapid expansion of mobile phones. iShamba (</span><em style=\"line-height: 1.6;\">translated to iFarm from Swahili) </em><span style=\"line-height: 1.6;\">provides quality information on farm inputs, best practices, weather and prices to the subscribed farmers. Its main aim is to improve the yields and income derived from selling produce for participating farmers and create a commercially sustainable business model for the SMS service.  It works through a platform that digitally manages agricultural information and disseminates it through an SMS distribution and call centre facility without the need for a Mobile Network Operator. Since its inception, the iShamba SMS service has recorded a 50% increase in usage.</span></p>', '<p>iShamba provides SMS advice tailored to the crops and livestock each farmer holds, local to their location in Kenya, and in tune with their region’s crop cycle. The platform makes it possible to deliver real time information allowing farmers to make informed decisions on farm inputs, crops to plant, pricing and bargaining. Consequently, this increases farm level production and access to markets ensures an income plus improved livelihoods. Farmers using iShamba also benefit from special offers and discount prices offered by key EAC-based agri-product suppliers who are keen to use the platform as a direct channel to reach new customers.</p>', '<ul>\r\n<li>iShamba won two awards at the Mobile Innovation Awards i.e. Effective Integration of Mobile in an Omni Channel Strategy award &amp; Regional Award (Africa).</li>\r\n<li>At least 100,000 farmers subscribed to iShamba service – SMS logs, and payment / subscription records of participating farmers to the iShamba service</li>\r\n<li>Farmers increases farm gate prices of maize and potatoes by 5% as these crops prices are provided on the iShamba platform (determined from farmer’s output)</li>\r\n<li>Potato farmers subscribed to ishamba recording a greater yield by 50%, compared to those who had not subscribed</li>\r\n<li>Weather information by county level has also helped to serve as an early warning system to marginal areas (and those prone to flooding).</li>\r\n</ul>', 'images/Screen-Shot-2017-09-05-at-6.07.22-AM.png', 1, 235, 'Granter', 'US$ 347, 060', 'Mediae Company, Kenya', '20,000 smallholder farmers in Kenya', '', '2017-08-01', '2017-08-31', 235, '2017-08-29 00:00:00', 0, 1, 0, '0000-00-00 00:00:00', 12, 0, 1, '2017-08-29 19:10:40', '0000-00-00 00:00:00', 'ishamba', '2017-09-12 17:17:37', 234, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(4, 0, 84, 13, 'Regional', 'Arusha-TEPP', '<p>Arusha-TEPP Project </p>', 'Business Competitiveness', '<p>why here</p>', '', '', '', '', 3, 235, 'Granter', '190000', 'Arusha-TEPP', 'tRGT', '', '2017-08-01', '2018-08-06', 235, '2017-10-12 00:00:00', 0, 2, 0, '0000-00-00 00:00:00', 15, 0, 1, '2017-08-29 19:10:40', '0000-00-00 00:00:00', 'arusha-tepp', '2017-10-30 20:58:00', 234, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(5, 0, 84, 3, 'Burundi', 'Search for Common Ground (Phase II)', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">Improved relationships between small traders and border officials which contribute to improved trade climate &amp; governance in Burundi and around the EAC community.</span></p>', 'Business Competitiveness', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">Burundi, as the other East African economies, has a large informal trade sector, non-integrated with the formal economy and large business. Informal trade across borders is most often important to rural livelihoods, managed by individuals with a daily profit of under $50 per day. A 2009 study by the African Trade Policy Centre shows that informal cross border traders across the region do not use available formal systems/structures for most of their transactions. This makes it difficult for regional trade policy initiatives such as those under the EAC and its Customs Union Protocol to have any significant impact on this informal trade. The traders show little evidence of knowledge regarding the EAC Customs Protocol and even less motivation to use it to facilitate trading activities.</span></p>', '<p style=\"margin: 0px 0px 1em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: TMEA, Helvetica, sans-serif; vertical-align: baseline; text-align: justify;\">The objective of this project is to improve the relationships between small traders and border officials. Key outputs include;</p>\r\n<ul style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: TMEA, Helvetica, sans-serif; vertical-align: baseline; list-style: none outside none; text-align: justify;\">\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: square inside;\">Consolidating the results of the first phase of the project</li>\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: square inside;\">Extending the project to the Burundi-Tanzania border</li>\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: square inside;\">Reinforcing Government-Trader Engagement on Cross-Border Trade</li>\r\n</ul>', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">Trade Mark East Africa will provide support to Search for Common Ground (SFCG) to implement a second phase of the project.</span></p>', '', '', 4, 240, '', 'US$ 200,000', 'Search for Common Ground', 'Small traders and Border officials', '', '2013-10-01', '2017-10-03', 234, '2017-10-13 00:00:00', 0, 3, 0, '0000-00-00 00:00:00', 11, 0, 1, '2017-10-13 04:13:57', '0000-00-00 00:00:00', 'search-for-common-ground-phase-ii', '2017-10-30 21:23:28', 234, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(6, 0, 84, 3, 'Burundi', 'Forum for Strengthening Civil Society-Phase II', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">Increased competitiveness of Burundian traders and products through increased access to trade and market opportunities information for Burundian traders.</span></p>', 'Business Competitiveness', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">Burundi being seen as the weakest link of the group of the East African Countries, the majority of Burundians are still up to now asking on the process on the chances of success of Burundi in that new dynamic of regional integration. Thus, periodic analysis of impacts at various levels, on the opportunities and ways to take full advantage of this integration and in a progressive way remain then necessary. The success of the integration process depends on the commitment of states to strengthen their economic, social and political relationship through a real ownership of the process by the respective populations. It is clear that in Burundi the different stages of the process are not yet mastered, the opinion is under-informed, the content of the various existing protocols is beyond reach of the majority of the population, and the benefits of integration have not yet been sufficiently explained.</span></p>', '<p style=\"margin: 0px 0px 1em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: TMEA, Helvetica, sans-serif; vertical-align: baseline; text-align: justify;\">Through advocacy, contribute to an effective integration of Burundi in EAC by an improved investment climate and conditions. Key outputs include;</p>\r\n<ul style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: TMEA, Helvetica, sans-serif; vertical-align: baseline; list-style: none outside none; text-align: justify;\">\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: square inside;\">Framework for cross-border exchanges between Tanzania &amp; Burundi developed</li>\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: square inside;\">Advocacy campaign on reduction of bank interest rates conducted</li>\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: square inside;\">Advocacy plan around \'\'doing business\'\' &amp; opportunities developed and implemented</li>\r\n</ul>', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">TradeMark East Africa (TMEA) will provide technical support to Forum for the Reinforcement of the Civil Society (FORSC) to implement this project.</span></p>', '', '', 4, 240, '', 'US$128,000', 'Forum for the Reinforcement of the Civil Society (FORSC)', 'Traders in Burundi', '', '2014-10-01', '2016-10-04', 234, '2017-10-13 00:00:00', 0, 4, 234, '2017-10-30 21:25:11', 11, 0, 1, '2017-10-13 04:32:38', '0000-00-00 00:00:00', 'forum-for-strengthening-civil-society-phase-ii', '2017-10-30 21:20:05', 234, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(7, 0, 84, 1, 'Kenya', 'Port Reitz road', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">Expanded access/offtake for the Kipevu West Container Terminal which will in turn increase the capacity and efficiency at the Port of Mombasa</span></p>', 'Reduced Barriers to Trade or Outcome', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">The single largest project currently underway at the Port of Mombasa is theconstruction of a new container terminal at Kipevu West. The project, which is scheduled to be completed in August 2016 is expected to greatly improve the capacity and efficiency of the Port by bringing on board an extra 19,940 passenger car units (CPU) capacity by 2018 being an increase from the 2,874 CPU recorded in 2014. To support access to the Kipevu West container terminal, The Government of Kenya is currently undertaking the construction of the Mombasa Southern By pass project which is expected to be completed by 2018. In the period preceding the completion of the by-pass, the Port Reitz Road will be crucial to the Kipevu West Container Terminal as it will be the only access to the new container terminal. A traffic survey undertaken by the Kenya National Highways Agency (KeNHA) in January 2014 demonstrates that the present infrastructure in the Port Reitz area will be inadequate to meet the increased traffic demand. The survey recommends urgent expansion of the Port Reitz road into a dual carriageway.</span></p>', '<p class=\"p1\" style=\"margin: 0px 0px 1em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: TMEA, Helvetica, sans-serif; vertical-align: baseline; text-align: justify;\">The project will expand Port Reitz Road from the Port Reitz Hospital Gate, (located approximately 200m past the Kipevu West Terminal access road) to the junction with the Airport Road., and the Moi International Airport Road from Changamwe roundabout.  Specifically, the project includes: </p>\r\n<p class=\"p1\" style=\"margin: 0px 0px 1em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: TMEA, Helvetica, sans-serif; vertical-align: baseline; text-align: justify;\">a) Dualling and Improving the existing Port Reitz and Moi International Airport access roads covering 6.4Km length; </p>\r\n<p class=\"p1\" style=\"margin: 0px 0px 1em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: TMEA, Helvetica, sans-serif; vertical-align: baseline; text-align: justify;\">b) Improving Traffic movement at Intersections including installing traffic lights and grading separated junctions; </p>\r\n<p class=\"p1\" style=\"margin: 0px 0px 1em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: TMEA, Helvetica, sans-serif; vertical-align: baseline; text-align: justify;\">c) Installing road drainage facilities and sidewalks along Port Reitz road and Moi airport access roads on either side of the project roads; and</p>\r\n<p class=\"p1\" style=\"margin: 0px 0px 1em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: TMEA, Helvetica, sans-serif; vertical-align: baseline; text-align: justify;\">d) Installing road signs and markings.</p>', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">TMEA is providing funding for the Mombasa Port project and is supporting KeNHA to implement the project on time and ensure quality.</span></p>', '', '', 1, 0, '', '20M USD', 'Kenya National Highways Authority', 'Transporters along the Northern Corridor', '', '2014-10-05', '2017-10-02', 234, '2017-10-13 00:00:00', 0, 5, 0, '0000-00-00 00:00:00', 12, 0, 1, '2017-10-13 04:35:55', '0000-00-00 00:00:00', 'port-reitz-road', '2017-10-13 04:36:10', 234, '{\"robots\":\"\",\"rights\":\"\"}', '', ''),
(8, 0, 84, 1, 'Kenya', 'Mombasa port infrastructure works', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">Kenya Ports Authority (KPA) increases capacity especially in container trade and improves efficiency in handling of cargo and ships</span></p>', 'Reduced Barriers to Trade or Outcome', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">The Port of Mombasa has over the years recorded significant growth in traffic volumes. This high growth has put a strain on existing port infrastructure, necessitating costly investments to improve operations and service delivery. Despite the efforts by Kenya Ports Authority (KPA) to expand capacity and improve efficiency in cargo handling, the port still faces capacity constraints and service delivery challenges. These include low labour productivity, poor cargo off take by rail and road, cargo clearance delays, lack of full automation, operational wastages and poor resource utilization. These are compounded by changing ship technology; poor urban planning around the port; a changing legal and policy environment; corruption occurring along the Northern Corridor.</span></p>', '<table style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: TMEA, Helvetica, sans-serif; vertical-align: baseline; border-collapse: collapse; border-spacing: 0px; text-align: justify; width: 596px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">\r\n<tr style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">\r\n<td style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: top; text-align: left; height: 15px; width: 596px;\">\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">TMEA and KPA have agreed on a programme of activities to provide KPA with additional capacity to handle increasing demand which include:</p>\r\n<ul style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: none outside none;\">\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: square inside;\">Legal and Regulatory Revision;</li>\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: square inside;\">Port-wide Productivity Improvement;</li>\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: square inside;\">Infrastructure and Facilities Improvement;</li>\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: square inside;\">Preliminary analysis of the Mombasa Dry Port Initiative;</li>\r\n<li style=\"margin: 0px; padding: 0px; border: 0px; font-style: inher', '<p><span style=\"font-family: TMEA, Helvetica, sans-serif; font-size: 16px; text-align: justify;\">TMEA’s support to KPA is delivered through financial aid and technical support.</span></p>', '', '', 1, 0, '', 'US$ 45,509,504.19', 'Kenya Ports Authority (KPA)', 'Kenya Ports Authority, Kenya National Highways Authority, Kenya Urban Roads Authority, PSOs and CSOs, Importers and Exporters, Freight forwarders', '', '2013-10-01', '2017-10-02', 234, '2017-10-13 04:38:21', 0, 6, 0, '2017-10-13 04:38:21', 12, 0, 1, '2017-10-13 04:38:21', '0000-00-00 00:00:00', 'mombasa-port-infrastructure-works', '0000-00-00 00:00:00', 234, '{\"robots\":\"\",\"rights\":\"\"}', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_redirect_links`
--

DROP TABLE IF EXISTS `tol2j_redirect_links`;
CREATE TABLE `tol2j_redirect_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_url` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_url` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_scheduler_jobs`
--

DROP TABLE IF EXISTS `tol2j_scheduler_jobs`;
CREATE TABLE `tol2j_scheduler_jobs` (
  `uuid` char(36) NOT NULL,
  `identifier` varchar(255) NOT NULL DEFAULT '',
  `package` varchar(64) NOT NULL DEFAULT '',
  `frequency` varchar(128) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `queue` tinyint(4) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `state` text,
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `completed_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_scheduler_jobs`
--

INSERT INTO `tol2j_scheduler_jobs` (`uuid`, `identifier`, `package`, `frequency`, `status`, `queue`, `ordering`, `state`, `modified_on`, `completed_on`) VALUES
('31f902f5-9f21-4328-bf5d-fec87b7c613e', 'com://admin/docman.job.cache', 'docman', '0 0 * * *', 0, 0, 1, '{\"clean\":1509416066}', '2017-10-31 02:14:26', '2017-10-31 02:14:26'),
('e46f3920-bd16-4610-8963-1eef59b74787', 'com://admin/docman.job.categories', 'docman', '*/5 * * * *', 0, 0, 3, '{\"queue\":[]}', '2017-10-31 15:37:52', '2017-10-31 15:37:52'),
('5165d90d-cac1-466b-b4b4-503bed079587', 'com://admin/docman.job.documents', 'docman', '*/5 * * * *', 0, 0, 2, '{\"queue\":[]}', '2017-10-31 15:37:47', '2017-10-31 15:37:47');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_scheduler_metadata`
--

DROP TABLE IF EXISTS `tol2j_scheduler_metadata`;
CREATE TABLE `tol2j_scheduler_metadata` (
  `type` varchar(32) NOT NULL DEFAULT '',
  `sleep_until` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_run` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_scheduler_metadata`
--

INSERT INTO `tol2j_scheduler_metadata` (`type`, `sleep_until`, `last_run`) VALUES
('metadata', '2017-10-31 18:40:00', '2017-10-31 18:37:53');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_schemas`
--

DROP TABLE IF EXISTS `tol2j_schemas`;
CREATE TABLE `tol2j_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_schemas`
--

INSERT INTO `tol2j_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.6.0-2016-06-05'),
(10004, '1.0.8-2016-01-20'),
(10007, '1.2-2015-02-10'),
(10015, '3.0.0-beta2'),
(10025, 'install.mysql.utf8'),
(10033, '1.0.0'),
(10034, '1.0.0');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_session`
--

DROP TABLE IF EXISTS `tol2j_session`;
CREATE TABLE `tol2j_session` (
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `guest` tinyint(4) UNSIGNED DEFAULT '1',
  `time` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_session`
--

INSERT INTO `tol2j_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('6ee01ae75342a42fb4724ba017ec0020', 0, 1, '1509465108', '__koowa|a:3:{s:10:\"__metadata\";a:1:{s:5:\"timer\";a:3:{s:5:\"start\";i:1509464256;s:4:\"last\";i:1509464270;s:3:\"now\";i:1509465108;}}s:11:\"__attribute\";a:1:{s:4:\"user\";a:12:{s:2:\"id\";i:0;s:5:\"email\";N;s:4:\"name\";N;s:8:\"username\";N;s:8:\"password\";N;s:4:\"salt\";s:0:\"\";s:9:\"authentic\";b:0;s:7:\"enabled\";b:1;s:7:\"expired\";b:1;s:10:\"attributes\";a:0:{}s:5:\"roles\";a:0:{}s:6:\"groups\";a:0:{}}}s:9:\"__message\";a:0:{}}joomla|s:868:\"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjoyOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjozOntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjM6e3M6NzoiY291bnRlciI7aTo0O3M6NToidG9rZW4iO3M6MzI6InR4MUdSYnpWdFlrM2ZrbGJDM3hIVkdSVW50RVZQWHk2IjtzOjU6InRpbWVyIjtPOjg6InN0ZENsYXNzIjozOntzOjU6InN0YXJ0IjtpOjE1MDk0NjQyNjA7czo0OiJsYXN0IjtpOjE1MDk0NjQyNjk7czozOiJub3ciO2k6MTUwOTQ2NTEwNzt9fXM6ODoicmVnaXN0cnkiO086MjQ6Ikpvb21sYVxSZWdpc3RyeVxSZWdpc3RyeSI6Mjp7czo3OiIAKgBkYXRhIjtPOjg6InN0ZENsYXNzIjoxOntzOjU6InVzZXJzIjtPOjg6InN0ZENsYXNzIjoxOntzOjU6ImxvZ2luIjtPOjg6InN0ZENsYXNzIjoxOntzOjQ6ImZvcm0iO086ODoic3RkQ2xhc3MiOjE6e3M6NDoiZGF0YSI7YToxOntzOjY6InJldHVybiI7czozOToiaW5kZXgucGhwP29wdGlvbj1jb21fdXNlcnMmdmlldz1wcm9maWxlIjt9fX19fXM6OToic2VwYXJhdG9yIjtzOjE6Ii4iO31zOjQ6InVzZXIiO086NToiSlVzZXIiOjE6e3M6MjoiaWQiO2k6MDt9fX1zOjk6InNlcGFyYXRvciI7czoxOiIuIjt9\";', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_spmedia`
--

DROP TABLE IF EXISTS `tol2j_spmedia`;
CREATE TABLE `tol2j_spmedia` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `alt` varchar(255) NOT NULL,
  `caption` varchar(2048) NOT NULL,
  `description` mediumtext NOT NULL,
  `type` varchar(100) NOT NULL DEFAULT 'image',
  `extension` varchar(100) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_spmedia`
--

INSERT INTO `tol2j_spmedia` (`id`, `title`, `path`, `thumb`, `alt`, `caption`, `description`, `type`, `extension`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'logo-200x80-modified', 'images/2016/10/04/logo-200x80-modified.png', '', 'logo-200x80-modified', '', '', 'image', 'com_sppagebuilder', '2016-10-04 10:08:54', 234, '0000-00-00 00:00:00', 0),
(3, 'home_button', 'images/2016/10/04/home_button.png', '', 'home_button', '', '', 'image', 'com_sppagebuilder', '2016-10-04 10:16:01', 234, '0000-00-00 00:00:00', 0),
(4, '1475275129_Streamline-18', 'images/2016/10/06/1475275129_Streamline-18.png', '', '1475275129_Streamline-18', '', '', 'image', 'com_sppagebuilder', '2016-10-06 07:09:52', 234, '0000-00-00 00:00:00', 0),
(5, '61ccb4ce4a6a5f10aa097d9baedc7b37', 'images/2016/10/06/61ccb4ce4a6a5f10aa097d9baedc7b37.jpg', 'images/2016/10/06/_spmedia_thumbs/61ccb4ce4a6a5f10aa097d9baedc7b37.jpg', '61ccb4ce4a6a5f10aa097d9baedc7b37', '', '', 'image', 'com_sppagebuilder', '2016-10-06 09:37:30', 234, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_sppagebuilder`
--

DROP TABLE IF EXISTS `tol2j_sppagebuilder`;
CREATE TABLE `tol2j_sppagebuilder` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `text` mediumtext NOT NULL,
  `published` tinyint(3) NOT NULL DEFAULT '1',
  `catid` int(10) NOT NULL,
  `access` int(10) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` bigint(20) NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` bigint(20) NOT NULL DEFAULT '0',
  `og_title` varchar(255) NOT NULL,
  `og_image` varchar(255) NOT NULL,
  `og_description` varchar(255) NOT NULL,
  `page_layout` varchar(55) DEFAULT NULL,
  `language` char(7) NOT NULL,
  `hits` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_sppagebuilder`
--

INSERT INTO `tol2j_sppagebuilder` (`id`, `title`, `alias`, `text`, `published`, `catid`, `access`, `ordering`, `created_time`, `created_user_id`, `modified_time`, `modified_user_id`, `og_title`, `og_image`, `og_description`, `page_layout`, `language`, `hits`) VALUES
(1, 'Home', 'home', '[{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":0,\"margin\":\"\",\"padding\":\"170px 0\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_position\":\"50% 50%\",\"background_attachment\":\"inherit\",\"background_size\":\"cover\",\"background_repeat\":\"no-repeat\",\"background_image\":\"images/feature-bg1.jpg\",\"color\":\"\",\"background_color\":\"\",\"title_position\":\"sppb-text-center\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"\",\"title_fontsize\":\"\",\"heading_selector\":\"h1\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"class\":\"\",\"animationdelay\":300,\"animationduration\":500,\"animation\":\"fadeInUp\",\"padding\":\"\",\"color\":\"\",\"background\":\"\"},\"attr\":[{\"name\":\"call_to_action\",\"title\":\"Call to Action\",\"atts\":{\"title\":\"HELIX3 FRAMEWORK\",\"heading_selector\":\"h1\",\"title_fontsize\":\"72\",\"title_text_color\":\"#ffffff\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"20\",\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore. sed do eiusmod tempor incididunt ut labore et dolore\",\"subtitle_fontsize\":\"18\",\"subtitle_text_color\":\"#ffffff\",\"text\":\"\",\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"button_text\":\"Download Now!\",\"button_url\":\"http://www.joomshaper.com/helix\",\"button_size\":\"lg\",\"button_type\":\"primary\",\"button_icon\":\"\",\"button_block\":\"\",\"button_target\":\"_blank\",\"button_position\":\"bottom\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":\"3333\",\"settings\":{\"title\":\"\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"background_color\":\"\",\"color\":\"\",\"background_image\":\"\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"\",\"margin\":\"70px 0 \",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":300,\"animationdelay\":100,\"class\":\"\",\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Fully Responsive\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_url\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-laptop\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":500,\"animationdelay\":200,\"class\":\"\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Unique Design\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_url\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-dashboard\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"pork sirloi loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":500,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Easy to Customize\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_url\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-gear\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"andoe sirloin pork loine beefb uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":500,\"animationdelay\":400,\"class\":\"\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Awesome Support\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_url\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-envelope\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"picola sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":\"66\",\"settings\":{\"title\":\"\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"background_color\":\"#f5f5f5\",\"color\":\"\",\"background_image\":\"\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"100px 0\",\"margin\":\"100px 0 \",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-6\",\"settings\":{\"sortableitem\":\"[object Object]\",\"class\":\"\",\"animationdelay\":300,\"animationduration\":300,\"animation\":\"fadeInLeft\",\"padding\":\"\",\"color\":\"\",\"background\":\"\"},\"attr\":[{\"name\":\"video\",\"title\":\"Video\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"url\":\"http://vimeo.com/4445417\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-6\",\"settings\":{\"sortableitem\":\"[object Object]\",\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInRight\",\"animationduration\":300,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"text_block\",\"title\":\"Text Block\",\"atts\":{\"title\":\"Helix3 Overview\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"text\":\"The Helix Framework is one of the best light and feature rich responsive framework for Joomla templating. It is so easy to develop and control of Joomla templates for you and your clients. Helix framework will make your experience of creating website much smoother than ever before. <br /><br />Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"30\",\"hidden_md\":\"0\",\"hidden_sm\":\"0\",\"hidden_xs\":\"0\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"button\",\"title\":\"Button\",\"atts\":{\"text\":\"Documentation\",\"url\":\"http://www.joomshaper.com/documentation/\",\"size\":\"lg\",\"type\":\"primary\",\"icon\":\"\",\"target\":\"\",\"block\":\"\",\"margin\":\"\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"title\":\"Showcase\",\"heading_selector\":\"h2\",\"title_fontsize\":36,\"title_text_color\":\"#333333\",\"title_margin_top\":0,\"title_margin_bottom\":20,\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip\\n\\n\\n\\n\\n\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-center\",\"background_color\":\"\",\"color\":\"\",\"background_image\":\"\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"\",\"margin\":\"100px 0px 0px\",\"fullscreen\":1},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"\",\"animationduration\":\"\",\"animationdelay\":\"\",\"class\":\"\"},\"attr\":[{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"20\",\"hidden_md\":\"0\",\"hidden_sm\":\"0\",\"hidden_xs\":\"0\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"module\",\"title\":\"Joomla Module\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"id\":\"98\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":0,\"margin\":\"0px 0px 100px 0\",\"padding\":\"100px 0\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_image\":\"\",\"color\":\"\",\"background_color\":\"#f5f5f5\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"\",\"title_fontsize\":\"\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"zoomIn\",\"animationduration\":300,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"testimonialpro\",\"title\":\"Testimonial Pro\",\"atts\":{\"autoplay\":\"1\",\"arrows\":\"1\",\"class\":\"\"},\"scontent\":[{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Jony Daku\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}},{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Michel Cattar\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}},{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Nanchel Culy\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}}]}]}]},{\"type\":\"sp_row\",\"layout\":\"66\",\"settings\":{\"title\":\"\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"background_color\":\"\",\"color\":\"\",\"background_image\":\"\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"\",\"margin\":\"100px 0\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-6\",\"settings\":{\"class\":\"\",\"animationdelay\":300,\"animationduration\":500,\"animation\":\"fadeInLeft\",\"padding\":\"\",\"color\":\"\",\"background\":\"\",\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"text_block\",\"title\":\"Text Block\",\"atts\":{\"title\":\"Highly Compatible\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <br /><br />Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident\",\"alignment\":\"sppb-text-left\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-6 active-column-parent\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInRight\",\"animationduration\":500,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"55\",\"hidden_md\":\"0\",\"hidden_sm\":\"0\",\"hidden_xs\":\"0\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"image\",\"title\":\"Image\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"image\":\"images/compatible.png\",\"alt_text\":\"\",\"position\":\"sppb-text-left\",\"link\":\"\",\"target\":\"\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"title\":\"\",\"heading_selector\":\"h1\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-center\",\"background_color\":\"\",\"color\":\"\",\"background_image\":\"images/cta-bg.jpg\",\"background_repeat\":\"no-repeat\",\"background_size\":\"cover\",\"background_attachment\":\"fixed\",\"background_position\":\"50% 50%\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"100px 0\",\"margin\":\"\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"zoomIn\",\"animationduration\":400,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"call_to_action\",\"title\":\"Call to Action\",\"atts\":{\"title\":\"Ready to our latest creation Helix III\",\"heading_selector\":\"h2\",\"title_fontsize\":\"36\",\"title_text_color\":\"#ffffff\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"subtitle_text_color\":\"#ffffff\",\"text\":\"\",\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"button_text\":\"Download Now!\",\"button_url\":\"http://www.joomshaper.com/helix\",\"button_size\":\"lg\",\"button_type\":\"primary\",\"button_icon\":\"\",\"button_block\":\"\",\"button_target\":\"_blank\",\"button_position\":\"bottom\",\"class\":\"\"},\"scontent\":[]}]}]}]', 1, 0, 1, 0, '2015-01-22 07:12:35', 391, '2015-11-02 10:10:39', 47, '', '', '', '1', '*', 52),
(2, 'Corporate', 'corporate', '[{\"type\":\"sp_row\",\"layout\":\"66\",\"settings\":{\"title\":\"\",\"heading_selector\":\"h1\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-center\",\"background_color\":\"\",\"color\":\"\",\"background_image\":\"images/slider.jpg\",\"background_repeat\":\"no-repeat\",\"background_size\":\"cover\",\"background_attachment\":\"inherit\",\"background_position\":\"50% 50%\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"10px 0 0\",\"margin\":\"\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-6\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInLeftBig\",\"animationduration\":300,\"animationdelay\":300,\"class\":\"\",\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"100\",\"hidden_md\":\"0\",\"hidden_sm\":\"0\",\"hidden_xs\":\"0\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Clean and Unique Free  Joomla Template\",\"heading_selector\":\"h3\",\"title_fontsize\":\"48\",\"title_fontweight\":\"\",\"title_text_color\":\"#333333\",\"title_position\":\"before\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"\",\"icon_size\":\"\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod temporconsectetur adipisicing elit sed do.\",\"alignment\":\"sppb-text-left\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"button\",\"title\":\"Button\",\"atts\":{\"text\":\"Download Now!\",\"url\":\"http://www.joomshaper.com/helix\",\"size\":\"lg\",\"type\":\"primary\",\"icon\":\"\",\"target\":\"_blank\",\"block\":\"\",\"margin\":\"30px 0 0\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-6\",\"settings\":{\"class\":\"\",\"animationdelay\":300,\"animationduration\":300,\"animation\":\"fadeInRightBig\",\"padding\":\"\",\"color\":\"\",\"background\":\"\",\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"image\",\"title\":\"Image\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"image\":\"images/slide1.png\",\"position\":\"sppb-text-center\",\"link\":\"http://\",\"target\":\"\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":\"3333\",\"settings\":{\"title\":\"Our Services\",\"heading_selector\":\"h2\",\"title_fontsize\":36,\"title_text_color\":\"\",\"title_margin_top\":0,\"title_margin_bottom\":20,\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-center\",\"background_color\":\"#f5f5f5\",\"color\":\"\",\"background_image\":\"\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"100px 0px\",\"margin\":\"0px \",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"sortableitem\":\"[object Object]\",\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":300,\"animationdelay\":100,\"class\":\"\"},\"attr\":[{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"50\",\"hidden_md\":\"0\",\"hidden_sm\":\"0\",\"hidden_xs\":\"0\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Fully Responsive\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-laptop\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":300,\"animationdelay\":200,\"class\":\"\"},\"attr\":[{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"50\",\"hidden_md\":\"0\",\"hidden_sm\":\"0\",\"hidden_xs\":\"0\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Unique Design\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-dashboard\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":300,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"50\",\"hidden_md\":\"0\",\"hidden_sm\":\"0\",\"hidden_xs\":\"0\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Easy to Customize\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-gear\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":300,\"animationdelay\":400,\"class\":\"\"},\"attr\":[{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"50\",\"hidden_md\":\"0\",\"hidden_sm\":\"0\",\"hidden_xs\":\"0\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Awesome Support\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-envelope\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"background_attachment\":\"inherit\",\"fullscreen\":1,\"margin\":\"0px\",\"padding\":\"0px\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_position\":\"0 50%\",\"background_size\":\"contain\",\"background_repeat\":\"no-repeat\",\"background_image\":\"\",\"color\":\"\",\"background_color\":\"\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"\",\"title_fontsize\":\"\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"sortableitem\":\"[object Object]\",\"class\":\"\",\"animationdelay\":\"\",\"animationduration\":\"\",\"animation\":\"\",\"padding\":\"\",\"color\":\"\",\"background\":\"\"},\"attr\":[{\"name\":\"image_content\",\"title\":\"Image Content\",\"atts\":{\"image\":\"images/corporate.jpg\",\"image_alignment\":\"left\",\"title\":\"We Build our products to Represent our Values\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"text\":\"<br />The Helix Framework is one of the best light and feature rich responsive framework for Joomla templating. It is so easy to develop and control of Joomla templates for you and your clients. Helix framework will make your experience of creating website much smoother than ever before.<br /><br />It is so easy to develop and control of Joomla templates for you and your clients. Helix framework will make your experience of creating website much smoother than ever before.\",\"button_text\":\"\",\"button_url\":\"\",\"button_size\":\"\",\"button_type\":\"primary\",\"button_icon\":\"\",\"button_block\":\"\",\"button_target\":\"\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":0,\"margin\":\"0px 0px 0px\",\"padding\":\"100px 0\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_image\":\"\",\"color\":\"\",\"background_color\":\"#f5f5f5\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"\",\"title_fontsize\":\"\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"zoomIn\",\"animationduration\":300,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"testimonialpro\",\"title\":\"Testimonial Pro\",\"atts\":{\"autoplay\":\"1\",\"arrows\":\"1\",\"class\":\"\"},\"scontent\":[{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Jony Daku\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}},{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Michel Cattar\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}},{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Nanchel Culy\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}}]}]}]},{\"type\":\"sp_row\",\"layout\":\"3333\",\"settings\":{\"title\":\"\",\"heading_selector\":\"h1\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-center\",\"background_color\":\"\",\"color\":\"\",\"background_image\":\"images/client-bg.png\",\"background_repeat\":\"no-repeat\",\"background_size\":\"cover\",\"background_attachment\":\"inherit\",\"background_position\":\"50% 50%\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"70px 0\",\"margin\":\"0px\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"zoomIn\",\"animationduration\":400,\"animationdelay\":300,\"class\":\"\",\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"image\",\"title\":\"Image\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"image\":\"images/client1.png\",\"position\":\"sppb-text-center\",\"link\":\"http://www.joomshaper.com/\",\"target\":\"\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"zoomIn\",\"animationduration\":400,\"animationdelay\":450,\"class\":\"\"},\"attr\":[{\"name\":\"image\",\"title\":\"Image\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"image\":\"images/client2.png\",\"position\":\"sppb-text-center\",\"link\":\"http://www.joomshaper.com/\",\"target\":\"\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"zoomIn\",\"animationduration\":400,\"animationdelay\":600,\"class\":\"\"},\"attr\":[{\"name\":\"image\",\"title\":\"Image\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"image\":\"images/client3.png\",\"position\":\"sppb-text-center\",\"link\":\"http://www.joomshaper.com/\",\"target\":\"\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3 active-column-parent\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"zoomIn\",\"animationduration\":400,\"animationdelay\":750,\"class\":\"\"},\"attr\":[{\"name\":\"image\",\"title\":\"Image\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"image\":\"images/client4.png\",\"position\":\"sppb-text-center\",\"link\":\"http://www.joomshaper.com/\",\"target\":\"\",\"class\":\"\"},\"scontent\":[]}]}]}]', -2, 0, 1, 0, '2015-01-22 07:12:35', 391, '2015-03-02 11:21:57', 133, '', '', '', '1', '*', 0),
(3, 'Portfolio', 'portfolio', '[{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"title\":\"\",\"heading_selector\":\"h1\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-center\",\"background_color\":\"#049bef\",\"color\":\"\",\"background_image\":\"\",\"background_repeat\":\"no-repeat\",\"background_size\":\"cover\",\"background_attachment\":\"inherit\",\"background_position\":\"50% 50%\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"100px 0px 0px\",\"margin\":\"\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12 active-column-parent\",\"settings\":{\"sortableitem\":\"[object Object]\",\"class\":\"\",\"animationdelay\":\"\",\"animationduration\":\"\",\"animation\":\"\",\"padding\":\"\",\"color\":\"#ffffff\",\"background\":\"\"},\"attr\":[{\"name\":\"call_to_action\",\"title\":\"Call to Action\",\"atts\":{\"title\":\"Helix V3 Portfolio Layout\",\"heading_selector\":\"h3\",\"title_fontsize\":\"48\",\"title_text_color\":\"#ffffff\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"subtitle_text_color\":\"#ffffff\",\"text\":\"\",\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"button_text\":\"Download Now\",\"button_url\":\"#\",\"button_size\":\"lg\",\"button_type\":\"default\",\"button_icon\":\"\",\"button_block\":\"\",\"button_target\":\"\",\"button_position\":\"bottom\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"100\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"image\",\"title\":\"Image\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"image\":\"images/showcase/portfolio-banner.png\",\"position\":\"sppb-text-center\",\"link\":\"#\",\"target\":\"\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":\"3333\",\"settings\":{\"title\":\"\",\"heading_selector\":\"h2\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":0,\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-center\",\"background_color\":\"\",\"color\":\"\",\"background_image\":\"\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"\",\"margin\":\"100px 0 \",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"sortableitem\":\"[object Object]\",\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"\",\"animationduration\":\"\",\"animationdelay\":\"\",\"class\":\"\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Fully Responsive\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-laptop\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Unique Design\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-dashboard\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Easy to Customize\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-gear\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Awesome Support\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-envelope\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":1,\"margin\":\"100px 0px 0px\",\"padding\":\"\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_image\":\"\",\"color\":\"\",\"background_color\":\"\",\"title_position\":\"sppb-text-center\",\"subtitle_fontsize\":\"\",\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip\\n\\n\\n\",\"title_margin_bottom\":20,\"title_margin_top\":0,\"title_text_color\":\"#333333\",\"title_fontsize\":36,\"heading_selector\":\"h2\",\"title\":\"Showcase\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{},\"attr\":[{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"30\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"module\",\"title\":\"Joomla Module\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"id\":\"98\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":0,\"margin\":\"0px 0px 0px\",\"padding\":\"100px 0\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_image\":\"\",\"color\":\"\",\"background_color\":\"#fafafa\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"\",\"title_fontsize\":\"\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{},\"attr\":[{\"name\":\"testimonialpro\",\"title\":\"Testimonial Pro\",\"atts\":{\"autoplay\":\"1\",\"arrows\":\"1\",\"class\":\"\"},\"scontent\":[{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Jony Daku\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}},{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Michel Cattar\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}},{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Nanchel Culy\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}}]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"title\":\"\",\"heading_selector\":\"h1\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-center\",\"background_color\":\"\",\"color\":\"\",\"background_image\":\"images/cta_bg.jpg\",\"background_repeat\":\"no-repeat\",\"background_size\":\"cover\",\"background_attachment\":\"inherit\",\"background_position\":\"50% 50%\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"100px 0px\",\"margin\":\"0px\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"\",\"animationduration\":\"\",\"animationdelay\":\"\",\"class\":\"\"},\"attr\":[{\"name\":\"call_to_action\",\"title\":\"Call to Action\",\"atts\":{\"title\":\"Ready to our latest creation Helix III\",\"heading_selector\":\"h2\",\"title_fontsize\":\"36\",\"title_text_color\":\"#ffffff\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"30\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"subtitle_text_color\":\"#ffffff\",\"text\":\"\",\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"button_text\":\"Download\",\"button_url\":\"#\",\"button_size\":\"lg\",\"button_type\":\"primary\",\"button_icon\":\"\",\"button_block\":\"\",\"button_target\":\"\",\"button_position\":\"bottom\",\"class\":\"\"},\"scontent\":[]}]}]}]', -2, 0, 1, 0, '2015-01-22 07:12:35', 391, '2015-02-03 06:17:16', 864, '', '', '', '1', '*', 0),
(4, 'Services', 'services', '[{\"type\":\"sp_row\",\"layout\":\"444\",\"settings\":{\"fullscreen\":0,\"margin\":\"100px 0 \",\"padding\":\"\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_image\":\"\",\"color\":\"\",\"background_color\":\"\",\"title_position\":\"sppb-text-center\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":0,\"title_text_color\":\"\",\"title_fontsize\":\"\",\"heading_selector\":\"h2\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-4\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"\",\"animationduration\":\"\",\"animationdelay\":\"\",\"class\":\"\",\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Fully Responsive\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-laptop\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]},{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"50\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Well Documentattion\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-book\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-4\",\"settings\":{\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Unique Design\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-dashboard\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]},{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"50\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Page Builder\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-arrows\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-4\",\"settings\":{\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Easy to Customize\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-gear\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]},{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"50\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Dedicated Support\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-users\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":0,\"margin\":\"0px\",\"padding\":\"60px 0px\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_position\":\"50% 50%\",\"background_attachment\":\"inherit\",\"background_size\":\"cover\",\"background_repeat\":\"no-repeat\",\"background_image\":\"images/demo/cta_bg.jpg\",\"color\":\"\",\"background_color\":\"\",\"title_position\":\"sppb-text-center\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"\",\"title_fontsize\":\"\",\"heading_selector\":\"h1\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"class\":\"\",\"animationdelay\":\"\",\"animationduration\":\"\",\"animation\":\"\",\"padding\":\"\",\"color\":\"\",\"background\":\"\"},\"attr\":[{\"name\":\"image\",\"title\":\"Image\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"image\":\"images/client-white.png\",\"position\":\"sppb-text-center\",\"link\":\"#\",\"target\":\"\",\"class\":\"\"},\"scontent\":[]}]}]}]', -2, 0, 1, 0, '2015-01-22 07:12:35', 391, '2015-01-26 19:05:52', 391, '', '', '', '1', '*', 0);
INSERT INTO `tol2j_sppagebuilder` (`id`, `title`, `alias`, `text`, `published`, `catid`, `access`, `ordering`, `created_time`, `created_user_id`, `modified_time`, `modified_user_id`, `og_title`, `og_image`, `og_description`, `page_layout`, `language`, `hits`) VALUES
(5, 'Pricing Table', 'pricing-table', '[{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":0,\"margin\":\"\",\"padding\":\"40px 0px 25px\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_image\":\"\",\"color\":\"\",\"background_color\":\"#049bef\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"#ffffff\",\"title_fontsize\":36,\"heading_selector\":\"h2\",\"title\":\"Our Services\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{},\"attr\":[]}]},{\"type\":\"sp_row\",\"layout\":\"444\",\"settings\":{\"fullscreen\":0,\"margin\":\"100px 0 \",\"padding\":\"\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_image\":\"\",\"color\":\"\",\"background_color\":\"\",\"title_position\":\"sppb-text-center\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":0,\"title_text_color\":\"\",\"title_fontsize\":\"\",\"heading_selector\":\"h2\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-4\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"\",\"animationduration\":\"\",\"animationdelay\":\"\",\"class\":\"\",\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Fully Responsive\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-laptop\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]},{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"50\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Well Documentattion\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-book\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-4\",\"settings\":{\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Unique Design\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-dashboard\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]},{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"50\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Page Builder\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-arrows\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-4\",\"settings\":{\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Easy to Customize\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-gear\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]},{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"50\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Dedicated Support\",\"heading_selector\":\"h3\",\"title_fontsize\":\"24\",\"title_text_color\":\"#333333\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-users\",\"icon_size\":\"40\",\"icon_color\":\"#333333\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"20\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-left\",\"class\":\"feature-box-title\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":0,\"margin\":\"0px\",\"padding\":\"60px 0px\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_position\":\"50% 50%\",\"background_attachment\":\"inherit\",\"background_size\":\"cover\",\"background_repeat\":\"no-repeat\",\"background_image\":\"images/demo/cta_bg.jpg\",\"color\":\"\",\"background_color\":\"\",\"title_position\":\"sppb-text-center\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"\",\"title_fontsize\":\"\",\"heading_selector\":\"h1\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"class\":\"\",\"animationdelay\":\"\",\"animationduration\":\"\",\"animation\":\"\",\"padding\":\"\",\"color\":\"\",\"background\":\"\"},\"attr\":[{\"name\":\"image\",\"title\":\"Image\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"image\":\"images/client-white.png\",\"position\":\"sppb-text-center\",\"link\":\"#\",\"target\":\"\",\"class\":\"\"},\"scontent\":[]}]}]}]', -2, 0, 1, 0, '2015-01-22 07:12:35', 391, '2015-01-23 11:13:01', 391, '', '', '', '1', '*', 0),
(6, 'Portfolio', 'portfolio-2', '[{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":0,\"margin\":\"\",\"padding\":\"170px 0\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_position\":\"50% 50%\",\"background_attachment\":\"inherit\",\"background_size\":\"cover\",\"background_repeat\":\"no-repeat\",\"background_image\":\"images/featured-bg2.jpg\",\"color\":\"\",\"background_color\":\"\",\"title_position\":\"sppb-text-center\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"\",\"title_fontsize\":\"\",\"heading_selector\":\"h1\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"class\":\"\",\"animationdelay\":300,\"animationduration\":500,\"animation\":\"fadeInUp\",\"padding\":\"\",\"color\":\"\",\"background\":\"\"},\"attr\":[{\"name\":\"call_to_action\",\"title\":\"Call to Action\",\"atts\":{\"title\":\"FREE JOOMLA TEMPLATE\",\"heading_selector\":\"h1\",\"title_fontsize\":\"72\",\"title_text_color\":\"#ffffff\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"20\",\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore. sed do eiusmod tempor incididunt ut labore et dolore\",\"subtitle_fontsize\":\"18\",\"subtitle_text_color\":\"#ffffff\",\"text\":\"\",\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"button_text\":\"Download Now!\",\"button_url\":\"http://www.joomshaper.com/helix\",\"button_size\":\"lg\",\"button_type\":\"primary\",\"button_icon\":\"\",\"button_block\":\"\",\"button_target\":\"_blank\",\"button_position\":\"bottom\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":\"3333\",\"settings\":{\"title\":\"\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"background_color\":\"#f5f5f5\",\"color\":\"\",\"background_image\":\"\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"70px 0  5px\",\"margin\":\"0px\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":300,\"animationdelay\":100,\"class\":\"\",\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Fully Responsive\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-laptop\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":500,\"animationdelay\":200,\"class\":\"\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Unique Design\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-dashboard\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":500,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Easy to Customize\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-gear\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":500,\"animationdelay\":400,\"class\":\"\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Awesome Support\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-envelope\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":\"3333\",\"settings\":{\"title\":\"\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"background_color\":\"#f5f5f5\",\"color\":\"\",\"background_image\":\"\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"50px 0  100px\",\"margin\":\"0px 0px 100px\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":300,\"animationdelay\":100,\"class\":\"\",\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Page Builder\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-arrows\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":500,\"animationdelay\":200,\"class\":\"\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Mega Menu\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-list\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":500,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Layout Builder\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-check-square-o\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"fadeInDown\",\"animationduration\":500,\"animationdelay\":400,\"class\":\"\"},\"attr\":[{\"name\":\"feature\",\"title\":\"Feature Box\",\"atts\":{\"title\":\"Google Font\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_position\":\"after\",\"feature_type\":\"icon\",\"feature_image\":\"\",\"icon_name\":\"fa-google\",\"icon_size\":\"42\",\"icon_color\":\"\",\"icon_background\":\"\",\"icon_border_color\":\"\",\"icon_border_width\":\"\",\"icon_border_radius\":\"\",\"icon_margin_top\":\"0\",\"icon_margin_bottom\":\"\",\"icon_padding\":\"\",\"text\":\"Sirloin pork loine beefb andoe uillen capicola shank swine chuck flank tritip picola kevin filet mignon\",\"alignment\":\"sppb-text-center\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"title\":\"Showcase\",\"heading_selector\":\"h2\",\"title_fontsize\":36,\"title_text_color\":\"#333333\",\"title_margin_top\":0,\"title_margin_bottom\":20,\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip\\n\\n\\n\\n\\n\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-center\",\"background_color\":\"\",\"color\":\"\",\"background_image\":\"\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"\",\"margin\":\"100px 0px 0px\",\"fullscreen\":1},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"\",\"animationduration\":\"\",\"animationdelay\":\"\",\"class\":\"\"},\"attr\":[{\"name\":\"empty_space\",\"title\":\"Empty Space\",\"atts\":{\"gap\":\"20\",\"class\":\"\"},\"scontent\":[]},{\"name\":\"module\",\"title\":\"Joomla Module\",\"atts\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"id\":\"107\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":0,\"margin\":\"0px \",\"padding\":\"100px 0\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_image\":\"\",\"color\":\"\",\"background_color\":\"#f5f5f5\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"\",\"title_fontsize\":\"\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"zoomIn\",\"animationduration\":300,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"testimonialpro\",\"title\":\"Testimonial Pro\",\"atts\":{\"autoplay\":\"1\",\"arrows\":\"1\",\"class\":\"\"},\"scontent\":[{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Jony Daku\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}},{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Michel Cattar\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}},{\"type\":\"repeatable\",\"name\":\"sp_testimonialpro_item\",\"atts\":{\"title\":\"Nanchel Culy\",\"avatar\":\"images/testimonial.png\",\"avatar_style\":\"sppb-img-circle\",\"message\":\"Helix is a Awesome Framework for Joomla. It’s able me to easily customize the Joomla Templates Thanks to Helix Team. Really Appreciate it.\",\"url\":\"\"}}]}]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"title\":\"\",\"heading_selector\":\"h1\",\"title_fontsize\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-center\",\"background_color\":\"\",\"color\":\"\",\"background_image\":\"images/cta-bg.jpg\",\"background_repeat\":\"no-repeat\",\"background_size\":\"cover\",\"background_attachment\":\"fixed\",\"background_position\":\"50% 50%\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"100px 0\",\"margin\":\"0px\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"animation\":\"zoomIn\",\"animationduration\":400,\"animationdelay\":300,\"class\":\"\"},\"attr\":[{\"name\":\"call_to_action\",\"title\":\"Call to Action\",\"atts\":{\"title\":\"Ready to our latest creation Helix III\",\"heading_selector\":\"h2\",\"title_fontsize\":\"36\",\"title_text_color\":\"#ffffff\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"subtitle_text_color\":\"#ffffff\",\"text\":\"\",\"background\":\"\",\"color\":\"\",\"padding\":\"\",\"button_text\":\"Download Now!\",\"button_url\":\"http://www.joomshaper.com/helix\",\"button_size\":\"lg\",\"button_type\":\"primary\",\"button_icon\":\"\",\"button_block\":\"\",\"button_target\":\"_blank\",\"button_position\":\"bottom\",\"class\":\"\"},\"scontent\":[]}]}]}]', -2, 0, 1, 0, '2015-01-22 07:12:35', 391, '2015-02-04 09:07:57', 864, '', '', '', '1', '*', 1),
(7, 'Document Management System Design', 'document-management-system-design', '[{\"type\":\"sp_row\",\"layout\":\"39\",\"settings\":{\"fullscreen\":0,\"margin\":\"\",\"padding\":\"\",\"class\":\"\",\"id\":\"Document Management System Design Header\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_position\":\"0 0\",\"background_attachment\":\"inherit\",\"background_size\":\"cover\",\"background_repeat\":\"no-repeat\",\"background_image\":\"\",\"color\":\"#345487\",\"background_color\":\"#ffffff\",\"title_position\":\"sppb-text-left\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"#345487\",\"title_fontweight\":\"\",\"title_fontsize\":20,\"heading_selector\":\"h1\",\"title\":\"Header\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"sortableitem\":\"[object Object]\"},\"attr\":[{\"type\":\"content\",\"name\":\"image\",\"title\":\"Image\",\"atts\":{\"admin_label\":\"Logo\",\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"image\":\"images/2016/10/04/logo-200x80-modified.png\",\"alt_text\":\"\",\"position\":\"sppb-text-left\",\"link\":\"http://gbc.me.ke/tmea\",\"target\":\"\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-9\",\"settings\":{},\"attr\":[{\"type\":\"content\",\"name\":\"image_content\",\"title\":\"Image Content\",\"atts\":{\"image\":\"images/2016/10/04/home_button.png\",\"image_alignment\":\"left\",\"title\":\"Home\",\"heading_selector\":\"h3\",\"title_fontsize\":\"20\",\"title_fontweight\":\"\",\"title_text_color\":\"#345487\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"text\":\"\",\"button_text\":\"Button Text\",\"button_url\":\"http://\",\"button_size\":\"\",\"button_type\":\"default\",\"button_icon\":\"fa-home\",\"button_block\":\"\",\"button_target\":\"\",\"class\":\"\"},\"scontent\":[]}]}]},{\"type\":\"sp_row\",\"layout\":\"39\",\"settings\":{\"title\":\"Main Body\",\"heading_selector\":\"h2\",\"title_fontsize\":20,\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"background_color\":\"#ffffff\",\"color\":\"#345487\",\"background_image\":\"\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"\",\"margin\":\"\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"sortableitem\":\"[object Object]\"},\"attr\":[]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-9\",\"settings\":{},\"attr\":[]}]},{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":0,\"margin\":\"\",\"padding\":\"\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_image\":\"\",\"color\":\"#ffffff\",\"background_color\":\"#345487\",\"title_position\":\"sppb-text-center\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"#ffffff\",\"title_fontweight\":\"\",\"title_fontsize\":15,\"title\":\"Footer\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{},\"attr\":[]}]}]', -2, 0, 1, 0, '2016-10-04 09:06:45', 234, '2016-10-04 10:17:48', 234, '', '', '', '0', '*', 1),
(8, 'DMS Home', 'dms-home', '[{\"type\":\"sp_row\",\"layout\":\"39\",\"settings\":{\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":25,\"title_fontweight\":\"\",\"title_text_color\":\"#345487\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"subtitle\":\"\",\"subtitle_fontsize\":\"\",\"title_position\":\"sppb-text-left\",\"background_color\":\"#ffffff\",\"color\":\"#345487\",\"background_image\":\"images/2016/10/06/61ccb4ce4a6a5f10aa097d9baedc7b37.jpg\",\"background_repeat\":\"no-repeat\",\"background_size\":\"cover\",\"background_attachment\":\"fixed\",\"background_position\":\"0 0\",\"background_video\":0,\"background_video_mp4\":\"\",\"background_video_ogv\":\"\",\"id\":\"\",\"class\":\"\",\"padding\":\"\",\"margin\":\"\",\"fullscreen\":0},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-3\",\"settings\":{\"background\":\"\",\"color\":\"\",\"padding\":\"20px 20px 0 0\",\"animation\":\"\",\"animationduration\":300,\"animationdelay\":0,\"class\":\"\",\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"module\",\"title\":\"Joomla Module\",\"atts\":{\"admin_label\":\"Navigation Bar\",\"title\":\"Folders\",\"heading_selector\":\"h3\",\"title_fontsize\":\"15\",\"title_fontweight\":\"\",\"title_text_color\":\"#345487\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"module_type\":\"module\",\"id\":\"109\",\"position\":\"left\",\"class\":\"\"},\"scontent\":[]}]},{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-9 active-column-parent\",\"settings\":{\"sortableitem\":\"[object Object]\"},\"attr\":[{\"name\":\"module\",\"title\":\"Joomla Module\",\"atts\":{\"admin_label\":\"Main Page\",\"title\":\"Documents\",\"heading_selector\":\"h3\",\"title_fontsize\":\"18\",\"title_fontweight\":\"\",\"title_text_color\":\"#3473c7\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"module_type\":\"module\",\"id\":\"108\",\"position\":\"\",\"class\":\"\"},\"scontent\":[]}]}]}]', -2, 0, 1, 0, '2016-10-06 07:11:43', 234, '2016-10-06 09:37:55', 234, '', '', '', '0', '*', 4),
(9, 'Help', 'help', '[{\"type\":\"sp_row\",\"layout\":12,\"settings\":{\"fullscreen\":\"\",\"margin\":\"\",\"padding\":\"\",\"class\":\"\",\"id\":\"\",\"background_video_ogv\":\"\",\"background_video_mp4\":\"\",\"background_video\":0,\"background_position\":\"0 0\",\"background_attachment\":\"fixed\",\"background_size\":\"cover\",\"background_repeat\":\"no-repeat\",\"background_image\":\"\",\"color\":\"\",\"background_color\":\"\",\"title_position\":\"sppb-text-center\",\"subtitle_fontsize\":\"\",\"subtitle\":\"\",\"title_margin_bottom\":\"\",\"title_margin_top\":\"\",\"title_text_color\":\"\",\"title_fontweight\":\"\",\"title_fontsize\":\"\",\"heading_selector\":\"h3\",\"title\":\"\"},\"attr\":[{\"type\":\"sp_col\",\"class_name\":\"column-parent col-sm-12\",\"settings\":{\"class\":\"\",\"animationdelay\":0,\"animationduration\":300,\"animation\":\"\",\"padding\":\"\",\"color\":\"\",\"background\":\"\"},\"attr\":[{\"type\":\"content\",\"name\":\"text_block\",\"title\":\"Text Block\",\"atts\":{\"admin_label\":\"\",\"title\":\"\",\"heading_selector\":\"h3\",\"title_fontsize\":\"\",\"title_fontweight\":\"\",\"title_text_color\":\"\",\"title_margin_top\":\"\",\"title_margin_bottom\":\"\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget risus sollicitudin pellentesque et non erat. Maecenas nibh dolor, malesuada et bibendum a, sagittis accumsan ipsum. Pellentesque ultrices ultrices sapien, nec tincidunt nunc posuere ut. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam scelerisque tristique dolor vitae tincidunt. Aenean quis massa uada mi elementum elementum. Nec sapien convallis vulputate rhoncus vel dui.\",\"alignment\":\"sppb-text-left\",\"class\":\"\"},\"scontent\":[]}]}]}]', 1, 0, 1, 0, '2016-10-12 19:07:49', 234, '0000-00-00 00:00:00', 0, '', '', '', '0', '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_spsimpleportfolio_items`
--

DROP TABLE IF EXISTS `tol2j_spsimpleportfolio_items`;
CREATE TABLE `tol2j_spsimpleportfolio_items` (
  `spsimpleportfolio_item_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(55) NOT NULL,
  `category_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `video` text NOT NULL,
  `description` mediumtext,
  `spsimpleportfolio_tag_id` text NOT NULL,
  `url` text NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `language` varchar(255) NOT NULL DEFAULT '*',
  `access` int(5) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_spsimpleportfolio_items`
--

INSERT INTO `tol2j_spsimpleportfolio_items` (`spsimpleportfolio_item_id`, `title`, `alias`, `category_id`, `image`, `video`, `description`, `spsimpleportfolio_tag_id`, `url`, `enabled`, `language`, `access`, `ordering`, `created_by`, `created_on`, `modified_by`, `modified_on`, `locked_by`, `locked_on`) VALUES
(1, 'Responsive news Template', 'responsive-news-template', 0, 'images/showcase/showcase01.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"4\"]', '', 1, '*', 1, 0, 391, '2015-01-22 12:04:57', 391, '2015-01-23 10:40:23', 0, '0000-00-00 00:00:00'),
(2, 'Free Joomla Template', 'free-joomla-template', 0, 'images/showcase/showcase02.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"1\"]', '', 1, '*', 1, 0, 391, '2015-01-22 12:05:55', 391, '2015-01-23 10:40:31', 0, '0000-00-00 00:00:00'),
(3, 'Helix Free Template', 'helix-free-template', 0, 'images/showcase/showcase03.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"1\"]', '', 1, '*', 1, 0, 391, '2015-01-22 12:06:32', 391, '2015-01-23 10:40:03', 0, '0000-00-00 00:00:00'),
(4, 'Corporate Template', 'corporate-template', 0, 'images/showcase/showcase04.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"3\"]', '', 1, '*', 1, 0, 391, '2015-01-22 12:07:01', 391, '2015-01-23 10:29:07', 391, '2015-01-23 10:39:40'),
(5, 'SP Page Builder', 'sp-page-builder', 0, 'images/showcase/showcase05.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"2\"]', '', 1, '*', 1, 0, 391, '2015-01-22 12:07:30', 391, '2015-01-23 10:28:54', 0, '0000-00-00 00:00:00'),
(6, 'Organic Life Template', 'organic-life-template', 0, 'images/showcase/showcase06.jpg', '', '<p>Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p>Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami. Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball.</p>\r\n<p>Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"1\"]', '', 1, '*', 1, 0, 391, '2015-01-23 10:06:10', 391, '2015-01-23 10:28:42', 0, '0000-00-00 00:00:00'),
(7, 'Helix V3 Free Template', 'helix-v3-free-template', 0, 'images/showcase/showcase07.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"2\"]', '', 1, '*', 1, 0, 391, '2015-01-23 10:07:16', 391, '2015-01-23 10:28:29', 0, '0000-00-00 00:00:00'),
(8, 'Optima Premium Template', 'optima-premium-template', 0, 'images/showcase/showcase08.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"2\"]', '', 1, '*', 1, 0, 391, '2015-01-23 10:08:10', 391, '2015-01-23 10:28:18', 0, '0000-00-00 00:00:00'),
(9, 'Vocal Music Theme', 'vocal-music-theme', 0, 'images/showcase/showcase09.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"4\"]', '', 1, '*', 1, 0, 391, '2015-01-23 10:08:46', 391, '2015-01-23 10:28:04', 0, '0000-00-00 00:00:00'),
(10, 'Travel Premium Theme', 'travel-premium-theme', 0, 'images/showcase/showcase10.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"1\"]', '', 1, '*', 1, 0, 391, '2015-01-23 10:09:23', 391, '2015-01-23 10:27:52', 0, '0000-00-00 00:00:00'),
(11, 'Free Magazine Theme', 'free-magazine-theme', 0, 'images/showcase/showcase11.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"4\"]', '', 1, '*', 1, 0, 391, '2015-01-23 10:09:59', 391, '2015-01-23 10:27:42', 0, '0000-00-00 00:00:00'),
(12, 'Minima Joomla Template', 'minima-joomla-template', 0, 'images/showcase/showcase12.jpg', '', '<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Bacon ipsum dolor amet pork belly turducken strip steak meatloaf short ribs corned beef. Sausage landjaeger frankfurter leberkas chicken. Ball tip short loin picanha cow sausage. Short loin swine sausage, shank tail ham ball tip jerky boudin.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Sirloin pork loin beef andouille capicola shank swine chuck flank tri-tip. Capicola kevin filet mignon porchetta doner short ribs flank. Capicola pork chop turducken, ground round shoulder ribeye cupim doner jowl. Turkey shank pork belly jerky salami.</p>\r\n<p style=\"border: 0px; margin: 0px 0px 24px; padding: 0px; vertical-align: baseline; font-family: Georgia, \'Bitstream Charter\', serif; font-size: 16px; line-height: 24px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: initial;\">Chicken kevin pastrami tenderloin, tail corned beef prosciutto sirloin ribeye beef ribs pork chop tri-tip swine meatball. Pork chop swine porchetta fatback, strip steak filet mignon ham beef chicken meatloaf. Drumstick spare ribs corned beef, cow andouille cupim shankle pork beef ribs bacon flank salami leberkas. Shank turducken t-bone corned beef ground round shoulder cow beef ribs leberkas ribeye pork venison kevin meatloaf landjaeger. Ball tip jowl tongue venison. Ground round cow pork loin, sirloin venison cupim ham hock hamburger.</p>', '[\"1\"]', '', 1, '*', 1, 0, 391, '2015-01-23 10:10:38', 391, '2015-01-23 10:27:30', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_spsimpleportfolio_tags`
--

DROP TABLE IF EXISTS `tol2j_spsimpleportfolio_tags`;
CREATE TABLE `tol2j_spsimpleportfolio_tags` (
  `spsimpleportfolio_tag_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(55) NOT NULL,
  `language` varchar(255) NOT NULL DEFAULT '*'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tol2j_spsimpleportfolio_tags`
--

INSERT INTO `tol2j_spsimpleportfolio_tags` (`spsimpleportfolio_tag_id`, `title`, `alias`, `language`) VALUES
(1, 'Joomla', 'joomla', '*'),
(2, 'Responsive', 'responsive', '*'),
(3, 'Corporate', 'corporate', '*'),
(4, 'Magazine', 'magazine', '*');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_tags`
--

DROP TABLE IF EXISTS `tol2j_tags`;
CREATE TABLE `tol2j_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_tags`
--

INSERT INTO `tol2j_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 234, '2011-01-01 00:00:01', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_template_styles`
--

DROP TABLE IF EXISTS `tol2j_template_styles`;
CREATE TABLE `tol2j_template_styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `home` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_template_styles`
--

INSERT INTO `tol2j_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Fruit Shop', '{\"wrapperSmall\":53,\"wrapperLarge\":72,\"logo\":\"\",\"sitetitle\":\"Fruit Shop\",\"sitedescription\":\"The freshest fruit for you\",\"navposition\":\"left\",\"bootstrap\":\"\",\"templatecolor\":\"red\",\"headerImage\":\"\",\"backgroundcolor\":\"#eee\"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{\"showSiteName\":\"0\",\"colourChoice\":\"\",\"boldText\":\"0\"}'),
(7, 'protostar', 0, '0', 'protostar - Default', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}'),
(8, 'isis', 1, '1', 'isis - Default', '{\"templateColor\":\"\",\"logoFile\":\"\"}'),
(9, 'beez3', 0, '0', 'Beez3 - Default', '{\"wrapperSmall\":53,\"wrapperLarge\":72,\"logo\":\"\",\"sitetitle\":\"Joomla!\",\"sitedescription\":\"Open Source Content Management\",\"navposition\":\"left\",\"bootstrap\":\"\",\"templatecolor\":\"personal\",\"headerImage\":\"\",\"backgroundcolor\":\"#eee\"}'),
(10, 'shaper_helix3', 0, '1', 'shaper_helix3 - Default', '{\"sticky_header\":\"1\",\"favicon\":\"images\\/favicon.ico\",\"boxed_layout\":\"0\",\"logo_type\":\"text\",\"logo_position\":\"logo\",\"logo_image\":\"images\\/logo-200x90.png\",\"logo_image_2x\":\"\",\"mobile_logo\":\"\",\"logo_text\":\"TMEA EXTRANET\",\"logo_slogan\":\"\",\"body_bg_image\":\"\",\"body_bg_repeat\":\"no-repeat\",\"body_bg_size\":\"inherit\",\"body_bg_attachment\":\"fixed\",\"body_bg_position\":\"50% 50%\",\"enabled_copyright\":\"0\",\"copyright_position\":\"footer1\",\"copyright\":\"<left>Copyright 2016 Trade Mark East Africa | All Rights Reserved\\r\\n<\\/left>\\r\\n<right>\\r\\nPrivacy Policy | Disclaimer Notice | Terms of Use | Designed by GBC\\r\\n<\\/right>\",\"show_social_icons\":\"1\",\"social_position\":\"top1\",\"facebook\":\"https:\\/\\/www.facebook.com\\/TradeMarkEastAfrica\\/\",\"twitter\":\"https:\\/\\/twitter.com\\/TradeMarkEastA\",\"googleplus\":\"#\",\"pinterest\":\"#\",\"linkedin\":\"https:\\/\\/www.linkedin.com\\/company\\/trademark-east-africa\",\"dribbble\":\"#\",\"behance\":\"#\",\"youtube\":\"https:\\/\\/www.youtube.com\\/user\\/TradeMarkEA\",\"flickr\":\"https:\\/\\/www.flickr.com\\/photos\\/128062219@N08\\/sets\\/\",\"skype\":\"#\",\"vk\":\"#\",\"enable_contactinfo\":\"1\",\"contact_position\":\"top1\",\"contact_phone\":\"+254 20 423 5000\",\"contact_email\":\"info@trademarkea.com\",\"comingsoon_mode\":\"0\",\"comingsoon_title\":\"Coming Soon Title\",\"comingsoon_date\":\"05-10-2018\",\"comingsoon_content\":\"Coming soon content\",\"preset\":\"preset2\",\"preset1_bg\":\"#ffffff\",\"preset1_text\":\"#000000\",\"preset1_major\":\"#22b8f0\",\"preset2_bg\":\"#ffffff\",\"preset2_text\":\"#f50303\",\"preset2_major\":\"#383e94\",\"preset3_bg\":\"#ffffff\",\"preset3_text\":\"#000000\",\"preset3_major\":\"#2bb673\",\"preset4_bg\":\"#ffffff\",\"preset4_text\":\"#000000\",\"preset4_major\":\"#e0322f\",\"layoutlist\":\"1st-default.json\",\"layout\":\"[{\\\"type\\\":\\\"row\\\",\\\"layout\\\":\\\"237\\\",\\\"settings\\\":{\\\"name\\\":\\\"Header\\\",\\\"background_color\\\":\\\"\\\",\\\"color\\\":\\\"\\\",\\\"background_image\\\":\\\"images\\/strip.png\\\",\\\"link_color\\\":\\\"\\\",\\\"link_hover_color\\\":\\\"\\\",\\\"hidden_xs\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_md\\\":0,\\\"padding\\\":\\\"\\\",\\\"margin\\\":\\\"\\\",\\\"fluidrow\\\":0,\\\"custom_class\\\":\\\"fixed\\\"},\\\"attr\\\":[{\\\"type\\\":\\\"sp_col\\\",\\\"className\\\":\\\"layout-column col-sm-3 column-active\\\",\\\"settings\\\":{\\\"column_type\\\":0,\\\"custom_class\\\":\\\"\\\",\\\"md_hidden\\\":0,\\\"ms_hidden\\\":0,\\\"xs_hidden\\\":0,\\\"name\\\":\\\"menu\\\",\\\"hidden_xs\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_md\\\":1,\\\"sm_col\\\":\\\"\\\",\\\"xs_col\\\":\\\"col-xs-2\\\",\\\"sortableitem\\\":\\\"[object Object]\\\"}},{\\\"type\\\":\\\"sp_col\\\",\\\"className\\\":\\\"layout-column col-sm-2\\\",\\\"settings\\\":{\\\"column_type\\\":0,\\\"md_hidden\\\":0,\\\"ms_hidden\\\":0,\\\"xs_hidden\\\":0,\\\"custom_class\\\":\\\"left\\\",\\\"xs_col\\\":\\\"col-xs-6\\\",\\\"sm_col\\\":\\\"\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"logo\\\",\\\"sortableitem\\\":\\\"[object Object]\\\"}},{\\\"type\\\":\\\"sp_col\\\",\\\"className\\\":\\\"layout-column col-sm-7\\\",\\\"settings\\\":{\\\"custom_class\\\":\\\" pull-right\\\",\\\"xs_col\\\":\\\"col-xs-4\\\",\\\"sm_col\\\":\\\"\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"user_section\\\",\\\"column_type\\\":0}}]},{\\\"type\\\":\\\"row\\\",\\\"layout\\\":12,\\\"settings\\\":{\\\"name\\\":\\\"Page Title\\\",\\\"background_color\\\":\\\"\\\",\\\"color\\\":\\\"\\\",\\\"background_image\\\":\\\"\\\",\\\"link_color\\\":\\\"\\\",\\\"link_hover_color\\\":\\\"\\\",\\\"hidden_xs\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_md\\\":0,\\\"padding\\\":\\\"\\\",\\\"margin\\\":\\\"\\\",\\\"fluidrow\\\":1,\\\"custom_class\\\":\\\"hidden\\\"},\\\"attr\\\":[{\\\"type\\\":\\\"sp_col\\\",\\\"className\\\":\\\"layout-column col-sm-12\\\",\\\"settings\\\":{\\\"column_type\\\":0,\\\"custom_class\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"title\\\"}}]},{\\\"type\\\":\\\"row\\\",\\\"layout\\\":\\\"48\\\",\\\"settings\\\":{\\\"fluidrow\\\":0,\\\"margin\\\":\\\"\\\",\\\"padding\\\":\\\"10px 0\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"background_image\\\":\\\"\\\",\\\"color\\\":\\\"\\\",\\\"background_color\\\":\\\"\\\",\\\"custom_class\\\":\\\"\\\",\\\"md_hidden\\\":0,\\\"ms_hidden\\\":0,\\\"xs_hidden\\\":0,\\\"link_hover_color\\\":\\\"\\\",\\\"link_color\\\":\\\"\\\",\\\"text_color\\\":\\\"\\\",\\\"bg_image\\\":\\\"\\\",\\\"bg_color\\\":\\\"\\\",\\\"name\\\":\\\"Main Body\\\"},\\\"attr\\\":[{\\\"type\\\":\\\"sp_col\\\",\\\"className\\\":\\\"layout-column col-sm-4\\\",\\\"settings\\\":{\\\"column_type\\\":0,\\\"sortableitem\\\":\\\"[object Object]\\\",\\\"custom_class\\\":\\\"no-gutters\\\",\\\"md_hidden\\\":0,\\\"ms_hidden\\\":0,\\\"xs_hidden\\\":0,\\\"name\\\":\\\"left\\\",\\\"hidden_xs\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_md\\\":0,\\\"sm_col\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\"}},{\\\"type\\\":\\\"sp_col\\\",\\\"className\\\":\\\"layout-column col-sm-8\\\",\\\"settings\\\":{\\\"column_type\\\":1,\\\"sortableitem\\\":\\\"[object Object]\\\",\\\"xs_hidden\\\":0,\\\"ms_hidden\\\":0,\\\"md_hidden\\\":0,\\\"custom_class\\\":\\\"\\\",\\\"name\\\":\\\"\\\"}}]},{\\\"type\\\":\\\"row\\\",\\\"layout\\\":\\\"66\\\",\\\"settings\\\":{\\\"custom_class\\\":\\\"\\\",\\\"md_hidden\\\":0,\\\"ms_hidden\\\":0,\\\"xs_hidden\\\":0,\\\"link_hover_color\\\":\\\"\\\",\\\"link_color\\\":\\\"\\\",\\\"text_color\\\":\\\"\\\",\\\"bg_image\\\":\\\"\\\",\\\"bg_color\\\":\\\"\\\",\\\"name\\\":\\\"Footer\\\",\\\"background_color\\\":\\\"\\\",\\\"color\\\":\\\"\\\",\\\"background_image\\\":\\\"images\\/strip.png\\\",\\\"hidden_xs\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_md\\\":0,\\\"padding\\\":\\\"\\\",\\\"margin\\\":\\\"\\\",\\\"fluidrow\\\":1},\\\"attr\\\":[{\\\"type\\\":\\\"sp_col\\\",\\\"className\\\":\\\"layout-column col-sm-6\\\",\\\"settings\\\":{\\\"custom_class\\\":\\\"\\\",\\\"md_hidden\\\":0,\\\"ms_hidden\\\":0,\\\"xs_hidden\\\":0,\\\"name\\\":\\\"footer1\\\",\\\"column_type\\\":0,\\\"hidden_xs\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_md\\\":0,\\\"sm_col\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"sortableitem\\\":\\\"[object Object]\\\"}},{\\\"type\\\":\\\"sp_col\\\",\\\"className\\\":\\\"layout-column col-sm-6\\\",\\\"settings\\\":{\\\"custom_class\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"footer2\\\",\\\"column_type\\\":0}}]}]\",\"menu\":\"left-nav\",\"menu_type\":\"offcanvas\",\"dropdown_width\":\"\",\"menu_animation\":\"menu-fade\",\"enable_body_font\":\"1\",\"body_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"300\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h1_font\":\"1\",\"h1_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"800\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h2_font\":\"1\",\"h2_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"600\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h3_font\":\"1\",\"h3_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"regular\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h4_font\":\"1\",\"h4_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"regular\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h5_font\":\"1\",\"h5_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"800\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_h6_font\":\"1\",\"h6_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"800\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_navigation_font\":\"0\",\"navigation_font\":\"{\\\"fontFamily\\\":\\\"Open Sans\\\",\\\"fontWeight\\\":\\\"regular\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"enable_custom_font\":\"0\",\"custom_font\":\"{\\\"fontFamily\\\":\\\"ABeeZee\\\",\\\"fontWeight\\\":\\\"regular\\\",\\\"fontSubset\\\":\\\"latin\\\",\\\"fontSize\\\":\\\"\\\"}\",\"custom_font_selectors\":\"\",\"before_head\":\"\",\"before_body\":\"\",\"custom_css\":\"\",\"custom_js\":\"\",\"compress_css\":\"0\",\"compress_js\":\"0\",\"exclude_js\":\"\",\"lessoption\":\"1\",\"show_post_format\":\"1\",\"commenting_engine\":\"disqus\",\"disqus_subdomain\":\"\",\"disqus_devmode\":\"0\",\"intensedebate_acc\":\"\",\"fb_appID\":\"\",\"fb_width\":\"500\",\"fb_cpp\":\"10\",\"comments_count\":\"0\",\"social_share\":\"1\",\"image_small\":\"0\",\"image_small_size\":\"100X100\",\"image_thumbnail\":\"1\",\"image_thumbnail_size\":\"200X200\",\"image_medium\":\"0\",\"image_medium_size\":\"300X300\",\"image_large\":\"0\",\"image_large_size\":\"600X600\",\"blog_list_image\":\"default\"}');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_ucm_base`
--

DROP TABLE IF EXISTS `tol2j_ucm_base`;
CREATE TABLE `tol2j_ucm_base` (
  `ucm_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_ucm_content`
--

DROP TABLE IF EXISTS `tol2j_ucm_content`;
CREATE TABLE `tol2j_ucm_content` (
  `core_content_id` int(10) UNSIGNED NOT NULL,
  `core_type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `core_body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_checked_out_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_featured` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_publish_up` datetime NOT NULL,
  `core_publish_down` datetime NOT NULL,
  `core_content_item_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID from the individual type table',
  `asset_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'FK to the #__assets table.',
  `core_images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_urls` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Contains core content data in name spaced fields';

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_ucm_history`
--

DROP TABLE IF EXISTS `tol2j_ucm_history`;
CREATE TABLE `tol2j_ucm_history` (
  `version_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) UNSIGNED NOT NULL,
  `ucm_type_id` int(10) UNSIGNED NOT NULL,
  `version_note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `character_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_ucm_history`
--

INSERT INTO `tol2j_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(1, 19, 6, '', '2015-02-02 06:05:15', 234, 580, '8b8b28d0735048ef374cfed409baeca2e9c48a2a', '{\"id\":19,\"asset_id\":\"44\",\"parent_id\":\"1\",\"lft\":\"105\",\"rgt\":132,\"level\":1,\"path\":\"sample-data-articles\\/joomla\",\"extension\":\"com_content\",\"title\":\"Joomla!\",\"alias\":\"joomla\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 06:05:10\",\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"864\",\"created_time\":\"2011-01-01 00:00:01\",\"modified_user_id\":\"864\",\"modified_time\":\"2015-02-02 06:05:15\",\"hits\":\"0\",\"language\":\"*\",\"version\":\"1\"}', 0),
(2, 21, 6, '', '2015-02-02 06:05:23', 234, 872, '44c955d32c91be43379e62067ff88355a5e6afd7', '{\"id\":21,\"asset_id\":\"46\",\"parent_id\":\"19\",\"lft\":\"130\",\"rgt\":131,\"level\":2,\"path\":\"joomla\\/extensions\\/components\",\"extension\":\"com_content\",\"title\":\"Components\",\"alias\":\"components\",\"note\":\"\",\"description\":\"<p>Components are larger extensions that produce the major content for your site. Each component has one or more \\\"views\\\" that control how content is displayed. In the Joomla administrator there are additional extensions such as Menus, Redirection, and the extension managers.<\\/p>\",\"published\":\"1\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 06:05:17\",\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"864\",\"created_time\":\"2011-01-01 00:00:01\",\"modified_user_id\":\"864\",\"modified_time\":\"2015-02-02 06:05:23\",\"hits\":\"0\",\"language\":\"*\",\"version\":\"1\"}', 0),
(3, 79, 6, '', '2015-02-02 06:12:46', 234, 511, '633bb165d3f0f0ffc2722ee67a8a24736aadfb47', '{\"id\":79,\"asset_id\":179,\"parent_id\":\"1\",\"lft\":\"91\",\"rgt\":92,\"level\":1,\"path\":null,\"extension\":\"com_content\",\"title\":\"Blog\",\"alias\":\"blog\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"864\",\"created_time\":\"2015-02-02 06:12:46\",\"modified_user_id\":null,\"modified_time\":null,\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(4, 80, 6, '', '2015-02-02 18:42:14', 234, 511, 'd9e56992245d6c642c811b6be37a65ad944605d4', '{\"id\":80,\"asset_id\":191,\"parent_id\":\"1\",\"lft\":\"93\",\"rgt\":94,\"level\":1,\"path\":null,\"extension\":\"com_content\",\"title\":\"News\",\"alias\":\"news\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"864\",\"created_time\":\"2015-02-02 18:42:14\",\"modified_user_id\":null,\"modified_time\":null,\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(5, 80, 6, '', '2015-02-02 18:42:30', 234, 551, 'b0c69f126552d2f470dab4c2637f32d2d55d04ca', '{\"id\":80,\"asset_id\":\"191\",\"parent_id\":\"79\",\"lft\":\"92\",\"rgt\":93,\"level\":2,\"path\":\"news\",\"extension\":\"com_content\",\"title\":\"News\",\"alias\":\"news\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 18:42:24\",\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"864\",\"created_time\":\"2015-02-02 18:42:14\",\"modified_user_id\":\"864\",\"modified_time\":\"2015-02-02 18:42:30\",\"hits\":\"0\",\"language\":\"*\",\"version\":\"1\"}', 0),
(6, 81, 6, '', '2015-02-02 18:42:46', 234, 520, 'c1186cda9e6fae853472a26d68bc50d2f9af3eef', '{\"id\":81,\"asset_id\":192,\"parent_id\":\"79\",\"lft\":\"94\",\"rgt\":95,\"level\":2,\"path\":null,\"extension\":\"com_content\",\"title\":\"Tutorial\",\"alias\":\"tutorial\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"864\",\"created_time\":\"2015-02-02 18:42:46\",\"modified_user_id\":null,\"modified_time\":null,\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(7, 82, 6, '', '2015-02-02 18:42:56', 234, 516, '8433516aea93072c0f29f750669e5b4dfe733e72', '{\"id\":82,\"asset_id\":193,\"parent_id\":\"79\",\"lft\":\"96\",\"rgt\":97,\"level\":2,\"path\":null,\"extension\":\"com_content\",\"title\":\"Review\",\"alias\":\"review\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"864\",\"created_time\":\"2015-02-02 18:42:56\",\"modified_user_id\":null,\"modified_time\":null,\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(8, 83, 6, '', '2015-02-02 18:43:15', 234, 518, 'c054624137f36fae4f0cc107c6e7caa47aeb8401', '{\"id\":83,\"asset_id\":194,\"parent_id\":\"79\",\"lft\":\"98\",\"rgt\":99,\"level\":2,\"path\":null,\"extension\":\"com_content\",\"title\":\"Updates\",\"alias\":\"updates\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"864\",\"created_time\":\"2015-02-02 18:43:15\",\"modified_user_id\":null,\"modified_time\":null,\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(9, 71, 1, '', '2015-02-02 18:56:08', 234, 2452, '3199ad1fb625886490e0c300580c73a27ce5ad80', '{\"id\":71,\"asset_id\":195,\"title\":\"Sed non bibendum urna lorem ipsum dolor sit amet, consectetur adipiscing elit\",\"alias\":\"sed-non-bibendum-urna-lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit\",\"introtext\":\"\",\"fulltext\":\"\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 18:56:08\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2015-02-02 18:56:08\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"status\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"<blockquote class=\\\\\\\"twitter-tweet\\\\\\\" lang=\\\\\\\"en\\\\\\\"><p>Published a new blog entry One Month Extra for all JoomShaper Members in News. <a href=\\\\\\\"http:\\\\\\/\\\\\\/t.co\\\\\\/2pQYdykKy8\\\\\\\">http:\\\\\\/\\\\\\/t.co\\\\\\/2pQYdykKy8<\\\\\\/a><\\\\\\/p>&mdash; JoomShaper (@joomshaper) <a href=\\\\\\\"https:\\\\\\/\\\\\\/twitter.com\\\\\\/joomshaper\\\\\\/status\\\\\\/562210375480139777\\\\\\\">February 2, 2015<\\\\\\/a><\\\\\\/blockquote>\\\\r\\\\n<script async src=\\\\\\\"\\\\\\/\\\\\\/platform.twitter.com\\\\\\/widgets.js\\\\\\\" charset=\\\\\\\"utf-8\\\\\\\"><\\\\\\/script>\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(10, 71, 1, '', '2015-02-02 18:57:20', 234, 3565, 'd515334bc8af0f508f8d49ed3f265247e6927098', '{\"id\":71,\"asset_id\":\"195\",\"title\":\"Sed non bibendum urna lorem ipsum dolor sit amet, consectetur adipiscing elit\",\"alias\":\"sed-non-bibendum-urna-lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 18:56:08\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 18:57:20\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 18:56:08\",\"publish_up\":\"2015-02-02 18:56:08\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"status\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"<blockquote class=\\\\\\\"twitter-tweet\\\\\\\" lang=\\\\\\\"en\\\\\\\"><p>Published a new blog entry One Month Extra for all JoomShaper Members in News. <a href=\\\\\\\"http:\\\\\\/\\\\\\/t.co\\\\\\/2pQYdykKy8\\\\\\\">http:\\\\\\/\\\\\\/t.co\\\\\\/2pQYdykKy8<\\\\\\/a><\\\\\\/p>&mdash; JoomShaper (@joomshaper) <a href=\\\\\\\"https:\\\\\\/\\\\\\/twitter.com\\\\\\/joomshaper\\\\\\/status\\\\\\/562210375480139777\\\\\\\">February 2, 2015<\\\\\\/a><\\\\\\/blockquote>\\\\r\\\\n<script async src=\\\\\\\"\\\\\\/\\\\\\/platform.twitter.com\\\\\\/widgets.js\\\\\\\" charset=\\\\\\\"utf-8\\\\\\\"><\\\\\\/script>\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(11, 71, 1, '', '2015-02-02 19:27:38', 234, 3506, '65f0cb164ef3547aea586502a546465a75009405', '{\"id\":71,\"asset_id\":\"195\",\"title\":\"Sed non bibendum urna lorem ipsum dolor sit amet\",\"alias\":\"sed-non-bibendum-urna-lorem-ipsum-dolor-sit-amet\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 18:56:08\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 19:27:38\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 19:27:11\",\"publish_up\":\"2015-02-02 18:56:08\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"link\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"<blockquote class=\\\\\\\"twitter-tweet\\\\\\\" lang=\\\\\\\"en\\\\\\\"><p>Published a new blog entry One Month Extra for all JoomShaper Members in News. <a href=\\\\\\\"http:\\\\\\/\\\\\\/t.co\\\\\\/2pQYdykKy8\\\\\\\">http:\\\\\\/\\\\\\/t.co\\\\\\/2pQYdykKy8<\\\\\\/a><\\\\\\/p>&mdash; JoomShaper (@joomshaper) <a href=\\\\\\\"https:\\\\\\/\\\\\\/twitter.com\\\\\\/joomshaper\\\\\\/status\\\\\\/562210375480139777\\\\\\\">February 2, 2015<\\\\\\/a><\\\\\\/blockquote>\\\\r\\\\n<script async src=\\\\\\\"\\\\\\/\\\\\\/platform.twitter.com\\\\\\/widgets.js\\\\\\\" charset=\\\\\\\"utf-8\\\\\\\"><\\\\\\/script>\\\"}\",\"version\":3,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"2\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(12, 71, 1, '', '2015-02-02 20:27:55', 234, 3474, '83a23bb087b2f684a1a99ac8909d53860444eef9', '{\"id\":71,\"asset_id\":\"195\",\"title\":\"Doner spare ribs pastrami shank\",\"alias\":\"doner-spare-ribs-pastrami-shank\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 18:56:08\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:27:55\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 19:44:14\",\"publish_up\":\"2015-02-02 18:56:08\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"status\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"<blockquote class=\\\\\\\"twitter-tweet\\\\\\\" lang=\\\\\\\"en\\\\\\\"><p>Published a new blog entry One Month Extra for all JoomShaper Members in News. <a href=\\\\\\\"http:\\\\\\/\\\\\\/t.co\\\\\\/2pQYdykKy8\\\\\\\">http:\\\\\\/\\\\\\/t.co\\\\\\/2pQYdykKy8<\\\\\\/a><\\\\\\/p>&mdash; JoomShaper (@joomshaper) <a href=\\\\\\\"https:\\\\\\/\\\\\\/twitter.com\\\\\\/joomshaper\\\\\\/status\\\\\\/562210375480139777\\\\\\\">February 2, 2015<\\\\\\/a><\\\\\\/blockquote>\\\\r\\\\n<script async src=\\\\\\\"\\\\\\/\\\\\\/platform.twitter.com\\\\\\/widgets.js\\\\\\\" charset=\\\\\\\"utf-8\\\\\\\"><\\\\\\/script>\\\"}\",\"version\":4,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"5\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(13, 72, 1, '', '2015-02-02 20:29:50', 234, 2978, '2e634ee58bf5a238a9f23f8648ceb1740903121e', '{\"id\":72,\"asset_id\":196,\"title\":\"Jerky shank chicken boudin\",\"alias\":\"jerky-shank-chicken-boudin\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"video\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"http:\\\\\\/\\\\\\/vimeo.com\\\\\\/106306926\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(14, 72, 1, '', '2015-02-02 20:31:04', 234, 3015, 'dd4d438b486ad946a1e95cdd3e0fb3f6362dc0a0', '{\"id\":72,\"asset_id\":\"196\",\"title\":\"Jerky shank chicken boudin\",\"alias\":\"jerky-shank-chicken-boudin\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:31:04\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:29:50\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"video\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"http:\\\\\\/\\\\\\/vimeo.com\\\\\\/47505825\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(15, 72, 1, '', '2015-02-02 20:31:45', 234, 3015, 'd6a7362352a24b1ee69fcc4aa4377bf7d7de6c7a', '{\"id\":72,\"asset_id\":\"196\",\"title\":\"Jerky shank chicken boudin\",\"alias\":\"jerky-shank-chicken-boudin\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:31:45\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:31:40\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"video\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"http:\\\\\\/\\\\\\/vimeo.com\\\\\\/43426940\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":3,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(16, 73, 1, '', '2015-02-02 20:37:50', 234, 3008, '460756e5114f53fbad57d5f6ae16e526d075d1d6', '{\"id\":73,\"asset_id\":197,\"title\":\"Jerky shank chicken boudin (2)\",\"alias\":\"jerky-shank-chicken-boudin-2\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":0,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:31:45\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"standard\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(17, 73, 1, '', '2015-02-02 20:41:37', 234, 3036, '98e5d3dc541496f3f246af84a9202acdb7bd8a73', '{\"id\":73,\"asset_id\":\"197\",\"title\":\"Leberkas tail swine pork\",\"alias\":\"leberkas-tail-swine-pork\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>\\u00a0<\\/p>\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:41:37\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:41:31\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"standard\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"1\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(18, 73, 1, '', '2015-02-02 20:41:44', 234, 3018, 'a4b8c473d3b20bf02900c7dc1e0ceb2b34933c53', '{\"id\":73,\"asset_id\":\"197\",\"title\":\"Leberkas tail swine pork\",\"alias\":\"leberkas-tail-swine-pork\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:41:44\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:41:39\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"standard\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":3,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"1\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(19, 72, 1, '', '2015-02-02 20:41:51', 234, 2997, 'd7d89e0f2bdb0473656ea2c99972bce38c9f5863', '{\"id\":72,\"asset_id\":\"196\",\"title\":\"Jerky shank chicken boudin\",\"alias\":\"jerky-shank-chicken-boudin\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:41:51\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:41:46\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"video\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"http:\\\\\\/\\\\\\/vimeo.com\\\\\\/43426940\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":4,\"ordering\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0);
INSERT INTO `tol2j_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(20, 74, 1, '', '2015-02-02 20:45:07', 234, 2998, '93efe9629942027e6326ea1e2a9b0de9e6839399', '{\"id\":74,\"asset_id\":198,\"title\":\"Meatball kevin beef ribs shoulder\",\"alias\":\"meatball-kevin-beef-ribs-shoulder\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":0,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:41:44\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"standard\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(21, 74, 1, '', '2015-02-02 20:45:19', 234, 3035, '7423ef48e031b34fa2aeadc7ce2c862b53df4f2c', '{\"id\":74,\"asset_id\":\"198\",\"title\":\"Meatball kevin beef ribs shoulder\",\"alias\":\"meatball-kevin-beef-ribs-shoulder\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:45:19\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:45:11\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"gallery\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(22, 74, 1, '', '2015-02-02 20:53:42', 234, 3116, '15f8fb6bb8f068b4c2ea6609839940990835bb91', '{\"id\":74,\"asset_id\":\"198\",\"title\":\"Meatball kevin beef ribs shoulder\",\"alias\":\"meatball-kevin-beef-ribs-shoulder\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:53:42\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:53:38\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"gallery\\\",\\\"gallery\\\":\\\"{\\\\\\\"gallery_images\\\\\\\":[\\\\\\\"images\\\\\\/blog\\\\\\/blog06.jpg\\\\\\\",\\\\\\\"images\\\\\\/blog\\\\\\/blog05.jpg\\\\\\\",\\\\\\\"images\\\\\\/blog\\\\\\/blog04.jpg\\\\\\\"]}\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":3,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(23, 75, 1, '', '2015-02-02 20:53:52', 234, 2986, 'b733c0264029616c869839b0d11fa141f0a586ea', '{\"id\":75,\"asset_id\":199,\"title\":\"Leberkas tail swine pork (2)\",\"alias\":\"leberkas-tail-swine-pork-2\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":0,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:41:44\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"standard\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(24, 75, 1, '', '2015-02-02 20:55:25', 234, 3290, '5f3e633cad869e47c3feccb1cd2bec12a5e38550', '{\"id\":75,\"asset_id\":\"199\",\"title\":\"Leberkas tail swine pork (2)\",\"alias\":\"leberkas-tail-swine-pork-2\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":0,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:55:25\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:53:52\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"audio\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"<iframe width=\\\\\\\"100%\\\\\\\" height=\\\\\\\"450\\\\\\\" scrolling=\\\\\\\"no\\\\\\\" frameborder=\\\\\\\"no\\\\\\\" src=\\\\\\\"https:\\\\\\/\\\\\\/w.soundcloud.com\\\\\\/player\\\\\\/?url=https%3A\\\\\\/\\\\\\/api.soundcloud.com\\\\\\/tracks\\\\\\/28830162&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\\\\\\\"><\\\\\\/iframe>\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(25, 75, 1, '', '2015-02-02 20:55:33', 234, 3309, '4042e0f7cdaab4924473214ec8cb1e224fca233e', '{\"id\":75,\"asset_id\":\"199\",\"title\":\"Leberkas tail swine pork (2)\",\"alias\":\"leberkas-tail-swine-pork-2\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:55:33\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:55:25\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"audio\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"<iframe width=\\\\\\\"100%\\\\\\\" height=\\\\\\\"450\\\\\\\" scrolling=\\\\\\\"no\\\\\\\" frameborder=\\\\\\\"no\\\\\\\" src=\\\\\\\"https:\\\\\\/\\\\\\/w.soundcloud.com\\\\\\/player\\\\\\/?url=https%3A\\\\\\/\\\\\\/api.soundcloud.com\\\\\\/tracks\\\\\\/28830162&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\\\\\\\"><\\\\\\/iframe>\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":3,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(26, 75, 1, '', '2015-02-02 20:58:11', 234, 3325, '214eb92eda7e3fd1aa2e9358f79cd9f35c145ed1', '{\"id\":75,\"asset_id\":\"199\",\"title\":\"5 Effective Email Unsubscribe Pages\",\"alias\":\"5-effective-email-unsubscribe-pages\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:58:11\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:55:33\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"audio\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"<iframe width=\\\\\\\"100%\\\\\\\" height=\\\\\\\"450\\\\\\\" scrolling=\\\\\\\"no\\\\\\\" frameborder=\\\\\\\"no\\\\\\\" src=\\\\\\\"https:\\\\\\/\\\\\\/w.soundcloud.com\\\\\\/player\\\\\\/?url=https%3A\\\\\\/\\\\\\/api.soundcloud.com\\\\\\/tracks\\\\\\/28830162&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\\\\\\\"><\\\\\\/iframe>\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":4,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(27, 76, 1, '', '2015-02-02 20:59:21', 234, 3284, '4ae92dd3439354d44eaa990f051fe33a25a09410', '{\"id\":76,\"asset_id\":200,\"title\":\"Who Actually Clicks on Banner Ads?\",\"alias\":\"who-actually-clicks-on-banner-ads\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":0,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:58:11\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"audio\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"<iframe width=\\\\\\\"100%\\\\\\\" height=\\\\\\\"450\\\\\\\" scrolling=\\\\\\\"no\\\\\\\" frameborder=\\\\\\\"no\\\\\\\" src=\\\\\\\"https:\\\\\\/\\\\\\/w.soundcloud.com\\\\\\/player\\\\\\/?url=https%3A\\\\\\/\\\\\\/api.soundcloud.com\\\\\\/tracks\\\\\\/28830162&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\\\\\\\"><\\\\\\/iframe>\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(28, 76, 1, '', '2015-02-02 20:59:51', 234, 3036, '831346c8e036f9c50948e91fda6382464b93510f', '{\"id\":76,\"asset_id\":\"200\",\"title\":\"Who Actually Clicks on Banner Ads?\",\"alias\":\"who-actually-clicks-on-banner-ads\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":0,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 20:59:51\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:59:21\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"link\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"=\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"Responive Joomla Templates\\\",\\\"link_url\\\":\\\"http:\\\\\\/\\\\\\/www.joomshaper.com\\\\\\/joomla-templates\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(29, 76, 1, '', '2015-02-02 21:00:29', 234, 3054, '2b7e156f98ef23e5293b94c2c792d29e5c8e355f', '{\"id\":76,\"asset_id\":\"200\",\"title\":\"Who Actually Clicks on Banner Ads?\",\"alias\":\"who-actually-clicks-on-banner-ads\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 21:00:29\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 20:59:51\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"link\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"Responive Joomla Templates\\\",\\\"link_url\\\":\\\"http:\\\\\\/\\\\\\/www.joomshaper.com\\\\\\/joomla-templates\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":3,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(30, 76, 1, '', '2015-02-02 21:03:52', 234, 3267, '2cc28fa75fa79e432bd08cb348d8397b4b2bad98', '{\"id\":76,\"asset_id\":\"200\",\"title\":\"See the new Miss Universe get her crown\",\"alias\":\"see-the-new-miss-universe-get-her-crown\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 21:03:52\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 21:02:26\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"quote\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"Pork meatball ground round prosciutto. Sirloin bresaola ball tip shank tail porchetta pork boudin filet mignon flank jowl salami. Filet mignon bresaola pork boudin capicola prosciutto. Frankfurter chicken leberkas drumstick ball tip turducken rump spare ribs meatball.\\\",\\\"quote_author\\\":\\\"- John Doe\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":4,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(31, 76, 1, '', '2015-02-02 21:04:39', 234, 3397, '8beda0d66c8c213f9a621f81613a871e1cc86e4f', '{\"id\":76,\"asset_id\":\"200\",\"title\":\"See the new Miss Universe get her crown\",\"alias\":\"see-the-new-miss-universe-get-her-crown\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 21:04:39\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 21:04:08\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"quote\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"Pork meatball ground round prosciutto. Sirloin bresaola ball tip shank tail porchetta pork boudin filet mignon flank jowl salami. Filet mignon bresaola pork boudin capicola prosciutto. Frankfurter chicken leberkas drumstick ball tip turducken rump spare ribs meatball. Tail salami pork loin ham. Drumstick flank porchetta, hamburger ham swine biltong chicken pancetta. Spare ribs prosciutto t-bone.\\\",\\\"quote_author\\\":\\\"- John Doe\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":5,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0);
INSERT INTO `tol2j_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(32, 77, 1, '', '2015-02-02 21:05:10', 234, 3022, '4bea0e0fd22f934dff631c5d14e91c65cc80c5b5', '{\"id\":77,\"asset_id\":201,\"title\":\"Who Actually Clicks on Banner Ads? (2)\",\"alias\":\"who-actually-clicks-on-banner-ads-2\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":0,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 21:05:00\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"link\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"Responive Joomla Templates\\\",\\\"link_url\\\":\\\"http:\\\\\\/\\\\\\/www.joomshaper.com\\\\\\/joomla-templates\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(33, 77, 1, '', '2015-02-02 21:05:57', 234, 3378, '05e3b522a0e0b79dac1156b6a5be2c90e8edc821', '{\"id\":77,\"asset_id\":\"201\",\"title\":\"See the new Miss Universe get her crown\",\"alias\":\"see-the-new-miss-universe-get-her-crown\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":0,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"864\",\"created_by_alias\":\"\",\"modified\":\"2015-02-02 21:05:57\",\"modified_by\":\"864\",\"checked_out\":\"864\",\"checked_out_time\":\"2015-02-02 21:05:24\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"post_format\\\":\\\"quote\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"Pork meatball ground round prosciutto. Sirloin bresaola ball tip shank tail porchetta pork boudin filet mignon flank jowl salami. Filet mignon bresaola pork boudin capicola prosciutto. Frankfurter chicken leberkas drumstick ball tip turducken rump spare ribs meatball. Tail salami pork loin ham. Drumstick flank porchetta, hamburger ham swine biltong chicken pancetta. Spare ribs prosciutto t-bone.\\\",\\\"quote_author\\\":\\\"- John Doe\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"0\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(34, 73, 1, '', '2015-11-02 10:12:41', 234, 3055, 'b509aaf5641d99d1f403a21d43d0125fac737fce', '{\"id\":73,\"asset_id\":\"197\",\"title\":\"Pellentesque Habitant Morbi Tristique\",\"alias\":\"leberkas-tail-swine-pork\",\"introtext\":\"<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<\\/p>\\r\\n\",\"fulltext\":\"\\r\\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus<\\/p>\",\"state\":1,\"catid\":\"79\",\"created\":\"2015-02-02 20:29:50\",\"created_by\":\"47\",\"created_by_alias\":\"\",\"modified\":\"2015-11-02 10:12:41\",\"modified_by\":\"47\",\"checked_out\":\"47\",\"checked_out_time\":\"2015-11-02 10:12:17\",\"publish_up\":\"2015-02-02 20:29:50\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"images\\\\\\/blog\\\\\\/blog01.jpg\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\",\\\"spfeatured_image\\\":\\\"\\\",\\\"post_format\\\":\\\"standard\\\",\\\"gallery\\\":\\\"\\\",\\\"audio\\\":\\\"\\\",\\\"video\\\":\\\"\\\",\\\"link_title\\\":\\\"\\\",\\\"link_url\\\":\\\"\\\",\\\"quote_text\\\":\\\"\\\",\\\"quote_author\\\":\\\"\\\",\\\"post_status\\\":\\\"\\\"}\",\"version\":4,\"ordering\":\"4\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"97\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_updates`
--

DROP TABLE IF EXISTS `tol2j_updates`;
CREATE TABLE `tol2j_updates` (
  `update_id` int(11) NOT NULL,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `folder` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsurl` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `infourl` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Available Updates';

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_updates2`
--

DROP TABLE IF EXISTS `tol2j_updates2`;
CREATE TABLE `tol2j_updates2` (
  `update_id` int(11) NOT NULL,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `folder` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsurl` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `infourl` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Available Updates';

--
-- Dumping data for table `tol2j_updates2`
--

INSERT INTO `tol2j_updates2` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(0, 1, 700, 'Joomla', '', 'joomla', 'file', '', 0, '3.7.4', '', 'https://update.joomla.org/core/sts/extension_sts.xml', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_update_sites`
--

DROP TABLE IF EXISTS `tol2j_update_sites`;
CREATE TABLE `tol2j_update_sites` (
  `update_site_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `location` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Update Sites';

--
-- Dumping data for table `tol2j_update_sites`
--

INSERT INTO `tol2j_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(0, 'JEvents Package', 'extension', 'https://www.jevents.net/updates/-bG9jYWxob3N0L3RtZWFfc2Vw/pkg_jevents-update-3.2.21.xml', 1, 1509429609, ''),
(1, 'Joomla! Core', 'collection', 'https://update.joomla.org/core/list.xml', 0, 0, ''),
(2, 'Joomla! Extension Directory', 'collection', 'https://update.joomla.org/jed/list.xml', 1, 1509429629, ''),
(3, 'Accredited Joomla! Translations', 'collection', 'https://update.joomla.org/language/translationlist_3.xml', 0, 0, ''),
(4, 'Joomla! Update Component Update Site', 'extension', 'https://update.joomla.org/core/extensions/com_joomlaupdate.xml', 0, 0, ''),
(10, 'Helix3 - Ajax', 'extension', 'http://www.joomshaper.com/updates/plg-ajax-helix3.xml', 1, 1509429649, ''),
(11, 'System - Helix3 Framework', 'extension', 'http://www.joomshaper.com/updates/plg-system-helix3.xml', 1, 1509429670, ''),
(12, 'SP Page Builder', 'extension', 'http://www.joomshaper.com/updates/com-sp-page-builder-free.xml', 1, 1509429690, ''),
(13, 'shaper_helix3', 'extension', 'http://www.joomshaper.com/updates/shaper-helix3.xml', 1, 1509429725, ''),
(14, 'SP Simple Portfolio Module', 'extension', 'http://www.joomshaper.com/updates/mod-sp-simple-portfolio.xml', 1, 1509429745, ''),
(15, 'SP Simple Portfolio', 'extension', 'http://www.joomshaper.com/updates/com-sp-simple-portfolio.xml', 0, 0, ''),
(16, 'shaper_helix3', 'extension', 'https://www.joomshaper.com/updates/shaper-helix3.xml', 1, 1509429765, ''),
(17, 'DOCman', 'extension', 'https://api.joomlatools.com/extension/docman.xml', 1, 1509429785, '');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_update_sites_extensions`
--

DROP TABLE IF EXISTS `tol2j_update_sites_extensions`;
CREATE TABLE `tol2j_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Links extensions to update sites';

--
-- Dumping data for table `tol2j_update_sites_extensions`
--

INSERT INTO `tol2j_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(0, 10050),
(1, 700),
(2, 700),
(3, 802),
(4, 28),
(5, 10002),
(6, 10004),
(7, 10001),
(8, 10003),
(10, 10001),
(11, 10002),
(12, 10004),
(13, 10003),
(14, 10008),
(15, 10007),
(16, 10003),
(17, 10015);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_usergroups`
--

DROP TABLE IF EXISTS `tol2j_usergroups`;
CREATE TABLE `tol2j_usergroups` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_usergroups`
--

INSERT INTO `tol2j_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 82, 'Public'),
(2, 1, 2, 79, 'Registered'),
(8, 1, 80, 81, 'Super Users'),
(10, 2, 5, 6, 'Board of Directors'),
(11, 2, 27, 28, 'The Council'),
(15, 41, 19, 20, 'Noc - Uganda'),
(16, 39, 59, 60, 'Ugandan Programme Manager'),
(17, 40, 73, 74, 'Ugandan Programme Officer'),
(18, 41, 17, 18, 'Noc - Tanzania'),
(19, 41, 13, 14, 'Noc - Rwanda'),
(20, 41, 9, 10, 'Noc - Burundi'),
(21, 41, 15, 16, 'Noc - South Sudan'),
(22, 39, 57, 58, 'Tanzanian Programme Manager'),
(23, 39, 53, 54, 'Rwandan Programme Manager'),
(24, 39, 55, 56, 'South Sudanese Programme Manager'),
(25, 39, 49, 50, 'Burundian Programme Manager'),
(26, 40, 67, 68, 'Rwandan Programme Officer'),
(27, 40, 69, 70, 'South Sudanese Programme Officer'),
(28, 40, 63, 64, 'Burundian Programme Officer'),
(29, 40, 71, 72, 'Tanzanian Programme Officer'),
(30, 41, 11, 12, 'Noc - Kenya'),
(31, 39, 51, 52, 'Kenyan Programme Manager'),
(32, 40, 65, 66, 'Kenyan Programme Officer'),
(33, 2, 29, 78, 'TMEA Staff'),
(34, 2, 7, 26, 'Oversight Committees'),
(35, 33, 30, 31, 'CEO'),
(36, 33, 46, 47, 'Deputy CEO'),
(37, 33, 76, 77, 'Snr. Director '),
(38, 33, 32, 45, 'Country Directors'),
(39, 33, 48, 61, 'Programme Managers'),
(40, 33, 62, 75, 'Programme Officers'),
(41, 34, 8, 21, 'National Oversight Committee'),
(42, 34, 22, 25, 'Programme Co-ordination Committee'),
(43, 42, 23, 24, 'Arusha-TEPP'),
(44, 38, 35, 36, 'CD - Kenya'),
(45, 38, 43, 44, 'CD - Uganda'),
(46, 38, 37, 38, 'CD - Rwanda'),
(47, 38, 41, 42, 'CD - Tanzania'),
(48, 38, 39, 40, 'CD - South Sudan'),
(49, 38, 33, 34, 'CD - Burundi');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_users`
--

DROP TABLE IF EXISTS `tol2j_users`;
CREATE TABLE `tol2j_users` (
  `id` int(11) NOT NULL,
  `name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_users`
--

INSERT INTO `tol2j_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(234, 'Super User', 'admin', 'mbuluma@gbc.co.ke', '$2y$10$xgolcbsRaypmO14kU.96lOAxiOrsht1t6R5ZyMSVD5xlOcifigE0i', 0, 1, '2016-09-29 04:57:57', '2017-10-31 07:03:37', '0', '', '0000-00-00 00:00:00', 0, '', '', 0),
(235, 'Michael', 'admin2', 'bulumaknight@gmail.com', '$2y$10$y5Fqkk/gLQjcCPLYVvhDZ.xKcVaJdgHqv7FdkOXea.Qasw3tBSQGu', 0, 1, '2016-09-29 15:23:35', '2017-09-05 06:59:32', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(236, 'Francis Karuga', 'Frank', 'wamsfrank@gmail.com', '$2y$10$8aRuJoyG6MLiarde5N/8muFQxHCX2fcdSNKpvU4jJp3l/Smgib8Ou', 0, 1, '2016-09-30 06:08:56', '2016-10-14 07:02:54', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(237, 'Council 1', 'council_1', 'test@gbc.co.ke', '$2y$10$lurou4yHypoUfn1lzzGV0.AoxEYbZ0dMHMcuzn48WNbOuZ2PdwAFK', 0, 0, '2016-10-05 07:58:38', '2017-10-31 02:25:27', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(238, 'Ivan Kataka', 'ivan.kataka', 'ivan.kataka@trademarkea.com', '$2y$10$WqAFeII8qktave27wW8Au.fPaBw52.X0Q9uuYZXeM3l67B9JfJNSS', 0, 0, '2016-10-05 07:59:54', '2016-10-05 08:05:09', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(239, 'Liesa', 'Liesa', 'lbidali@gbc.co.ke', '$2y$10$V1N7yJVHYGD5fti9sijBiu2HcnahdJsmjr0Q4pl47w6jpEgl3NXlG', 0, 1, '2016-10-07 12:04:42', '2016-10-07 12:12:00', '', '{\"admin_style\":\"\",\"admin_language\":\"en-GB\",\"language\":\"en-GB\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(240, 'Joshua Mutunga', 'J_Mutunga', 'test@lcl.com', '$2y$10$.yj2zUzaXqZmGDQ0IQwujebPQNHHsbh6UOVNiaUpc7X2DWDhbnHSS', 0, 0, '2017-09-12 20:11:39', '2017-10-31 06:24:19', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(241, 'Damali Ssali', 'D_Ssali', 'test2@lcl.com', '$2y$10$q18DRmSKa8LaDht7E5t2f.jO695qFdvQ37XJovc5qW2KWOHSqHIt6', 0, 0, '2017-09-12 20:12:34', '0000-00-00 00:00:00', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(242, 'Board Member', 'board_1', 'board_1@lcl.com', '$2y$10$4Ihjcs//aVwXVTVU7Aueq.glqIIW8HDWE.wrcGr4/QoEa/AmjB/Aq', 0, 0, '2017-09-12 20:18:37', '2017-09-13 06:43:25', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(243, 'Burundi Member', 'burundi_1', 'burundi_1@lcl.com', '$2y$10$ADNKvyBboZPMg2pxf4.OyOgHx5ctcZ89ouWaVaKl0oGDgoSgE9ig2', 0, 0, '2017-09-13 00:10:52', '2017-09-15 10:15:27', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(244, 'CEO', 'ceo', 'ceo@tmea.org', '$2y$10$LKXCWxOcFRoEY9ucDCOzVu8T3YbwNlkOjsxThcNVQy6De24RE8DwC', 0, 0, '2017-10-30 17:18:38', '2017-10-31 07:09:49', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"en-GB\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"Africa\\/Nairobi\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(245, 'Deputy CEO', 'd_ceo', 'd_ceo@tmea.org', '$2y$10$o404I3NicY4JGnZCOYX0F.CtoZGQv.0EV1gwApIPUnZ4SXo0rZ0.6', 0, 0, '2017-10-30 17:19:20', '2017-10-30 18:09:14', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(246, 'CD Kenya', 'cd_kenya', 'cd_kenya@tmea.org', '$2y$10$ePZk6sttRStY.uZ5BFRnM.bqjgX8bBCcYJa68UEz1BXGzigNpj3Xa', 0, 0, '2017-10-30 17:41:57', '2017-10-31 02:26:39', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(247, 'Snr. Director ', 'snr_dir', 'snr_dir@tmea.org', '$2y$10$Xtx/ybxAZ/LuSEBsw0/aeOQImRRRToSpC2TT3ueH//I6DRxQOoHn.', 0, 0, '2017-10-30 18:15:50', '2017-10-31 02:21:21', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(248, 'PM Kenya', 'pm_kenya', 'pm_kenya@tmea.org', '$2y$10$YYA6H7G1l6vaArSKMf21YenMgf7L2sRVx5NIgwuN0CkOZ43DfNf96', 0, 0, '2017-10-30 20:10:27', '2017-10-31 02:52:14', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(249, 'PO Kenya', 'po_kenya', 'po_kenya@tmea.org', '$2y$10$2gajexyPFaLYcLeASR3nzeGC2H47DQiMwjiJFXcwGfZF.ARO5FitO', 0, 0, '2017-10-30 20:12:55', '2017-10-30 20:14:57', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(250, 'NOC Kenya', 'noc_kenya', 'noc_kenya@tmea.org', '$2y$10$5mMfoPhWOR.S9HtL7h3EkOtwDycIVlakvQCxjIwUL88AMOCH2iXQK', 0, 0, '2017-10-30 20:14:38', '2017-10-31 06:42:53', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0),
(251, 'Arusha-TEPP', 'arusha_tepp', 'arusha_tepp@tmea.org', '$2y$10$RTSwn5vB/vbbcXpaH4zoheaSoatcCPvNTC/IxiQEL9k44OitLPw22', 0, 0, '2017-10-30 20:16:22', '2017-10-30 20:58:40', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_user_keys`
--

DROP TABLE IF EXISTS `tol2j_user_keys`;
CREATE TABLE `tol2j_user_keys` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `series` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uastring` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_user_keys`
--

INSERT INTO `tol2j_user_keys` (`id`, `user_id`, `token`, `series`, `invalid`, `time`, `uastring`) VALUES
(1, 'council_1', '$2y$10$Y/KLS06AbDP3C8XikXpGSOY.BV01iH96FZrqfdd06IqtlBhSh7iYy', 'VEg81wAOhOCWJdhsi1xu', 0, '1508766407', 'joomla_remember_me_bafb178b4bae07b1c52422e36c3918f9');

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_user_notes`
--

DROP TABLE IF EXISTS `tol2j_user_notes`;
CREATE TABLE `tol2j_user_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_user_profiles`
--

DROP TABLE IF EXISTS `tol2j_user_profiles`;
CREATE TABLE `tol2j_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_user_usergroup_map`
--

DROP TABLE IF EXISTS `tol2j_user_usergroup_map`;
CREATE TABLE `tol2j_user_usergroup_map` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_user_usergroup_map`
--

INSERT INTO `tol2j_user_usergroup_map` (`user_id`, `group_id`) VALUES
(234, 8),
(235, 2),
(235, 10),
(236, 2),
(237, 2),
(237, 11),
(238, 2),
(239, 11),
(240, 2),
(240, 28),
(241, 2),
(241, 15),
(241, 16),
(242, 2),
(242, 10),
(243, 2),
(243, 20),
(244, 2),
(244, 33),
(244, 35),
(245, 2),
(245, 33),
(245, 36),
(246, 2),
(246, 44),
(247, 2),
(247, 37),
(248, 2),
(248, 31),
(249, 2),
(249, 32),
(250, 2),
(250, 30),
(251, 2),
(251, 43);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_utf8_conversion`
--

DROP TABLE IF EXISTS `tol2j_utf8_conversion`;
CREATE TABLE `tol2j_utf8_conversion` (
  `converted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_utf8_conversion`
--

INSERT INTO `tol2j_utf8_conversion` (`converted`) VALUES
(2);

-- --------------------------------------------------------

--
-- Table structure for table `tol2j_viewlevels`
--

DROP TABLE IF EXISTS `tol2j_viewlevels`;
CREATE TABLE `tol2j_viewlevels` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tol2j_viewlevels`
--

INSERT INTO `tol2j_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[2]'),
(3, 'Special', 3, '[8]'),
(6, 'Super Users', 4, '[8]'),
(7, 'Board of Directors', 0, '[10,11]'),
(8, 'Council Members', 0, '[10,11]'),
(9, 'Program Officers', 0, '[28,26,27,29,17]'),
(10, 'Administrators', 0, '[]'),
(11, 'Burundi', 0, '[10,20,11,35,49,36,25,28,37]'),
(12, 'Kenya', 0, '[10,30,11,35,44,36,31,32,37]'),
(13, 'Rwanda', 0, '[10,19,11,35,36,37]'),
(14, 'South Sudan', 0, '[10,21,11,35,36,37]'),
(15, 'Tanzania', 0, '[10,18,11,35,36,37]'),
(16, 'Uganda', 0, '[10,15,11,35,36,37]'),
(17, 'TMEA Staff', 0, '[2,10,11,33]'),
(18, 'The Council', 0, '[2,10,11]'),
(19, 'Oversight Committees', 0, '[2,10,34,11,35,36,37]'),
(20, 'Country Directors', 0, '[2,33]'),
(21, 'CD - Burundi', 0, '[2,49]'),
(22, 'PM - Burundi', 0, '[25]'),
(23, 'CEO', 0, '[35]'),
(24, 'Deputy CEO', 0, '[36]'),
(25, 'Arusha-TEPP', 0, '[10,42,43,11,35,47,36,37]');