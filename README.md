# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* TMEA Phase 2
* Version 2.0

[![buddy pipeline](https://app.buddy.works/gbc-/tmea-2/pipelines/pipeline/63553/badge.svg?token=2a7568ed065e1db7f05635207e9aae8a1ca1724286317b10d1cef8d5cfb611c4 "buddy pipeline")](https://app.buddy.works/gbc-/tmea-2/pipelines/pipeline/63553)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/48dda3d35b2443f08eec5aaaad8418bc)](https://www.codacy.com/app/buluma/tmea_2?utm_source=gbc_dev@bitbucket.org&amp;utm_medium=referral&amp;utm_content=gbc_dev/tmea_2&amp;utm_campaign=Badge_Grade)
### How do I get set up? ###

* Clone Repo
* Work on local branch
* export full database with drop table option selected
* upload db in db_dump as filename.sql (you may want to delete the previous filename.sql file)
* sync repo with bitbucket
* confirm site is working correctly: http://gbc.me.ke/tmea_phase2

### Who do I talk to? ###

* Michael Buluma